#!/usr/bin/env python3

import sys 
from ifw.fcfsim.mgr import sim 
from ifw.fcfsim import api as dvs

if __name__ == "__main__":
    sys.exit(dvs.utils.run_simulator(sim.MgrSimInterface, sim))
    
