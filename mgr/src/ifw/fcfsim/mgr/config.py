from __future__ import annotations

import importlib
import typing
from dataclasses import dataclass, field
from typing import Any

from ifw.fcfsim.simlib import api as dvs
from ifw.fcfsim.core import api as core

@dataclass
class DevInfo:
    """Hold sub-device information necessary to instantiate a simulator"""
    name: str
    InterfaceClass: type[dvs.ISimInterface[object]] 
    config: dvs.ISimConfig  

@dataclass
class MgrSimConfig(core.LogConfig):
    """Device Manager Configuration"""
    devices: list[DevInfo] = field(default_factory=list)
    interfaces: dict[str,type[dvs.ISimInterface[object]]] = field(default_factory=dict)
    log: str = "ifw.fcfsim.mgr"
    
    def validate_config(self) -> None:
        super().validate_config()
        for device in self.devices:
            device.config.log = self.log+"."+device.config.log

class MgrSimConfigLoader(dvs.SimConfigLoader):
    def dispatch(self, values: dict[str, Any]) -> typing.Iterator[tuple[str, Any]]:
        # Transfor configng devices and interfaces into python config instances 
        if 'devices' in values:
            interfaces = load_interfaces(values.pop('interfaces', []))
            yield 'interfaces', interfaces
            yield 'devices', parse_devices(interfaces, values.pop('devices', []))

        yield from super().dispatch(values)


############################################
#
# Function to transfor a sim manger configng information 
#  Into a valid DevsimMgrConfig. 
# We are making the maximum here to make sure that all path in config are okay 

# ------------ interfaces -----------------
class _CfgInterface(typing.TypedDict):
    name: str
    interface: str 

def load_interfaces(
    interfaces: list[_CfgInterface]
) -> dict[str,type[dvs.ISimInterface[object]]]:
    """load configng interfaces into a dictionary of name/Simulator Interface"""
    return {cfg['name']: load_interface(cfg['interface']) for cfg in interfaces}

def load_interface(python_path: str) -> type[dvs.ISimInterface[object]]:
    """Load an interface class from the interface path in configng"""
    *lib_path, app_class_name = python_path.split(".")
    lib = ".".join(lib_path)
    try:
        mod = importlib.import_module(lib)
        interface = getattr(mod, app_class_name)
    except (ImportError, AttributeError):
        raise ValueError(
            f"Cannot import device interface class {python_path!r}")
    return typing.cast(type[dvs.ISimInterface[object]],  interface)

# --------- devices ----------
class _CfgOverrides(typing.TypedDict):
    name: str 
    value: str

class _CfgDevice(typing.TypedDict):
    name: str 
    type: str 
    cfgfile: str 
    overrides: list[_CfgOverrides]

def parse_devices(
        interfaces: dict[str,type[dvs.ISimInterface[Any]]],
        devices: list[_CfgDevice]
    ) -> list[DevInfo]:
    """ Parse configng devices list  into a list of DevInfo """
    return [parse_device(interfaces, device) for device in devices]

def parse_device(
    interfaces: dict[str,type[dvs.ISimInterface[Any]]],
    device: _CfgDevice
) -> DevInfo:
    """ Parse a device configng model into a DevInfo """
    name = device['name'] 
    type_ = device['type'] 
    overrides = parse_overrides(device.get('overrides', []))
    cfgfile = device['cfgfile']
    
    try:
        InterfaceClass = interfaces[type_]
    except KeyError:
        raise ValueError(f"{name}: type named {type_!r} is not defined in interfaces")
    # Load config now with its overrides  
    config = InterfaceClass.get_config_factory().from_cii_config(cfgfile, **overrides)
    return DevInfo(name, InterfaceClass, config)       

def parse_overrides(overrides: list[_CfgOverrides]) -> dict[str, Any]:
    """ Parse mgr configng overrides into a dict of name/value"""
    return {o['name']:o['value'] for o in overrides}


