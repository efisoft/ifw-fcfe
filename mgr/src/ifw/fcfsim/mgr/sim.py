from __future__ import annotations

import functools
from dataclasses import dataclass, field
from typing import Any, Iterator
import typing

from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import api as opcua
from ifw.fcfsim.simlib import api as dvs
from ifw.fcfsim.mgr.config import (
    DevInfo,
    MgrSimConfig,
    MgrSimConfigLoader, 
)


@dataclass(frozen=True)
class SubDevice:
    """Hold Subdevices data and simmulator instance"""
    name: str 
    config: Any 
    interface: dvs.ISimInterface[object] 

def iter_sub_devices(
    devices: list[DevInfo]
) -> Iterator[SubDevice]:
    """ From a list of device config info build a SubDevice with simmulator instance """
    for devinfo in devices:
        interface =  devinfo.InterfaceClass.from_config(devinfo.config)
        yield SubDevice(devinfo.name, devinfo.config, interface) 



# We do not really need an Engine for DevsimMgr but it is a nice feature to have 
# for debug and unit test purposes. So developper can interact with simulators
class DevsimmgrEngine:
    def __init__(self, config: MgrSimConfig, sub_devices: list[SubDevice]):
        self.config = config
        for device in sub_devices:
            self.add_sub_device(device)
     
    def add_sub_device(self, sub_device: SubDevice) -> None:
        setattr(self, sub_device.name, sub_device.interface.get_engine())


@dataclass
class MgrSimInterface(dvs.BaseSimInterface):
    config: MgrSimConfig = field(default_factory=MgrSimConfig) 
    
    @functools.cached_property
    def sub_devices(self) -> list[SubDevice]:
        """Instanciate all sub devsim devices"""
        return list(iter_sub_devices(self.config.devices))
    
    @functools.cached_property  
    def engine(self) -> DevsimmgrEngine:
        return DevsimmgrEngine(self.config, self.sub_devices)

    @classmethod
    def get_config_factory(cls) -> core.IConfigFactory[MgrSimConfig]:
        return MgrSimConfigLoader().new_factory(MgrSimConfig)
    
    @classmethod
    def from_config(cls, config: MgrSimConfig) -> MgrSimInterface:
        return MgrSimInterface(config)

    def get_engine(self) -> DevsimmgrEngine:
        return self.engine

    def add_to_server(self, server_manager: opcua.ServerManager, /) -> None:
        for device in self.sub_devices:
            device.interface.add_to_server(server_manager)

    def iter_runners(self, excludes: set[str] = set()) -> Iterator[core.IRunner]:
        for device in self.sub_devices:
            yield from  device.interface.iter_runners(excludes=excludes | {'opcua'})

if __name__ == "__init__":
    #  DevsimMgr  should be conform to ISimInterface 
    _: type[dvs.ISimInterface[Any]] = MgrSimInterface 
