from __future__ import annotations
from types import TracebackType
from typing import Literal, Protocol
import abc 


class ITask(Protocol):
    """ A task define something which should be ran inside a loop box """

    async def initialise(self)->None:
        ... 

    async def start(self)->None:
        # e.g. starts Threads or open connections 
        ...

    async def end(self)->None:
        # e.g. close connection or third party running threads 
        ...

    async def next(self)->None:
        ...
    
    # give to task some context manager capability  
    async def __aenter__(self)->ITask:
        ...

    async def __aexit__(self, 
          exc_type: type[BaseException] | None,
          exc_val: BaseException|None,
          exc_tb: TracebackType|None
        )->bool:
        ...


# Define an Abstract class for tasks on top of the interface 
class BaseTask(abc.ABC):
    """ Hold a Task to be run in a loop 
    
    Methods for each steps: 

      - initialise: setup variables, connection etc ...
      - start: setup anything before running task in loop 
      - next: task cycle execution 
      - end: cleanup stuff, close connection etc ... 

    """

    @abc.abstractmethod
    async def initialise(self)->None:
        """ Initialisation of the task

        Shall be done before start is called. 
        If several task shall be run they will be initialised first 
        This will avoid to start unwanted process in case of task init error 
        """
        ... 

    @abc.abstractmethod
    async def start(self)->None:
        """ Get everything ready to execute the task """
        # e.g. starts Threads or open connections 
        ...

    @abc.abstractmethod
    async def end(self)->None:
        """ Close properly the task """
        # e.g. close connection or third party running threads 
        ...

    @abc.abstractmethod
    async def next(self)->None:
        """ Next cycled step to be done """
        ...

    async def __aenter__(self)->ITask:
        await self.initialise()
        await self.start() 
        return self 
    
    async def __aexit__(self, 
          exc_type: type[BaseException] | None,
          exc_val: BaseException|None,
          exc_tb: TracebackType|None
        )->Literal[False]:
        del exc_type, exc_val, exc_tb
        await self.end()
        return False 

