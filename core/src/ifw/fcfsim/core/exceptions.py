from __future__ import annotations
from typing import Any


class RpcException(RuntimeError):
    """Exception for method intended as RPC 
   
    This allows to have a more pythonic way to handle error but still 
    run a server properly. The RpcError will be catched by the server 
    and treated accordingly. 
    
    Args:
        msg: Error message 
        rpc_outputs: Output for the server (Typically a int error code)

    Example::
        
        def my_rpc(pos: float) -> int:
            if pos<0.0:
                raise EpcExection('pos should be <0', MyErrorEum.NEG_POS) 
            ...
            return 0 
    """
    def __init__(self, msg:str, rpc_outputs: Any):
        super().__init__(msg)
        self.rpc_outputs = rpc_outputs 


class StopTask(RuntimeError):
    """Exception used to stop a task loop properly 

    When running a task cyclically in a loop this exception 
    can be catched to exit properly.

    Typically used in a :class:`ifw.fcfsim.core.api.IRunner`
    """
    exit_code: int = 0
    def __init__(self, msg:str='' , exit_code:int|None=0):
        super().__init__(msg)
        if exit_code is not None:
            self.exit_code = exit_code
    

class ActionError(RuntimeError):
    """Explicit error raised during the execution of some actions

    An error code is associated to the ActionError.
    Typically raiseed in low level function and catched in high 
    level function.

    e.g. ``raise ActionError(MyErrorCodes.TIMEOUT)``
    

    """
    _error: int = 0
    def __init__(self, error: int):
        super().__init__(str(error))
        self._error = error

    def get_error_code(self) -> int:
        return self._error


