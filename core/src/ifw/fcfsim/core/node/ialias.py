from __future__ import annotations
from typing import Any, Protocol, runtime_checkable

@runtime_checkable
class IMultySolver(Protocol):
    """Value solver from N values to one and one to N"""

    def solve_multy(self, *__values: Any) -> Any:
        ...

    def unsolve_multy(self, __value: Any) -> tuple[Any, ...]:
        ...


@runtime_checkable
class ISingleSolver(Protocol):
    """One to one solver"""

    def solve(self, __value: Any) -> Any:
        ...

    def unsolve(self, __value: Any) -> Any:
        ...



