from __future__ import annotations
from dataclasses import dataclass, field
from datetime import datetime, timezone
import inspect
from types import TracebackType
from typing import Any, Callable, ClassVar, Iterable, Literal

from ifw.fcfsim.core.node.ibase import (
        IConnectionsManager,
        IDataEvent,
        IDataKey,
        IDataNode,
        ILinkCallback,
        IMetadataElement,
        INodeHandler
        )

DEFAULT_TIMESTAMP = datetime(1970, 1, 1, tzinfo=timezone.utc)
""" A default datetime to return when data timestamp is missing """


class Empty:
    """ Empty but not None """
    ...

def is_linkable(obj:Any)->bool:
    """ True if the object has attribute link capability 
    
    Typicaly True is the object is an instance of a class 
    decorated with @attrconnect 
    """
    try:
        obj.__data_connections__
    except AttributeError:
        return False 
    return True 


@dataclass
class MetadataElement:
    """ A single metadata holder for a data node   

    This has so far only a timestamp     
    """
    # This can evolve in a more sofisticated metadata holder
    # with unit, desciptions etc ...
    timestamp: datetime | None = None

    def get_timestamp(self, default: datetime = DEFAULT_TIMESTAMP) -> datetime:
        if self.timestamp is None:
            return default
        return self.timestamp

    def touch(self, dt: datetime | None = None) -> None:
        if dt is None:
            dt = datetime.now(timezone.utc)
        self.timestamp = dt


@dataclass
class MetadataManager:  # conform to IMetadataManager
    """ A collection of metadata """
    metadata: dict[IDataKey, IMetadataElement] = field(default_factory=dict)

    Element: ClassVar[type[IMetadataElement]] = MetadataElement

    def get(self, item: IDataKey) -> IMetadataElement:
        try:
            return self.metadata[item]
        except KeyError:
            raise ValueError(f"Cannot find metadata for {item!r}")

    def provide(self, item: IDataKey) -> IMetadataElement:
        try:
            element = self.metadata[item]
        except KeyError:
            element = self.Element()
            self.add(item, element)
        return element

    def touch(self, item: IDataKey, dt: datetime | None = None) -> None:
        self.provide(item).touch(dt)

    def add(self, item: IDataKey, metadata: IMetadataElement) -> None:
        self.metadata[item] = metadata

    def clear(self) -> None:
        """ Clear all metadata """
        self.metadata.clear()



@dataclass
class ConnectionElement:
    """A callback connection handler associated with an event"""
    event: IDataEvent
    callbacks: list[ILinkCallback] = field(default_factory=list)

    def add(self, callback: ILinkCallback) -> None:
        """Add a new connection callback"""
        if callback not in self.callbacks:
            self.callbacks.append(callback)

    def remove(self, callback: ILinkCallback) -> bool:
        """Remove a callback"""
        try:
            self.callbacks.remove(callback)
        except ValueError:
            return False
        else:
            return True

    def trigger(self, value: Any) -> None:
        """Trigger all callback with value"""
        for f in self.callbacks:
            f(value, self.event)


@dataclass
class ConnectionsManager:  # Conform to IConnectionsManager
    """A object managing data node connections 
    
    This should be used only for connection of nodes with the same parent.
    e.g. For @attrconnect decorated object, Map or Array  
    """

    connections: dict[IDataKey, ConnectionElement] = field(
        default_factory=dict)

    def trigger(self, item: IDataKey, value: Any) -> None:
        """ Trigger the recorded callback 
        
        If the connection describded by ``item`` does not exists, 
        nothing is done

        Args:
            item: a valid key for the connection  
            value: new value to be sent to the callback 
        """
        try:
            element = self.connections[item]
        except KeyError:
            pass
        else:
            element.trigger(value)

    def connect(self,
                item: IDataKey,
                event: IDataEvent,
                callback: ILinkCallback) -> None:
        """ Add a new connection to the manager """
        try:
            element = self.connections[item]
        except KeyError:
            element = ConnectionElement(event)
            self.connections[item] = element
        element.add(callback)

    def disconnect(self, item: IDataKey, callback: ILinkCallback) -> bool:
        """ Remove a connection from the manager """
        try:
            element = self.connections[item]
        except KeyError:
            return False
        return element.remove(callback)


# A place holder for future development and usage of Node
# A NodeHandler is abble to pull or push a bunch of nodes sharing the
# same context (e.i. same OPCU Server, same GUI) in one go.
# This base handler does not do anything special but getting and 
# setting nodes one by one
class BaseNodeHandler(INodeHandler[IDataNode[Any]]):
    def __eq__(self, right: Any) -> bool:
        # Any instance of BaseNodeHandler is equal
        return isinstance(right, BaseNodeHandler)

    def __hash__(self) -> int:
        return hash(BaseNodeHandler)

    def pull(self,
             nodes: Iterable[IDataNode[Any]],
             output: dict[IDataNode[Any], Any]) -> None:
        for node in nodes:
            output[node] = node.get()

    def push(self, node_values: dict[IDataNode[Any], Any]) -> None:
        for node, value in node_values.items():
            node.set(value)


basic_node_handler: INodeHandler[IDataNode[Any]] = BaseNodeHandler()
""" Default node handler used by all basic nodes """


def callback(ifunc: Callable[..., Any]) -> Callable[[Any, IDataEvent], None]:
    """Wrap a function to be conform to node callback connection signature 

    Data node connection function signature is
    ``f(value:Any, event:IDataEvent) -> None``
    This callback try to wrap a function so it is compatible with the
    expected signature 

    Exemple:
        
        c = callback( print ) # event parameter is dropped 
        c = callback( lambda v: ...) # event parameter is dropped 
        c = callback( lambda v,e: ...) # return the function 
        c = callback( lambda : ...) # value and event are dropped 
        # etc ...
    """
    try:
        s = inspect.signature(ifunc)
    except ValueError:
        # This is most probably a builtin type like int, bool, etc
        def func(value: Any, event: IDataEvent) -> None:
            ifunc(value)
    else:
        if len(s.parameters) > 1:
            no_default: int = sum(
                p.default is inspect._empty for p in s.parameters.values()
            )
            if no_default > 2:
                raise ValueError(
                    f"function should have no more than 2 positional arguments got {no_default}")

            func = ifunc
        elif len(s.parameters) == 1:
            def func(value: Any, event: IDataEvent) -> None:
                ifunc(value)
        else:
            def func(value: Any, event: IDataEvent) -> None:
                ifunc()
    return func


@dataclass
class SuspendedConnection:  # conform to IConnectionsManager
    """ Object used to suspend connection triggering from a connection manager 

    The callbacks are called with the resume method  
    """
    connection_manager: IConnectionsManager
    data: dict[IDataKey, Any] = field(default_factory=dict)

    def trigger(self, item: IDataKey, value: Any) -> None:
        self.data[item] = value

    def connect(self,
                item: IDataKey,
                event: IDataEvent,
                callback: ILinkCallback) -> None:
        self.connection_manager.connect(item, event, callback)

    def disconnect(self, item: IDataKey, callback: ILinkCallback) -> bool:
        return self.connection_manager.disconnect(item, callback)

    def resume(self) -> IConnectionsManager:
        """ Call all subscribed callback methods 
        
        Returns the orginial Connection Manager 
        """
        for item, value in self.data.items():
            self.connection_manager.trigger(item, value)
        return self.connection_manager

    def clear(self) -> None:
        """ Clear all suspended callback """
        self.data.clear()


@dataclass
class LockedConnection:  # conform to IConnectionsManager
    """ Object used to loose temporaly connections  """
    connection_manager: IConnectionsManager

    def trigger(self, item: IDataKey, value: Any) -> None:
        del item, value

    def connect(self,
                item: IDataKey,
                event: IDataEvent,
                callback: ILinkCallback) -> None:
        self.connection_manager.connect(item, event, callback)

    def disconnect(self, item: IDataKey, callback: ILinkCallback) -> bool:
        return self.connection_manager.disconnect(item, callback)

    def resume(self) -> IConnectionsManager:
        """ Returns the orginial Connection Manager """
        return self.connection_manager


class Suspend:
    """ Context manager to Suspend execution of Callback of a linkable object 
    
    All callback linked methods are suspended inside the context manager 
    They will all be triggered at exit of the context manager. 

    Example::
        
        with Suspend(obj):
            obj.var1 = 1  # <-- no callback called  
            obj.var2 = 2  # <-- no callback called 
        # <--  var1 and var2 subscribed callback are called here (if Any) 

    """
    _saved_link: IConnectionsManager
    _obj: Any

    class _NotLinkable:
        """ Mockup for a not-linkable object """
        _obj: object

        def __enter__(self) -> object:
            return self._obj

        def __exit__(self,
          exc_type: type[BaseException] | None,
          exc_val: BaseException | None,
          exc_tb: TracebackType | None
                     ) -> Literal[False]:
            del exc_type, exc_val, exc_tb  # not used here
            return False

    def __init__(self, obj: Any):
        if not is_linkable(obj):
            # make it work for any object
            # typing: cannot be handled by mypy
            self.__class__ = self._NotLinkable  # type: ignore
        self._obj = obj

    def __enter__(self) -> object:
        self._saved_link = self._obj.__data_connections__
        self._obj.__data_connections__ = SuspendedConnection(
            self._obj.__data_connections__)
        return self._obj

    def __exit__(self,
          exc_type: type[BaseException] | None,
          exc_val: BaseException | None,
          exc_tb: TracebackType | None
                 ) -> Literal[False]:
        del exc_type, exc_val, exc_tb  # not used here
        self._obj.__data_connections__ = self._obj.__data_connections__.resume()
        return False


class Lock(Suspend):
    """ Context manager to lock execution of Callback of a linkable Object 

    All callback methods will be disabled inside the context manager
    At exit of the context manager the links are re-established but no 
    callbacks are triggered (contrary to Suspend) 
    """

    def __enter__(self) -> object:
        self._saved_link = self._obj.__data_connections__
        self._obj.__data_connections__ = LockedConnection(
            self._obj.__data_connections__)
        return self._obj 

