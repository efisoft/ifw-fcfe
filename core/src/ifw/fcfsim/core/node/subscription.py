from __future__ import annotations
from dataclasses import InitVar, dataclass
from typing import Any, Callable, Iterable, overload
from ifw.fcfsim.core.node.base import Empty, callback
from ifw.fcfsim.core.node.ibase import IDataEvent, IDataNode, ILinkCallback, INodeSubscription


@dataclass(frozen=True)
class NodesSubscription:
    """connection housekeeping of several nodes"""
    nodes: tuple[IDataNode[Any],...]
    callback: ILinkCallback
    trigger: InitVar[bool] = True
    
    def __post_init__(self, trigger: bool = True) -> None:
        self.reconnect()
        if trigger:
            for source in self.nodes:
                val = source.get(Empty) 
                if val is not Empty:
                    self.callback(source.get(), source.get_event())
                    break # Trigger only ones
            
    def reconnect(self) -> None:
        try:
            self.disconnect()
        except ValueError: 
            pass
        for source in self.nodes:
            source.connect_callback(self.callback)

    def disconnect(self) -> None:
        for source in self.nodes:
            source.disconnect_callback(self.callback)

@dataclass(frozen=True)
class NodeSubscription:
    node: IDataNode[Any]
    callback: Callable[[Any, IDataEvent], None]
    trigger: InitVar[bool] = True

    def __post_init__(self,  trigger: bool = True) -> None:
        self.reconnect()
        if trigger:
            val = self.node.get(Empty) 
            if val is not Empty:
                self.callback(val, self.node.get_event())

    def reconnect(self) -> None:
        try:
            self.disconnect()
        except ValueError: 
            pass
        self.node.connect_callback(self.callback)

    def disconnect(self) -> None:
        self.node.disconnect_callback(self.callback)


@overload
def node_subscription(
        node: Iterable[IDataNode[Any]], 
        callback_func: Callable[...,Any], /, 
        trigger: bool = ...

        ) -> INodeSubscription: ...

@overload
def node_subscription(
        node: IDataNode[Any] , 
        callback_func: Callable[...,Any], /,
        trigger: bool = ...
        ) -> INodeSubscription: ...

def node_subscription(
       node: IDataNode[Any] | Iterable[IDataNode[Any]], 
       callback_func: Callable[...,Any], 
       trigger: bool = True 
    ) -> INodeSubscription:
    """High level function to connect a node to a callback function

    Args:
        node: A scalar data node or an iterable of data node 
        callback_func: A Callable with signature: 
            f() , f(value) or f(value, event)
        trigger (optional): default is True, trigger the callback now

    Returns:
        connection: a INodeSubscription 
            connection handler with .disconnect() and .reconnect()
            methods.

    """
    func = callback(callback_func)
    
    if isinstance(node, Iterable):
        return NodesSubscription(tuple(node), func, trigger=trigger)
    else:
        return NodeSubscription(node, func, trigger=trigger)



