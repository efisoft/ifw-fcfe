from __future__ import annotations
from collections.abc import Mapping
from dataclasses import dataclass
from typing import Any, Callable,  Literal, TypeVar
import weakref 
from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.ibase import (
        DataEventKind, IDataEvent, IDataNode, ILinkCallback, 
        IMetadataElement, INodeSubscription, INodeHandler, 
    )

from ifw.fcfsim.core.node.base import (
     Lock,  basic_node_handler,
)
from ifw.fcfsim.core.node.attribute_node import (
   get_connection_manager, get_metadata_manager
)


@dataclass(frozen=True, init=False)
class ObjDataEvent:  # Conform to IDataEvent Protocol
    """ Event linked to any object attribute modified """
    parent_ref: weakref.ReferenceType[object]

    def __init__(self, parent: object):
        self.__dict__['parent_ref'] = weakref.ref(parent)

    @property
    def kind(self) -> Literal[DataEventKind.OBJ]:
        return DataEventKind.OBJ

    def get_data_node(self) -> ObjectNode[Any] | None:
        """ Return the parent object """
        parent = self.parent_ref()
        if parent is None:
            return None
        return ObjectNode(parent)

    def __hash__(self) -> int:
        return hash(None)

    def __eq__(self, right: Any) -> bool:
        return bool(right is None)


V = TypeVar('V', bound=object, covariant=True)


@dataclass
class ObjectNode(IDataNode[V]):
    """Data node representing a full object
    
    A connection will connect any attribute changes
    """
    obj: V

    def __eq__(self, right: Any) -> bool:
        try:
            obj = right.array
        except AttributeError:
            return False
        return obj is self.obj

    def __hash__(self) -> int:
        return id(self.obj)

    def get_event(self) -> IDataEvent:
        return ObjDataEvent(self.obj)
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        """ Connect  a data change to a callable """
        connection_manager = get_connection_manager(self.obj)
        connection_manager.connect(None, self.get_event(), callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.obj)
        connection_manager.disconnect(None, callback)

    def trigger(self) -> None:
        connection_manager = get_connection_manager(self.obj)
        connection_manager.trigger(None, self.obj)

    def is_connectable(self) -> bool:
        try:
            get_connection_manager(self.obj)
        except ValueError:
            return False
        return True

    def get_metadata(self) -> IMetadataElement:
        return get_metadata_manager(self.obj).provide(None)

    def get(self, default: Any = None) -> V:
        del default  # not used in this context
        return self.obj

    def set(self, value: Mapping[str,Any]) -> None:
        """Set attributes from a dictionary"""
        with Lock(self.obj):
            for k,v in value.items():
                setattr(self.obj, k, v) 
        if self.is_connectable():
            self.trigger() 
        

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler

