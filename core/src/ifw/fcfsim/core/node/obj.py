from __future__ import annotations
from typing import  TypeVar
import typing
import functools

from ifw.fcfsim.core.node.base import (
        ConnectionsManager, MetadataManager
)
T = TypeVar('T', bound=type)

# to complex for type annotation, some __special__ attribute are created 
# on the class 
@typing.no_type_check 
def attrconnect(cls: T) -> T:
    """ Decorate a class to implement attribute connection capabilities 

    The decorator will implement:

    - the __setattr__ method of the class.
        A class instance attribute can then be connected to a function 
        of signature f(value,event) 
    - a __data_metadata__ place holder for metadata, contains timestamp 
        (last data change) 
    - __data_connections__ is a class property, in class instance it 
        contains the data connection manager 
    
    To connect an attribute to an @attrconnect decorated object one 
    can use :func:`ifw.core.api.data_node` ::

       data_node( obj ).connect( some_callback )  

    """
    if hasattr( cls, "__data_connections__"):
        # already defined 
        return cls
    
    # __data_connections__ and __data_metadata__ will be created once 
    # per instance.
    # use cached_property for that. See PEP 0487 for __set_name__
    cls.__data_connections__ = functools.cached_property(lambda _: ConnectionsManager()) 
    cls.__data_connections__.__set_name__(cls, "__data_connections__")
    
    cls.__data_metadata__ = functools.cached_property(lambda _: MetadataManager())   
    cls.__data_metadata__.__set_name__(cls, "__data_metadata__")
    
    # save the current implementation of __setattr__
    cls.__parent_setattr__ = cls.__setattr__ 
    def __setattr__(self, attr, value):
        self.__parent_setattr__(attr, value)
        self.__data_metadata__.touch(attr)
        self.__data_connections__.trigger(attr, value)
    
    cls.__setattr__ = __setattr__ 
    return cls 

