from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable, Literal, TypeVar
import weakref

from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.ibase import (
        DataEventKind, IDataEvent, IDataNode, ILinkCallback, 
        IMetadataElement, INodeSubscription, INodeHandler,
    )
from ifw.fcfsim.core.node.base import (
     basic_node_handler,
    )
from ifw.fcfsim.core.node.iarray import IArray
from ifw.fcfsim.core.node.item_node import (get_connection_manager, get_metadata_manager)


# A special case for slice which are not hashable until python 3.12 
@dataclass(frozen=True)
class FrozenSlice:
    """Make a hashable slice"""
    slc: slice 
    def __hash__(self)->int:
        return hash(( self.slc.start, self.slc.stop, self.slc.step )) 
    def __eq__(self, right: Any)->bool:
        try:
            right = right.slc # This might be a IFrozenSlice 
        except AttributeError:
            pass 
        return bool(( self.slc.start, self.slc.stop, self.slc.step) == right)

@dataclass(frozen=True, init=False)
class ArraySliceDataEvent:  # Conform to IDataEvent Protocol
    """ Event linked to an Array slice change """

    parent_ref: weakref.ReferenceType[IArray[Any]]
    """ Weak reference of the parent Array """
    frozen_slice: FrozenSlice
    """ slice container """

    def __init__(self, parent: IArray[Any], slc: slice):
        # write in __dict__ because the class is frozen
        self.__dict__['slc'] = FrozenSlice(slc)
        self.__dict__['parent_ref'] = weakref.ref(parent)

    @property
    def kind(self) -> Literal[DataEventKind.ARRAY]:
        return DataEventKind.ARRAY

    def get_data_node(self) -> SliceNode[list[Any]] | None:
        parent = self.parent_ref()
        if parent is None:
            return None
        return SliceNode(parent, self.frozen_slice.slc)

    def __hash__(self) -> int:
        return hash(self.frozen_slice)

    def __eq__(self, right: Any) -> bool:
        return bool(self.frozen_slice == right)

V = TypeVar('V', bound=list[Any], covariant=True)

@dataclass
class SliceNode(IDataNode[V]):
    """ Data node representing slice of an Array """
    array: IArray[Any]
    slc: slice

    def __eq__(self, right: Any) -> bool:
        try:
            array = right.array
            slc = right.slc
        except AttributeError:
            return False
        return array is self.array and slc == self.slc

    def __hash__(self) -> int:
        return hash(
            (id(self.array), (self.slc.start, self.slc.step, self.slc.stop)))

    def get_event(self) -> IDataEvent:
        return ArraySliceDataEvent(self.array, self.slc)
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        """ Connect a data change to a callable 
        The function must have the signture f(attr: str|None, value:Any)->None  
        """
        connection_manager = get_connection_manager(self.array)
        connection_manager.connect(FrozenSlice(self.slc),
                                   self.get_event(), callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.array)
        connection_manager.disconnect(FrozenSlice(self.slc), callback)

    def trigger(self) -> None:
        connection_manager = get_connection_manager(self.array)
        connection_manager.trigger(FrozenSlice(self.slc), self.get())

    def is_connectable(self) -> bool:
        try:
            get_connection_manager(self)
        except ValueError:
            return False
        return True

    def get_metadata(self) -> IMetadataElement:
        return get_metadata_manager(self.array).provide(FrozenSlice(self.slc))

    def get(self, default: Any = None) -> V:
        del default  # never used in this context
         # Do not understand the typing problem here ??? Check
        return self.array[self.slc] # type:ignore

    def set(self, value: Any) -> None:
        self.array[self.slc] = value

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler
