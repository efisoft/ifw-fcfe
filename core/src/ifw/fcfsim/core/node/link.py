from __future__ import annotations
from typing import Any, Callable, Iterable, overload
from ifw.fcfsim.core.node.ibase import IDataEvent, IDataNode, ILinkCallback, INodeSubscription
from ifw.fcfsim.core.node.node_handler import (
         pull_nodes, push_nodes, 
)
from ifw.fcfsim.core.node.subscription import (
    node_subscription, NodeSubscription, NodesSubscription
)


@overload
def node_link(source: IDataNode[Any], target: IDataNode[Any], /) -> NodeSubscription:
    ...

@overload 
def node_link(source: IDataNode[Any], func: Callable[[Any],Any], target: IDataNode[Any], /) -> NodeSubscription:
    ...

@overload
def node_link(source: Iterable[IDataNode[Any]], target: Iterable[IDataNode[Any]], /) -> NodesSubscription:
    ...

@overload 
def node_link(source: Iterable[IDataNode[Any]], func: Callable[...,tuple[Any,...]], target: Iterable[IDataNode[Any]], /) -> NodesSubscription:
    ...

@overload 
def node_link(source: Iterable[IDataNode[Any]], func: Callable[..., Any], target: IDataNode[Any], /) -> NodesSubscription:
    ...

@overload
def node_link(source: IDataNode[Any], func: Callable[[Any],tuple[Any,...]], target: Iterable[IDataNode[Any]], /) -> NodeSubscription:
    ...

def node_link(*args: Any, trigger: bool = True) -> INodeSubscription :
    if len(args) == 2:
        source, target = args
        if isinstance(source, Iterable):  
            source = tuple(source)
        if isinstance(target, Iterable):  
            target = tuple(target)
        func = _create_dummy_parser(source, target)
    elif len(args) == 3:
        source, func, target = args
        if isinstance(source, Iterable):  
            source = tuple(source)
        if isinstance(target, Iterable):  
            target = tuple(target)
    else:
        raise TypeError(f"Expecting two or three arguments got {len(args)}")
    
    if isinstance(source, Iterable):
        if isinstance(target, Iterable):
            callback = _n_to_n_link(source, func, target)
        else:
            callback = _n_to_one_link(source, func, target)

    else:
        if isinstance(target, Iterable):
            callback = _one_to_n_link(source, func, target)
        else:
            callback = _one_to_one_link(source, func, target)
    
    return node_subscription(source, callback, trigger=trigger)
   
def _one_to_one_link(
      source: IDataNode[Any], 
      parser:Callable[[Any],Any], 
      target: IDataNode[Any]
) -> ILinkCallback:
    def receive(value: Any, event: IDataEvent) -> None:
        target.set(parser(value))
    return receive

def _n_to_one_link(
        source: Iterable[IDataNode[Any]], 
        parser:Callable[...,Any], 
        target: IDataNode[Any]
) -> ILinkCallback:
    def receive(value: Any, event: IDataEvent) -> None:
        value_dict = pull_nodes(source)
        target.set(parser(*(value_dict[n] for n in source)))
    return receive

def _n_to_n_link(
        source: Iterable[IDataNode[Any]],
        parser: Callable[..., tuple[Any, ...]],
        target: Iterable[IDataNode[Any]]
) -> ILinkCallback:
    def receive(value: Any, event: IDataEvent) -> None:
        value_dict = pull_nodes(source)
        values = parser(*(value_dict[n] for n in source))
        push_nodes({t: v for v, t in zip(values, target)})
    return receive

def _one_to_n_link(
        source: IDataNode[Any],
        parser: Callable[..., tuple[Any, ...]],
        target: Iterable[IDataNode[Any]]
) -> ILinkCallback:
    def receive(value: Any, event: IDataEvent) -> None:
        values = parser(value)
        push_nodes({t: v for v, t in zip(values, target)})
    return receive

def _create_dummy_parser(
        source: IDataNode[Any] | tuple[IDataNode[Any]], 
        target: IDataNode[Any] | tuple[IDataNode[Any]] 
) -> Callable[..., Any]:
    ls, lt = 0, 0
    if isinstance(source, Iterable): 
        ls = len(source)
    if isinstance(target, Iterable): 
        lt = len(target)

    if ls != lt:
        raise ValueError(f"Parsing function can only be ommited if the number of nodes are equal got {ls} and {lt}")
    if ls>0: # Iterable 
        def func(*args: Any) -> tuple[Any,...]: return args #pyright:ignore 
    else:
        def func(x: Any) -> Any: return x #type:ignore[misc]
    return func



