from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable,  Literal, TypeVar
import weakref 
from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.ibase import (
        DataEventKind, IDataEvent, IDataNode, ILinkCallback, 
        IMetadataElement, INodeSubscription, INodeHandler, 
        IConnectionsManager, IMetadataManager
    )
from ifw.fcfsim.core.node.base import (
        Empty, basic_node_handler,
)


def get_connection_manager(obj: Any) -> IConnectionsManager:
    """ Return the connection manager associated to an object 

    If the object does not have a connection manager, a ValueError is raised 
    """
    try:
        return obj.__data_connections__ # type: ignore[no-any-return]  
    except AttributeError:
        raise ValueError( "Cannot found a connection manager for this object."
                          " missing __data_connections__."
                           "tips: maybe class hasn't been decorated with @attrconnect")


def get_metadata_manager(obj: Any) -> IMetadataManager:
    """ Return the metadata manager associated to an object 
    
    Raise a ValueError if no metadata manager 
    """
    try:
        return obj.__data_metadata__  # type: ignore[no-any-return]
    except AttributeError:
        raise ValueError("Object has no metadata manager."
                         " __data_metadata__ attribute is missing")

U = TypeVar('U', bound=Any, covariant=True)


@dataclass(frozen=True, init=False)
class AttrDataEvent:  # conform to IDataEvent
    """ Event linked to an object attribute has been changed 
    
    The parent object being most likely be decorated with @attrconnect
    """
    # weak reference to avoid circular references. Is it realy needed ?
    parent_ref: weakref.ReferenceType[object]
    """ Weak reference of the parent object """
    attr: str
    """ Attribute name in parent object """

    def __init__(self, parent: object, attr: str):
        # write in __dict__ because the class is frozen
        self.__dict__['attr'] = attr
        self.__dict__['parent_ref'] = weakref.ref(parent)

    @property
    def kind(self) -> Literal[DataEventKind.OBJ_ATTR]:
        """ Event Kind """
        return DataEventKind.OBJ_ATTR

    def get_data_node(self) -> AttributeNode[Any] | None:
        """Data node associated to the event"""
        parent = self.parent_ref()
        if parent is None:
            return None
        return AttributeNode(parent, self.attr)

    def __hash__(self) -> int:
        return hash(self.attr)

    def __eq__(self, right: Any) -> bool:
        return bool(self.attr == right)


class AttributeNode(IDataNode[U]):
    """ A data node representing an object attribute 
    
    In order to have connect, disconnect and trigger capabilities,   
    the object must be an instance of a class decorated with @attrconnect
    """
    obj: Any
    attr: str

    def __init__(self, obj: Any, attr: str):
        self.obj, self.attr = (obj, attr)

    def __eq__(self, right: Any) -> bool:
        try:
            obj = right.array
            attr = right.index
        except AttributeError:
            return False
        return obj is self.obj and attr == self.attr

    def __hash__(self) -> int:
        return hash((id(self.obj), self.attr))

    def get_event(self) -> IDataEvent:
        return AttrDataEvent(self.obj, self.attr)
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.obj)
        connection_manager.connect(self.attr, self.get_event(), callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.obj)
        connection_manager.disconnect(self.attr, callback)

    def trigger(self) -> None:
        connection_manager = get_connection_manager(self.obj)
        connection_manager.trigger(self.attr, self.get())

    def is_connectable(self) -> bool:
        try:
            get_connection_manager(self)
        except ValueError:
            return False
        return True

    def get_metadata(self) -> IMetadataElement:
        return get_metadata_manager(self.obj).provide(self.attr)

    def get(self, default: Any = Empty) -> U:
        try:
            return getattr(self.obj, self.attr)  # type: ignore[no-any-return]
        except AttributeError:
            if default is Empty:
                raise ValueError(
                    "Cannot get data, provide a default if needed")
            return default  # type: ignore[no-any-return]

    def set(self, value: Any) -> None:
        setattr(self.obj, self.attr, value)

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler



