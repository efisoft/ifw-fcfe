from __future__ import annotations
from dataclasses import  dataclass
from typing import Any,  Iterable, Literal, TypeVar, overload
import re

from ifw.fcfsim.core.node.ibase import (
    IDataItem, IDataNode, IItem,  INodeFactory, INodeHandler,
)

from ifw.fcfsim.core.node.index_node import IndexNode 
from ifw.fcfsim.core.node.slice_node import SliceNode

from ifw.fcfsim.core.node.attribute_node import AttributeNode 
from ifw.fcfsim.core.node.object_node import ObjectNode

from ifw.fcfsim.core.node.dependency import (
    get_item_dependencies, LinkedDataNode 
)

from ifw.fcfsim.core.node.item_node import ItemNode, _KeyType

@dataclass
class Item:
    """String Item holder"""
    key: _KeyType
   

def resolve_data_path(obj: Any, path: str) -> tuple[Any, str | int | slice | Item]:
    """ Navigate though data from attribute path 
    
    Return the last parent and the last attribute name or index

    '[n]' is understood as an item of data (e.i. data[n]) It works only if 
    n is an integer.

    resolve_data_path(data, "a") -> (data, "a")
    resolve_data_path(data, "a.b") -> (data.a, "b")
    resolve_data_path(data, "a.b.c") -> (data.a.b, "c")
    resolve_data_path(data, "a.b[1].c") -> (data.a.b[1], "c")
    resolve_data_path(data, "a.b[1]") -> (data.a.b, 1)
    etc ...

    """
    *data_path, opath = path.split(".")
    try:
        for p in data_path:
            obj, pa = _walk_through_items(obj, p)
            if isinstance(pa, str):
                obj = getattr(obj, pa)
            else:
                obj = obj[pa]
        return _walk_through_items(obj, opath)
    except (AttributeError, KeyError) as ext:
        raise ValueError(f"Cannot resolve the path {path!r}") from ext

def _parse_slice_elem(elem:str)->int|None:
    elem = elem.strip()
    if not elem: 
        return None 
    if elem == "None": 
        return None
    return int(elem)

# _re_match_subscription = re.compile(  "^([^[]*)[\\[]([0-9:]+)[\\]](.*)")
_re_match_subscription = re.compile(  "^([^[]*)[\\[]([0-9:a-zA-Z_. ]+)[\\]](.*)")
def _walk_through_items(obj:Any, path:str)->tuple[object, str|int|slice|Item]:
    m = _re_match_subscription.match(path)
    if not m:
        return obj, path 
    a, si, rest = m.groups() 
    i : int|slice|Item
    if ':' in si:
        i = slice( *( _parse_slice_elem(s) for s in si.split(":")) )
    else:
        try:
            i = int(si)
        except (ValueError, TypeError):
            i = Item(si) 
    if not rest:
        if a:
            return getattr(obj, a), i
        else:
            return obj, i
    else:
        if a:
            if isinstance(i, Item):
                return _walk_through_items( getattr(obj, a)[i.key], rest)
            else:
                return _walk_through_items( getattr(obj, a)[i], rest)
        else:
            if isinstance(i, Item):
                return _walk_through_items( obj[i.key], rest)
            else:
                return _walk_through_items( obj[i], rest)


def get_value(obj:Any, path:str)->Any:
    """ Get an object value from a string path 
    
    e.g. ``get_value(obj, "a.b[1].c")`` returns ``obj.a.b[1].c``
    """
    obj, i = resolve_data_path(obj, path)
    if isinstance(i, str):
        return getattr( obj, i ) 
    if isinstance(i, Item):
        return obj[i.key]
    return obj[i] 

def set_value(obj:Any, path:str, value:Any)->None:
    """ Set value from a string path 

    e.g. ``set_value( obj, "a.b[1].c", "new_val")`` 
    is equivalent to  ``obj.a.b[1].c = "new_val"``
    """
    obj, i = resolve_data_path(obj, path)
    if isinstance(i, str):
        setattr( obj, i, value) 
    elif isinstance(i, Item):
        obj[i.key] = value
    else:
        obj[i] = value 



VN = TypeVar('VN',bound=IDataNode[Any])


@overload
def data_node(obj: Any, path: str) -> IDataNode[Any]:
    ...


@overload
def data_node(obj: Any, path: int) -> IndexNode[Any]:
    ...


@overload
def data_node(obj: Any, path: slice) -> SliceNode[Any]:
    ...


@overload
def data_node(obj: Any, path: Literal[None]) -> ObjectNode[Any]:
    ...


@overload
def data_node(obj: Any, path: INodeFactory[VN]) -> VN:
    ...

@overload
def data_node(obj: Any, path: IItem) -> ItemNode[Any]:
    ...

def data_node(obj: Any, path:IDataItem) -> IDataNode[Any]:
    """ Return a data node from a given object and a 'path'  
    
    The returned IDataNode object will depend on the nature of path 
    end point. 
    
    Exemple:
        
        data_node( obj, "a.c.d") # similar to  
        data_node( obj.a.c, "d") 
        # they return an AttributeNode 
        
        data_node(obj, "a.c[2]") # similar to 
        data_node(obj.a.c, 2) 
        # they return an IndexNode 
        
        data_node(obj,None) # return a ObjectNode 
        
        data_node(obj, "a.c[:]") # similar to 
        data_node(obj.a.c, slice(None,None)) 
        # they return a SliceNode

        
        data_node(obj, "a.b.c").get() == obj.a.b.c 
        data_node(a, 1).get() == a[1]  
    """ 
        
    if isinstance(path, str):
        obj, attr_or_index = resolve_data_path(obj, path)
    elif isinstance(path, (int, slice, Item)):
        attr_or_index = path 
    elif path is None:
        return ObjectNode(obj)
    elif isinstance(path, INodeFactory):
        return path.make_data_node(obj)
    else:
        raise ValueError(f"Unexpected path type, expected a str, in , slice or None got a {type(path)}")

    if isinstance(attr_or_index, int):
        return IndexNode(obj, attr_or_index)
    elif isinstance( attr_or_index,  slice):
        return SliceNode(obj, attr_or_index)
    elif isinstance( attr_or_index, Item):
        return ItemNode(obj, attr_or_index.key)
    else:
        dependencies = get_item_dependencies(obj, attr_or_index)
        if dependencies is not None:
            return LinkedDataNode( 
                    AttributeNode(obj,attr_or_index), 
                    tuple(data_node(obj,i) for i in dependencies)
                )
        return AttributeNode(obj, attr_or_index)


def data_nodes(obj: Any, *pathes:IDataItem)->list[IDataNode[Any]]:
    """Return a list of data node from the same marser object"""
    return [data_node(obj, path) for path in pathes]


@overload
def pull_nodes(nodes: Iterable[IDataNode[Any]], results: None) -> dict[IDataNode[Any], Any]:
    ...


@overload
def pull_nodes(nodes: Iterable[IDataNode[Any]]) -> dict[IDataNode[Any], Any]:
    ...


@overload
def pull_nodes(nodes: Iterable[IDataNode[Any]], results: dict[IDataNode[Any], Any]) -> None:
    ...


def pull_nodes(
    nodes: Iterable[IDataNode[Any]],
    results: dict[IDataNode[Any], Any] | None = None
) -> None | dict[IDataNode[Any], Any]:
    """ Pull a bunch of node values into a dictionary of node/value pairs 

    Args:
        nodes: Iterable of IDataNodes 
        results (optional): if given write the output inside and return None 

    Returns:
        results: dictionary of DataNode/values if results input is not given, None
            otherwise 
    """
    return_results = False
    if results is None:
        return_results = True
        results = {}
    handlers: dict[INodeHandler[IDataNode[Any]], list[IDataNode[Any]]] = {}
    # classify all nodes by handler
    for node in nodes:
        handlers.setdefault(node.get_node_handler(), []).append(node)

    for handler, nodelist in handlers.items():
        handler.pull(nodelist, results)

    if return_results:
        return results
    else:
        return None


def push_nodes(node_values: dict[IDataNode[Any], Any]) -> None:
    """ Push a bunch of node values 

    Args:
        node_values:  dictionary of IDataNode/value pairs to be set 
    """
    handlers: dict[INodeHandler[IDataNode[Any]],
        dict[IDataNode[Any], Any]] = {}
    # Classify by handler
    for node, value in node_values.items():
        handlers.setdefault(node.get_node_handler(), {})[node] = value

    for handler, nvs in handlers.items():
        handler.push(nvs)


def read_nodes(*nodes: IDataNode[Any]) -> tuple[Any, ...]:
    """ Get nodes value and return in a tuple of node input dimension """
    results: dict[IDataNode[Any], Any] = {}
    pull_nodes(nodes, results)
    return tuple(results[node] for node in nodes)



