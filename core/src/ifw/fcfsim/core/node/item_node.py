from __future__ import annotations
from dataclasses import dataclass
from typing import (
        Any, Callable, Literal, 
        TypeVar, Union
    )
from collections.abc import MutableMapping
from ifw.fcfsim.core.node.subscription import node_subscription
from typing_extensions import TypeAlias
import weakref
from ifw.fcfsim.core.node.ibase import (
        DataEventKind, 
        IMetadataElement, 
        IMetadataManager, 
        IConnectionsManager, 
        IDataEvent, 
        IDataNode, 
        ILinkCallback,
        INodeSubscription, 
        INodeHandler,
        )
from ifw.fcfsim.core.node.base import Empty, basic_node_handler


_KeyType: TypeAlias = Union[str, int]

def get_connection_manager(obj: Any) -> IConnectionsManager:
    """ Return the connection manager associated to an Array 

    If the Array does not have a connection manager, a ValueError is raised 
    """
    try:
        return obj.__data_connections__ # type: ignore[no-any-return]  
    except AttributeError:
        raise ValueError("Cannot found a connection manager for this object"
                        f" Expecting an Array got a {type(obj)}")


def get_metadata_manager(obj: Any) -> IMetadataManager:
    """ Return the metadata manager associated to an Array 
    
    Raise a ValueError if no metadata manager 
    """
    try:
        return obj.__data_metadata__  # type: ignore[no-any-return]
    except AttributeError:
        raise ValueError("Cannot find metadata manager on object"
                        f" Expecting an Array got a {type(obj)}")


@dataclass(frozen=True, init=False)
class ItemDataEvent:  # conform to IDataEvent
    """ Event linked to a item change 

    Most likely happening on an Map object item changes. 
    """

    parent_ref: weakref.ReferenceType[MutableMapping[_KeyType, Any]]
    """ Weak reference of the parent Array """

    item: _KeyType
    """ Array index """

    def __init__(self, 
            parent: MutableMapping[_KeyType, Any],
            item: _KeyType):
        # write in __dict__ because the class is frozen
        self.__dict__['item'] = item
        self.__dict__['parent_ref'] = weakref.ref(parent)

    @property
    def kind(self) -> Literal[DataEventKind.ITEM]:
        return DataEventKind.ITEM

    def get_data_node(self) -> ItemNode[Any] | None:
        parent = self.parent_ref()
        if parent is None:
            return None
        return ItemNode(parent, self.item)

    def __hash__(self) -> int:
        return hash(self.item)

    def __eq__(self, right: Any) -> bool:
        return bool(self.item == right)


U = TypeVar('U', bound=Any, covariant=True)
@dataclass(frozen=True)
class ItemNode(IDataNode[U]):
    """ A data node representing an Map item """
    mapping: MutableMapping[_KeyType, U]
    """ Parent mapping"""
    
    item: _KeyType
    """ mapping item"""

    def __eq__(self, right: Any) -> bool:
        try:
            m = right.mapping
            i = right.item
        except AttributeError:
            return False
        return m is self.mapping and i == self.item

    def __hash__(self) -> int:
        return hash((id(self.mapping), self.item))

    def get_event(self) -> IDataEvent:
        return ItemDataEvent(self.mapping, self.item)
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.mapping)
        connection_manager.connect(self.item, self.get_event(), callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.mapping)
        connection_manager.disconnect(self.item, callback)

    def trigger(self) -> None:
        connection_manager = get_connection_manager(self.mapping)
        connection_manager.trigger(self.item, self.get())

    def is_connectable(self) -> bool:
        try:
            get_connection_manager(self)
        except ValueError:
            return False
        return True

    def get_metadata(self) -> IMetadataElement:
        return get_metadata_manager(self.mapping).provide(self.item)

    def get(self, default: Any = Empty) -> U:
        try:
            return self.mapping[self.item]
        except KeyError:
            if default is Empty:
                raise ValueError(
                    "Cannot get data, provide a default if needed")
            return default  # type: ignore[no-any-return]

    def set(self, value: Any) -> None:
        self.mapping[self.item] = value

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler

