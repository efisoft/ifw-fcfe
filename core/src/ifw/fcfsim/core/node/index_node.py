from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable, Literal, Protocol, TypeVar
import weakref

from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.ibase import (
        DataEventKind, IDataEvent, IDataNode, ILinkCallback, 
        IMetadataElement, INodeSubscription, INodeHandler,
    )
from ifw.fcfsim.core.node.base import (
        Empty, basic_node_handler,
    )

from ifw.fcfsim.core.node.item_node import (get_connection_manager, get_metadata_manager)



T = TypeVar('T', bound=Any, covariant=True)
class _Indexable(Protocol[T]):
    """IndexNode should work on any subscriptable obect"""
    def __getitem__(self, index: int, /) -> T:
        ...
    def __setitem__(self, index: int, value: Any, /) -> None:
        ...


@dataclass(frozen=True, init=False)
class IndexDataEvent:  # conform to IDataEvent
    """ Event linked to a numerical index change 

    Most likely happening on an Array object item changes. 
    """

    parent_ref: weakref.ReferenceType[_Indexable[Any]]
    """ Weak reference of the parent Array """
    index: int
    """ Array index """

    def __init__(self, parent:  _Indexable[Any], index: int):
        # write in __dict__ because the class is frozen
        self.__dict__['index'] = index
        self.__dict__['parent_ref'] = weakref.ref(parent)

    @property
    def kind(self) -> Literal[DataEventKind.ARRAY_INDEX]:
        return DataEventKind.ARRAY_INDEX

    def get_data_node(self) -> IndexNode[Any] | None:
        parent = self.parent_ref()
        if parent is None:
            return None
        return IndexNode(parent, self.index)

    def __hash__(self) -> int:
        return self.index

    def __eq__(self, right: Any) -> bool:
        return bool(self.index == right)


U = TypeVar('U', bound=Any, covariant=True)

@dataclass(frozen=True)
class IndexNode(IDataNode[U]):
    """ A data node representing an Array item at a specific index """
    array: _Indexable[U]
    index: int

    def __eq__(self, right: Any) -> bool:
        try:
            a = right.array
            i = right.index
        except AttributeError:
            return False
        return a is self.array and i == self.index

    def __hash__(self) -> int:
        return hash((id(self.array), self.index))

    def get_event(self) -> IDataEvent:
        return IndexDataEvent(self.array, self.index)
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.array)
        connection_manager.connect(self.index, self.get_event(), callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        connection_manager = get_connection_manager(self.array)
        connection_manager.disconnect(self.index, callback)

    def trigger(self) -> None:
        connection_manager = get_connection_manager(self.array)
        connection_manager.trigger(self.index, self.get())

    def is_connectable(self) -> bool:
        try:
            get_connection_manager(self)
        except ValueError:
            return False
        return True

    def get_metadata(self) -> IMetadataElement:
        return get_metadata_manager(self.array).provide(self.index)

    def get(self, default: Any = Empty) -> U:
        try:
            return self.array[self.index] 
        except (IndexError, KeyError) as err:
            if default is Empty:
                raise ValueError(
                    f"Cannot get data associated to inde {self.index}, "
                    "provide a default if needed") from err
            return default  # type: ignore[no-any-return]

    def set(self, value: Any) -> None:
        self.array[self.index] = value

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler

_ = IndexNode([], 0)
__ = IndexNode({}, 0)
