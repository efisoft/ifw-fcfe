from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any, Callable, Literal, TypeVar

from ifw.fcfsim.core.node.base import ConnectionElement, MetadataElement, basic_node_handler
from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.ibase import DataEventKind, IDataEvent, IDataNode, ILinkCallback, IMetadataElement, INodeSubscription, INodeHandler

@dataclass(frozen=True)
class FuncDataEvent:  # conform to IDataEvent
    """ Event linked to a numerical index change 

    Most likely happening on an Array object item changes. 
    """
    getter: Callable[[],Any]  
    setter: Callable[[Any],None] | None
    
    @property
    def kind(self) -> Literal[DataEventKind.FUNC]:
        return DataEventKind.FUNC

    def get_data_node(self) -> FuncNode[Any] | None:
        return FuncNode(self.getter, self.setter)
        
    def __hash__(self) -> int:
        return hash((id(self.getter), id(self.setter)))
    
    def __eq__(self, right: Any) -> bool:
        try:
            g = right.getter
            s = right.setter
        except AttributeError:
            return False
        return g is self.getter and s == self.setter

T = TypeVar('T', bound=Any, covariant=True)

@dataclass(frozen=True)
class FuncNode(IDataNode[T]):
    """ A data node with a getter and optional setter method """
    getter: Callable[[],T]  
    setter: Callable[[Any],None] | None = None

    metadata: IMetadataElement = field(default_factory=MetadataElement)
    connections: ConnectionElement = field(init=False)
    def __post_init__(self) -> None:
        self.__dict__['connections'] = ConnectionElement(FuncDataEvent(self.getter, self.setter))

    def __eq__(self, right: Any) -> bool:
        try:
            g = right.getter
            s = right.setter
        except AttributeError:
            return False
        return g is self.getter and s == self.setter

    def __hash__(self) -> int:
        return hash((id(self.getter), id(self.setter)))

    def get_event(self) -> IDataEvent:
        return FuncDataEvent(self.getter, self.setter)
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        if self.setter is None:
            raise ValueError("Cannot connect a FuncNode without a setter (setter is None)")
        self.connections.add(callback)
        
    def disconnect_callback(self, callback: ILinkCallback) -> None:
        try:
            self.connections.remove(callback)
        except ValueError:
            return

    def trigger(self) -> None:
        val = self.get()
        self.metadata.touch()
        self.connections.trigger(val)

    def is_connectable(self) -> bool:
        return self.setter is not None
        
    def get_metadata(self) -> IMetadataElement:
        return self.metadata

    def get(self, default: Any = None) -> T:
        return self.getter()

    def set(self, value: Any) -> None:
        if self.setter is None:
            raise ValueError("Cannot set a FuncNode without a setter method")
        self.setter(value)
        self.metadata.touch()
        self.connections.trigger(value)

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler


_ : IDataNode[int] =  FuncNode(int)

