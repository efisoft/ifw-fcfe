from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable, TypeVar, cast

from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.ibase import (
        IDataEvent, IDataNode, ILinkCallback, 
        IMetadataElement, INodeSubscription, INodeHandler
)
from ifw.fcfsim.core.node.base import basic_node_handler


def dependencies(*deps: str) -> Callable[[Callable[..., Any]], Callable[..., Any]]:
    def dec(fun: Callable[..., Any]) -> Any:
        # typing: force attr attrib
        fun.__data_dependencies__ = tuple(deps)# type:ignore
        return fun
    return dec

def get_item_dependencies(obj:object, attr:str) -> tuple[str,...] | None:
    try:
        member = getattr( obj.__class__, attr)
    except AttributeError:
        return None
    try:
        return cast(tuple[str,...], tuple( member.__data_dependencies__ ))
    except AttributeError:
        try:
            # it can be a property 
            return cast(tuple[str,...], tuple( member.fget.__data_dependencies__ ))
        except AttributeError:
            return None


@dataclass(frozen=True)
class ReferenceCallback:
    """ A simple callable connection callback for dependencies 

    object design to trigger node dependency callbacks 
    """
    event: IDataEvent
    trigger: Callable[[], None]

    # conform to ILinkCallback
    def __call__(self, value: Any, event: IDataEvent) -> None:  
        self.trigger()

    def __hash__(self) -> int:
        return hash(self.event)

    def __eq__(self, right: Any) -> bool:
        try:
            return bool(self.event == right.event)
        except AttributeError:
            return False


T = TypeVar('T', bound=Any,  covariant=True)


@dataclass
class LinkedDataNode(IDataNode[T]):
    """ A data node attached to some reference nodes
    
    Designed to be used along with @property and @dependency decorators 
    in class declaration.

    When making a connection a change in reference node will trigger 
    the connection of the slaved node
    """

    slaved_node: IDataNode[T]
    reference_nodes: tuple[IDataNode[Any], ...]

    def __eq__(self, right: Any) -> bool:
        try:
            pn = right.property_node
            rn = right.reference_nodes
        except AttributeError:
            return False
        return self.slaved_node is pn and self.reference_nodes == rn

    def __hash__(self) -> int:
        return hash((self.slaved_node, self.reference_nodes))

    def get_event(self) -> IDataEvent:
        return self.slaved_node.get_event()
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        refcallback = ReferenceCallback(
            self.slaved_node.get_event(), self.trigger)
        for node in self.reference_nodes:
            node.connect_callback(refcallback)
        self.slaved_node.connect_callback(callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        refcallback = ReferenceCallback(
            self.slaved_node.get_event(), self.trigger)
        for node in self.reference_nodes:
            node.connect_callback(refcallback)
        self.slaved_node.disconnect_callback(callback)

    def trigger(self) -> None:
        self.slaved_node.trigger()

    def is_connectable(self) -> bool:
        return self.slaved_node.is_connectable()

    def get_metadata(self) -> IMetadataElement:
        return self.slaved_node.get_metadata()

    def get(self, default: Any = None) -> T:
        return self.slaved_node.get(default)

    def set(self, value: Any) -> None:
        self.slaved_node.set(value)

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler

