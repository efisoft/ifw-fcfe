from __future__ import annotations
from typing import Any, Iterable, Protocol, TypeVar, overload


U = TypeVar('U', bound=Any)


class IArray(Protocol[U]):
    """Static Array with data connection capabilities"""
    @overload
    def __getitem__(self, item: slice) -> list[U]:
        ...

    @overload
    def __getitem__(self, item: int) -> U:
        ...

    @overload
    def __setitem__(self, item: slice, value: Iterable[U]) -> None:
        ...

    @overload
    def __setitem__(self, item: int, value: U) -> None:
        ...


