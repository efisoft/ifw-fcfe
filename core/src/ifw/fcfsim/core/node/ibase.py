from __future__ import annotations
from datetime import datetime
import enum
from typing import (Any, Callable, Hashable, Iterable,
                    Protocol, TypeVar, Union, runtime_checkable)
from typing_extensions import TypeAlias


class DataEventKind(enum.IntEnum):
    """Kind of Events recognized connection callback event argument """
    OBJ_ATTR = 1
    ARRAY_INDEX = 2
    OBJ = 3
    ARRAY = 4
    ALIAS = 5
    STANDALONE = 6
    ITEM = 7
    FUNC = 8
    CUSTOM = 20
    

@runtime_checkable
class ILinkable(Protocol):
    """Represent an object with attribute connection capabilities
    
    They are object for which the class was decorated with @attrconnect
    """
    __data_connections__: property
    __data_metadata__: property

class IDataEvent(Protocol):
    """Event Interface received by connected callback"""
    # This is mode aplace hodler for now
    @property
    def kind(self) -> DataEventKind:
        ...

    def get_data_node(self) -> IDataNode[Any] | None:
        ...

    def __hash__(self) -> int:
        # Data Event shall be hashable
        ...

    def __eq__(self, right: Any) -> bool:
        # eq necessary to identify Events (e.g. in dict key)
        ...


ILinkCallback = Callable[[Any, IDataEvent], None]
""" callback method signature for linkable objects (@attrconnect)
Expected signature f(attr_name, new_value) for linked object
Where attr_name is a string for attribute name 
    or can be None if new_value is on a  whole object 
"""


class IMetadataElement(Protocol):
    """Metadata singletone associated to a Data Node"""

    def get_timestamp(self, default: datetime = ...) -> datetime:
        """Return the last modification timestamp or default"""
        ...

    def touch(self, dt: datetime | None = None) -> None:
        """Touch timestamp to now or given datetime"""
        ...



class IMetadataManager(Protocol):
    """Manage several metadata elements"""

    def get(self, item: IDataKey) -> IMetadataElement:
        """Get the metadata element of a given item

        Should raise ValueError if not found
        """
        ...

    def provide(self, item: IDataKey) -> IMetadataElement:
        """Get the metadata element of a given item 

        Build and store one if it does not exists
        """
        ...

    def add(self, item: IDataKey, metadata: IMetadataElement) -> None:
        """Store a new metadata element for the given item"""
        ...

    def touch(self, item: IDataKey, dt: datetime | None = None) -> None:
        ...


class IConnectionsManager(Protocol):
    """Manage several connection identified with a Data Key"""

    def trigger(self, item: IDataKey, value: Any) -> None:
        """Trigger value changes connection of an item with value"""
        ...

    def connect(self,
          item: IDataKey,
          event: IDataEvent,
          callback: Callable[[IDataEvent, Any], None]) -> None:
        """Connect event and callback"""
        ...

    def disconnect(self,
            item: IDataKey,
            callback: Callable[[IDataEvent, Any], None]) -> bool:
        """Disconnect the gieven item and callback"""
        ...




T = TypeVar('T', covariant=True)


class IDataNode(Protocol[T]):
    """ Handy object dedicated to abstract the handling of data value

    A data node can be modified and connected to changes with the 
    abstraction of where is the data. 
    """

    def get_event(self) -> IDataEvent:
        """Return the associated data event 

        data event are used in connected callback functions
        """
        ...
    
    def connect(self, func: Callable[...,Any], trigger: bool = ...) -> INodeSubscription:
        """High level function to connect the node and return a connection handler 
        
        The callback function can be of signature: 
            f() , f(value:Any), or f(value, event:IDataEvent) 
        If the trigger flag is true, the callback function is called at connection

        See also lower level connect_callback and disconnect_callback function
        """
        ...

    def connect_callback(self, callback: ILinkCallback) -> None:
        """Connect callback to a data change 
        callback shall be of signature f(value: Any, event: IDataEvent) 
        For a more flexible high level method see the connect method.
        """
        ...

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        """Disconnect the callback of a data change"""
        ...

    def trigger(self) -> None:
        """ Trigger connection callback with current stored value """
        ...

    def is_connectable(self) -> bool:
        """True if the node can be connected to callback"""
        ...

    def get_metadata(self) -> IMetadataElement:
        """Return an object holding Node Metadata """
        ...

    def get(self, default: Any = ...) -> T:
        """Get related data.

        Raise a ValueError if no data reachable and default is not set 
        """
        ...

    def set(self, value: Any) -> None:
        """ set any value to data 
        
        Callback links will be triggered 
        """
        ...

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        """ Return a handy object to handle a bunch of nodes of the same context """
        ...


V = TypeVar('V', bound=IDataNode[Any], covariant=True)


@runtime_checkable
class INodeFactory(Protocol[V]):
    def make_data_node(self, obj: Any) -> V:
        ...

class IItem(Protocol):
    """String Item holder"""
    key: str


IDataItem: TypeAlias = Union[str, int, None,
                             slice, IItem, INodeFactory[IDataNode[Any]]]
""" Item type which should be accepted has valid from user """

# IDataKey:  TypeAlias = Union[str, int, None, IFrozenSlice, IDataEvent]
IDataKey: TypeAlias = Hashable
""" Item type has stored in dictionary of connections 
They are slightly different than IDataItem because some of them 
are not hashable 
"""


UN = TypeVar('UN', bound=IDataNode[Any])


class INodeHandler(Protocol[UN]):
    """Handled several nodes in ones"""

    def __hash__(self) -> int:
        ...

    def pull(self, nodes: Iterable[UN], output: dict[UN, Any]) -> None:
        ...

    def push(self, node_values: dict[UN, Any]) -> None:
        ...


class INodeSubscription(Protocol):
    """Carry node(s) connection to a callback"""
    def disconnect(self) -> None:
        ...

    def reconnect(self) -> None:
        ...


