from __future__ import annotations

from dataclasses import dataclass, field
from typing import Any, Callable, Literal, TypeVar
import weakref

from ifw.fcfsim.core.node.base import (
        ConnectionElement, Empty, MetadataElement, basic_node_handler,
    )
from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.ibase import (
        DataEventKind, IDataEvent, IDataNode, ILinkCallback, 
        IMetadataElement, INodeSubscription, INodeHandler,
    )


@dataclass(frozen=True, init=False)
class StandaloneDataEvent:
    """ Event linked to a standalone node  

    see :class:`ifw.core.api.StandaloneNode`
    """

    parent_ref: weakref.ReferenceType[StandaloneNode[Any]]
    """ Weak reference of the node itslef """
    node_id: int
    """ Node python id """

    def __init__(self, parent: StandaloneNode[Any]):
        # write in __dict__ because the class is frozen
        self.__dict__['parent_ref'] = weakref.ref(parent)
        self.__dict__['node_id'] = id(parent)

    @property
    def kind(self) -> Literal[DataEventKind.STANDALONE]:
        return DataEventKind.STANDALONE

    def get_data_node(self) -> StandaloneNode[Any] | None:
        return self.parent_ref()

    def __hash__(self) -> int:
        return self.node_id

    def __eq__(self, right: Any) -> bool:
        try:
            rid = right.node_id
        except AttributeError:
            return False
        return bool(rid == self.node_id)


T = TypeVar('T',bound=Any,  covariant=True)


@dataclass
class StandaloneNode(IDataNode[T]):
    """ A Standalone Data Node without any relation to other parent object 
    
    The value is stored inside this object
    """
    value: Any = Empty

    metadata: IMetadataElement = field(default_factory=MetadataElement)

    connections: ConnectionElement = field(init=False)

    def __hash__(self) -> int:
        return id(self)
    
    def __eq__(self, value: object, /) -> bool:
        return value is self

    def __post_init__(self) -> None:
        self.connections = ConnectionElement(StandaloneDataEvent(self))

    def get_event(self) -> IDataEvent:
        return self.connections.event
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        self.connections.add(callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        try:
            self.connections.remove(callback)
        except ValueError:
            return

    def trigger(self) -> None:
        val = self.get()
        self.metadata.touch()
        self.connections.trigger(val)

    def is_connectable(self) -> bool:
        return True

    def get_metadata(self) -> IMetadataElement:
        return self.metadata

    def get(self, default: Any = None) -> T:
        return default if self.value is Empty else self.value# type: ignore[no-any-return]

    def set(self, value: Any) -> None:
        self.value = value
        self.metadata.touch()
        self.connections.trigger(value)

    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler
