from __future__ import annotations
from typing import (
        Any, Generic, Iterator, 
        TypeVar, Union
    )
from collections.abc import MutableMapping
from typing_extensions import TypeAlias
from ifw.fcfsim.core.node.base import MetadataManager, ConnectionsManager
from ifw.fcfsim.core.node.item_node import ItemNode 

# Restric Key Type to be String and Iteger which should be enough 
# for most application involving client/server applications
_KeyType: TypeAlias = Union[str, int]

T = TypeVar('T', bound=Any)
K = TypeVar('K', bound=_KeyType)

class Map(MutableMapping[_KeyType,T], Generic[T]):
    """Wrap a dictionary into an dictionary like object with data connections

    .. Note::

        The data dictionary  in ``m = Map(data)`` is not copied so any item setting
        in m (e.g. ``m['key1'] = val1``) is done on data.
    
    Example::

        from ifw.fcfsim.core import api as core 
        m = core.Map()
        m['a'] = 1 
        
        a_node = core.data_node(m, '[a]')
        assert a_node.get() == m['a']
        a_node.connect(print, False)
        m['a'] = 2

    """
    _data: MutableMapping[_KeyType, T]
    __data_connections__: ConnectionsManager 
    __data_metadata__: MetadataManager 

    def __init__(self, data: MutableMapping[_KeyType, T] | None = None):
        if data is None:
            data = {}
        self._data = data 
        
        self.__data_connections__ = ConnectionsManager()
        self.__data_metadata__ = MetadataManager()
    
    def __setitem__(self, item: _KeyType, value: T) -> None:
        self._data[item] = value 
        self.__data_connections__.trigger(item, value)
        self.__data_metadata__.touch(item)
    
    def __delitem__(self, key: _KeyType, /) -> None:
        del self._data[key]
        # shall we keep or remove connections ??
        # try:
        #     del self.__data_connections__.connections[key]
        #     del self.__data_metadata__.metadata[key]
        # except KeyError:
        #     pass

    def __getitem__(self, item: _KeyType) -> T:
        return self._data[item]
    
    def __iter__(self) -> Iterator[_KeyType]:
        return iter(self._data)
    
    def __len__(self) -> int:
        return len(self._data)

    def __repr__(self) -> str:
        return "Map("+repr(self._data)+")"

    def __str__(self) -> str:
        return "Map("+str(self._data)+")"

    def get_node(self, item: _KeyType) -> ItemNode[T]:
        return ItemNode(self, item)




  
