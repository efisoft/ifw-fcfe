from __future__ import annotations
from typing import Any, Generic, TypeVar

from ifw.fcfsim.core.node.iarray import IArray
from ifw.fcfsim.core.node.ibase import IDataNode
from ifw.fcfsim.core.node.node_handler import data_node

T = TypeVar('T', bound=IDataNode[Any])
class ArrayNodeView(Generic[T]):
    def __init__(self, array: IArray[Any]):
        self._array = array 

    def __getitem__(self, item: int| slice) -> IDataNode[Any]:
        return data_node(self._array, item) 



U = TypeVar('U', bound=Any)
class ArrayNodeViewObj(Generic[U]):
    def __init__(self, array: IArray[Any], NvClass: type):
        self._array = array 
        self._NV = NvClass

    def __getitem__(self, item: int| slice) -> U:
        return self._NV( self._array[item] )# type:ignore


