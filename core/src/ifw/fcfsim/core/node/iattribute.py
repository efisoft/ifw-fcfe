from __future__ import annotations
from typing import Protocol, runtime_checkable



@runtime_checkable
class ILinkable(Protocol):
    """Represent an object with attribute connection capabilities
    
    They are object for which the class was decorated with @attrconnect
    """
    __data_connections__: property
    __data_metadata__: property



