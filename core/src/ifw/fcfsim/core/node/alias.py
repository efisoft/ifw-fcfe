from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any, Callable, Generic, Iterable, TypeVar, cast, overload

from ifw.fcfsim.core.node.subscription import node_subscription
from ifw.fcfsim.core.node.dependency import ReferenceCallback
from ifw.fcfsim.core.node.ibase import (
        IDataEvent, IDataItem, IDataNode, ILinkCallback, 
        IMetadataElement, INodeSubscription, INodeHandler,
)
from ifw.fcfsim.core.node.base import basic_node_handler

from ifw.fcfsim.core.node.ialias import IMultySolver, ISingleSolver
from ifw.fcfsim.core.node.standalone import StandaloneNode 
from ifw.fcfsim.core.node.node_handler import data_node

## 
# TODO: SG Clean and comment this module this is a mess 
class _DummySingleAliasSolver:
    def solve(self, value: Any) -> Any:
        return value

    def unsolve(self, value: Any) -> Any:
        return value

class _DummyMultyAliasSolver:
    def solve_multy(self, *values: Any) -> tuple[Any,...]:
        return values

    def unsolve_multy(self, value: tuple[Any,...]) -> tuple[Any,...]:
        return value


T = TypeVar('T',bound=Any,  covariant=True)








@dataclass(init=False)
class MultyAliasNode(IDataNode[T]):
    """ An N to one Alias node

    It uses a tuple of reference node and a solver object to solve new
    value from the reference node and optionaly to set new value into reference
    nodes from one value
    """
    
    reference_nodes: tuple[IDataNode[Any], ...]
    """ node masters """

    solver: IMultySolver
    """ Solver to transform n value into one

    where n is the length of reference node
    The solver may also do the reverse transformation
    """

    _cnode: StandaloneNode[T]
    """ Used only to handle connections """

    def __init__(self,
         reference_nodes: Iterable[IDataNode[Any]],
         solver: IMultySolver | Callable[..., Any]
        ):
        self.reference_nodes = tuple(reference_nodes)
        self.solver = _parse_multy_solver(solver)
        self._cnode = StandaloneNode()

    def __eq__(self, right: Any) -> bool:
        try:
            solver = right.solver
            rn = right.reference_nodes
        except AttributeError:
            return False
        return self.solver is solver and self.reference_nodes == rn

    def __hash__(self) -> int:
        return hash((id(self.solver), self.reference_nodes))

    def get_event(self) -> IDataEvent:
        return self._cnode.get_event()
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback) -> None:
        refcallback = ReferenceCallback(self._cnode.get_event(), self.trigger)
        for node in self.reference_nodes:
            node.connect_callback(refcallback)
        self._cnode.connect_callback(callback)

    def disconnect_callback(self, callback: ILinkCallback) -> None:
        refcallback = ReferenceCallback(self._cnode.get_event(), self.trigger)
        for node in self.reference_nodes:
            node.connect_callback(refcallback)
        self._cnode.disconnect_callback(callback)

    def trigger(self) -> None:
        val = self.get()
        self._cnode.metadata.touch()
        self._cnode.connections.trigger(val)

    def is_connectable(self) -> bool:
        return self._cnode.is_connectable()

    def get_metadata(self) -> IMetadataElement:
        return self._cnode.get_metadata()

    def get(self, default: Any = None) -> T:
        del default # not used here 
        return self.solver.solve_multy(*(n.get() for n in self.reference_nodes)) # type:ignore[no-any-return]

    def set(self, value: Any) -> None:
        values=self.solver.unsolve_multy(value)
        # Add strict=True if python >=3.10
        for n, value in zip(self.reference_nodes, values):
            n.set(value)


    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        return basic_node_handler



U = TypeVar('U',bound=Any,  covariant=True)


@dataclass(init=False)
class SingleAliasNode(IDataNode[U]):
    """ A One to One alias node """
    reference_node: IDataNode[Any]
    solver: ISingleSolver = field(default_factory=_DummySingleAliasSolver)

    _cnode: StandaloneNode[U] = field(default_factory=StandaloneNode)
    """ Used only to handle connections """   
    def __init__(self, 
         reference_node:  IDataNode[Any], 
         solver: ISingleSolver| Callable[[Any], Any] = _DummySingleAliasSolver()
        ):
        self.reference_node = reference_node
        self.solver = _parser_scalar_solver(solver)
        self._cnode = StandaloneNode()
    
    def __eq__(self, right: Any)->bool:
        try:
            solver = right.solver
            rn = right.reference_node
        except AttributeError:
            return False 
        return self.solver is solver and self.reference_node==rn 
    
    def __hash__(self)->int:
        return hash( (id(self.solver), self.reference_node) )

    def get_event(self)->IDataEvent:
        return self._cnode.get_event() 
    
    def connect(self, func: Callable[..., Any], trigger: bool = True) -> INodeSubscription:
        return node_subscription(self, func, trigger=trigger)

    def connect_callback(self, callback: ILinkCallback)->None:
        refcallback = ReferenceCallback(self._cnode.get_event(), self.trigger) 
        self.reference_node.connect_callback(refcallback)
        self._cnode.connect_callback(callback)

    def disconnect_callback(self, callback: ILinkCallback)->None:
        refcallback = ReferenceCallback(self._cnode.get_event(), self.trigger)
        self.reference_node.connect_callback(refcallback)
        self._cnode.disconnect_callback(callback)
    
    def trigger(self)->None:
        val = self.get()
        self._cnode.metadata.touch()
        self._cnode.connections.trigger(val)
    
    def is_connectable(self)->bool:
        return self._cnode.is_connectable()
    
    def get_metadata(self) -> IMetadataElement:
        return self._cnode.get_metadata()

    def get(self, default:Any = None)->U:
        """ get related data """
        return self.solver.solve( self.reference_node.get() ) # type:ignore[no-any-return]

    def set(self, value:Any)->None:
        """ Cannot set an alias """
        self.reference_node.set(  self.solver.unsolve( value ) )
    
    def get_node_handler(self)->INodeHandler[IDataNode[Any]]:
        return basic_node_handler


V = TypeVar('V', bound=Any)

@dataclass(init=False)
class MultyAlias(Generic[V]):
    """ A Multy Alias property 

    It is intended to be an object attributes which can get and set 
    other object attributes defined by a list of items.
    The nodes associated to items are retrieve using data_node(obj, item)
    so they can be a complexe string path 'a.b[0].c' in the object tree

    example::
        
        from ifw.fcfsim.core import api as core
        import dataclasses 
        
        @core.attrconnect
        @dataclasses.dataclass
        class Vector:
            x: float = 0.0
            y: float = 0.0
            norm = core.node.MultyAlias(('x','y'), lambda x,y: abs(complex(x,y)))

        assert Vector(3,4).norm == 5.0
    
    Any connection made to norm data node will be triggered on x or y changes.


    Instead of a function, a solver can be used, it must have a solve and unsolve
    method. 
    """
    items: tuple[IDataItem, ...]
    solver: IMultySolver

    def __init__(self,
            items: Iterable[IDataItem],
            solver: Callable[..., Any] | IMultySolver):
        self.items = tuple(items)
        self.solver = _parse_multy_solver(solver)

    def make_data_node(self, obj: Any) -> MultyAliasNode[V]:
        nodes = tuple(data_node(obj, item) for item in self.items)
        return MultyAliasNode(nodes, self.solver)

    @property
    def __data_dependencies__(self) -> tuple[str, ...]:
        return cast(tuple[str],self.items) 

    @overload
    def __get__(self, parent: None, owner: type | None = None) -> MultyAlias[V]:
        ...

    @overload
    def __get__(self, parent: object, owner: type | None = None) -> V:
        ...

    def __get__(self, parent: object | None, owner: type | None = None) -> V | MultyAlias[V]:
        if parent is None:
            return self 
        return cast(V, 
                    self.solver.solve_multy(*(data_node(parent, item).get() for item in self.items))
                ) 

    def __set__(self, parent: object, value: V) -> None:
        values = self.solver.unsolve_multy(value)
        # should add strict=True in python >=3.10
        for item, value in zip(self.items, values):
            data_node(parent, item).set(value)



W = TypeVar('W', bound=Any)
@dataclass(init=False)
class SingleAlias(Generic[W]):
    item: IDataItem 
    solver: ISingleSolver = _DummySingleAliasSolver()

    def __init__(self, item: IDataItem, solver: ISingleSolver|Callable[[Any],Any] = _DummySingleAliasSolver()):
        self.item = item 
        self.solver = _parser_scalar_solver( solver )

    def make_data_node( self, obj:Any)->SingleAliasNode[W]:
        return SingleAliasNode( data_node(obj, self.item), self.solver )
    
    @property
    def __data_dependencies__(self)->tuple[str,...]:
        return (cast(str,self.item), ) 

    @overload
    def __get__(self, parent:None, owner:type|None=None)->SingleAlias[W]:
        ...
    @overload
    def __get__(self, parent:object,  owner:type|None=None)->W:
        ...
    
    def __get__(self, parent:Any, owner:type|None= None)->W|SingleAlias[W]:
        if parent is None:
            return self
        return cast(W, self.solver.solve(data_node(parent,self.item).get()))
    
    def __set__(self, parent:object, value:W)->None:
        data_node(parent, self.item).set( self.solver.unsolve(value))


@overload
def node_alias(
        unsolvefunc: Callable[[Any],Any] | None,    
        node: IDataNode[Any],
        solvefunc: Callable[[Any],Any] | None, /
) -> SingleAliasNode[Any]:
    ...

@overload
def node_alias(
        node: IDataNode[Any],
        solvefunc: Callable[[Any],Any] | None, /
) -> SingleAliasNode[Any]:
    ...

@overload
def node_alias(
        node: IDataNode[Any], /
) -> SingleAliasNode[Any]:
    ...

@overload
def node_alias(
        unsolvefunc: Callable[[Any],Any] | None,    
        node: IDataNode[Any], /
) -> SingleAliasNode[Any]:
    ...

@overload
def node_alias(
    unsolvefunc: Callable[[Any],tuple[Any,...]] | None, 
    node: Iterable[IDataNode[Any]],
    func: Callable[..., Any] | None , /
) -> MultyAliasNode[Any]:
    ...

@overload
def node_alias(
    node: Iterable[IDataNode[Any]],
    func: Callable[..., Any] | None , /
) -> MultyAliasNode[Any]:
    ...


@overload
def node_alias(
    unsolvefunc: Callable[[Any],tuple[Any,...]] | None, 
    node: Iterable[IDataNode[Any]], /
) -> MultyAliasNode[Any]:
    ...

@overload
def node_alias(
    node: Iterable[IDataNode[Any]], /
) -> MultyAliasNode[Any]:
    ...


def node_alias(*args: Any) -> IDataNode[Any]:
    """ Create a virtual node aliased to other node or nodes
    
    The alias can be a one2one representation of the original nodes(s)
    or can handle in and out transformation. 
    
    Accept one two or three arguments, for scalar node:  

         - node_alias(node)  
         - node_alias(node, func_out) where func_out is of signature func_out(origin_value) -> new_value 
            and represent how to convert value from original node 
         - node_alias(func_in, node, func_out) where func_in is of signature func_in(new_value) -> origin_value
            
    Similarly for multiple nodes :

        - node_alias(nodes) without function the in and out value is a tuple of lengh of nodes  
        - node_alias(nodes, func) where func has the signature func(v1,v2,...,vn) -> value 
        - node_alias(fun_in, nodes, func_out) where func_in has the signature func_in(value) -> (v1, v2, ..., vn)
    
    Example:
        
        from ifw.fcfsim.core import api as core 

        size_meter = core.node.StandaloneNode(2.0)
        size_mm = core.node_alias(lambda v:v/1000.0, size_meter, lambda v:v*1000)
    
        assert size_mm.get() == 2000.0
        
        size_mm.set( 3000.0 )
        assert size_meter.get() == 3.0

    """
    if len(args) == 1:
        node_or_nodes, = args
        if isinstance(node_or_nodes, Iterable):
            return MultyAliasNode(node_or_nodes,_DummyMultyAliasSolver() )
        else:
            return SingleAliasNode(node_or_nodes, _DummySingleAliasSolver())
    
    if len(args) == 2:
        func, node_or_nodes = args 
        if callable(node_or_nodes):
            node_or_nodes, func = func, node_or_nodes
            if isinstance(node_or_nodes, Iterable):
                return MultyAliasNode(node_or_nodes, _OneWayMultySolver(func))
            else:
                return SingleAliasNode(node_or_nodes, _OneWaySingleSolver(func))
        else:
            if isinstance(node_or_nodes, Iterable):
                return MultyAliasNode(node_or_nodes, _OneWayInMultySolver(func))
            else:
                return SingleAliasNode(node_or_nodes, _OneWayInSingleSolver(func))

    if len(args) == 3:
        unsolver, node_or_nodes, solver = args 
        if isinstance(node_or_nodes, Iterable):
            return MultyAliasNode( node_or_nodes, _TwoWayMultyAliasSolver(solver, unsolver))
        else:
            return SingleAliasNode( node_or_nodes, _TwoWaySingleAliasSolver(solver, unsolver))
    
    raise ValueError(f"node_alias takes 1, 2 or 3 arguments, got {len(args)}")
        


@overload
def alias(
        unsolvefunc: Callable[[Any],Any] | None,    
        item: str,
        solvefunc: Callable[[Any],Any] | None, /
) -> SingleAlias[Any]:
    ...

@overload
def alias(
        item: str,
        solvefunc: Callable[[Any],Any] | None, /
) -> SingleAlias[Any]:
    ...

@overload
def alias(
        item: str, /
) -> SingleAlias[Any]:
    ...

@overload
def alias(
        unsolvefunc: Callable[[Any],Any] | None,    
        item: str, /
) -> SingleAlias[Any]:
    ...

@overload
def alias(
    unsolvefunc: Callable[[Any],tuple[Any,...]] | None, 
    item: list[str] | tuple[str,...],
    solvefunc: Callable[..., Any] | None , /
) -> MultyAlias[Any]:
    ...

@overload
def alias(
    item: list[str] | tuple[str,...],
    solvefunc: Callable[..., Any] | None , /
) -> MultyAlias[Any]:
    ...

@overload
def alias(
    unsolvefunc: Callable[[Any],tuple[Any,...]] | None, 
    item: list[str] | tuple[str,...], /
) -> MultyAlias[Any]:
    ...

@overload
def alias(
    item: list[str] | tuple[str,...], /
) -> MultyAlias[Any]:
    ...



def alias(*args:Any) -> SingleAlias[Any] | MultyAlias[Any]:
    if len(args) == 1:
        item_or_items, = args
        if isinstance(item_or_items, str):
            return SingleAlias(item_or_items, _DummySingleAliasSolver())
        else:
            return MultyAlias(item_or_items,_DummyMultyAliasSolver() )

    if len(args) == 2:
        func, item_or_items = args 
        if callable(item_or_items):
            item_or_items, func = func, item_or_items
            if isinstance(item_or_items,str):
                return SingleAlias(item_or_items, _OneWaySingleSolver(func))
            else:
                return MultyAlias(item_or_items, _OneWayMultySolver(func))
        else:
            if isinstance(item_or_items, str):
                return SingleAlias(item_or_items, _OneWayInSingleSolver(func))
            else:
                return MultyAlias(item_or_items, _OneWayInMultySolver(func))
    if len(args) == 3:
        unsolver, item_or_items, solver = args 
        if isinstance(item_or_items, str):
            return SingleAlias(item_or_items, _TwoWaySingleAliasSolver(solver, unsolver))
        else:
            return MultyAlias(item_or_items, _TwoWayMultyAliasSolver(solver, unsolver))
    raise ValueError(f"alias takes 1, 2 or 3 arguments, got {len(args)}")
      

def node_grouped_subscription( 
      nodes: Iterable[IDataNode[Any]], 
      callback_func: Callable[...,Any], /, 
      trigger: bool = True
                           ) -> INodeSubscription:
    """Connect several node into one function 

    Contrary to node_subscription, the callback function is called with 
    N positional arguments, N being the number of input nodes
    
    Args:
        node: An iterable of data node 
        callback_func: A Callable with signature: 
            f(val1, val2, ..., valn) where n is the number of node and 
                val* the value returned by each nodes 
        trigger (optional): default is True, trigger the callback now

    Returns:
        subscription: a INodeSubscription 
            subscription handler with .disconnect() and .reconnect()
            methods.
    
    Notes: 
        The callback function is called each time one of the input node
        has been changed. So the function may be called several time in 
        a row if data nodes was changed one after the other.
    """
    alias = node_alias(nodes)
    def callback_wrapper(args: tuple[Any], event: IDataEvent) -> None:
        callback_func(*args)
    return node_subscription(alias, callback_wrapper, trigger = trigger)

        


##########################################################################
# Private 

def _parse_multy_solver(solver: Callable[..., Any] | IMultySolver) -> IMultySolver:
    """Transform a Callable in MultySolver if needed"""
    if isinstance(solver, IMultySolver):
        return solver
    elif isinstance(solver, Callable):  # type:ignore[arg-type]
        return _OneWayMultySolver(solver)
    else:
        raise ValueError("Un expected argument for multy solver")



@dataclass
class _OneWaySingleSolver:
    func: Callable[[Any], Any]
    def solve(self, value:Any)->Any:
        return self.func(value)
    def unsolve(self, value:Any)->Any:
        raise ValueError("This Alias is not reversible")

@dataclass
class _OneWayInSingleSolver:
    func: Callable[[Any], Any]
    def solve(self, value:Any)->Any:
        return value
    def unsolve(self, value:Any)->Any:
        return self.func(value)
 
@dataclass
class _TwoWaySingleAliasSolver:
    """Just wrap to function into a solver"""
    solver : Callable[[Any],Any]
    unsolver: Callable[[Any], Any]

    def solve(self, value: Any) -> Any:
        return self.solver(value)

    def unsolve(self, value: Any) -> Any:
        return self.unsolver(value)


@dataclass
class _TwoWayMultyAliasSolver:
    """Just wrap to function into a solver"""
    solver : Callable[...,Any]
    unsolver: Callable[[Any], tuple[Any,...] ]

    def solve_multy(self, *values: Any) -> Any:
        return self.solver(*values)

    def unsolve_multy(self, value: Any) -> tuple[Any,...]:
        return self.unsolver(value)


@dataclass
class _OneWayMultySolver:
    func: Callable[..., Any]
    def solve_multy(self, *values:Any)->Any:
        return self.func(*values)
    def unsolve_multy(self, value:Any)->tuple[Any,...]:
        raise ValueError("This Alias is not reversible")

@dataclass
class _OneWayInMultySolver:
    func: Callable[[Any], tuple[Any,...]]
    def solve_multy(self, *values:Any)->Any:
        return values
    def unsolve_multy(self, value:Any)->tuple[Any,...]:
        return self.func(value)


def _parser_scalar_solver(
        solver: ISingleSolver | Callable[[Any], Any] | None) -> ISingleSolver:
    if isinstance(solver, ISingleSolver):
        return solver
    elif solver is None:
        return _DummySingleAliasSolver()
    elif isinstance(solver, Callable):  # type:ignore[arg-type]
        return _OneWaySingleSolver(solver)
    else:
        raise ValueError("Un expected argument for solver")




