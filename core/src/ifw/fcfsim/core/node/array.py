from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any, Callable, Generic, Iterable, Iterator, TypeVar, overload
from ifw.fcfsim.core.node.ibase import (
        IDataNode,
        IConnectionsManager, 
        IMetadataManager,
    )
from ifw.fcfsim.core.node.base import (
    ConnectionsManager, MetadataManager
)

from ifw.fcfsim.core.node.index_node import IndexNode
from ifw.fcfsim.core.node.slice_node import FrozenSlice, SliceNode

T = TypeVar('T', bound=Any)


@dataclass(init=False)
class Array(Generic[T]):
    """ An Array with fix size, mutable item and item connection capability 
    
    It does not behave as a numpy array but more like a tuple but with 
    the ability to change item values and to connect callback method at item 
    changes.

    The callback connection can be done per item or slice of item.

    Exemple: 
        
        from ifw.fcfsim.core import Array, data_node
        a = Array( [0,0,0] ) 
        # is equivalent to
        a = Array( 3, int) 
    """
    _data: list[T]
    __data_connections__: IConnectionsManager = field(repr=False)
    __data_metadata__: IMetadataManager = field(repr=False)

    @overload
    def __init__(self, data: int, constructor: Callable[[], T]) -> None:
        ...

    @overload
    def __init__(self, data: Iterable[T]) -> None:
        ...

    def __init__(self,
                 data: Iterable[T] | int,
                 constructor: Callable[[], T] | None = None) -> None:
        if isinstance(data, int):
            if constructor is None:
                raise ValueError(
                    "expecting a type, of a item factory when first argument is int")
            self._data = [constructor() for _ in range(data)]
        else:
            if constructor is not None:
                raise ValueError(
                    "Exepcting only one argument when first argument is iterable")
            self._data = list(data)

        self.__data_connections__ = ConnectionsManager()
        self.__data_metadata__ = MetadataManager()

    @overload
    def __getitem__(self, item: slice) -> list[T]:
        ...

    @overload
    def __getitem__(self, item: int) -> T:
        ...

    def __getitem__(self, item: int | slice) -> list[T] | T:
        if isinstance(item, (slice, int)):
            return self._data[item]
        else:
            raise TypeError(
                f"Array indice must be integers of slices, not {type(item)}")

    @overload
    def __setitem__(self, item: slice, value: Iterable[T]) -> None:
        ...

    @overload
    def __setitem__(self, item: int, value: T) -> None:
        ...

    def __setitem__(self, item: slice | int, value: T | Iterable[T]) -> None:
        if isinstance(item, slice):
            self._data[item] = value
            indexes = range(len(self))[item]
            for i in indexes:
                self.__setitem__(i, self._data[i])
            self.__data_connections__.trigger(FrozenSlice(item), value)

        elif isinstance(item, int):
            #Should be catched by overload ?
            self._data[item] = value # type:ignore 
            self.__data_connections__.trigger(item, value)
            self.__data_metadata__.touch(item)
        else:
            raise TypeError(
                f"Array indice must be integers of slices, not {type(item)}")
    
    @classmethod
    def Factory(
            cls: type[Array[T]],  
            size: int, 
            constructor: Callable[[], T]
                ) -> Callable[[],Array[T]]:
        def make_new_array() -> Array[T]:
            return cls(size, constructor)
        return make_new_array

    @overload
    def get_node(self, item: int) -> IndexNode[T]:
        ...

    @overload
    def get_node(self, item: slice) -> SliceNode[list[T]]:
        ...

    def get_node(self, item: int | slice) -> IndexNode[T] | SliceNode[list[T]]:
        if isinstance(item, slice):
            return SliceNode(self, item)
        else:
            return IndexNode(self, item)

    def get_index_nodes(self, idx: slice | Iterable[int]) -> list[IDataNode[T]]:
        if isinstance(idx, slice):
            idx = range(len(self._data))[idx]
        return [self.get_node(i) for i in idx]

    def count(self, value: T) -> int:
        return self._data.count(value)

    def __iter__(self) -> Iterator[T]:
        return self._data.__iter__()

    def __len__(self) -> int:
        return len(self._data)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self._data!r})"

    






