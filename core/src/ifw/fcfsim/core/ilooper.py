from __future__ import annotations
from typing import Awaitable, Callable, Protocol


class ILooper(Protocol):
    async def run(self, 
            afunc: Callable[ [], Awaitable[None]], 
            is_alive: Callable[ [], bool] = lambda :True
        )->int: 
        """ run the asynchrone function in a loop """
        ...


