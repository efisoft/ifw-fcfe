from __future__ import annotations
from typing import Protocol

class ITimer(Protocol):
    """ A Timer helps to compute execution time """
    def tick(self)->None:
        """ tic before code logic """
        ...
    def tock(self)->None:
        """ tac when code finished """
        ...
    def sleep(self, period:float)->bool:
        """ Sleep the necessary amount of time 

        Return True is the tock-tick time exceed a limit (overload)
        """
        ...
    async def asleep(self, period:float)->bool:
        """ Sleep asynchroniously the necessary amount of time 
        
        Return True is the tocc-tick time exceed a limit (overload) 
        """
        ...


