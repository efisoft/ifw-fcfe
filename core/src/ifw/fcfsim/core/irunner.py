from __future__ import annotations
from typing import Protocol

# The initialision step to runnuer is volontary separated
# So when one has a list of runner, the initialisation can be done first, 
# the programm can be interupted there before starting some async tasks


class IRunner(Protocol):
    """ A runner is devided in 2 async steps: initialise and run (in loop) 
    
    The run method is running some task, most likely in a loop until some condition 
    are met to exit. 
    """

    async def run(self) -> int:
        """ Run something and return an exit code """
        ...

    async def initialise(self) -> None:
        """ Initialise anything needed to the run method """
        ...

    def stop(self) -> None:
        """ Stop the runner if it supports it """
        ...

    def is_alive(self) -> bool:
        """ Send True if the runner loop is running """
        ...

    def is_ready(self) -> bool:
        """ Send True if the task runs nominally

        e.g. for instance when a client connection is done
        """
        ...
