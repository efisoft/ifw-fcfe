""" Toolbox to handle logs 

Also used as an interface for logging system 
"""
from __future__ import annotations
import argparse
from dataclasses import dataclass
import logging 
import sys
from typing import Any, Protocol, cast

from ifw.fcfsim.core.config import ConfigLoader
from ifw.fcfsim.core.iconfig import IConfigFactory
from ifw.fcfsim.core.ilog import ILogConfig, ILogger

LOG_FORMAT = (
    "%(asctime)s.%(msecs)03d:%(levelname)s:%(name)s:%(threadName)s:"
    + "%(module)s:%(lineno)d:%(funcName)s: %(message)s"
)

class ILogCmdArgs(Protocol):
    """ expected vars comming from command line args parser """
    log_level: str 
    loggers: str 
    verbose: bool  
    list_loggers: list[str]


def get_log_argument_parser(
        arg_parser: argparse.ArgumentParser | None = None
    ) -> argparse.ArgumentParser: 
    """ Return an argument parser for application with log

        Args:
           arg_parser (optional): an existing argument parser for which elements 
               will be added. If None a fresh one is created. 

        Returns:
           arg_parser: arg_parser input if given or a new one 
    """

    if arg_parser is None:
        arg_parser = argparse.ArgumentParser(description="Log")
     
    arg_parser.add_argument(
        "--log-level",
        choices=("CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"),
        help="set log level",
        default="WARNING",
    )
    arg_parser.add_argument(
        "--loggers",
        help="set loggers on which to apply log level (\"<logger1>[,<logger2>...]\")",
        default="ifw.core.opcua"
    )
    arg_parser.add_argument(
        "--list-loggers",
        action="store_true",
        help="list loggers defined and exit",
        default=False
    )
    arg_parser.add_argument(
         "--verbose",
         action="store_true",
         help="output log on stdout",
         default=False
    )
    return arg_parser

def init_config_logger(config: ILogConfig) -> None:
    """ Init loggers as defined inside config """
    init_loggers(config.log, config.log_level, config.log_verbose)

@dataclass
class LogConfigLoader(ConfigLoader):
    """ Class to update a LogConfig object from argv and/or config files"""
    def load_argv(self, config: Any,  argv:list[str])->None:
        """ Update configuration from an argv list 

        Warning: This method can trigger a SystemExit(0) (e.g. in case of --help)
        """
        args_parser = get_log_argument_parser()
        if "-h" in argv or "--help" in argv:
            args_parser.print_help()
            raise SystemExit(0)

        args = cast(ILogCmdArgs, args_parser.parse_args(argv)) 
        self.load_args(config, args)

    def load_args(self, config: Any, args: ILogCmdArgs)->None:
        """ Update configuration from an argument structure (e.i. parsed argvs) """
        if args.list_loggers:
            print_existing_loggers()
            raise SystemExit(0)
        init_loggers(args.loggers, log_level=args.log_level, verbose=args.verbose)

        config.log_level = args.log_level 
        config.log_verbose = args.verbose
        init_config_logger(config)



@dataclass
class LogConfig:
    """A Base Config class for Logging purposes"""
    
    log: str = "ifw"
    """Logger name"""
    
    log_level: str = "WARNING"
    """Logger Level"""

    log_verbose: bool = False
    """True for verbose"""

    def init_logger(self, 
            logs: str | logging.Logger | list[logging.Logger] | None = None) -> None:
        """Init the logger defined in config 
        
        Alternatively an optional list of logger can be set
        """
        if logs is None:
            logs = self.log
        init_loggers(logs, log_level=self.log_level, verbose = self.log_verbose)

    def validate_config(self) -> None:
        self.init_logger()

def get_logger(loggger_name: str) -> ILogger:
    """ Return the logger """
    return logging.getLogger(loggger_name)


def print_existing_loggers() -> None:
    """ Just print existing loggers in stdout """
    print(format_existing_loggers())


def format_existing_loggers() -> str:
    """ Format existing loggers in a string """
    texts = ["", "Loggers defined:"]
    texts.extend(str(logger) for logger in logging.root.manager.loggerDict)
    texts.append("")
    return "\n".join(texts)


def init_loggers(
    logs: str | logging.Logger | list[ILogger],
    log_level: str | None = None,
    verbose: bool = True
) -> None:
    """ Init some loggers  
    
    Args:
        logs: log name(s) separated by " " or "," or a logger, or a list of logger
        log_level: valid log level name DEBUG, INFO, WARNING, ERROR
        verbose: if True (default) make the logger console verbose  
    """
    log_list = _parse_log(logs)

    if verbose:
        _set_loggers_verbose(log_list)
    if log_level is not None:
        _set_loggers_level(log_list, log_level)


def _parse_log(logs: str | logging.Logger | list[logging.Logger]) -> list[logging.Logger]:
    if isinstance(logs, str):
        log_str_list: list[str] = logs.replace(" ", ",").split(",")
        return [get_logger(lname.strip()) for lname in log_str_list if lname.strip()]
    if isinstance(logs, logging.Logger):
        return [logs]
    else:
        return logs


def _set_loggers_verbose(loggers: list[logging.Logger]) -> None:
    """ Consol logging for the given logger names """
    for logger_obj in loggers:
        stream_handler = logging.StreamHandler(stream=sys.stdout)
        formatter = logging.Formatter(LOG_FORMAT, datefmt="%Y-%m-%dT%H:%M:%S")
        stream_handler.setFormatter(formatter)
        logger_obj.addHandler(stream_handler)


def _set_loggers_level(loggers: list[logging.Logger], log_level: str) -> None:
    """ set the log level for several loggers identified by name """
    try:
        log_level = getattr(logging, log_level)
    except AttributeError:
        raise ValueError(f"Unknown log level {log_level}")
    for logger in loggers:
        logger.setLevel(logging.getLevelName(log_level))



