from __future__ import annotations
from enum import Enum, IntEnum
from typing import Any


def get_enum_text(member: Any) -> str:
    """ Get the defined text of an enumerator member 

    If the `text` if not defined the enumerator member name 
    is returned. 
    The text mean to be a short description (e.g. error description)
    """
    try:
        return member.text  # type:ignore
    except AttributeError:
        pass
    try:
        return member.name  # type:ignore[no-any-return]
    except AttributeError:
        return f"E?_{member}"


def get_error_text(code: int | IntEnum) -> str:
    """ Get the defined text of an IntEnum enumerator member 

    If the `text` if not defined the enumerator member name 
    is returned or 'UNKNOW ERROR' if this is a int 
    """
    try:
        return code.text  # type:ignore
    except AttributeError:
        pass
    try:
        return code.name  # type:ignore[union-attr]
    except AttributeError:
        return f"UNKOWN ERROR({code})"


def set_enum_text(member: Enum, text: str) -> None:
    """ Set the `text` attribute to an Enumerator member """
    member.text = text  # type: ignore


def get_enum_name(member: Any) -> str:
    """ Get the defined name of an enumerator member  

    If the `name` 'UNKNOWN' is returned

    This can actually work with any object with ``name`` attribute
    """
    try:
        return member.name  # type:ignore[no-any-return]
    except AttributeError:
        return "UNKNOWN"


def split_path(path: str) -> tuple[str, str]:
    """ Split a dotted path into (root,name) tuple

    e.g. :  split_path('a.b.c') == ('a.b', 'c') 
    """
    *root, elem = path.split(".")
    return ".".join(root), elem


def join_path(*paths: str) -> str:
    """ join non-empty strings by dots 

    e.g.: join_path('a','','b')  == 'a.b'
    """
    return ".".join(p.strip() for p in paths if p.strip())


def get_code_and_text(
    enumerator: Any,  # could not found better typing for enumerator
    code: int,
    text: str | None = None
) -> tuple[int, str]:
    """ Handy function returning resolving an error text from Int enumerator 

    - if text is None:
        
        - if code is not in enumerator return 'Unknown: {code}'
        - if code is in enumerator, looks for enum text attribute or return 
            enum name 

    - if text is a string, return as it is 
    """
    try:
       code = enumerator(code)
    except ValueError:
        text = f'Unknown: {code}' if text is None else text
    else:
        text = get_enum_text(code)
    return code, text
