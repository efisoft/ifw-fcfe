from __future__ import annotations
from typing import Any,  Protocol,  TypeVar

U = TypeVar('U', bound=Any, covariant=True)

class IIoReader(Protocol[U]):
    """A data reader based on file path or link"""

    def read(self, filename: str) -> U:
        """Load the targeted file and read appropriate content"""
        ...
