from __future__ import annotations
from typing import Any, Callable, Iterator, Protocol, TypeVar
from typing_extensions import TypeAlias


IParser: TypeAlias = Callable[[str,Any], Iterator[tuple[str,Any]]]
ICfgParser: TypeAlias = Callable[[dict[str,Any]], Iterator[tuple[str,Any]]] 

T = TypeVar('T', covariant=True)

class IConfigFactory(Protocol[T]):
    """Used to build a configuration object"""

    def from_cii_config(self, filename: str, **overrides: Any) -> T:
        """Make config from a yaml/Cii config file"""
        ...

    def from_argv(self, argv: list[str]) -> T:
        """Make config from an argv list"""
        ...

    def from_dict(self, values: dict[str,Any]) ->T:
        """Make a new config from dict extracted from cfg file"""
        ...
 
    def new(self) -> T:
        """Make a new config"""
        ...

TC = TypeVar('TC', bound=object, contravariant=True)
TF = TypeVar('TF', bound=object)

class IConfigLoader(Protocol[TC]):
    """Used to update a config structure from various sources"""

    def load_dict(self, config: TC, values: dict[str, Any]) -> None:
        """Load a dictionary comming from a config file"""
        ...

    def load_cii_config(self, config: TC, filename: str, **overrides: Any) -> None:
        """Update config from a yaml/Cii config file"""
        ...

    def load_argv(self, config: TC, argv: list[str]) -> None:
        """Load config from an argv list"""
        ...

    def new_factory(self, Config: Callable[[],TF]) -> IConfigFactory[TF]:
        ...


