from __future__ import annotations
from dataclasses import dataclass
import traceback
from types import TracebackType
from typing import Literal

from ifw.fcfsim.core.isession import ISession
from ifw.fcfsim.core.runner import run_in_thread
from ifw.fcfsim.core.irunner import IRunner


@dataclass
class Session: # conform to ISession 
    """ Holding a runner and method to interact with it
    
    It comes with methods to execute runner/stop/check the runner. 
    The runner can be executed within a context manager:: 

    with my_session:
        engine.do_something()

    """
    runner: IRunner
    """Runner. can be a GroupRunner() to execute several in the same go"""
    
    initialise: bool = True  

    def get_runner(self) -> IRunner:
        return self.runner
    
    async def run(self) -> int:
        await self.runner.initialise()
        return await self.runner.run() 
     
    def is_alive(self) -> bool:
        return self.runner.is_alive()
    
    def is_ready(self) -> bool:
        return self.runner.is_ready()

    def stop(self) -> None:
        self.runner.stop() 
    
    def start(self) -> None:
        run_in_thread(self.runner, initialise=self.initialise)


    def __enter__(self) -> ISession:
        if not self.is_alive():
            self.start()
        return self 
    
    def __exit__(self,
          exc_type: type[BaseException] | None,
          exc_val: BaseException | None,
          exc_tb: TracebackType | None
    ) -> Literal[False]:
        del exc_val # not used
        self.stop()
        if exc_type is not None:
            traceback.print_tb(exc_tb)
        return False


