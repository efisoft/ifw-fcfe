from __future__ import annotations
from ifw.fcfsim.core.itask import BaseTask, ITask


class TaskGroup(BaseTask):
    """Group several Task into one"""
    tasks: tuple[ITask, ...]

    def __init__(self, *tasks: ITask):
        self.tasks = tasks

    async def initialise(self) -> None:
        for task in self.tasks:
            await task.initialise()

    async def start(self) -> None:
        for task in self.tasks:
            await task.start()

    async def end(self) -> None:
        for task in reversed(self.tasks):
            await task.end()

    async def next(self) -> None:
        for task in self.tasks:
            await task.next()
