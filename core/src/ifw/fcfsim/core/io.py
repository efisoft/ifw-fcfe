from __future__ import annotations
from dataclasses import dataclass
import io
from typing import Any, Callable, Generic, TypeVar
import elt.configng
import ifw.core.utils.utils
import yaml #type: ignore # type: ignore[unused-ignore] # yaml is not typed 

def _to_python_object(root: Any) -> Any:
    if hasattr(type(root), "get_nr_iterator"):
        return {e.get_name(): _to_python_object(e) for e in root.get_nr_iterator()}

    kind = root.get_kind()
    if kind == elt.configng.CiiConfigNodeKind.MAP:
        return root.as_map()
    if kind == elt.configng.CiiConfigNodeKind.SCALAR:
        return root.get_value()
    if kind == elt.configng.CiiConfigNodeKind.SEQUENCE:
        return root.as_list()
    return None


def _dummy_checker(cfg: elt.configng.CiiConfigDocument) -> None:
    """Dummy config checker for the loader 

    Does not actually check anything 
    """
    del cfg
    return

def _dummy_parser(obj: Any) -> Any:
    return obj 

U = TypeVar('U', bound=Any)


@dataclass
class CiiConfigReader(Generic[U]):
    """Generic object to read a Cii ConfigNg file"""

    prefix: str | None = None
    """Prefix path, where to found the root object inside config

    If None the first item found is taken
    If '' the document root is the returned root 
    Otherwise a string path can be provided e.g. 'server.endpoint'
    """

    checker: Callable[[elt.configng.CiiConfigDocument], None] = _dummy_checker
    """A function to first check integrity of the document"""
    
    parser: Callable[[Any], U] = _dummy_parser
    """Parser for the returned object"""

    def read(self, filename: str) -> U:
        """ Load the Cii file and extract python object """
        cfg = elt.configng.CiiConfigClient.load(filename)
        self.checker(cfg)

        namespace = cfg.get_instances()
        if self.prefix is None:
            # teke the first member in config
            root = next(namespace.get_nr_iterator())
        elif not self.prefix:
            root = namespace
        else:
            root = namespace
            for p in self.prefix.split("."):
                root = root[p]

        obj: U = _to_python_object(root)
        return self.parser(obj)



def load_cii_config(filename: str, prefix: str | None = None) -> Any:
    """ load data in config file at given prefix path name 

    The config file shall be relative to one of the 
    path known by CII (see CiiConfigClient). 

    If prefix is "" the root document will be returned
    If prefix is None the first object found in document 
    is returned. 
    If prefix is e.g. 'a.b' the root['a']['b'] element is 
    returned 
    """
    return CiiConfigReader(prefix).read(filename)


V = TypeVar('V', bound=Any)

@dataclass
class YamlReader(Generic[V]):
    prefix: str  = ""
    """Prefix path, where to found the root object inside yaml

    If '' the document root is the returned root 
    Otherwise a string path can be provided e.g. 'server.endpoint'
    """
    parser: Callable[[Any], V] = _dummy_parser
    """Parser for the returned object"""
    
    def read(self, filename: str) -> V:
        file = ifw.core.utils.utils.find_file(filename)
        with open(file) as f:
            return self.read_yaml(f)

    def read_yaml(self, yaml_string: str|io.TextIOBase) -> V:
        """ read object from string or File Stream """
        obj = yaml.load(yaml_string, yaml.CBaseLoader)
        if self.prefix:
            for p in self.prefix.split("."):
                obj = obj[p]
        if not hasattr(obj, "items"):
            raise ValueError("Expecting a mapping endpoint,"
                            f" got a {type(obj)}")
        return self.parser(obj) 

