from __future__ import annotations

import time
from typing import Any, Callable, Iterable, Protocol, runtime_checkable
from ifw.fcfsim.core.node.api import pull_nodes, IDataNode, INodeHandler
from typing_extensions import TypeAlias
from ifw.fcfsim.core.isignals import ISignals
from ifw.fcfsim.core.signals import get_global_signals
from ifw.fcfsim.core.timer import Timer

CheckFuncs: TypeAlias = Callable[[], bool]

@runtime_checkable
class _Gettable(Protocol):
    def get(self) -> Any:
        ...
    def get_node_handler(self) -> INodeHandler[IDataNode[Any]]:
        ...


def _make_lag(lag_time: float) -> CheckFuncs:
    start_time = time.time()
    def check_lag() -> bool:
        return (time.time() - start_time) > lag_time 
    return check_lag

def _make_lag_with_func(lag_time: float, check_func: CheckFuncs) -> CheckFuncs:
    start_time = time.time()
    def check_lag() -> bool:
        if (time.time() - start_time) > lag_time:
            return check_func()
        return False
    return check_lag

def lag(lag_time, check_func: CheckFuncs | None = None) -> CheckFuncs:
    """ Return a function wich will return True when the time_lag is passed 
    
    If an second argument is given this should be a function returning a 
    bolean which will be queried and returned after the lag.

    Example:
         
        from ifw.fcfsim.core.wait import wait, lag
        wait( lag(2) ) # will wait approx 2 seconds  
        wait( lag(2, lag(4)) ) # will wait approx 4 seconds 

    """
    if check_func is None:
        return _make_lag(lag_time)
    else:
        return _make_lag_with_func(lag_time, check_func)

def aggregate(*checks: CheckFuncs | IDataNode[Any], operand: str = "all") -> CheckFuncs:
    """Merge several function (or data nodes) returning a boolean into one 
    
    Args:
       \\*checks: function f()->bool of IDataNode[bool]
       operand (Optional): Operand for all condition, default is "all"
            "all" -> all function result is true 
            "none" -> no result  is True, all False 
            "any" -> one of the result is True 
            "any_false" -> one of the result is False 

    Returns:
       checks: combined function
    """
    methods, nodes = _parse_check_methods(checks)
    opf = _get_operand_methods(operand)
    def check() -> bool:
        return opf(methods, nodes)
    return check

def wait(
    *checks: CheckFuncs | IDataNode[Any],
    period: float = 0.1,
    timeout: float = 60,
    signals: ISignals | None = None,
    lag: float = 0.0,
    operand: str = "all"
) -> None:
    """Wait for some condition to be True

    Wait accepts several function of signature f() -> bool  
    It does also accept IDataNode object, in this case the get 
    function is taken. 

    Args:
        \\*checks: functions or data nodes 
        period (Optional) : cycle period in second for each call and 
              node pulling 
        timeout (Optional): TimeoutError raised after timeout in second
        signals (Optional): Liten to signal to end the wait 
        lag (Optional): minimum amount of time before returning 
        operand (Optional): Operand for all condition, default is "all"
            "all" -> Wait until all is true 
            "none" -> Wait until nothing is True 
            "any" -> Wait until one of the condition is True 
            "any_false" -> Wait until one of the condition is False 
    """
    opf = _get_operand_methods(operand)
    methods, nodes = _parse_check_methods(checks)
    if signals is None:
        signals = get_global_signals()

    timer = Timer()
    if lag > 0.0:
        methods.insert(0, _make_lag(lag))
    start_time = time.time()
    while True:
        timer.tick()
        if opf(methods, nodes):
            break

        timer.tock()
        timer.sleep(period)
        if signals.must_exit():
            break
        if (time.time() - start_time) > timeout:
            raise TimeoutError(f"Timeout after {timeout} seconds")


def _parse_check_methods(
        checks: Iterable[CheckFuncs  | IDataNode[Any]], 
) -> tuple[list[CheckFuncs], list[IDataNode[Any]]]:
    functions = []
    nodes = []
    for check in checks:
        if isinstance(check, _Gettable):
            nodes.append(check)
        else:
            functions.append(check)
    return functions, nodes

def _get_operand_methods(
    operand: str
) -> Callable[[Iterable[CheckFuncs], Iterable[IDataNode[Any]]], bool]:
    try:
        return _logic_loockup[operand]
    except KeyError:
        raise ValueError(f"{operand!r} is not a valid argument for operand"
                         " expecting: 'all', 'none', 'any' of 'any_false'")




def _all_true(
        functions: Iterable[CheckFuncs], 
        nodes: Iterable[IDataNode[Any]]) -> bool:
    """ all_true(lst) -> return True if all function  list return True """
    for func in functions:
        if not func():
            return False
    return all(pull_nodes(nodes))

def _all_false(
        functions: Iterable[CheckFuncs], 
        nodes: Iterable[IDataNode[Any]]) -> bool:
    """ all_false(lst) -> return True if all function in the list return False """

    for func in functions:
        if func():
            return False
    return not any(pull_nodes(nodes).values())
    
def _any_true(
        functions: Iterable[CheckFuncs], 
        nodes: Iterable[IDataNode[Any]]) -> bool:
    """ any_true(lst) -> return True if one of the function in the list return True """
    for func in functions:
        if func():
            return True        
    return any(pull_nodes(nodes).values())

def _any_false(
        functions: Iterable[CheckFuncs], 
        nodes: Iterable[IDataNode[Any]]) -> bool:
    """ any_false(lst) -> return True if one of the function in the list return False """
    for func in functions:
        if not func():
            return True
    return not all(pull_nodes(nodes).values())


_logic_loockup = {
    "all"  : _all_true,
    "none": _all_false,
    "any"  : _any_true,
    "any_false" : _any_false,
}



      
