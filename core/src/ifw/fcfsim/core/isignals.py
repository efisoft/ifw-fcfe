from __future__ import annotations
from typing import Protocol

class ISignals(Protocol):
    """ A Signals is the central space to handle app exit signal """
    def must_exit(self)->bool:
        """ A flag to exit application """
        ...
    def exit(self, code:int)->None:
        """ Command to send an exit code """
        ...
    def get_exit_code(self)->int:
        """ Return the last exit code """
        ...
    def reset(self)->None:
        """ Reset internal signals state """
        ...
