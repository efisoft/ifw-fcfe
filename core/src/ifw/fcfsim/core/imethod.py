from __future__ import annotations

from typing import Any, Callable, Protocol, TypeVar, runtime_checkable


T = TypeVar('T', bound=Callable[...,Any], covariant=True)

@runtime_checkable
class IMethodNode(Protocol[T]):
    """Hold the definition of a method or function
    
    This is not intended to be used at runtime but only to 
    abstract where the method is located and eventually overide it
    """
    def get_method(self) -> T:
        ...

    def overide_method(self, method: Callable[...,Any], prefix:str = ...) -> None:
        ...

    def __hash__(self) -> int:
        ...
    
    def __eq__(self, right: Any) -> bool:
        ...

