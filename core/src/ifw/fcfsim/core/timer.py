from __future__ import annotations
import time
import asyncio 
from dataclasses import dataclass, field

from ifw.fcfsim.core.log import get_logger

log = get_logger("devsim")


@dataclass
class Timer:
    """ Timing utility to run something with a constant period 

    Exemple::

        timer = Timer()
        timer.tick()
        ... # logic bisness 
        timer.tock()
        overloaded = timer.sleep(0.1) # or await timer.asleep(0.1)
        if overloaded:
            log.warning("Cycle overload")

    """

    _tick: float = field(default_factory=time.time, init=False)
    _tock: float = field(default_factory=time.time, init=False)

    def tick(self) -> None:
        self._tick = time.time()

    def tock(self) -> None:
        self._tock = time.time()

    def sleep(self, period: float) -> bool:
        """ sleep for the necessary amount of time 
        
        period is in second
        Must be called after a tic and a tac 
        return True if execution time (tock-tick) overload period 
        """
        delta = self._tock - self._tick
        time.sleep(max(period - delta, 0.0))
        return delta > period

    async def asleep(self, period: float) -> bool:
        """ sleep for the necessary amount of time 
        
        period is in second
        Must be called after a tic and a tac
        return True if execution time (tock-tick) overload period 
        """
        delta = self._tock - self._tick
        await asyncio.sleep(max(period - delta, 0.0))
        return delta > period
