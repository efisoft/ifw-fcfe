from __future__ import annotations
from dataclasses import dataclass, field
from typing import  Awaitable, Callable

from ifw.fcfsim.core.exceptions import StopTask
from ifw.fcfsim.core.ilog import ILogger
from ifw.fcfsim.core.isignals import ISignals
from ifw.fcfsim.core.signals import get_global_signals 
from ifw.fcfsim.core.timer import Timer
from ifw.fcfsim.core.log import get_logger
from ifw.fcfsim.core.itimer import ITimer

log = get_logger("devsim")


@dataclass
class Looper:
    """ Handy object for cycle looping 
    
    Execute async method in a loop at constant rate   
    """
    period: float = 1.0
    """Cycle period"""

    signals: ISignals = field(default_factory=get_global_signals)
    """Interup signals to listen"""

    log: ILogger = field(default_factory=lambda: log)
    """ log instance """

    id: str = ""
    """Loop id for log purposes"""

    Timer: Callable[[], ITimer] = Timer
    """Timer type or timer factory to handle cycle time"""

    async def run(self,
            afunc: Callable[[], Awaitable[None]],
            is_alive: Callable[[], bool] = lambda: True
                  ) -> int:
        """Run an asynchrone function periodically"""
        timer = self.Timer()
        signals = self.signals
        overload_counter: int = 0
        while is_alive():
            if signals.must_exit():
                break

            timer.tick()
            try:
                await afunc()
            except StopTask as exp:
                return exp.exit_code
            timer.tock()

            overload = await timer.asleep(self.period)
            if overload:
                overload_counter += overload
                self.log.info(
                    f"Loop {self.id!r}: Cycle overloaded #{overload_counter}")
        return signals.get_exit_code()
