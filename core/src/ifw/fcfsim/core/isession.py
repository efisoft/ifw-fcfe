from __future__ import annotations
from types import TracebackType
from typing import Any, Protocol, TypeVar

from ifw.fcfsim.core.irunner import IRunner

T = TypeVar('T', bound=Any)

class ISession(Protocol):
    """ Helping class to simplify execution of engine's runners """
    
    def get_runner(self) -> IRunner:
        ...
    
    async def run(self) -> int:
        ...
       
    def is_alive(self) -> bool:
        ...
    
    def is_ready(self) -> bool:
        ...

    def stop(self) -> None:
        ...
    
    def start(self) -> None:
        ...

    def __enter__(self) -> ISession:
        ...
    
    def __exit__(self, 
          exc_type: type[BaseException] | None,
          exc_val: BaseException | None,
          exc_tb: TracebackType | None
        ) -> bool:
        ...



