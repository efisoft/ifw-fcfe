from __future__ import annotations
import asyncio
from dataclasses import dataclass, field
import threading
import traceback
from types import TracebackType
from typing import Any, Generic, Literal, TypeVar
from ifw.fcfsim.core.ilog import ILogger
from ifw.fcfsim.core.log import get_logger
from ifw.fcfsim.core.irunner import  IRunner
from ifw.fcfsim.core.itask import  ITask
from ifw.fcfsim.core.ilooper import ILooper
from ifw.fcfsim.core.looper import Looper
from ifw.fcfsim.core.wait import wait

@dataclass
class TaskRunner:
    """ Object to init and run a task in a loop 
    
    Conform to :class:`ifw.fcfsim.core.IRunner` 

    Args:
        task: Task to be initialised and run 
        looper: Looper to run the task. 
            Default is a :class:`ifw.fcfsim.core.Looper` of 1 second period
        log: logging instance
    """
    task: ITask 
    looper: ILooper = field(default_factory= lambda :Looper(1.0))
    log: ILogger = get_logger('task runner')
    
    _alive_flag: bool = field(default=False, init=False)
    _started_flag: bool = field(default=False, init=False) 
    async def initialise(self) -> None:
        self._started_flag = False
        return await self.task.initialise() 
    
    async def run(self)->int:
        self._alive_flag = False 
        self._started_flag = False
        await self.task.start()
        self._started_flag = True
        nextf, is_alive = self.task.next, self.is_alive 
        self._alive_flag = True 

        try:
            exit_code = await self.looper.run(nextf, is_alive)
        except BaseException as exception:
            await self.task.end()
            self.log.error(exception, exc_info=True)
            exit_code = 1
        else:
            self.log.info(f"Runner finished normaly with exit_code {exit_code}")
            await self.task.end()
        self._alive_flag = False 
        self._started_flag = False
        return exit_code             
    
    def is_alive(self) -> bool:
        return self._alive_flag
    
    def is_ready(self) -> bool:
        return self._started_flag 

    def stop(self)->None:
        self._alive_flag = False 
        self._started_flag = False

@dataclass(init=False)
class RunnerGroup:
    runners: list[IRunner] 
    
    def __init__(self, *runners: IRunner):
        self.runners = list(runners) 

    async def initialise(self) -> None:
        for runner in self.runners:
            await runner.initialise()
    
    async def run(self) -> int:
        # run all in asyncio tasks Maybe use TaskGroup if python > 3.11  
        tasks = [asyncio.create_task(runner.run()) for runner in self.runners] 
        for task in tasks:
            await task   
        return max(task.result() for task in tasks)
    
    def add(self, *runners: IRunner) -> None:
        """ Add one or several runners """
        self.runners.extend(runners)

    def stop(self) -> None:
        for runner in self.runners:
            runner.stop()

    def is_alive(self) -> bool:
        # True if one of the returner is still alive
        for runner in self.runners:
            if runner.is_alive():
                return True
        return False
    
    def is_ready(self) -> bool:
        # True if all runners are available
        return all( r.is_ready() for r in self.runners)

    
def run_in_thread(
        runner: IRunner, 
        initialise: bool=True, 
    )->threading.Thread:
    """ Execute a runner inside a Thread 
    
    The Thread object is started and then returned. 

    Args: 
        runner: runner to execute 
        initialise: default is True, initialisation is done before running
    """
    if initialise:
        async def afunc() -> int:
            await runner.initialise()
            return await runner.run(  )
        thread = threading.Thread(target=asyncio.run, args=(afunc(),))
    else:
        thread = threading.Thread(target=asyncio.run, args=(runner.run(), ))
    
    thread.start()
    return thread



if __name__ == "__main__":
    # Protocol static type checking  
    import typing 
    if typing.TYPE_CHECKING:
        _: type[IRunner] = TaskRunner
