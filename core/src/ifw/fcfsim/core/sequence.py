
from __future__ import annotations

from typing import Callable, Iterable

from ifw.fcfsim.core.isignals import ISignals
from ifw.fcfsim.core.wait import wait


def sequence(
    *func_nodes: Callable[[], Callable[[], bool]] | Iterable[Callable[[], Callable[[], bool]]],
    period: float = 0.1,
    timeout: float = 60,
    signals: ISignals | None = None,
    lag: float = 0.0,
    operand: str = "all"
) -> None:
    """Execute fonction or a group of function in parallel 
    
    This is used mostly in client application. 

    Each function must return a function used to check if the given 
    action has been done (return True for action done). It is expected 
    that each command change things on server side and returns quickly 
    before the action is finished. 
    """
    def wait_func(*f: Callable[[],bool]) -> None:
        wait(*f, period=period, timeout=timeout, signals=signals, lag=lag, operand=operand)
        
    for node in func_nodes:
        if callable(node):
            wait_func(node())
        else:
            wait_func(*(f() for f in node))
