""" Tools related to Configuration 

- A ConfigLoader is an object with several method to transfer user 
    config information into a python config object 
- A ConfigFactory creates Configuration object from various sources 
    (e.g. cii configng, argument var list, ... ) 

"""
from __future__ import annotations

from dataclasses import dataclass, field
from enum import Enum
from typing import Any, Callable, Generic, Iterator, TypeVar

from ifw.fcfsim.core.iconfig import IConfigFactory, IConfigLoader
from ifw.fcfsim.core.io import load_cii_config


class Extras(str, Enum):
    """ Enumerator of extras option for config loader """
    
    allow = "allow"
    """Directive to allow unknown attribute to be set"""
    
    forbid = "forbid"
    """Directive to forbid any unknown attribute -> Error"""
    
    ignore = "ignore"
    """Directive to ignore silently unknown attributes"""


T = TypeVar('T', bound=object)

def parse_values(__values__: dict[str, Any], **parsers: Callable[[Any],Any] ) -> Iterator[tuple[str,Any]]:
    """Helper to parse a user dictionary with a set of functions 
    
    If dictionary has a a key defined inside parsers it is poped from 
    values, parsed and a new key, value pair is yielded  
    """
    for key, parser in parsers.items():
        if key in __values__:
            yield key, parser(__values__.pop(key))

@dataclass 
class ConfigLoader:
    cfg_prefix: str | None = None
    """Prefix name pointing on config root 
        
        e.g. `a.b.c` means that cfg['a']['b']['c'] is taken as the
        root config dictionary. 
        If None, this will be the first child found
        If '' the yaml root is the file root
    """

    extras: Extras = Extras.allow
    """Rules to apply from incoming key/value pairs not in config 
    
        can be: 
            - "allow", default, 
                any file entries is set in
                the config data object 
            - "ignore", extra attribute are not set in the data
            - "forbid", 
                an error is raised if a keyword in the file 
                does not correspond to attribute in coonfig structure
    """
    
    parsers: dict[str, Callable[[Any], Any]] = field(default_factory=dict)
    """A dictionary of value parsers 

    The key is as found in the user config space and is also the targeted 
    config object attribute. 

    For more complex parsing one should Inherit this class and rewrite the 
    dispatch method.
    """

    def load_cii_config(self, config: Any, filename: str, **overrides: Any) -> None:
        """Update config from a yaml/Cii config file"""
        cfg = load_cii_config(filename, self.cfg_prefix)
        override_cfg(cfg, overrides)
        self.load_dict(config, cfg)
     
    def dispatch(self, values: dict[str, Any]) -> Iterator[tuple[str,Any]]:
        """Parse a dictionary of User configng into iterator of name,value pairs 
        
        The name is the parameter name in the config space 
        """
        for key, parser in self.parsers.items():
            if key in values:
                yield key, parser(values.pop(key))
        # by default all other values are attribute of config 
        for attr, value in values.items():
            yield (attr, value)
     
    def get_config_setter(self) -> Callable[[object, str, Any], None]:
        """Return a function suitable to set new value into config object"""
        return _get_setter(self.extras)

    def load_dict(self, config: Any, values: dict[str, Any] ) -> None:
        """Update config from a dictionary or kwargs"""
        setter = self.get_config_setter()
        
        for attr, value in self.dispatch(values.copy()):
            setter(config, attr, value)

    def load_argv(self, config: Any, argv: list[str]) -> None:
        """Update config from an command argument string list"""
        del config, argv
        raise NotImplementedError('load_argv')
    
    
    def new_factory(self, Config: Callable[[],T]) -> IConfigFactory[T]:
        """ Return a Config factory with the given config type 

        Args:
            Config: a Config class or a function returning a config instance
        """
        return ConfigFactory(Config, self)

 
@dataclass
class ConfigFactory(Generic[T]):
    """Factory to instanciate a config structure object"""

    Config: Callable[[], T]
    """Configuration factory 

    Mostlikely a class but can be a factory function
    """
    
    loader: IConfigLoader[T]
    """ ConfigLoader used to fill the config object """
    
    no_validation: bool = False
    """ If True No config validation is performed afer creating config """    

    def new(self) -> T:
        """Return a new config object """
        return self.Config()

    def from_dict(self, values: dict[str,Any]) ->T:
        """ Create a new config from dictionary comming from config file"""
        config = self.Config()
        self.loader.load_dict(config, values)
        self.validate(config)
        return config

    def from_cii_config(self, filename: str, **overrides: Any) -> T:
        """ Create a new config structure from Cii """
        config = self.Config()
        self.loader.load_cii_config(config, filename, **overrides)
        self.validate(config)
        return config

    def from_argv(self, argv: list[str], **overrides: Any) -> T:
        """ Create a new config structure from system argv 

        The parsing of argv must be implemented inside the loader 
        """
        config = self.Config()
        self.loader.load_argv(config, argv, **overrides)
        self.validate(config)
        return config
    
    def validate(self, config: object) -> None:
        """Validate the config object"""
        # By default we simply look for a validate_config method 
        if self.no_validation: 
            return 
        try:
            validator = config.validate_config # type: ignore
        except AttributeError:
            return 
        else:
            validator()


def override_cfg(cfg: dict[str,Any], overrides: dict[str,Any]) -> None:
    """ Update a dictionary of str/value from an overrides dictionary 

    Overrides dictionary can have complex key representing a path into 
    ``cfg``. for instance:: 

           a key 'a.b.c' will target the cfg['a']['b']['c'] value 
           'a' is sinply cfg['a']
    """
    for path, value in overrides.items():
        try:
            data, key = _walk_dict(cfg, path)
        except (ValueError, KeyError) as er:
            raise ValueError(f"Cannot override key {path!r} in config dictionary") from er
        else:
            data[key] = value

#######################################################################


def _get_setter(extras: Extras | str) -> Callable[[object, str, Any], None]:
    """ return a setter function according to extras option """
    if extras == Extras.allow:
        return setattr
    if extras == Extras.forbid:
        return _setattr_forbiden
    if extras == Extras.ignore:
        return _setattr_ignored
    raise ValueError(f"wrong value for extras expecting a {Extras}"
                     " got a {type(extras)} ")


def _setattr_forbiden(obj: object, attr: str, value: Any) -> None:
    """ Setting new  attribute is forbiden """
    try:
        getattr(obj, attr)
    except AttributeError:
        raise ValueError(f"Attribute {attr!r} does not exist in config"
                         " cannot set when extras is 'forbid'")
    else:
        setattr(obj, attr, value)


def _setattr_ignored(obj: object, attr: str, value: Any) -> None:
    """ Setting new attribute is ignore if the attribute does not exists """
    try:
        getattr(obj, attr)
    except AttributeError:
        pass
    else:
        setattr(obj, attr, value)


def _walk_dict(data: dict[str,Any], path: str) -> tuple[dict[str,Any], str]:
    *keys, last_key = [k for k in path.split(".") if k]
    for key in keys:
        data = data[key]
    return data, last_key 
