from __future__ import annotations
from dataclasses import dataclass, field
import signal
from types import TracebackType
from typing import Iterable, Literal
import traceback
from ifw.fcfsim.core.isignals import ISignals


# The Global App Signal will listen to these signals by default
DEFAULT_GLOBAL_SIGNALS = (signal.SIGTERM, signal.SIGINT)

@dataclass(init=False)
class AppSignals:
    """ An application signal to use across tasks runners, threads 
    
    Threaded applications must monitor the must_exit() method of a signals 
    object. 

    By default SIGTERM and SIGINT is monitored and trigger a
    must_exit() -> True

    Signal can be used in context manager, at exit of the context it will 
    send an exit(0) or exit(1) in case of error. 

    To share a global signal accross applications one should use the 
    get_global_signals() function 

    Note that signals is intended to end applications threads properly 
    if a signals.exit(code) has been send a signals.reset() is necessary 
    to restars the thread if needed. 
    """
    exit_flag: bool = False 
    exit_code: int  = 0 
    
    def __init__(self, sig_connections: Iterable[int] = DEFAULT_GLOBAL_SIGNALS) -> None:
        # link system signals to this Signal object
        def signal_received(signum: int, _: None | object = None) -> None:
            self.exit(signum)
        for sig in sig_connections:
            signal.signal(sig, signal_received)

    def must_exit(self) -> bool:
        """ Return True if application should exist 
        This is used in looped task to break 
        """
        return self.exit_flag

    def exit(self, code: int) -> None:
        """ Tells who ever is listening this signals with must_exit() to exit """
        self.exit_flag = True
        self.exit_code = code

    def get_exit_code(self) -> int:
        """ Return the last exit code triggered at exit """
        return self.exit_code

    def reset(self) -> None:
        """ Reset the signals to 0"""
        self.exit_code = 0
        self.exit_flag = False

    def __enter__(self) -> AppSignals:
        return self

    def __exit__(self,
          exc_type: type[BaseException] | None,
          exc_val: BaseException | None,
          exc_tb: TracebackType | None
                 ) -> Literal[False]:
        del exc_val
        if exc_type is None:
            self.exit(0)
        else:
            traceback.print_tb(exc_tb)
            self.exit(1)
        return False

@dataclass
class SlaveSignals:
    """ A Signal slave will listen to its hown exit directive and a master signal 

    This is usefull to end a sub-application but still listen to
    the master application.
    """
    master: ISignals
    exit_flag: bool = field(default=False, init=False)
    exit_code: int = field(default=0, init=False)

    def must_exit(self) -> bool:
        """ Return True if application should exit 

        This is used in looped task to break 
        """
        return self.exit_flag or self.master.must_exit()

    def exit(self, code: int) -> None:
        """ Tells who ever is listening this signals with must_exit() to exit """
        self.exit_flag = True
        self.exit_code = code

    def get_exit_code(self) -> int:
        """ Return the last exit code triggered at exit """
        return self.exit_code or self.master.get_exit_code()

    def reset(self) -> None:
        self.exit_code = 0
        self.exit_flag = False

    def __enter__(self) -> SlaveSignals:
        return self

    def __exit__(self,
          exc_type: type[BaseException] | None,
          exc_val: BaseException | None,
          exc_tb: TracebackType | None
                 ) -> Literal[False]:
        del exc_val
        if exc_type is None:
            self.exit(0)
        else:
            traceback.print_tb(exc_tb)
            self.exit(1)
        return False


__global_signals__: ISignals | None = None


def get_global_signals() -> ISignals:
    """ Get the default signals object 

    By default this is an AppSignals which will respond to 
    SIGTERM and SIGINIT
    """
    global __global_signals__
    if __global_signals__ is None:
        __global_signals__ = AppSignals()
    return __global_signals__


def set_global_signals(signals: ISignals) -> None:
    """ set a Signal as global (returned by get_global_signals) """
    global __global_signals__
    __global_signals__ = signals


def new_slave_signals() -> ISignals:
    """ Retrun a slave signal of the global_signal

    It will listen to its own exit directives And also the global signals
    """
    return SlaveSignals(get_global_signals())
