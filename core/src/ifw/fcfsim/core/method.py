from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable

from ifw.fcfsim.core.node.api import resolve_data_path
from ifw.fcfsim.core.imethod import IMethodNode



@dataclass
class FuncNode:
    """Hold a function. Cannot be altered"""
    func: Callable[...,Any]
    
    def get_method(self) -> Callable[...,Any]:
        return self.func

    def overide_method(self, method: Callable[...,Any], prefix:str = "") -> None:
        raise ValueError("This method has no parent, thefore cannot be changed")
    
    def __hash__(self) -> int:
        return hash(self.func)

    def __eq__(self, right: Any) -> bool:
        try:
            return self.func is right.func 
        except Exception:
            return False

@dataclass
class MethodNode:
    """Hold amethod of an object"""
    obj: object 
    attr: str 

    def get_method(self) -> Callable[...,Any]:
        return getattr(self.obj, self.attr) # type:ignore[no-any-return] # we should assume it is a callable

    def overide_method(self, method: Callable[..., Any], prefix:str = "") -> None:
        # TODO check consistancy with existing method ?
        # the overided method shall be of the same signature
        setattr(self.obj, prefix+self.attr, method)

    def __hash__(self) -> int:
        return hash((id(self.obj), self.attr))

    def __eq__(self, right: Any) -> bool:
        try:
            return bool(self.obj == right.obj and self.attr == right.attr)
        except Exception:
            return False

@dataclass
class ListedFuncNode:
    """Hold amethod of an object"""
    obj: list[Callable[..., Any]]
    index: int 

    def get_method(self) -> Callable[...,Any]:
        return self.obj[self.index] 

    def overide_method(self, method: Callable[..., Any], prefix:str = "") -> None:
        # TODO check consistancy with existing method ?
        # the overided method shall be of the same signature
        self.obj[self.index] = method

    def __hash__(self) -> int:
        return hash((id(self.obj), self.index))

    def __eq__(self, right: Any) -> bool:
        try:
            return bool(self.obj == right.obj and self.index == right.index)
        except Exception:
            return False



@dataclass 
class GlobalFuncNode:
    """Hold a global function by its name"""
    name: str 

    def get_method(self) -> Callable[...,Any]:
        return globals()[self.name]  # type:ignore[no-any-return] # we should assume it is a callable
    
    def overide_method(self, method: Callable[..., Any], prefix:str = "") -> None:
        globals()[prefix+self.name] = method
    
    def __hash__(self) -> int:
        return hash(self.name)

    def __eq__(self, right: Any) -> bool:
        if isinstance( right, GlobalFuncNode):
            return self.name == right.name
        return False


def method_node(obj: object | Callable[..., Any], path: str | None) -> IMethodNode[Callable[...,Any]]:
    if path is not None:
        obj, item = resolve_data_path(obj, path)
    else:
        if callable(obj):
            return FuncNode(obj)
        else:
            raise ValueError(
                'If path is None, expecting a Callable at first argument')

    if isinstance(item, str):
        return MethodNode(obj, item)
    else:
        raise ValueError(f"Invalid method path: {path}")



