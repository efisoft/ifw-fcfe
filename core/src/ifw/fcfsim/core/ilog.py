from __future__ import annotations
from typing import Protocol
import logging
from typing_extensions import TypeAlias 

ILogger: TypeAlias = logging.Logger

class ILogConfig(Protocol):
    """Interface for Logging initialisator"""
    log: str 
    log_level: str 
    log_verbose: bool



## Place holder for a logging system 
# Make it compatible with the logging.Logger 
# class ILogger(Protocol):
#     level: int 
#     name: str 

#     def error(self,msg:str, *arg:object,  exc_info:logging._ExcInfoType = None, stack_info: bool = False, stacklevel: int = 1, extra: Mapping[str, object] | None = None)->None:
#         ...
#     def warning(self,msg:str, *arg:object, **kwargs:Any)->None:
#         ...
#     def info(self,msg:str, *arg:object, **kwargs:Any)->None:
#         ...
#     def debug(self,msg:str, *arg:object, **kwargs:Any)->None:
#         ...
#     def log(self, level:int, msg:str, *arg:object, **kwargs:Any)->None:
#         ...

# if __name__ == "__main__":
#     import logging 
#     logging._ExcInfoType
#     l: ILogger = logging.Logger('test')


