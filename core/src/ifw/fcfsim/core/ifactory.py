from __future__ import annotations
from typing import Any, Callable, Iterator, Protocol, TypeVar
from typing_extensions import TypeAlias


IParser: TypeAlias = Callable[[str,Any], Iterator[tuple[str,Any]]]
ICfgParser: TypeAlias = Callable[[dict[str,Any]], Iterator[tuple[str,Any]]] 

T = TypeVar('T', covariant=True)

class IFactory(Protocol[T]):
    """A generic way to to build an object"""

    def from_cii_config(self, filename: str, **overrides: Any) -> T:
        """Make config from a yaml/Cii config file"""
        ...

    def from_argv(self, argv: list[str]) -> T:
        """Make config from an argv list"""
        ...

    def from_dict(self, values: dict[str,Any]) ->T:
        """Make a new config from dict. Probably extracted from cfg file"""
        ...
 
    def new(self) -> T:
        """Make a new config"""
        ...

