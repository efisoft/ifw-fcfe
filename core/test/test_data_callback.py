


from ifw.fcfsim.core.api import Array, callback, data_node
import pytest


def test_callback_wrapper():
    
    d: dict[str,int|None] = {'val':None}
    
    def noarg():
        d['val'] = 999

    def onearg(value):
        d['val'] = value 
    
    def twoarg(value, event):
        d['val'] = value 
        d['index'] = event.index
    
    def threearg(value, event, other):
        ...

    a = Array(3, int)
    
    data_node(a,0).connect( callback(onearg) )
    a[0] = 1 
    assert d['val'] == 1
    
    data_node(a,1).connect( callback(twoarg) )
    a[1] = 2 
    assert d['val'] == 2
    assert d['index'] == 1


    data_node(a,2).connect( callback(noarg) )
    a[2] = 3
    assert d['val'] == 999
    
    with pytest.raises( ValueError):
        callback( threearg )

    callback( print )  
