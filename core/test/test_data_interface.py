from ifw.fcfsim.core.api import ILinkable, attrconnect


def test_if_object_is_linkable():
    @attrconnect
    class C:
        x: float = 0.0 
    assert isinstance( C(), ILinkable)
