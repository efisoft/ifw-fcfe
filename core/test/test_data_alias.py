from __future__ import annotations
from dataclasses import dataclass, field
from functools import partial
from typing import Any, NamedTuple
from ifw.fcfsim.core.node.alias import SingleAliasNode
from ifw.fcfsim.core.node.api import (
        IDataEvent, attrconnect, data_node,
        MultyAlias, MultyAliasNode,  Array, SingleAlias, StandaloneNode, alias, callback, dependencies, node_alias
    )
import pytest
import math 

def test_item_alias() -> None:


    @attrconnect
    @dataclass
    class Data:
        x: int = 0 
        
        @property
        @dependencies("x")
        def val(self)->int:
            return self.x
        
        @val.setter
        def val(self,v:int) -> None:
            self.x = v
    
    data = Data()

    assert data.x == 0 
    assert data.val == 0
    data.x =10 
    assert data.x == 10 
    assert data.val == 10
    data.val = 20 
    assert data.x == 20 
    assert data.val == 20 
    
    d = {'val':None}
    def con(val, event):
        d['val'] = val 
    dn = data_node( data, "val")
    dn.connect(con)
    data.val = 40
    assert d['val'] == 40 
    data.x = 50 
    assert d['val'] == 50
    assert data.val == 50 

def test_comp_alias():
    @attrconnect
    @dataclass
    class Data:
        x: int = 1 
        y: int = 2
        # total = Alias(lambda *a: sum(a), "x y")
        
        @property
        @dependencies("x", "y")
        def total(self)->int:
            return self.x+self.y

    data = Data() 
    
    assert data.total == 3
    d = {'val':None}
    def con(val, event):
        d['val'] = val 
    dn = data_node( data, "total")
    dn.connect( con ) 
    data.y = 3
    assert data.total == 4
    assert d['val'] == 4

_n_callback_call = 0 
def test_alias_unique_ref_link():
    
    @attrconnect
    @dataclass
    class Data:
        x: int = 1 
        y: int = 2
        # total = Alias(lambda *a: sum(a), "x y")
        
        @property
        @dependencies("x", "y")
        def total(self)->int:
            return self.x+self.y
    
    def callback(val:Any, event:IDataEvent):
        global _n_callback_call
        _n_callback_call += 1 
    def callback2(val:Any, event:IDataEvent):
        pass  
    d = Data()
    data_node( d, "total").connect(callback)
    data_node( d, "total").connect(callback2)
    d.x = 2 
    assert _n_callback_call == 1
    d.y = 4
    assert _n_callback_call == 2


def test_alias_setting():

    @attrconnect
    @dataclass
    class Data:
        l_m: float = 1.0  
        
        @property
        @dependencies("l")
        def l_km(self)->float:
            return self.l_m / 1000 
        @l_km.setter
        def l_km(self, value:float)->None:
            self.l_m = value * 1000.0

    data = Data()
    data_node( data, "l_km").set( 2 )

    assert data.l_m ==  2000 
    d = {'val1':None, 'val2':None}
    def con1(val, event):
        d['val1'] = val 
    def con2(val, event):
        d['val2'] = val
    data_node( data, "l_km").connect( con1 )
    data_node( data, "l_m").connect( con2 )
    data.l_km = 5
    assert d['val1'] == 5 
    assert d['val2'] == 5000



def test_standalone_alias():

    node = StandaloneNode(99)
    na = node_alias( node ) 

    assert na.get() == 99
    na.set( 1 ) 
    assert node.get() == 1

    na = node_alias( node, lambda x:x*100)
    node.set( 1) 
    assert na.get() == 100
    with pytest.raises(ValueError):
        na.set( 1) 

    class Meter2Km:
        @staticmethod 
        def solve(value)->Any:
            return value /1000.
        @staticmethod
        def unsolve(value)->tuple[Any,...]:
            return value*1000
     
    node.set( 1000.0 )
    na = SingleAliasNode(node, Meter2Km)
    assert na.get() == 1.0  
    na.set( 2 )
    assert node.get() == 2000


def test_alias_property_scalar():
    
    @dataclass
    class Scaler:
        s: float 
        def solve(self, value:float)->float:
            return value * self.s 
        def unsolve(self, value:float)->float:
            return value / self.s 
    

    @dataclass
    class Printer:
        f: str = "{}" 
        def solve(self, value:float)->str:
            return self.f.format( value )
        def unsolve(self, value:float)->None:
            raise ValueError("Read Only Alias")


    @attrconnect
    @dataclass
    class Data:
        x: float = 1.0 
        
        # z: float = property( fget=lambda s: s.x)
        y = alias("x")
        xm = SingleAlias("y", Scaler(1e3))
        p = SingleAlias( "x", Printer("{:2E}"))
    data = Data()
    assert data.y == 1.0
    assert data.xm == 1000.0
    data.xm = 3000.0 
    assert data.x == 3.0
    

    
    d = {}
    data_node(data, "xm").connect( callback( lambda v:d.__setitem__('val',v)))
    data_node(data, "x").connect( callback( lambda v:d.__setitem__('valx',v)))

    data.x = 5.0 
    assert d['val'] == 5000.0
    assert d['valx'] == 5.0
    data.xm = 6000.0 
    assert d['val'] == 6000.0 
    assert d['valx'] == 6.0

    s: str = data.p 

def test_alias_property_multy():
    class D:
        real: float = 3.0 
        imag: float = 4.0
        
        c1 =  alias(("real", "imag"), complex)
        c2 =  alias( lambda x:(x.real, x.imag),   ("real", "imag"), complex)

        @property 
        @dependencies("real", "imag")
        def c3(self) -> complex:
            return complex(self.real, self.imag)
        @c3.setter
        def c3(self, value: complex) -> None:
            self.real = value.real 
            self.imag = value.imag

    d = D() 
    assert d.c1 == 3.0 + 4.0j
    assert d.c2 == 3.0 + 4.0j 
    d.c2 = 1.0j
    assert d.real == 0.0 
    assert d.imag == 1.0
    
    d.c3 = 10+11j
    assert d.real == 10.0 
    assert d.imag == 11.0


def test_nested_alias():
    
    class Cart2Polar1:
        def solve_multy( self,x, y, *_)->tuple[float,float]:
            # x,y = cart
            return math.sqrt(x*x+y*y) , math.atan2(y,x)
        def unsolve_multy(self, polar: tuple[float, float])->tuple[float, float]:
            r, t = polar 
            return r*math.cos(t) , r*math.sin(t)
    
    class Cart2Polar2:
        def solve( self, c: Coordinate)->tuple[float,float]:
            x,y = c.x, c.y 
            return math.sqrt(x*x+y*y) , math.atan2(y,x)
        def unsolve(self, polar: tuple[float, float])->Coordinate:
            r, t = polar 
            return Coordinate( r*math.cos(t) , r*math.sin(t))
   

    @attrconnect
    @dataclass
    class Coordinate:
        x: float = 3.0 
        y: float = 4.0
        
        mag = SingleAlias[float](None, lambda c: math.sqrt(c.x*c.x+c.y*c.y))
        polar = MultyAlias[tuple[float,float]](("x", "y"), Cart2Polar1()) 



    @attrconnect
    @dataclass
    class Data:
        label: str = ""
        c: Coordinate = field(default_factory= Coordinate)
        a: Array[float] = field(default_factory= lambda : Array( [3,4] ) )
        mag = alias( ("c.x", "c.y"), lambda x,y: math.sqrt(x*x+y*y))
        polar = SingleAlias[tuple[float,float]]( "c", Cart2Polar2()) 
        @property
        @dependencies("c.x", "c.y")
        def mag2(self):
            return math.sqrt( self.c.x**2 + self.c.y**2)
        mag3 = alias( ("a[0]", "a[1]"), lambda x,y: math.sqrt(x*x+y*y))
    

    data = Data()
    assert data.mag == 5.0
    assert data.mag2 == 5.0 
    assert data.mag3 == 5.0 
    assert data.c.mag == 5.0 
    assert data.c.polar == (5.0, math.atan2(4.0,3.0) )
    assert data.polar == (5.0, math.atan2(4.0,3.0) )


    d = {}
    def con(val,e):
        d['val'] = val 
    def con2(val,e):
        d['val2'] = val
    def con3(val,e):
        d['val3'] = val

    data_node( data, "mag").connect( con )
    data_node( data, "mag2").connect( con2 )
    data_node( data, "mag3").connect( con3 )

    data.c.x = 0.0 
    assert d['val'] == 4.0 
    assert d['val2'] == 4.0 
    
    data.a[0] = 0.0 
    assert d['val3'] == 4.0 
    
    data.c.polar = (10.0, 0.0) 
    assert data.c.x == 10.0 
    assert data.c.y == 0.0

def test_obj_alias():
    class Cart(NamedTuple):
        x: float 
        y: float 
    
    class Polar(NamedTuple):
        r: float 
        theta: float 

    class Cart2Polar:
        def solve( self, cart: Cart)->Polar:
            x,y = cart
            return Polar( math.sqrt(x*x+y*y) , math.atan2(y,x) )
        def unsolve(self, polar: Polar)->Cart:
            r, t = polar 
            return Cart(r*math.cos(t) , r*math.sin(t))

    @attrconnect
    @dataclass
    class Data:
        cart: Cart = Cart(3,4)
        polar = SingleAlias[Polar]("cart", Cart2Polar())
    
    data = Data() 
    assert data.polar.r == 5.0
    assert data.polar.theta == math.atan2(4,3)
    
    d = {}
    def con1(value):
        d['cart'] = value
    def con2(value):
        d['polar'] = value 
    
    data_node(data, "cart").connect( callback(con1))
    data_node(data, "polar").connect( callback(con2))
    data.cart = Cart(10., 0.0)
    assert d['cart'] == (10., 0.0)
    assert d['polar'] == (10.0, 0.0)


def test_node_alias_func_scalar():
    
    s = StandaloneNode(100)

    n = node_alias(s) 
    assert n.get() == 100 
    n.set(200) 
    assert n.get() == 200 
    assert s.get() == 200 

    #########################
    s = StandaloneNode(100)
    n = node_alias(s, lambda x:x+10)
    assert n.get() == 110
    with pytest.raises(ValueError):
        n.set(30)


    #########################
    s = StandaloneNode(100)
    n = node_alias(lambda x: x-10,  s, lambda x:x+10)
    assert n.get() == 110
    n.set(200)
    assert n.get() == 200 
    assert s.get() == 190
    

    #########################
    s = StandaloneNode(100)
    n = node_alias(lambda x: int(x),  s)
    assert n.get() == 100
    n.set("200")
    assert n.get() == 200 
    assert s.get() == 200



def test_node_alias_func_multy():
    
    s1 = StandaloneNode(3)
    s2 = StandaloneNode(4)
    n = node_alias((s1,s2))
    assert n.get() == (3,4)
    n.set((1,2))
    assert s1.get() == 1 
    assert s2.get() == 2

    #########################

    s1 = StandaloneNode(3)
    s2 = StandaloneNode(4)
    
    n = node_alias((s1,s2), complex)  
    assert n.get() == 3+4j
    with pytest.raises(ValueError):
        n.set(1j)
    
    #########################
    s1 = StandaloneNode(3)
    s2 = StandaloneNode(4)
    
    n = node_alias( lambda c:(c.real, c.imag), (s1,s2), complex)
    assert n.get() == 3+4j
    n.set( 2j )
    assert s1.get() == 0 
    assert s2.get() == 2



