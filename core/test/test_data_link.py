
from ifw.fcfsim.core.node.api import ( Lock, Suspend, attrconnect, is_linkable,  resolve_data_path,  data_node)

import pytest 

target = {}


def test_data_path():
    class B:
        l = [0,1,2,3]
        l2 = [[11,12,13], [21,22,23]]
    class A:
        b = B 
        lb = [B,B]

    assert resolve_data_path( A, "b") == (A, "b")
    assert resolve_data_path( A, "b.l") == (A.b , "l")
    assert resolve_data_path( A, "b.l[0]") == (A.b.l, 0)
    assert resolve_data_path( A, "b.l2[0][1]") == (A.b.l2[0], 1)
    assert resolve_data_path( A, "lb[0].l") == ( A.lb[0], "l")  
    assert resolve_data_path( A, "lb[0].l[0]") == ( A.lb[0].l, 0)
    with pytest.raises( ValueError ):
        resolve_data_path(  A, "A.b.l[toto]")

    with pytest.raises( ValueError ):
        resolve_data_path(  A, "A.b.l2[10]")

    assert resolve_data_path( A, "lb[0].l[0:2]") == ( A.lb[0].l, slice(0,2) ) 
    assert resolve_data_path( A, "lb[0].l[:2]") == ( A.lb[0].l, slice(None,2) )
    assert resolve_data_path( A, "lb[0].l[:]") == ( A.lb[0].l, slice(None,None) )
    assert resolve_data_path( A, "lb[0].l[::2]") == ( A.lb[0].l, slice(None,None,2) )
    assert resolve_data_path( A, "lb[0].l[1:2:3]") == ( A.lb[0].l, slice(1,2,3) )
@attrconnect
class Data:
    x: float = 0.0
    y: float = 0.0 
    
    def __setattr__(self, attr, value):
        target["_"+attr] = value 
        super().__setattr__(attr, value)

def connect(data,attr):
    def si(val, e):
        target[attr] = val
    data_node( data, attr).connect(si)


def test_is_linkable():
    assert is_linkable( Data()) 

def test_attribute_is_linked():
    target.clear()
    data = Data()
    


    connect( data, 'x')
    data.x = 10.0
    assert data.x == 10.0
    assert target['x'] == 10.0 

def test_not_linked():
    target.clear()
    data = Data()

    # y is not connected 
    data.y = 20.0
    assert data.y == 20.0
    assert target.get('y',0.0) == 0.0 
    

def test_parent_setattr_is_conserved():
    target.clear()
    data = Data()
    
    data.y = 20.0
    assert target['_y'] == 20.0

    connect( data, 'x')
    data.x = 10.0
    assert target['x'] == 10.0 
    assert target['_x'] == 10.0 


def test_suspended_connection():
    target.clear()
    data = Data()
    connect( data, 'x')
    connect( data, 'y')

    data.x = 10
    data.y = 5 
    assert target['x'] == 10
    assert target['y'] == 5
    with Suspend(data):
        data.x = 100
        data.y = 50
        assert target['x'] == 10
        assert target['y'] == 5
    assert target['x'] == 100
    assert target['y'] == 50

def test_locked_connection():
    target.clear()
    data = Data()
    connect( data, 'x')
    connect( data, 'y')

    data.x = 10
    data.y = 5 
    assert target['x'] == 10
    assert target['y'] == 5
    with Lock(data):
        data.x = 100
        data.y = 50
        assert target['x'] == 10
        assert target['y'] == 5
    assert target['x'] == 10
    assert target['y'] == 5

