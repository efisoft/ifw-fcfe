
from __future__ import annotations
import time 
import functools
from ifw.fcfsim import api as dvs
from ifw.fcfsim.api import sm
from ifw.fcfsim.devices.lamp import define as dfn 
from ifw.fcfsim.devices.lamp.gen.iengine import ILampSimEngine, ILampBl 
from ifw.fcfsim.devices.lamp.config import LampSimConfig

# ----------------------------------------------
# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.LampState, dfn.LampSubstate
lamp_state_mapping: dict[str,tuple[dfn.LampState, dfn.LampSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready::Off": (S.NOTOP, SS.NOTOP_READY_OFF), 
  "On::NotOperational::Ready::On": (S.NOTOP, SS.NOTOP_READY_ON), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::Operational::Disabling": (S.OP, SS.OP_DISABLING), 
  "On::Operational::Off": (S.OP, SS.OP_OFF), 
  "On::Operational::SwitchingOff": (S.OP, SS.OP_SWITCHING_OFF), 
  "On::Operational::Cooling": (S.OP, SS.OP_COOLING), 
  "On::Operational::On": (S.OP, SS.OP_ON), 
  "On::Operational::SwitchingOn": (S.OP, SS.OP_SWITCHING_ON), 
  "On::Operational::Warming": (S.OP, SS.OP_WARMING), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
  "On::NotOperational::Initialising": (S.OP, SS.NONE), 
  "On::NotOperational::Ready": (S.NOTOP, SS.NOTOP_INITIALISING), 
}
del S, SS
# ----------------------------------------------


class LampBl(ILampBl):
    # This class must implement all abstract methods of ILampBl

    def __init__(self, engine: ILampSimEngine) -> None:
        self.engine = engine
     
    # ===================== Data Model ===========================
    
    @functools.cached_property
    def cfg(self) -> dfn.LampCfg: #pyright:ignore 
        return dfn.LampCfg()

    @functools.cached_property
    def ctrl(self) -> dfn.LampCtrl: #pyright:ignore 
        return dfn.LampCtrl()

    @functools.cached_property
    def info(self) -> dfn.LampInfo: #pyright:ignore
        return dfn.LampInfo()

    @functools.cached_property
    def stat(self) -> dfn.LampStat: #pyright:ignore
        return dfn.LampStat()

    # ===================== shortcuts ============================

    @property
    def config(self) -> LampSimConfig: 
        return self.engine.get_config()
    
    @property
    def log(self) -> dvs.core.ILogger: 
        return self.engine.log

    # ========================= Helpers  ==============================

    def ok(self)->dfn.LampRpcError:
        """ reset rpc error and return OK code (should be 0) """
        self.set_rpc_error( dfn.LampRpcError.OK)
        return dfn.LampRpcError.OK
    
    def error(self,  err_code:dfn.LampRpcError, err_text:str|None=None)->dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error( err_code, err_text)
        return dvs.RpcException( self.stat.rpc_error_text, self.stat.rpc_error_code) 
        

    def set_error(self,  err_code:int, err_text:str="")->None:
        """ set error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text( dfn.LampError, err_code, err_text)
        self.stat.error_code = code
        self.stat.error_text = text
    
    def set_rpc_error(self,  err_code:int, err_text:str|None=None)->None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text( dfn.LampRpcError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text

    def check_local(self)->None:
        """ Check if in Local mode, raise an RpcException if in local """
        # The RpcException is catched properly by the server and will return the error code 
        if self.stat.local:
            raise self.error( dfn.LampRpcError.LOCAL )  
    
    def set_last_command(self,  command: dfn.LampCommand)->None:
        """ Set the command (code & text) in engine stat """
        self.stat.last_command = command
        self.stat.last_command_text = command.name # Check if it has last_command_text ?
    
    def set_command(self, command: dfn.LampCommand)->None:
        """ Set the command in engine ctrl and fill stat accordingly """
        self.ctrl.command = command 
        self.ctrl.execute = True 
        self.set_last_command(command)
    
    def set_state(self, state: dfn.LampState, substate: dfn.LampSubstate) -> None:
        stat = self.stat
        stat.state = int(state) 
        stat.state_text =  dvs.core.get_enum_name(state)
        stat.substate = int(substate) 
        stat.substate_text = dvs.core.get_enum_name(substate)   
    
    def schedule(self, event_name:str, description: str = "") -> None:
        self.engine.schedule_event(sm.Event(event_name), description)

   
    #========================= RPC Methods Business Logic  =========================

    def reset(self)->int:
        """ reset Lamp """
        self.check_local()
        self.set_command( dfn.LampCommand.RESET )
        self.stat.check_time_left = False
        self.stat.time_left = 0
        return self.ok()

    def on(self, intensity: float, time_on_limit: int) -> int:
        """ on Lamp """
        self.check_local()
        stat, ctrl = self.stat, self.ctrl
        if stat.state != dfn.LampState.OP:
            raise self.error(dfn.LampRpcError.NOT_OP)
        if stat.substate == dfn.LampSubstate.OP_COOLING:
            raise self.error(dfn.LampRpcError.COOLING)
        if stat.substate == dfn.LampSubstate.OP_SWITCHING_OFF:
            raise self.error(dfn.LampRpcError.SWITCHING_OFF)

        self.log.debug(
            f" On command: intensity={intensity}, time_on_limit={time_on_limit}"
        )
        ctrl.intensity = intensity
        ctrl.time_on_limit = int(time_on_limit)
        self.set_command(dfn.LampCommand.ON)
        return self.ok()

    def off(self) -> int:
        """ off Lamp """
        self.check_local()
        stat= self.stat 
        if stat.state != dfn.LampState.OP:
            raise self.error(dfn.LampRpcError.NOT_OP)
        
        if stat.substate ==dfn.LampSubstate.OP_SWITCHING_ON:
            raise self.error(dfn.LampRpcError.SWITCHING_ON)
        stat.check_time_left = False 
        stat.time_left = 0 
        self.set_command(dfn.LampCommand.OFF)
        return self.ok() 

    def stop(self) -> int:
        """ stop Lamp """
        self.check_local()
        self.set_command(dfn.LampCommand.STOP)
        return self.ok()

    def set_log(self, log: bool) -> int:
        """ set_log Lamp """
        self.engine.log.error("set_log received but not yet implemented")
        return 0

    def disable(self) -> int:
        """ disable Lamp """
        self.check_local()
        stat = self.stat
        if stat.state != dfn.LampState.OP:
            raise self.error(dfn.LampRpcError.NOT_OP)

        self.set_command(dfn.LampCommand.DISABLE)
        return self.ok()

    def enable(self) -> int:
        """ enable Lamp """
        self.check_local()
        stat = self.stat
        if stat.state != dfn.LampState.NOTOP:
            raise self.error(dfn.LampRpcError.NOT_NOTOP_READY)

        self.set_command(dfn.LampCommand.ENABLE)
        return self.ok()

    def init(self) -> int:
        """ init Lamp """
        self.check_local()
        stat = self.stat
        if stat.substate not in [
        	dfn.LampSubstate.NOTOP_NOTREADY,
        	dfn.LampSubstate.NOTOP_ERROR,
        ]:
            raise self.error(dfn.LampRpcError.NOT_NOTOP_NOTREADY)

        self.set_command(dfn.LampCommand.INIT)
        return self.ok()

    def set_debug(self, debug: bool) -> int:
        """ set_debug Lamp """
        self.engine.log.error("set_debug received but not yet implemented")
        return 0

    async def initialise(self) -> None:
        self.log.info("Initialising Device Simulator")
        cfg = self.cfg
        ctrl = self.ctrl
        stat = self.stat
        config = self.config

        # Set default values for various configuration, control and status parameters:
        cfg.cooldown = int(config.cool_down)
        cfg.initial_state = config.initial_state
        cfg.max_on = int(config.max_on)
        cfg.warmup = int(config.warm_up)

        ctrl.execute = False
        ctrl.intensity = 0.0
        ctrl.command = dfn.LampCommand.NONE
        ctrl.time_on_limit = 1

        self.log.info(f"Local Mode is: {config.local_mode}")

        stat.local = config.local_mode
        stat.time_off = 0
        stat.time_on = 0
        stat.intensity = 0.0
        stat.time_left = 0

        stat.error_code = dfn.LampError.OK
        stat.error_text = ""
        stat.rpc_error_code = dfn.LampRpcError.OK
        stat.rpc_error_text = ""

        if config.auto_enter_op:
            ctrl.command = dfn.LampCommand.INIT
            ctrl.execute = True

    def handle_command(self) -> None:
        """ handle a new incoming command (when ctrl.execute==True) """
        if not self.ctrl.execute:
            return

        ctrl = self.ctrl

        self.set_error(dfn.LampError.OK)
        self.log.debug(f"Runner received command {ctrl.command}")

        command = ctrl.command
        ctrl.command = dfn.LampCommand.NONE
        ctrl.execute = False

        if command == dfn.LampCommand.RESET:
            self.schedule(dfn.LampEvent.Reset, "Reset Lamp Device Command")
        elif command == dfn.LampCommand.INIT:
            self.schedule(dfn.LampEvent.Init, "Init Lamp Device Command")
        elif command == dfn.LampCommand.STOP:
            self.schedule(dfn.LampEvent.Stop,
                          "Stop On-Going Operation Command")
        elif command == dfn.LampCommand.ENABLE:
            self.schedule(dfn.LampEvent.Enable, "Enable Lamp Device Command")
        elif command == dfn.LampCommand.DISABLE:
            self.schedule(dfn.LampEvent.Disable, "Disable Lamp Device Command")
        elif command == dfn.LampCommand.ON:
            self.schedule(dfn.LampEvent.On, "Switch On Lamp Command")
        elif command == dfn.LampCommand.OFF:
            self.schedule(dfn.LampEvent.Off, "Switch Off Lamp Command")
        else:
            self.set_error(-999, "BUG: Unknown Command")

    async def next(self) -> None:
        self.handle_command()
        # Nothing else to do. All is in off_activity, on_activity and on_execute_action

    # ==================== State Machine Business Logic ==========================

    def listen_event(self, event: sm.Event) -> None:
        self.stat.event_desc = event.getId()

    def listen_state(self,
          status: set[sm.State]
                     ) -> None:
        # Inject SCXML state into engine state/substate data
        for state in status:
            if isinstance(state, sm.StateAtomic):
                if state.getId() == "Off":
                    self.engine.exit(0)
                    time.sleep(1.0)
                else:
                    try:
                        state, substate = lamp_state_mapping[state.getId()]
                    except KeyError:
                        self.log.error(
                            f"Could not resolve state coming from state machine: {state.getId()}")
                    else:
                        self.set_state(state, substate)

    # ~~~~~~~~~~~~~~~~~ Activity Methods ~~~~~~~~~~~~~~~~~~

    def disabling_activity(self,
          handler: sm.IActivityHandler
                           ) -> None:
        if handler.is_running():
            time.sleep(self.config.sim_delay)
            self.stat.time_on = 0
            self.stat.time_off = 0
            handler.send_internal_event(
                sm.Event(dfn.LampEvent.DisableOff))
            handler.stop()

    def warmingup_activity(self,
          handler: sm.IActivityHandler
                           ) -> None:
        stat = self.stat
        if handler.is_running():
            self.log.debug(f"Activity {handler.get_id()} is running ...")
            time.sleep(self.config.warm_up)
            stat.time_on = 0
            stat.time_on_start = int(time.time() + 0.5)
            handler.send_internal_event(
                sm.Event(dfn.LampEvent.WarmedUp))

    def switchingon_activity(self,
          handler: sm.IActivityHandler
                             ) -> None:
        stat = self.stat
        if handler.is_running():
            time.sleep(self.config.sim_delay)
            stat.time_off = 0
            handler.send_internal_event(
                sm.Event(dfn.LampEvent.IsOn))

    def switchingoff_activity(self,
          handler: sm.IActivityHandler
                              ) -> None:
        stat = self.stat
        if handler.is_running():
            time.sleep(self.config.sim_delay)
            stat.time_off = 0
            stat.time_on = 0
            stat.intensity = 0.0
            handler.send_internal_event(
                sm.Event(dfn.LampEvent.IsOff))


    def _get_time_limit(self) -> int:
        stat, cfg = self.stat, self.cfg
        if cfg.max_on > 0 and stat.time_on_limit > 0:
            return min( cfg.max_on, stat.time_on_limit )
        elif cfg.max_on > 0:
            return  cfg.max_on
        else:
            return  stat.time_on_limit


    def on_activity(self,
          handler: sm.IActivityHandler
        ) -> None:

        stat, cfg = self.stat, self.cfg
         
        stat.check_time_left = (cfg.max_on > 0) or (stat.time_on_limit > 0)
         
        if stat.check_time_left:
            time_limit = self._get_time_limit()
        else:
            time_limit = 0 
        
        period = 1./ self.config.update_frequency / 2.0 # make it twice faster 
        timer = dvs.core.Timer()

        stat.time_on_start = int(time.time() + 0.5) # S.G. wHy the 0.5 ?
        stat.time_on = 0

        while handler.is_running():
            timer.tick()
            stat.time_on = int( time.time() - stat.time_on_start)
            if stat.check_time_left:
                stat.time_left = time_limit - stat.time_on    
            
                if (cfg.max_on> 0) and (stat.time_on >= cfg.max_on):
                    self.log.info("MaxOn condition exceeded - switching off lamp ...")
                    self.set_error( dfn.LampError.MAXON )
                    handler.send_internal_event(sm.Event(dfn.LampEvent.ErrMaxOn))
                    break 

                elif (stat.time_on_limit > 0) and (stat.time_on >= stat.time_on_limit):
                    self.log.info("TimeLimitOn condition exceeded - switching off lamp ...")
                    handler.send_internal_event(sm.Event(dfn.LampEvent.LimitOn))
                    break
            timer.tock()
            timer.sleep(period)

        stat.check_time_left = False
        stat.time_left = 0  

    def off_activity(self,
          handler: sm.IActivityHandler
                     ) -> None:
        stat = self.stat
        timer = dvs.core.Timer()
        period = 1. / self.config.update_frequency / 2.0
        stat.time_off = 0
        stat.time_off_start = int(time.time() + 0.5)
        stat.time_left = 0
        stat.check_time_left = False
        while handler.is_running():
            timer.tick()
            stat.time_off = int(time.time() - stat.time_off_start)

            timer.tock()
            timer.sleep(period)

    def coolingdown_activity(self,
          handler: sm.IActivityHandler
                             ) -> None:
        stat = self.stat
        if handler.is_running():
            time.sleep(self.config.cool_down)
            stat.time_off = 0
            stat.time_off_start = int(time.time() + 0.5)
            handler.send_internal_event(
                sm.Event(dfn.LampEvent.CooledDown))

    def initialising_activity(self,
          handler: sm.IActivityHandler
                              ) -> None:
        stat = self.stat
        if handler.is_running():
            self.log.debug(f"Activity {handler.get_id()} is running ...")
            time.sleep(self.config.sim_init_time)

            stat.time_on = 0
            stat.time_off = 0

            # TODO: Handle InitOn as well.
            handler.send_internal_event(
                sm.Event(dfn.LampEvent.InitOff))
            if self.config.auto_enter_op:
                handler.send_internal_event(
                    sm.Event(dfn.LampEvent.Enable))
            self.log.debug(f"Activity {handler.get_id()} terminating")

    # ~~~~~~~~~~~~~~~~~ Action Methods ~~~~~~~~~~~~~~~~~~
    #  to be re-defined case by case if needed

    def set_action_description(self,
       handler: sm.IActionHandler,
       context: sm.Context
                               ) -> None:
        """ A Generic action method """
        del context  # context not used
        self.stat.event_desc = handler.get_id()

    def on_execute_action(self,
            handler: sm.IActionHandler,
            context: sm.Context) -> None:
        stat, ctrl = self.stat, self.ctrl
        stat.intensity = ctrl.intensity
        # Copy the controler
        stat.time_on_limit = ctrl.time_on_limit
        ctrl.time_on_limit = 0
        stat.time_on = 0
        self.set_action_description(handler, context)

    def unexp_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def reset_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def disable_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def init_complete_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def init_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def enable_complete_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def err_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def off_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def updatetimes_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def init_reset_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def init_stop_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def stop_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)



if __name__=="__main__":
    _: type[ILampBl] = LampBl


