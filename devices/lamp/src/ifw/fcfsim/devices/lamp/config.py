from __future__ import annotations 
from ifw.fcfsim import api as dvs
from dataclasses import dataclass

LampSimConfigLoader = dvs.SimConfigLoader

@dataclass
class LampSimConfig(dvs.SimConfig):
    """ Lamp configuration """
    log: str = "devsim.lamp"
    state_machine_scxml: str = "config/fcfsim/devices/lamp/lamp.scxml.xml" 
    opcua_profile: str = "config/fcfsim/devices/lamp/lamp.namespace.yaml"   
    initial_state: bool = False
    warm_up: float = 0.0
    cool_down: float = 0.0
    max_on: int = 0

