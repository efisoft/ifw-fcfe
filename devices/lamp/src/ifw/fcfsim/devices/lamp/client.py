# ********************************
# Code generated by IPAG generator
# 2025-02-11 11:59:13,
# ********************************
from __future__ import annotations 
from dataclasses import dataclass, field 
import typing
from ifw.fcfsim import api as dvs 
# This module also act as an api for client business 
from ifw.fcfsim.devices.lamp.gen.client_engine import LampClientEngine as LampClientEngine
from ifw.fcfsim.devices.lamp import define as define 



@dataclass
class LampClientInterface:
    """Lamp Client Interface""" 
    prefix: str
    namespace: int = dvs.opcua.defaults.namespace  
    identifier: str = dvs.opcua.defaults.identifier
    engine: LampClientEngine = field(default_factory=LampClientEngine)

    def get_engine(self) -> LampClientEngine:
        return self.engine 
    
    def add_to_client(self, client_manager: dvs.opcua.ClientManager) -> None:
        profile_file = "config/fcfsim/devices/lamp/lamp.namespace.yaml"
        client_manager.add_profile( 
            dvs.opcua.read_profile(profile_file), 
            self.get_engine(),
            namespace=self.namespace, 
            identifier=self.identifier, 
            prefix= self.prefix
        )
        client_manager.add_ua_info(self.get_engine().ua_info)



