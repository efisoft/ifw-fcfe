# File generated once by Tmc2Fcs @efisoft on 09 Oct 2024
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations

from dataclasses import dataclass

from ifw.fcfsim.devices.lamp.gen import define_auto as auto

# Normalise Names from Auto Generated Enumerators or Classes  
# Edit if necessary. This renamed classes are  most likely used 
# by the business logic. Edit with care
LampState = auto.E_LAMP_STATE
LampSubstate = auto.E_LAMP_SUBSTATE
LampCommand = auto.E_LAMP_COMMAND
LampError = auto.E_LAMP_ERROR
LampRpcError = auto.E_LAMP_RPC_ERROR
LampActivity = auto.LampActivity
LampAction = auto.LampAction
LampEvent = auto.LampEvent




##### Data Structures #####
# Transform some TC_* names into python class name 
# Warning: Do not remove or rename one of this class, they are used by the engine 
# Eventually add some custom properties 
@dataclass
class LampCfg(auto.T_LAMP_CFG):
    """ Cfg class of Lamp device """
    
@dataclass
class LampCtrl(auto.T_LAMP_CTRL):
    """ Ctrl class of Lamp device """
    
@dataclass
class LampInfo(auto.T_LAMP_INFO):
    """ Info class of Lamp device """
    date: str = "09 Oct 2024"            
    name: str = "FCF DeviceSimulatorLamp"            
    platform: str = "WS Simulator"        

@dataclass
class LampStat(auto.T_LAMP_STAT):
    """ Stat class of Lamp device """
    


# Set Error description 
# one can redefine error description here if not define in define_auto
# e.g.  dvs.set_enum_text( LampError.LOCAL, 'Control not allowed. Motor in Local mode.')



