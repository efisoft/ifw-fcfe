#!/usr/bin/env python3
import sys 
from ifw.fcfsim import api as dvs
from ifw.fcfsim.devices.lamp import sim as lamp 
if __name__ == "__main__":
    sys.exit(dvs.utils.run_simulator(lamp.LampSimInterface, lamp))


