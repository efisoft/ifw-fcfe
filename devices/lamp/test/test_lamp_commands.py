import time
import pytest 
from ifw.fcfsim.devices.lamp.define import LampCommand,  LampSubstate
from ifw.fcfsim.devices.lamp import sim as lsim 
from ifw.fcfsim import api as dvs


@pytest.fixture(autouse=True)
def interface()-> lsim.LampSimInterface:
    config = lsim.LampSimConfig(
            sim_init_time=0.0,  
            sim_delay=0.0, 
            update_frequency=20
    )
    # the opcua server will not run on this session
    return lsim.LampSimInterface.from_config(config)

@pytest.fixture(autouse=True)
def simulator()-> dvs.Simulator:
    return dvs.Simulator( excludes={'opcua'} )


def test_lamp_sm_sequence(simulator: dvs.Simulator, interface: lsim.LampSimInterface):
    lamp = simulator.attach(interface)
    session = simulator.new_session()

    with session: # start threads and return an engine to communicate with simulator 
        assert session.is_alive()

        lamp.reset()
        time.sleep(0.1)
        
        lamp.init()
        assert lamp.stat.last_command == LampCommand.INIT
        time.sleep(0.1)
        assert lamp.stat.substate == LampSubstate.NOTOP_READY_OFF
        
        lamp.enable()
        time.sleep(0.1)
        assert lamp.stat.substate == LampSubstate.OP_OFF

        lamp.disable()
        time.sleep(0.1)
        assert lamp.stat.substate == LampSubstate.NOTOP_READY_OFF
        
        lamp.enable()
        time.sleep(0.1)
        assert lamp.stat.substate == LampSubstate.OP_OFF
        
        lamp.on(50.0, 10)
        time.sleep(0.1)
        assert lamp.stat.substate == LampSubstate.OP_ON
        assert lamp.stat.intensity == 50.0

        lamp.off()
        time.sleep(0.1)
        assert lamp.stat.substate == LampSubstate.OP_OFF

        lamp.reset()
        time.sleep(0.1)
        assert lamp.stat.substate == LampSubstate.NOTOP_NOTREADY

