
# File generated once by Tmc2Fcs @efisoft on 18 Jun 2024
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations

import enum
from dataclasses import dataclass

from ifw.fcfsim.devices.drot.gen import define_auto as auto
from ifw.fcfsim.devices.motor import client as motorclient
from ifw.fcfsim.devices.motor import sim as motorsim

MotorMode = motorsim.define.MotorMode
MotorCommand = motorsim.define.MotorCommand 
MotorMoveType = motorsim.define.MotorMoveType

# Normalise Names from Auto Generated Enumerators  
# Edit if necessary. This Enums most likely used by the business logic 
# DrotCommand = auto.E_MA_COMMAND 
MotorStatus = auto.E_MOTOR_STATUS 
DrotError = auto.E_MOTOR_ERROR
DrotRpcError = auto.E_MA_RPC_ERROR 
DrotState = auto.E_MA_STATE
DrotSubstate = auto.E_MA_SUBSTATE

class DrotMode(enum.IntEnum):
	ENG		= 0
	STAT	= 1
	SKY		= 2
	ELEV	= 3
	USER	= 4

class DrotCommand(enum.IntEnum):
	NONE = 0
	INIT = 1
	STOP = 2
	TRK_MOVE = 3
	START_TRACK = 4
	STOP_TRACK = 5
	RESET = 6
	ENABLE = 7
	DISABLE = 8
	MOVE_MASTER = 9

##### State Machine #####

DrotActivity = auto.DrotActivity
DrotAction = auto.DrotAction
DrotEvent = auto.DrotEvent


##### Data Structures #####
# Transform TC name into python class name 
# Warning: Do not remove or rename one of this class, they are used by the engine 
# Eventually add some custom properties 
@dataclass
class DrotCfg(auto.T_DROT_CFG):
    """ Cfg class of Drot device """
    
    
@dataclass
class DrotCtrl(auto.T_DROT_CTRL):
    """ Ctrl class of Drot device """
    
    
@dataclass
class DrotStat(auto.T_DROT_STAT):
    """ Stat class of Drot device """
    error_text: str = "" 
    

# This is a Composition  
MotorSimEngine = motorsim.MotorSimEngine
MotorClientEngine = motorclient.MotorClientEngine

