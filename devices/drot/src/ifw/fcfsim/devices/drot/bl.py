
from __future__ import annotations
from typing import  Any, Literal
import time 
import functools
from ifw.fcfsim  import api as dvs
from ifw.fcfsim.api import sm

from ifw.fcfsim.devices.drot import define as dfn 
from ifw.fcfsim.devices.drot.gen.iengine import IDrotSimEngine, IDrotBl 
from ifw.fcfsim.devices.drot.config import DrotSimConfig
from ifw.fcfsim.devices.motor.ma import ma

# ----------------------------------------------
# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.DrotState, dfn.DrotSubstate
drot_state_mapping: dict[str,tuple[dfn.DrotState, dfn.DrotSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready": (S.NOTOP, SS.NOTOP_READY), 
  "On::NotOperational::Initialising": (S.NOTOP, SS.NOTOP_INITIALISING), 
  "On::NotOperational::Aborting": (S.NOTOP, SS.NOTOP_ABORTING), 
  "On::NotOperational::Resetting": (S.NOTOP, SS.NOTOP_RESETTING), 
  "On::NotOperational::Enabling": (S.NOTOP, SS.NOTOP_ENABLING), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::Operational::Disabling": (S.OP, SS.OP_DISABLING), 
  "On::Operational::Standstill": (S.OP, SS.OP_STANDSTILL), 
  "On::Operational::Moving": (S.OP, SS.OP_MOVING), 
  "On::Operational::Stopping": (S.OP, SS.OP_STOPPING), 
  "On::Operational::Tracking": (S.OP, SS.OP_TRACKING), 
  "On::Operational::Presetting": (S.OP, SS.OP_PRESETTING), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
}
del S, SS
# ----------------------------------------------


class DrotBl(IDrotBl):
    # This class must implement all abstract methods of IDrotBl

    def __init__(self, engine: IDrotSimEngine) -> None:
        self.engine = engine
        self._stat = dfn.DrotStat()
        self._cfg = dfn.DrotCfg()
        self._ctrl = dfn.DrotCtrl()
        self._motor =  dfn.MotorSimEngine(
             config=self.config.cfg_motor,
             signals=self.engine.signals
            )

    # ===================== Data Model ===========================
    
    @property
    def cfg(self) -> dfn.DrotCfg: 
        return self._cfg

    @property
    def ctrl(self) -> dfn.DrotCtrl: 
        return self._ctrl

    @property
    def stat(self) -> dfn.DrotStat: 
        return self._stat

    @property  
    def motor(self) -> dfn.MotorSimEngine: #pyright: ignore
        return self._motor

    # ===================== shortcuts ============================

    @property
    def config(self) -> DrotSimConfig: 
        return self.engine.get_config()
    
    @property
    def log(self) -> dvs.core.ILogger: 
        return self.engine.log
    
    # ========================= Multy Axes Helper ==================
    @functools.cached_property
    def ma_std(self) -> ma.MaStandard:
        return ma.MaStandard((self.motor, ), 
                            stat =self.stat, 
                            ctrl = self.ctrl, 
                            cfg = self.cfg, 
                            log = self.log) 
    
    @functools.cached_property
    def ma_move(self) -> ma.MaMoveOneAxis:
        return ma.MaMoveOneAxis(
                         self.motor, 
                         stat = self.stat, 
                         ctrl = self.ctrl, 
                         cfg = self.cfg, 
                         log = self.log) 

    # ========================= Helpers  ==============================

    def ok(self)->int:
        """ reset rpc error and return OK code (should be 0) """
        self.set_rpc_error( dfn.DrotRpcError.OK)
        return int(dfn.DrotRpcError.OK)
    
    def error(self,  err_code:dfn.DrotRpcError, err_text:str|None=None)->dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error( err_code, err_text)
        return dvs.RpcException( self.stat.rpc_error_text, self.stat.rpc_error_code) 
        

    def set_error(self,  err_code:int, err_text:str="")->None:
        """ set error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text( dfn.DrotError, err_code, err_text)
        self.stat.error_code = code
        self.stat.error_text = text
    
    def set_rpc_error(self,  err_code:int, err_text:str|None=None)->None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text( dfn.DrotRpcError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text

    def check_local(self)->None:
        """ Check if in Local mode, raise an RpcException if in local """
        # The RpcException is catched properly by the server and will return the error code 
        if self.stat.local:
            raise self.error( dfn.DrotRpcError.LOCAL )  
        if self.motor.stat.local:
            raise self.error( dfn.DrotRpcError.LOCAL )
    
    def set_last_command(self,  command: dfn.DrotCommand)->None:
        """ Set the command (code & text) in engine stat """
        # nothing to do here Drot has no last_command
    
    def set_command(self, command: dfn.DrotCommand)->None:
        """ Set the command in engine ctrl and fill stat accordingly """
        self.ctrl.command = command 
        self.ctrl.execute = True 
        self.set_last_command(command)
   
    def set_state(self, state: dfn.DrotState, substate: dfn.DrotSubstate) -> None:
        stat = self.stat.sm
        stat.state = int(state) 
        stat.state_text =  dvs.core.get_enum_name(state)
        stat.substate = int(substate) 
        stat.substate_text = dvs.core.get_enum_name(substate)   

  
    #========================= RPC Methods Business Logic  =========================

    def move_vel(self, vel:float)->int:
        """ move_vel Drot """
        self.ma_move.move_vel(vel)
        self.ctrl.mode = dfn.DrotMode.ENG
        return self.ok() 

    def move_abs(self, pos:float, vel:float)->int:
        """ move_abs Drot """
        self.check_local()
        self.ma_move.move_abs(pos, vel)
        self.ctrl.mode = dfn.DrotMode.ENG
        return self.ok() 

    def disable(self)->int:
        """ disable Drot """
        return self.ma_std.disable()

    def move_angle(self, angle:float)->int:
        """ move_angle Drot """
        self.check_local()
        ctrl = self.ctrl 
        ctrl.mode = dfn.DrotMode.ENG
        ctrl.posang = angle
        ctrl.target_pos = 0.0 
        ctrl.target_vel = self.motor.config.default_velocity
        self.set_command(dfn.DrotCommand.TRK_MOVE)
        return self.ok() 

    def start_track(self, mode:int, angle:float)->int:
        """ start_track Drot """
        ctrl = self.ctrl 
        ctrl.mode = mode 
        ctrl.posang = angle 
        return self.ma_move.start_track()

    def stop_track(self)->int:
        """ stop_track Drot """
        return self.ma_move.stop_track()

    def reset(self)->int:
        """ reset Drot """
        return self.ma_std.reset()

    def track_offset(self, offset:float)->int:
        """ track_offset Drot """
        self.check_local()
        self.ctrl.offset = offset 
        return self.ok() 

    def enable(self)->int:
        """ enable Drot """
        return self.ma_std.enable()

        
    def stop(self)->int:
        """ stop Drot """
        return self.ma_move.stop()

    def move_rel(self, pos:float, vel:float) -> int:
        """ move_rel Drot """
        self.ma_move.move_rel(pos, vel)
        self.ctrl.mode = dfn.DrotMode.ENG 
        return self.ok() 

    def init(self) -> int:
        """ init Drot """
        return self.ma_std.init()

    async def initialise(self)->None:
        stat, config = self.stat, self.config 

        stat.local = config.local_mode 
        self.engine.log.info(f"Local Mode is: {stat.local}")
        stat.error_code = 0 
        await self.ma_std.initialise()
    
    def handle_command(self)->None:
        if not self.ctrl.execute :
            return 
        stat, ctrl, motor = self.stat, self.ctrl , self.motor
        command = ctrl.command 
        ctrl.command = dfn.DrotCommand.NONE
        ctrl.execute = False 
        
        ## Check standard commands 
        schedule = self.engine.schedule_event
        if self.ma_move.handle_ma_move_command(command, schedule):
            return 
        if self.ma_std.handle_ma_command(command, schedule):
            return 
        
        ## Commands not in ma_std or ma_move 
        if command == dfn.DrotCommand.TRK_MOVE:
            if ctrl.posang != 0:
                motor.ctrl.position = ctrl.posang/2.0 
                motor.ctrl.velocity = motor.config.default_velocity
                  
                 
            else:
                if ctrl.target_pos!=0.0 and ctrl.target_vel!=0.0:
                    motor.ctrl.position = ctrl.target_pos 
                else:
                    motor.ctrl.position = 0.0 
                motor.ctrl.velocity = ctrl.target_vel 

            motor.ctrl.command = dfn.MotorCommand.MOTOR_MOVE
            motor.ctrl.execute = True
            schedule( 
                  sm.Event( dfn.DrotEvent.Move) , "TrkCommand.TRK_MOVE" 
                ) 
            ctrl.target_pos = 0.0 
            ctrl.target_vel = 0.0 


        else:
            self.log.error( f"Not implemented or unknown command {command} ")


    async def next(self)->None:
        await self.ma_std.next()
        self.handle_command()

    # ==================== State Machine Business Logic ==========================

    def listen_event(self, event:sm.Event)->None:
        self.stat.sm.event_desc = event.getId()

    def listen_state(self,  
          status: set[sm.State]
        ) -> None:
        # Inject SCXML state into engine state/substate data 
        for state in status:
            if isinstance(state, sm.StateAtomic):
                if state.getId() == "Off":
                    self.engine.exit(0) 
                    time.sleep(1.0)
                else:
                    try:
                        state, substate = drot_state_mapping[state.getId()]
                    except KeyError:
                        self.log.error(f"Could not resolve state comming from state machine: {state.getId()}")
                    else:
                        self.set_state(state, substate)


    # ~~~~~~~~~~~~~~~~~ Activity Methods ~~~~~~~~~~~~~~~~~~ 

    def moving_activity(self,
          handler: sm.IActivityHandler
        )->None:
        self.ma_move.moving_activity(handler)
       
    def stopping_activity(self,
          handler: sm.IActivityHandler
        )->None:
        time.sleep(self.config.sim_delay )
        self.ma_move.moving_activity(handler)
       
    def presetting_activity(self,
          handler: sm.IActivityHandler
        )->None:
        start_time = time.time()
        def resolver() -> bool:
            return (time.time() - start_time) > self.config.sim_delay
        self.ma_move.presetting_activity(
                handler,  resolver
        )

    def disabling_activity(self,
          handler: sm.IActivityHandler
        )->None:
        time.sleep( self.config.sim_delay )
        self.ma_std.disabling_activity(handler)
       
    def resetting_activity(self,
          handler: sm.IActivityHandler
        )->None:
        self.ma_std.resetting_activity(handler)

    def enabling_activity(self,
          handler: sm.IActivityHandler
        )->None:
        self.ma_std.enabling_activity(handler)

    def tracking_activity(self,
          handler: sm.IActivityHandler
        )->None:
        tracker = ma.MaSinusTracker(period=60., amplitude=40.)
        self.ma_move.tracking_activity(
                handler, tracker, 
                self.config.update_frequency / 4.0
        )

        
    def initialising_activity(self,
          handler: sm.IActivityHandler
        )->None:
        self.ma_std.initialising_activity(handler)
        
    def aborting_activity(self,
          handler: sm.IActivityHandler
        )->None:
        self.ma_std.aborting_activity(handler)
        
    # ~~~~~~~~~~~~~~~~~ Action Methods ~~~~~~~~~~~~~~~~~~ 
    #  to be re-defined case by case if needed 
    
    def set_action_description(self,
       handler:sm.IActionHandler, 
       context:sm.Context
      )->None:
        """ A Generic action method """
        del context # context not used 
        self.stat.sm.event_desc = handler.get_id() 
    
    # ! Action are missing in the scxml 

if __name__=="__main__":
    _: type[IDrotBl] = DrotBl


