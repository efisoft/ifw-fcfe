from __future__ import annotations 
from typing import Any, Iterator
from ifw.fcfsim  import api as dvs
from dataclasses import dataclass, field
from ifw.fcfsim.devices.motor import sim as motor

class DrotSimConfigLoader(dvs.SimConfigLoader):
    def dispatch(self, values: dict[str, Any]) -> Iterator[tuple[str, Any]]:
        # load the motor config directly 
        open_motor_config = motor.MotorSimInterface.get_config_factory().from_cii_config
        yield from dvs.core.parse_values(
                values, 
                cfg_motor = open_motor_config)
        yield from super().dispatch(values)

@dataclass
class DrotSimConfig(dvs.SimConfig):
    """ Drot configuration """
    log: str = "devsim.drot"
    state_machine_scxml: str = "config/fcfsim/devices/drot/drot.scxml.xml" 
    opcua_profile: str = "config/fcfsim/devices/drot/drot.namespace.yaml"   
    cfg_motor: motor.MotorSimConfig = field(default_factory=motor.MotorSimConfig)
    
    ############################################################################## 
    def __post_init__(self) -> None:
        self.validate_config()

    def validate_config(self) -> None:
        dvs.SimConfig.validate_config(self)
        # Overides the opcua configuration for child motor
        # Whatever was in config is wrong
        self.cfg_motor.opcua_prefix = dvs.core.join_path(self.opcua_prefix, "motor")
        self.cfg_motor.opcua_identifier = self.opcua_identifier
        self.cfg_motor.opcua_namespace = self.opcua_namespace
    
