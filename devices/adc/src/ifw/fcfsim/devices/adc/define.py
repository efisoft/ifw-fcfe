
# File generated once by Tmc2Fcs @efisoft on 11 Sep 2024
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations

import enum
from dataclasses import dataclass

from ifw.fcfsim.devices.adc.gen import define_auto as auto
from ifw.fcfsim.devices.motor import client as motorclient
from ifw.fcfsim.devices.motor import sim as motorsim

# Normalise Names from Auto Generated Enumerators  
# Edit if necessary. This Enums most likely used by the business logic 
AdcCommand = auto.E_MA_COMMAND 
AdcError = auto.E_MOTOR_ERROR
AdcRpcError = auto.E_MA_RPC_ERROR 
AdcState = auto.E_MA_STATE
AdcSubstate = auto.E_MA_SUBSTATE
AdcAxesId = auto.E_ADC_AXES_ID
##### State Machine #####

AdcActivity = auto.AdcActivity
AdcAction = auto.AdcAction
AdcEvent = auto.AdcEvent

class AdcMode(enum.IntEnum):
    ENG		= 0
    STAT	= 1
    AUTO    = 2


##### Data Structures #####
# Transform TC name into python class name 
# Warning: Do not remove or rename one of this class, they are used by the engine 
# Eventually add some custom properties 
@dataclass
class AdcCfg(auto.T_ADC_CFG):
    """ Cfg class of Adc device """
    
    
@dataclass
class AdcCtrl(auto.T_ADC_CTRL):
    """ Ctrl class of Adc device """
    
    
@dataclass
class AdcStat(auto.T_ADC_STAT):
    """ Stat class of Adc device """
    # some helpers 
    

# This is a Composition  
MotorSimEngine = motorsim.MotorSimEngine
MotorClientEngine = motorclient.MotorClientEngine

# Set Error description 
# one can redefine error description here if not define in define_auto
# e.g.  dvs.core.set_enum_text( AdcError.LOCAL, 'Control not allowed. Motor in Local mode.')



