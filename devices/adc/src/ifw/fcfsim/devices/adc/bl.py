
from __future__ import annotations

import functools
import time

from ifw.fcfsim import api as dvs
from ifw.fcfsim.api import sm
from ifw.fcfsim.devices.adc import define as dfn
from ifw.fcfsim.devices.adc.config import AdcSimConfig
from ifw.fcfsim.devices.adc.gen.iengine import IAdcBl, IAdcSimEngine
from ifw.fcfsim.devices.motor.ma import ma

# -------------------------------------------
# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.AdcState, dfn.AdcSubstate
adc_state_mapping: dict[str,tuple[dfn.AdcState, dfn.AdcSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready": (S.NOTOP, SS.NOTOP_READY), 
  "On::NotOperational::Initialising": (S.NOTOP, SS.NOTOP_INITIALISING), 
  "On::NotOperational::Aborting": (S.NOTOP, SS.NOTOP_ABORTING), 
  "On::NotOperational::Resetting": (S.NOTOP, SS.NOTOP_RESETTING), 
  "On::NotOperational::Enabling": (S.NOTOP, SS.NOTOP_ENABLING), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::Operational::Disabling": (S.OP, SS.OP_DISABLING), 
  "On::Operational::Standstill": (S.OP, SS.OP_STANDSTILL), 
  "On::Operational::Moving": (S.OP, SS.OP_MOVING), 
  "On::Operational::Stopping": (S.OP, SS.OP_STOPPING), 
  "On::Operational::Tracking": (S.OP, SS.OP_TRACKING), 
  "On::Operational::Presetting": (S.OP, SS.OP_PRESETTING), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
}
del S, SS
# ----------------------------------------------



class AdcBl(IAdcBl):
    # This class must implement all abstract methods of IAdcBl

    def __init__(self, engine: IAdcSimEngine) -> None:
        self.engine = engine
     
        self._stat = dfn.AdcStat()
        self._cfg = dfn.AdcCfg()
        self._ctrl = dfn.AdcCtrl()
        self._motor1 =  dfn.MotorSimEngine(
             config=self.config.cfg_motor1,
             signals=self.engine.signals
            )
        self._motor2 =  dfn.MotorSimEngine(
             config=self.config.cfg_motor2,
             signals=self.engine.signals
            )

    # ===================== Data Model ===========================
    
    @property
    def cfg(self) -> dfn.AdcCfg:  
        return self._cfg

    @property
    def ctrl(self) -> dfn.AdcCtrl:   
        return self._ctrl

    @property
    def stat(self) -> dfn.AdcStat:   
        return self._stat

    @property  
    def motor1(self) -> dfn.MotorSimEngine:  
        return self._motor1

    @property  
    def motor2(self) -> dfn.MotorSimEngine:  
        return self._motor2

    # ===================== shortcuts ============================
    
    @property
    def config(self) -> AdcSimConfig: 
        return self.engine.get_config()
    
    @property
    def log(self) -> dvs.core.ILogger: 
        return self.engine.log
    
    # ========================= Multy Axes Helper ==================
    @functools.cached_property
    def ma_std(self) -> ma.MaStandard:
        return ma.MaStandard((self.motor1, self.motor2), 
                            stat =self.stat, 
                            ctrl = self.ctrl, 
                            cfg = self.cfg, 
                            log = self.log) 
    
    @functools.cached_property
    def ma_move(self) -> ma.MaMove:
        return ma.MaMove((self.motor1, self.motor2), 
                            stat =self.stat, 
                            ctrl = self.ctrl, 
                            cfg = self.cfg, 
                            log = self.log) 

    # =============================================================

    def ok(self) -> dfn.AdcRpcError:
        """ reset rpc error and return OK code (should be 0) """
        self.set_rpc_error(dfn.AdcRpcError.OK)
        return dfn.AdcRpcError.OK

    def error(self,
              err_code: int, 
              err_text: str | None = None) -> dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error(err_code, err_text)
        return dvs.RpcException(self.stat.rpc_error_text,
                                self.stat.rpc_error_code)

    def set_error(self, err_code: int, err_text: str = "") -> None:
        """ set error, code & text,  in engine stat """
        self.ma_std.set_error(err_code, err_text)
        
    def set_rpc_error(self, err_code: int, err_text: str | None = None) -> None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(
            dfn.AdcError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text

    def check_local(self) -> None:
        """ Check if in Local mode, raise an RpcException if in local """
        return self.ma_std.check_local()
       
    def set_last_command(self, command: dfn.AdcCommand) -> None:
        """ Set the command (code & text) in engine stat """
        self.ma_std.set_last_command(command)

    def set_command(self, command: dfn.AdcCommand) -> None:
        """ Set the command in engine ctrl and fill stat accordingly """
        self.ma_std.set_command(command)
       
    def set_state(self, state: dfn.AdcState, substate: dfn.AdcSubstate) -> None:
        self.ma_std.set_state(state, substate)

   
    #========================= RPC Methods Business Logic  =========================
    
    def resolve_axes(self, axis_id: int) -> list[int]:
        """Build a list of motor index from a axis_id 
        
        see dfn.AdcAxesId
        """
        if axis_id == dfn.AdcAxesId.ALL_AXES:
            return [0, 1]
        if axis_id == dfn.AdcAxesId.ADC_1:
            return [0] 
        if axis_id == dfn.AdcAxesId.ADC_2:
            return [1]
        raise self.error(999, "Invalid axis id")

    def move_vel(self, axis: int, vel: float) -> int:
        """ move_vel Adc """
        ret = self.ma_move.move_vel(self.resolve_axes(axis), vel)
        self.ctrl.mode = dfn.AdcMode.ENG
        return ret 

    def move_abs(self, axis: int, pos: float, vel: float) -> int:
        """ move_abs Adc """
        ret = self.ma_move.move_abs(self.resolve_axes(axis), pos, vel)
        self.ctrl.mode = dfn.AdcMode.ENG
        return ret 
        
    def move_rel(self, axis: int, pos: float, vel: float) -> int:
        """ move_abs Adc """
        ret = self.ma_move.move_rel(self.resolve_axes(axis), pos, vel)
        self.ctrl.mode = dfn.AdcMode.ENG
        return ret 

    def disable(self) -> int:
        """ disable Adc """
        return self.ma_std.disable()
        
    def move_angle(self, angle: float) -> int:
        """ move_angle Adc """
        raise NotImplementedError()
        self.check_local()
        # <- Do something ->
        self.set_command(dfn.AdcCommand.MOVE_ANGLE)
        return self.ok()

    def start_track(self, angle: float) -> int:
        """ start_track Adc """
        
        self.ctrl.mode = dfn.AdcMode.AUTO
        self.ctrl.posang[:] = [angle]*2 # ???
        return self.ma_move.start_track()

    def stop_track(self) -> int:
        """ stop_track Adc """
        return self.ma_move.stop_track()
        
    def reset(self) -> int:
        """ reset Adc """
        return self.ma_std.reset()

    def enable(self) -> int:
        """ enable Adc """
        return self.ma_std.enable()

    def stop(self) -> int:
        """ stop Adc """
        return self.ma_std.stop()

    def init(self) -> int:
        """ init Adc """
        return self.ma_std.init()

    async def initialise(self) -> None:
        await self.ma_std.initialise()
    
    def handle_command(self)->None:
        if not self.ctrl.execute :
            return 
        ctrl = self.ctrl 
        command = ctrl.command 
        ctrl.command = dfn.AdcCommand.NONE
        ctrl.execute = False 
        
        ## Check standard commands 
        schedule = self.engine.schedule_event
        if self.ma_move.handle_ma_move_command(command, schedule):
            return 
        if self.ma_std.handle_ma_command(command, schedule):
            return 
         
        self.log.error( f"Not implemented or unknown command {command} ")



    async def next(self) -> None:
        await self.ma_std.next()
        self.handle_command()

    # ==================== State Machine Business Logic ==========================

    def listen_event(self, event:sm.Event) -> None:
        self.ma_std.listen_event(event)

    def listen_state(self,  
          status: set[sm.State]
        ) -> None:
        self.ma_std.listen_state(status, self.engine.exit)
    # ~~~~~~~~~~~~~~~~~ Activity Methods ~~~~~~~~~~~~~~~~~~ 

    def moving_activity(self,
          handler: sm.IActivityHandler
        )->None:
        return self.ma_move.moving_activity(handler)

    def stopping_activity(self,
          handler: sm.IActivityHandler
        )->None:
        return self.ma_move.stopping_activity(handler)
        
    def presetting_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        start_time = time.time()
        def resolver() -> bool:
            return (time.time() - start_time) > self.config.sim_delay
        return self.ma_move.presetting_activity(handler, resolver)

    def disabling_activity(self,
          handler: sm.IActivityHandler
        )->None:
        return self.ma_std.disabling_activity(handler)

    def resetting_activity(self,
          handler: sm.IActivityHandler
        )->None:
        return self.ma_std.resetting_activity(handler)

    def enabling_activity(self,
          handler: sm.IActivityHandler
        )->None:
        return self.ma_std.enabling_activity(handler)

    def tracking_activity(self,
          handler: sm.IActivityHandler
        )->None:
        trackers = [
                ma.MaSinusTracker(period=60., amplitude=2.), 
                ma.MaSinusTracker(period=60., amplitude=2.), 
            ]
        self.ma_move.tracking_activity(
                handler, 
                trackers, 
                self.config.update_frequency / 4.0)
       
    def initialising_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        return self.ma_std.initialising_activity(handler)

    def aborting_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        return self.ma_std.aborting_activity(handler)

    # ~~~~~~~~~~~~~~~~~ Action Methods ~~~~~~~~~~~~~~~~~~ 
    #  to be re-defined case by case if needed 
    
    def set_action_description(self,
       handler:sm.IActionHandler, 
       context:sm.Context
      )->None:
        """ A Generic action method """
        self.ma_std.set_action_description(handler, context)
	

if __name__=="__main__":
    _: type[IAdcBl] = AdcBl


