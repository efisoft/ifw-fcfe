from __future__ import annotations 
from ifw.fcfsim import api as dvs
from dataclasses import dataclass, field
from typing import Any , Iterator
from ifw.fcfsim.devices.motor import sim as motor


class AdcSimConfigLoader(dvs.SimConfigLoader):
    def dispatch(self, values: dict[str, Any]) -> Iterator[tuple[str, Any]]:
        # Open the motors config on the fly
        open_motor = motor.MotorSimInterface.get_config_factory().from_cii_config
        yield from dvs.core.parse_values(
                values, cfg_motor1 = open_motor, cfg_motor2 = open_motor)
        yield from super().dispatch(values)

@dataclass
class AdcSimConfig(dvs.SimConfig):
    """ Adc configuration """
    log: str = "devsim.adc"
    state_machine_scxml: str = "config/fcfsim/devices/adc/adc.scxml.xml" 
    opcua_profile: str = "config/fcfsim/devices/adc/adc.namespace.yaml"   

    # cfg_motor# are MotorSimConfig they are load from config file by the AdcSimConfigLoader
    cfg_motor1: motor.MotorSimConfig = field(default_factory=motor.MotorSimConfig)
    cfg_motor2: motor.MotorSimConfig = field(default_factory=motor.MotorSimConfig)

    ############################################################################## 
    def __post_init__(self) -> None:
        self.validate_config()

    def validate_config(self) -> None:
        dvs.SimConfig.validate_config(self)
        # overrides sub config parameters they make sens only 
        # in standalone mode 
        self.cfg_motor1.opcua_prefix = dvs.core.join_path(self.opcua_prefix,"motor1")
        self.cfg_motor1.opcua_identifier = self.opcua_identifier
        self.cfg_motor1.opcua_namespace = self.opcua_namespace
        
        self.cfg_motor2.opcua_prefix = dvs.core.join_path(self.opcua_prefix,"motor2")
        self.cfg_motor2.opcua_identifier = self.opcua_identifier
        self.cfg_motor2.opcua_namespace = self.opcua_namespace

