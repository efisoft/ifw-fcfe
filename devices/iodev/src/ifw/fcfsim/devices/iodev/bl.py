from __future__ import annotations

import functools
import time
from dataclasses import dataclass

from ifw.fcfsim.devices.iodev import define as dfn
from ifw.fcfsim.devices.iodev.channel import ChannelSim, make_channels
from ifw.fcfsim.devices.iodev.config import IodevSimConfig
from ifw.fcfsim.devices.iodev.gen.iengine import IIodevBl, IIodevSimEngine
from ifw.fcfsim import api as dvs
from ifw.fcfsim.api import sm

# ----------------------------------------------
# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.IodevState, dfn.IodevSubstate
iodev_state_mapping: dict[str,tuple[dfn.IodevState, dfn.IodevSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready": (S.NOTOP, SS.NOTOP_READY), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::Operational::Monitoring": (S.OP, SS.OP_MONITORING), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
}
del S, SS
# ----------------------------------------------


@dataclass
class IodevBl(IIodevBl):
    # This class must implement all abstract methods of IIodevBl
    def __init__(self, engine: IIodevSimEngine) -> None:
        self.engine = engine
        self._stat = dfn.IodevStat84()
        self._cfg = dfn.IodevCfg()
        self._ctrl = dfn.IodevCtrl()
        self._info = dfn.IodevInfo()
 
    # ===================== Data Model ===========================
    
    @property
    def ctrl(self) -> dfn.IodevCtrl: 
        return self._ctrl

    @property
    def stat(self) -> dfn.IodevStat84: 
        return self._stat

    @property
    def cfg(self) -> dfn.IodevCfg: 
        return self._cfg

    @property
    def info(self) -> dfn.IodevInfo: 
        return self._info 

    @functools.cached_property
    def channels(self) -> list[ChannelSim]:
        """Instanciate a list of simulated channels from config"""
        return make_channels(self, self.config.channels)

    # ===================== shortcuts ============================

    @property
    def config(self) -> IodevSimConfig: 
        return self.engine.get_config()
    
    @property
    def log(self) -> dvs.core.ILogger: 
        return self.engine.log

    # ========================= Helpers  ==============================

    def ok(self) -> dfn.IodevRpcError:
        """ reset rpc error and return OK code (should be 0) """
        self.set_rpc_error(dfn.IodevRpcError.OK)
        return dfn.IodevRpcError.OK
    
    def error(self, err_code: dfn.IodevRpcError, err_text: str|None=None) -> dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error(err_code, err_text)
        return dvs.RpcException( self.stat.rpc_error_text, self.stat.rpc_error_code) 
        

    def set_error(self, err_code: int, err_text: str="") -> None:
        """ set error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(dfn.IodevError, err_code, err_text)
        self.stat.error_code = code
        self.stat.error_text = text
    
    def set_rpc_error(self, err_code: int, err_text: str | None=None) -> None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(dfn.IodevRpcError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text

    def check_local(self) -> None:
        """ Check if in Local mode, raise an RpcException if in local """
        # The RpcException is catched properly by the server and will return the error code 
        if self.stat.local:
            raise self.error(dfn.IodevRpcError.LOCAL)  
    
    def set_last_command(self,  command: dfn.IodevCommand) -> None:
        """ Set the command (code & text) in engine stat """
        self.stat.last_command = command
        self.stat.last_command_text = command.name # Check if it has last_command_text ?
    
    def set_command(self, command: dfn.IodevCommand) -> None:
        """ Set the command in engine ctrl and fill stat accordingly """
        self.ctrl.command = command 
        self.ctrl.execute = True 
        self.set_last_command(command)

    def set_state(self, state: dfn.IodevState, substate: dfn.IodevSubstate) -> None:
        stat = self.stat
        stat.state = int(state) 
        stat.state_text =  dvs.core.get_enum_name(state)
        stat.substate = int(substate) 
        stat.substate_text = dvs.core.get_enum_name(substate)   
    
    def schedule(self, event_name:str, description: str = "") -> None:
        self.engine.schedule_event(sm.Event(event_name), description)

    # This class must implement all abstract methods of IIodevBl
    #========================= RPC Methods Business Logic  =========================

    def reset(self) -> int:
        """ reset Iodev """
        self.check_local()
        self.set_command(dfn.IodevCommand.RESET)
        return self.ok()


    def set_debug(self, debug: bool) -> int:
        """ set_debug Iodev """
        self.log.error("set_debug received but not yet implemented")
        return 0


    def set_outputs(self) -> int:
        """ set_outputs Iodev """
        self.check_local()
        if self.stat.state != dfn.IodevState.OP:
            raise self.error(dfn.IodevRpcError.NOT_OP)
        self.set_command(dfn.IodevCommand.SET_OUTPUTS)
        return self.ok() 

    def stop(self) -> int:
        """ stop Iodev """
        raise NotImplementedError()
        self.check_local()
        # <- Do something -> 
        self.set_command(dfn.IodevCommand.STOP)
        return self.ok() 

    def set_log(self, log: bool) -> int:
        """ set_log Iodev """
        self.log.error("set_log received but not yet implemented")
        return 0


    def disable(self) -> int:
        """ disable Iodev """
        self.check_local()
        stat = self.stat		
        if stat.state != dfn.IodevState.OP:
            raise self.error(dfn.IodevRpcError.NOT_OP)
        
        self.set_command(dfn.IodevCommand.DISABLE )
        return self.ok()


    def enable(self) -> int:
        """ enable Iodev """
        self.check_local()
        stat = self.stat		
        if stat.state != dfn.IodevState.NOTOP:
            raise self.error(dfn.IodevRpcError.NOT_NOTOP_NOTREADY)
        if stat.substate != dfn.IodevSubstate.NOTOP_READY:
            raise self.error(dfn.IodevRpcError.NOT_NOTOP_NOTREADY)

        self.set_command(dfn.IodevCommand.ENABLE )
        return self.ok()


    def init(self) -> int:
        """ init Iodev """
        self.check_local()
        stat = self.stat 		
        if stat.substate not in [
        	dfn.IodevSubstate.NOTOP_NOTREADY,
        	dfn.IodevSubstate.NOTOP_ERROR, 
            ]:
            raise self.error(dfn.IodevRpcError.NOT_NOTOP_NOTREADY)
        
        self.set_command(dfn.IodevCommand.INIT )
        return self.ok()

    async def initialise(self) -> None:
        """Device simulator initialisation"""
        stat, cfg = self.stat, self.cfg
        
        stat.local = self.config.local_mode
        self.log.info(f"Local Mode is {stat.local}")
        if self.config.auto_enter_op:
            cfg.auto_op = True

        if cfg.auto_op:
            self.schedule(dfn.IodevEvent.Init, "Init IoDev device Auto-op")
            
            
    def handle_command(self) -> None:
        ctrl = self.ctrl
        if not ctrl.execute:
            return 
        command = ctrl.command
        ctrl.command = dfn.IodevCommand.NONE
        ctrl.execute = False

        if command == dfn.IodevCommand.RESET:
            self.schedule(dfn.IodevEvent.Reset, "Reset IoDev Device")
        elif command == dfn.IodevCommand.INIT:
            self.schedule(dfn.IodevEvent.Init, "Init IoDev device")
        elif command == dfn.IodevCommand.STOP:
            self.log.error("STOP Event not implemented in scxml file")
        elif command == dfn.IodevCommand.ENABLE:
            self.schedule(dfn.IodevEvent.Enable, "Enable Iodev Device")
        elif command == dfn.IodevCommand.DISABLE:
            self.schedule(dfn.IodevEvent.Disable, "Disable Iodev Device")
        elif command == dfn.IodevCommand.SET_OUTPUTS:
            self.log.warning("Set Outputs not implemented")
        else:
            self.set_error(dfn.IodevError.WRONG_CMD)

    async def next(self) -> None:
        """Next cycle business"""
        self.handle_command()
        for ch in self.channels:
            ch.update() # update simulated channels to new value
        self.stat.cycle_counter += 1

    # ==================== State Machine Business Logic ==========================

    def listen_event(self, event: sm.Event) -> None:
        self.stat.event_desc = event.getId()
    
    def listen_state(self,  
          status: set[sm.State]
        ) -> None:
        # Inject SCXML state into engine state/substate data 
        for state in status:
            if isinstance(state, sm.StateAtomic):
                if state.getId() == "Off":
                    self.engine.exit(0) 
                    time.sleep(0.2)
                else:
                    try:
                        state, substate = iodev_state_mapping[state.getId()]
                    except KeyError:
                        self.log.error(f"Could not resolve state comming from state machine: {state.getId()}")
                    else:
                        self.set_state(state, substate)

    # ~~~~~~~~~~~~~~~~~ Activity Methods ~~~~~~~~~~~~~~~~~~ 

    # ~~~~~~~~~~~~~~~~~ Action Methods ~~~~~~~~~~~~~~~~~~ 
    #  to be re-defined case by case if needed 
    
    def set_action_description(self,
       handler: sm.IActionHandler, 
       context: sm.Context
      ) -> None:
        """ A Generic action method """
        del context # context not used 
        self.stat.event_desc = handler.get_id() 

    def reset_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)


    def init_execute_action(self, 
         handler: sm.IActionHandler, 
         context: sm.Context
                    ) -> None:

        time.sleep(self.config.sim_init_time)
        if self.cfg.auto_op:
            self.schedule(dfn.IodevEvent.Enable, "Enable Auto op")
        self.set_action_description(handler, context)

if __name__=="__main__":
    _: type[IIodevBl] = IodevBl


