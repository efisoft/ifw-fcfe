# File generated once by Tmc2Fcs @efisoft on 09 Oct 2024
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations

from dataclasses import dataclass, field

from ifw.fcfsim import api as dvs
from ifw.fcfsim.devices.iodev.gen import define_auto as auto

# Normalise Names from Auto Generated Enumerators or Classes  
# Edit if necessary. This renamed classes are  most likely used 
# by the business logic. Edit with care
IodevState = auto.E_IODEV_STATE
IodevSubstate = auto.E_IODEV_SUBSTATE
IodevCommand = auto.E_IODEV_COMMAND
IodevError = auto.E_IODEV_ERROR
IodevRpcError = auto.E_IODEV_RPC_ERROR
IodevActivity = auto.IodevActivity
IodevAction = auto.IodevAction
IodevEvent = auto.IodevEvent




##### Data Structures #####
# Transform some TC_* names into python class name 
# Warning: Do not remove or rename one of this class, they are used by the engine 
# Eventually add some custom properties 
@dataclass
class IodevCtrl(auto.T_IODEV_CTRL):
    """ Ctrl class of Iodev device """

@dataclass
class IodevStat84(auto.T_IODEV_STAT_8_4): # ToDo replace by an empty iodev 
    """ Stat class of Iodev device """
    # Dynamic container for channels 
    ai_channels: dvs.core.Map[float] = field(default_factory=dvs.core.Map[float])
    di_channels: dvs.core.Map[bool] = field(default_factory=dvs.core.Map[bool])

@dataclass
class IodevCfg(auto.T_IODEV_CFG):
    """ Cfg class of Iodev device """
    
@dataclass
class IodevInfo(auto.T_IODEV_INFO):
    """ Info class of Iodev device """
    date: str = "09 Oct 2024"            
    name: str = "FCF DeviceSimulatorIodev"            
    platform: str = "WS Simulator"        

