#  ____   ___    _   _  ___ _____   _____ ____ ___ _____ 
# |  _ \ / _ \  | \ | |/ _ \_   _| | ____|  _ \_ _|_   _|
# | | | | | | | |  \| | | | || |   |  _| | | | | |  | |  
# | |_| | |_| | | |\  | |_| || |   | |___| |_| | |  | |  
# |____/ \___/  |_| \_|\___/ |_|   |_____|____/___| |_|  
#
# This file was automaticaly generated by Tmc2Fcs @efisoft on 11 Feb 2025
# You should normaly be abble to code all business logic by edditing bl.py and define.py
# which are safely protected. 
# If an edit is however needed (it shouldn't be) one must rename this files (and related imports) 
# so they are safelly protected 
#

from __future__ import annotations
from dataclasses import dataclass, field
import typing
import datetime 
import enum
from ifw.fcfsim.api import core


class IodevActivity:
    """
    Iodev activities 
    """

   

#################################################################################################

class IodevAction:
    """
    Iodev actions 
    """
    ResetExecute: typing.ClassVar[str] = "Reset.Execute" 
    EnableExecute: typing.ClassVar[str] = "Enable.Execute" # Warning Action has no match into SCXML
    DisableExecute: typing.ClassVar[str] = "Disable.Execute" # Warning Action has no match into SCXML
    UnexpExecute: typing.ClassVar[str] = "Unexp.Execute" # Warning Action has no match into SCXML
    InitExecute: typing.ClassVar[str] = "Init.Execute" 
    SetExecute: typing.ClassVar[str] = "Set.Execute" # Warning Action has no match into SCXML
    StopExecute: typing.ClassVar[str] = "Stop.Execute" # Warning Action has no match into SCXML
    ErrExecute: typing.ClassVar[str] = "Err.Execute" # Warning Action has no match into SCXML



#################################################################################################



class IodevEvent:
    """
    Iodev events 
    """
    HwOk: typing.ClassVar[str] =  "HwOk"
    Disable: typing.ClassVar[str] =  "Disable"
    ErrHw: typing.ClassVar[str] =  "ErrHw"
    Init: typing.ClassVar[str] =  "Init"
    AutoOp: typing.ClassVar[str] =  "AutoOp"
    Enable: typing.ClassVar[str] =  "Enable"
    Reset: typing.ClassVar[str] =  "Reset"



#################################################################################################
#  Data Classes. Since generated we are generous on the static classes we can define 
#   - An Interface class (a Protocol) 
#   - The concrete data Class with some *_node properties pointing to data node



#---------------------------------------------
# FB_IODEV_CHANNEL
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CHANNEL:
    ...

class IFB_IODEV_CHANNEL(typing.Protocol):
    ...

#---------------------------------------------
# FB_IODEV_CH_ANALOG
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_ANALOG(FB_IODEV_CHANNEL):
    conversion: int = 0 
    par1: float = 0 
    par2: float = 0 
    par3: float = 0 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def conversion_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "conversion")
    @property
    def par1_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "par1")
    @property
    def par2_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "par2")
    @property
    def par3_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "par3")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_ANALOG(IFB_IODEV_CHANNEL, typing.Protocol):
    conversion: int 
    par1: float 
    par2: float 
    par3: float 

#---------------------------------------------
# FB_IODEV_CH_ANLG_OUT
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_ANLG_OUT(FB_IODEV_CH_ANALOG):
    q_n_value: int = 0 
    value: int = 0 
    value_user: float = 0.0 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def q_n_value_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "q_n_value")
    @property
    def value_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "value")
    @property
    def value_user_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "value_user")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_ANLG_OUT(IFB_IODEV_CH_ANALOG, typing.Protocol):
    q_n_value: int 
    value: int 
    value_user: float 

#---------------------------------------------
# FB_IODEV_CH_DIGITAL
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_DIGITAL(FB_IODEV_CHANNEL):
    label_0: str = "0" 
    label_1: str = "1" 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def label_0_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "label_0")
    @property
    def label_1_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "label_1")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_DIGITAL(IFB_IODEV_CHANNEL, typing.Protocol):
    label_0: str 
    label_1: str 

#---------------------------------------------
# FB_IODEV_CH_DIG_OUT
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_DIG_OUT(FB_IODEV_CH_DIGITAL):
    q_b_value: bool = False 
    value: bool = False 
    value_user: str = "" 
    conversion: int = 0 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def q_b_value_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "q_b_value")
    @property
    def value_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "value")
    @property
    def value_user_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "value_user")
    @property
    def conversion_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "conversion")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_DIG_OUT(IFB_IODEV_CH_DIGITAL, typing.Protocol):
    q_b_value: bool 
    value: bool 
    value_user: str 
    conversion: int 

#---------------------------------------------
# FB_IODEV_CH_REAL
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_REAL(FB_IODEV_CHANNEL):
    conversion: int = 0 
    par1: float = 0 
    par2: float = 0 
    par3: float = 0 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def conversion_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "conversion")
    @property
    def par1_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "par1")
    @property
    def par2_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "par2")
    @property
    def par3_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "par3")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_REAL(IFB_IODEV_CHANNEL, typing.Protocol):
    conversion: int 
    par1: float 
    par2: float 
    par3: float 

#---------------------------------------------
# FB_IODEV_CH_REAL_IN
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_REAL_IN(FB_IODEV_CH_REAL):
    value: float = 0.0 
    value_user: float = 0.0 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def value_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "value")
    @property
    def value_user_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "value_user")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_REAL_IN(IFB_IODEV_CH_REAL, typing.Protocol):
    value: float 
    value_user: float 

#---------------------------------------------
# FB_IODEV_CH_ANLG_IN
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_ANLG_IN(FB_IODEV_CH_ANALOG):
    value: int = 0 
    value_user: float = 0.0 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def value_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "value")
    @property
    def value_user_node(self) -> core.IDataNode[float]:
        return core.node.AttributeNode(self, "value_user")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_ANLG_IN(IFB_IODEV_CH_ANALOG, typing.Protocol):
    value: int 
    value_user: float 

#---------------------------------------------
# FB_IODEV_CH_DIG_IN
#---------------------------------------------
@core.attrconnect
@dataclass
class FB_IODEV_CH_DIG_IN(FB_IODEV_CH_DIGITAL):
    value: bool = False 
    value_user: str = "0" 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def value_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "value")
    @property
    def value_user_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "value_user")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IFB_IODEV_CH_DIG_IN(IFB_IODEV_CH_DIGITAL, typing.Protocol):
    value: bool 
    value_user: str 

#---------------------------------------------
# T_IODEV_INFO
#---------------------------------------------
@core.attrconnect
@dataclass
class T_IODEV_INFO:
    date: str = "2019-04-24" 
    description: str = "State Machine based IODev controller" 
    name: str = "FB_IODEV" 
    platform: str = "CoDeSys" 
    synopsis: str = "State Machine based IODev controller" 
    version_major: int = 1 
    version_minor: int = 1 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def date_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "date")
    @property
    def description_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "description")
    @property
    def name_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "name")
    @property
    def platform_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "platform")
    @property
    def synopsis_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "synopsis")
    @property
    def version_major_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "version_major")
    @property
    def version_minor_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "version_minor")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IT_IODEV_INFO(typing.Protocol):
    date: str 
    description: str 
    name: str 
    platform: str 
    synopsis: str 
    version_major: int 
    version_minor: int 

#---------------------------------------------
# T_IODEV_CFG
#---------------------------------------------
@core.attrconnect
@dataclass
class T_IODEV_CFG:
    name: str = "IODev" 
    auto_op: bool = False 
    user_config: bool = False 
    num_di: int = 0 
    num_ai: int = 0 
    num_ri: int = 0 
    num_dis_i: int = 0 
    num_ti: int = 0 
    num_do: int = 0 
    num_ao: int = 0 
    num_dis_o: int = 0 
    num_to: int = 0 
    log_ext_time: bool = False 
    debug: bool = False 
    log: bool = False 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def name_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "name")
    @property
    def auto_op_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "auto_op")
    @property
    def user_config_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "user_config")
    @property
    def num_di_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_di")
    @property
    def num_ai_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_ai")
    @property
    def num_ri_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_ri")
    @property
    def num_dis_i_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_dis_i")
    @property
    def num_ti_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_ti")
    @property
    def num_do_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_do")
    @property
    def num_ao_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_ao")
    @property
    def num_dis_o_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_dis_o")
    @property
    def num_to_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "num_to")
    @property
    def log_ext_time_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "log_ext_time")
    @property
    def debug_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "debug")
    @property
    def log_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "log")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IT_IODEV_CFG(typing.Protocol):
    name: str 
    auto_op: bool 
    user_config: bool 
    num_di: int 
    num_ai: int 
    num_ri: int 
    num_dis_i: int 
    num_ti: int 
    num_do: int 
    num_ao: int 
    num_dis_o: int 
    num_to: int 
    log_ext_time: bool 
    debug: bool 
    log: bool 

#---------------------------------------------
# T_IODEV_STAT
#---------------------------------------------
@core.attrconnect
@dataclass
class T_IODEV_STAT:
    local: bool = False 
    state: int = 1 
    substate: int = 100 
    error_code: int = 0 
    last_command: int = 0 
    rpc_error_code: int = 0 
    status: int = 0 
    state_text: str = "NOT OP" 
    substate_text: str = "NOT READY" 
    error_text: str = "OK" 
    rpc_error_text: str = "OK" 
    status_text: str = "OK" 
    last_command_text: str = "NONE" 
    lib_version: str = "UNKNOWN" 
    action_desc: str = "" 
    event_desc: str = "" 
    cycle_counter: int = 0 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def local_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "local")
    @property
    def state_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "state")
    @property
    def substate_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "substate")
    @property
    def error_code_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "error_code")
    @property
    def last_command_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "last_command")
    @property
    def rpc_error_code_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "rpc_error_code")
    @property
    def status_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "status")
    @property
    def state_text_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "state_text")
    @property
    def substate_text_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "substate_text")
    @property
    def error_text_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "error_text")
    @property
    def rpc_error_text_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "rpc_error_text")
    @property
    def status_text_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "status_text")
    @property
    def last_command_text_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "last_command_text")
    @property
    def lib_version_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "lib_version")
    @property
    def action_desc_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "action_desc")
    @property
    def event_desc_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, "event_desc")
    @property
    def cycle_counter_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "cycle_counter")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IT_IODEV_STAT(typing.Protocol):
    local: bool 
    state: int 
    substate: int 
    error_code: int 
    last_command: int 
    rpc_error_code: int 
    status: int 
    state_text: str 
    substate_text: str 
    error_text: str 
    rpc_error_text: str 
    status_text: str 
    last_command_text: str 
    lib_version: str 
    action_desc: str 
    event_desc: str 
    cycle_counter: int 

#---------------------------------------------
# T_IODEV_STAT_8_4
#---------------------------------------------
@core.attrconnect
@dataclass
class T_IODEV_STAT_8_4(T_IODEV_STAT):
    di: core.Array[FB_IODEV_CH_DIG_IN] = field(default_factory= lambda : core.Array(8,FB_IODEV_CH_DIG_IN)) 
    ai: core.Array[FB_IODEV_CH_ANLG_IN] = field(default_factory= lambda : core.Array(4,FB_IODEV_CH_ANLG_IN)) 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IT_IODEV_STAT_8_4(IT_IODEV_STAT, typing.Protocol):
    di: core.Array[IFB_IODEV_CH_DIG_IN] 
    ai: core.Array[IFB_IODEV_CH_ANLG_IN] 

#---------------------------------------------
# T_IODEV_CTRL
#---------------------------------------------
@core.attrconnect
@dataclass
class T_IODEV_CTRL:
    command: int = 0 
    execute: bool = False 

    # ~~~~~~~~~~~~~~~~~~~~~~~~~ Node Helper ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def command_node(self) -> core.IDataNode[int]:
        return core.node.AttributeNode(self, "command")
    @property
    def execute_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, "execute")
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class IT_IODEV_CTRL(typing.Protocol):
    command: int 
    execute: bool 


class IIodev(typing.Protocol):
    ctrl: T_IODEV_CTRL
    stat: T_IODEV_STAT_8_4
    cfg: T_IODEV_CFG
    info: T_IODEV_INFO


@core.attrconnect
@dataclass
class Iodev:
    ctrl: T_IODEV_CTRL = field(default_factory=T_IODEV_CTRL) 
    stat: T_IODEV_STAT_8_4 = field(default_factory=T_IODEV_STAT_8_4) 
    cfg: T_IODEV_CFG = field(default_factory=T_IODEV_CFG) 
    info: T_IODEV_INFO = field(default_factory=T_IODEV_INFO) 
    




#################################################################################################
# Enumerators
#


#************************************************************
#  Enumerators (Device related) 
#************************************************************

#################################################################################################
class E_IODEV_ERROR(enum.IntEnum):
    """
    Enumeration for E_IODEV_ERROR generated automaticcaly from PLC lib tmc file
    """
    OK = 0
    HW_NOT_OP = 1
    WRONG_CMD = 2
    INIT_FAILURE = 3
    NOT_INITIALISED = 90
    #  Simulator error
    ZERO_POINTER = 100


#################################################################################################
class E_IODEV_COMMAND(enum.IntEnum):
    """
    Enumeration for E_IODEV_COMMAND generated automaticcaly from PLC lib tmc file
    """
    NONE = 0
    RESET = 1
    INIT = 2
    STOP = 3
    ENABLE = 4
    DISABLE = 5
    SET_OUTPUTS = 6


#################################################################################################
class E_IODEV_STATE(enum.IntEnum):
    """
    Enumeration for E_IODEV_STATE generated automaticcaly from PLC lib tmc file
    """
    NONE = 0
    NOTOP = 1
    OP = 2


#################################################################################################
class E_IODEV_SUBSTATE(enum.IntEnum):
    """
    Enumeration for E_IODEV_SUBSTATE generated automaticcaly from PLC lib tmc file
    """
    NONE = 0
    NOTOP_NOTREADY = 100
    NOTOP_READY = 101
    NOTOP_ERROR = 199
    OP_MONITORING = 200
    OP_ERROR = 299


#################################################################################################
class E_IODEV_RPC_ERROR(enum.IntEnum):
    """
    Enumeration for E_IODEV_RPC_ERROR generated automaticcaly from PLC lib tmc file
    """
    OK = 0
    NOT_OP = -1
    NOT_NOTOP_READY = -2
    NOT_NOTOP_NOTREADY = -3
    LOCAL = -4


#################################################################################################
class E_IODEV_STATUS(enum.IntEnum):
    """
    Enumeration for E_IODEV_STATUS generated automaticcaly from PLC lib tmc file
    """
    OK = 0
    ERROR = 1


#################################################################################################
class E_IODEV_CONVERSION(enum.IntEnum):
    """
    Enumeration for E_IODEV_CONVERSION generated automaticcaly from PLC lib tmc file
    """
    CONVERSION_IODEV_NONE = 0
    #  Digital signals
    CONVERSION_IODEV_DIGITAL = 1
    #  Analog signals
    CONVERSION_IODEV_LINEAR = 2
    CONVERSION_IODEV_QUADRATIC = 3
    CONVERSION_IODEV_EXP = 4
    CONVERSION_IODEV_EXP10 = 5



#************************************************************
#  Other Enumerators (Probably Garbage ?)
#************************************************************

#################################################################################################
class E_HashPrefixTypes(enum.IntEnum):
    """
    Enumeration for E_HashPrefixTypes generated automaticcaly from PLC lib tmc file
    """
    #  2#, 8#, 16# 
    HASHPREFIX_IEC = 0
    #  0 for octal type, 0x, 0X for hex else none  
    HASHPREFIX_STDC = 1


#################################################################################################
class E_SBCSType(enum.IntEnum):
    """
    Enumeration for E_SBCSType generated automaticcaly from PLC lib tmc file
    """
    #  Windows 1252 (default) 
    ESBCS_WESTERNEUROPEAN = 1
    #  Windows 1251 
    ESBCS_CENTRALEUROPEAN = 2


#################################################################################################
class E_RouteTransportType(enum.IntEnum):
    """
    Enumeration for E_RouteTransportType generated automaticcaly from PLC lib tmc file
    """
    EROUTETRANSPORT_NONE = 0
    EROUTETRANSPORT_TCP_IP = 1
    EROUTETRANSPORT_IIO_LIGHTBUS = 2
    EROUTETRANSPORT_PROFIBUS_DP = 3
    EROUTETRANSPORT_PCI_ISA_BUS = 4
    EROUTETRANSPORT_ADS_UDP = 5
    EROUTETRANSPORT_FATP_UDP = 6
    EROUTETRANSPORT_COM_PORT = 7
    EROUTETRANSPORT_USB = 8
    EROUTETRANSPORT_CAN_OPEN = 9
    EROUTETRANSPORT_DEVICE_NET = 10
    EROUTETRANSPORT_SSB = 11
    EROUTETRANSPORT_SOAP = 12


#################################################################################################
class E_ArgType(enum.IntEnum):
    """
    Enumeration for E_ArgType generated automaticcaly from PLC lib tmc file
    """
    ARGTYPE_UNKNOWN = 0
    ARGTYPE_BYTE = 1
    ARGTYPE_WORD = 2
    ARGTYPE_DWORD = 3
    ARGTYPE_REAL = 4
    ARGTYPE_LREAL = 5
    ARGTYPE_SINT = 6
    ARGTYPE_INT = 7
    ARGTYPE_DINT = 8
    ARGTYPE_USINT = 9
    ARGTYPE_UINT = 10
    ARGTYPE_UDINT = 11
    #  string of type T_MaxString! 
    ARGTYPE_STRING = 12
    ARGTYPE_BOOL = 13
    #  byte buffer 
    ARGTYPE_BIGTYPE = 14
    #  unsigned 64 bit ingeger (T_ULARGE_INTEGER, ULINT) 
    ARGTYPE_ULARGE = 15
    #  unsigned 128 bit integer (T_UHUGE_INTEGER) 
    ARGTYPE_UHUGE = 16
    #  signed 64 bit integer (T_LARGE_INTEGER, LINT) 
    ARGTYPE_LARGE = 17
    #  signed 128 bit integer (T_HUGE_INTEGER) 
    ARGTYPE_HUGE = 18
    #  LWORD value
    ARGTYPE_LWORD = 19


#################################################################################################
class E_WATCHDOG_TIME_CONFIG(enum.IntEnum):
    """
    Enumeration for E_WATCHDOG_TIME_CONFIG generated automaticcaly from PLC lib tmc file
    """
    EWATCHDOG_TIME_DISABLED = 0
    EWATCHDOG_TIME_SECONDS = 1
    EWATCHDOG_TIME_MINUTES = 2


#################################################################################################
class TcEventSeverity(enum.IntEnum):
    """
    Enumeration for TcEventSeverity generated automaticcaly from PLC lib tmc file
    """
    TCEVENTSEVERITY_VERBOSE = 0
    TCEVENTSEVERITY_INFO = 1
    TCEVENTSEVERITY_WARNING = 2
    TCEVENTSEVERITY_ERROR = 3
    TCEVENTSEVERITY_CRITICAL = 4


#################################################################################################
class TcSourceInfoType(enum.IntEnum):
    """
    Enumeration for TcSourceInfoType generated automaticcaly from PLC lib tmc file
    """
    UNDEFINED = 0
    ID = 1
    GUID = 2
    NAME = 4


#################################################################################################
class TcEventArgumentType(enum.IntEnum):
    """
    Enumeration for TcEventArgumentType generated automaticcaly from PLC lib tmc file
    """
    UNDEFINED = 0
    BOOLEAN = 1
    INT8 = 2
    INT16 = 3
    INT32 = 4
    INT64 = 5
    UINT8 = 6
    UINT16 = 7
    UINT32 = 8
    UINT64 = 9
    FLOAT = 10
    DOUBLE = 11
    CHAR = 12
    WCHAR = 13
    STRINGTYPE = 14
    WSTRINGTYPE = 15
    EVENTREFERENCE = 16
    FORMATSTRING = 17
    EXTERNALTIMESTAMP = 18
    BLOB = 19
    ADSNOTIFICATIONSTREAM = 20
    UTF8ENCODEDSTRING = 21


#################################################################################################
class EPlcPersistentStatus(enum.IntEnum):
    """
    Enumeration for EPlcPersistentStatus generated automaticcaly from PLC lib tmc file
    """
    PS_NONE = 0
    PS_ALL = 1
    PS_PARTIAL = 2


#################################################################################################
class _Implicit_KindOfTask(enum.IntEnum):
    """
    Enumeration for _Implicit_KindOfTask generated automaticcaly from PLC lib tmc file
    """
    _IMPLICIT_CYCLIC = 0
    _IMPLICIT_EVENT = 1
    _IMPLICIT_EXTERNAL = 2
    _IMPLICIT_FREEWHEELING = 3






#################################################################################################
# Set Error description 







if __name__ == "__main__":
    pass

#




