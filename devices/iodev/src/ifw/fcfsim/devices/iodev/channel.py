from __future__ import annotations
from dataclasses import dataclass, field
import functools
import random
from typing import Any, Callable, Iterable, Protocol, runtime_checkable
from ifw.fcfsim.core import api as core 
import math 
import time
import numpy as np 

_twopi = 2*math.pi

@dataclass
class ChannelInstance:
    """A Generic Channel status

    This is used by the ChannelComputer to model a value at a time T 
    """
    data_node: core.IDataNode[Any]
    start_time: float = field(default_factory=time.time)
    last_update: float = field(default_factory=time.time)

    
@runtime_checkable
class IChannelComputer(Protocol):
    """Porotocol for a Channel Simulation Computer"""
    def compute(self, instance: ChannelInstance) -> float:
        ...
    
@dataclass 
class SinWave:
    """A Sinnusoid simulator"""
    scale: float = 1.0 
    offset: float = 0.0 
    period: float = 1.0 

    def compute(self, instance: ChannelInstance) -> float:
        dt = time.time() - instance.start_time
        return math.sin(dt / self.period * _twopi) * self.scale + self.offset


@dataclass
class SquareWave:
    """Simulate a Digital Input square wave behaviour of the signal."""
    period_high: float = 1.0 
    period_low: float = 1.0 
    def compute(self, instance: ChannelInstance) -> bool:
        if instance.data_node.get():
            if (time.time() - instance.last_update) >= self.period_high:
                return False 
        else:
            if (time.time() - instance.last_update) >= self.period_low:
                return False 
        return bool(instance.data_node.get())


@dataclass 
class AnyFunction:
    """Encapsulate Any function in a ChannelComputer"""
    func: Callable[[],Any]
    def compute(self, instance: ChannelInstance) -> Any:
        return self.func()

@dataclass 
class StaticValue:
    value: Any 
    def compute(self, instance: ChannelInstance) -> Any:
        return self.value


def parse_function(function_def: str) -> IChannelComputer:
    """Parse the user definition of the function 

    The user shall define something returning a function or an object 
    with the compute(instance: ChannelInstance) signature
    
    the following name space is available:
        math :  math module 
        random: random module 
        np: numpy module 
        partial: functools partial function 
        static: To create a static value 
        ai_sine_wave: A computer factory for a sin wave  

    """
    gls = {
         "math": math,
         "random": random,
         "np": np, 
         "partial": functools.partial,
         "static": StaticValue, 
         "ai_sine_wave": SinWave,
         "di_square_wave" : SquareWave,
    }
    try:
        obj = eval(function_def,gls, {})
    except Exception as exc:
        raise ValueError(f"Error when building user specified function: {function_def}") from exc
    if isinstance(obj, IChannelComputer):
        return obj
    if callable(obj):
        return AnyFunction(obj)
    raise ValueError(f"user function: {function_def} is not returning a callable."
                      "Use e.g. 'static(1.3)' if you need static value")


@dataclass
class ChannelConfig:
    map: str  
    function: str

    computer: IChannelComputer = field(init=False)
    def __post_init__(self) -> None:
        """Catch any error at config construction level"""
        self.computer = parse_function(self.function)

@dataclass
class ChannelSim:
    data_node: core.IDataNode[Any]
    computer: IChannelComputer

    
    instance: ChannelInstance = field(init=False)
    def __post_init__(self) -> None:
        self.instance = ChannelInstance(
            self.data_node 
        )
    
    def update(self) -> None:
        """Update data node with new value"""
        value = self.computer.compute(self.instance)
        # update only if value has changed
        if value != self.data_node.get(default=None):
            self.data_node.set(value)
            self.instance.last_update = time.time()

def make_channels(engine: Any, channel_configs: Iterable[ChannelConfig]) -> list[ChannelSim]:
    """Instanciate a list of simulated channels from config"""

    channels: list[ChannelSim] = []
    for chan_config in channel_configs:
        channels.append( 
            ChannelSim(
                data_node = core.data_node(engine, chan_config.map), 
                computer = chan_config.computer
            )
        )
    return channels






if __name__ == "__main__":
    f = parse_function("ai_sine_wave(2.0, 0.0, 1e-9)")
    node: core.IDataNode[float] = core.node.StandaloneNode(10.0)
    i = ChannelInstance(node, start_time = 0.0 )
    print(f.compute(i))
    print( parse_function( "static(99)").compute(i))
    print( parse_function( "lambda: 2*random.random()*30").compute(i))
    print( parse_function( "lambda: np.random.normal(4.5, 10.0)").compute(i))
    print( parse_function( "partial(np.random.normal,4.5, 10.0)").compute(i))

