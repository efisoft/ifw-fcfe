from __future__ import annotations 
import typing
from typing import Any
from ifw.fcfsim import api as dvs
from dataclasses import dataclass, field

from ifw.fcfsim.devices.iodev.channel import ChannelConfig 


class IodevSimConfigLoader(dvs.SimConfigLoader):
    def dispatch(self, values: dict[str, Any]) -> typing.Iterator[tuple[str, Any]]:
        # Parse configng channels list into a list of ChannelConfig  
        if 'channels' in values:
            yield 'channels', [ChannelConfig(**kwargs) for kwargs in values.pop('channels')]
        yield from super().dispatch(values)


@dataclass
class IodevSimConfig(dvs.SimConfig):
    """ Iodev configuration """
    log: str = "devsim.iodev"
    state_machine_scxml: str = "config/fcfsim/devices/iodev/iodev.scxml.xml" 
    opcua_profile: str = "config/fcfsim/devices/iodev/iodev.namespace.yaml"   
    channels: list[ChannelConfig] = field(default_factory=list)
    
    @classmethod
    def get_factory(cls) -> dvs.core.IConfigFactory[IodevSimConfig]:
        return IodevSimConfigLoader().new_factory(cls)        

