#!/usr/bin/env python3
import sys 
from ifw.fcfsim import api as dvs
from ifw.fcfsim.devices.shutter import sim as shutter 
if __name__ == "__main__":
    sys.exit(dvs.utils.run_simulator(shutter.ShutterSimInterface, shutter))


