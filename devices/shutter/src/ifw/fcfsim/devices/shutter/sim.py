# ********************************
# Code generated by IPAG generator
# 2025-02-06 21:41:30,
# ********************************
from __future__ import annotations 
from dataclasses import dataclass, field 
import typing
import functools
from ifw.fcfsim import api as dvs 
# These module as part of the Shutter.sim api  
from ifw.fcfsim.devices.shutter.gen.engine import ShutterSimEngine as ShutterSimEngine
from ifw.fcfsim.devices.shutter import define as define 
from ifw.fcfsim.devices.shutter.config import (
        ShutterSimConfig as ShutterSimConfig, 
    ShutterSimConfigLoader as ShutterSimConfigLoader, 
)


@dataclass
class ShutterSimInterface(dvs.BaseSimInterface):
    engine: ShutterSimEngine = field(default_factory=ShutterSimEngine)
    

    @classmethod
    def get_config_factory(cls) -> dvs.core.IConfigFactory[ShutterSimConfig]:
        """Return a config factory for Shutter device"""
        return ShutterSimConfigLoader().new_factory(ShutterSimConfig)

    @classmethod
    def from_config(cls, config: ShutterSimConfig) -> ShutterSimInterface:
        return cls(ShutterSimEngine(config, log=dvs.core.get_logger(config.log)))
    
    def get_engine(self) -> ShutterSimEngine:
        return self.engine 
    
    def add_to_server(self, server_manager: dvs.opcua.ServerManager) -> None:
        dvs.utils.add_to_server( 
            server_manager, self.engine.get_config(), self.engine)
    
    def iter_runners(self, excludes: set[str] = set()) -> typing.Iterator[dvs.core.IRunner]:
        yield from dvs.utils.iter_runners(self, excludes)





