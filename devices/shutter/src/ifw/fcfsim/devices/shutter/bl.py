
from __future__ import annotations
import time 
import functools
from ifw.fcfsim import api as dvs
from ifw.fcfsim.api import sm
from ifw.fcfsim.devices.shutter import define as dfn 
from ifw.fcfsim.devices.shutter.gen.iengine import IShutterSimEngine, IShutterBl 
from ifw.fcfsim.devices.shutter.config import ShutterSimConfig

# ----------------------------------------------
# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.ShutterState, dfn.ShutterSubstate
shutter_state_mapping: dict[str,tuple[dfn.ShutterState, dfn.ShutterSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready::Closed": (S.NOTOP, SS.NOTOP_READY_CLOSED), 
  "On::NotOperational::Ready::Open": (S.NOTOP, SS.NOTOP_READY_OPEN), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::Operational::Disabling": (S.OP, SS.OP_DISABLING), 
  "On::Operational::Closed": (S.OP, SS.OP_CLOSED), 
  "On::Operational::Closing": (S.OP, SS.OP_CLOSING), 
  "On::Operational::Opening": (S.OP, SS.OP_OPENING), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
  "On::Operational::Open": (S.OP, SS.NONE), 
  "On::NotOperational::Initialising": (S.NOTOP, SS.NOTOP_INITIALISING), 
  "On::NotOperational::Ready": (S.OP, SS.OP_OPEN), 
}
del S, SS
# ----------------------------------------------



class ShutterBl(IShutterBl):
    # This class must implement all abstract methods of IShutterBl

    def __init__(self, engine: IShutterSimEngine) -> None:
        self.engine = engine
     
    # ===================== Data Model ===========================
    
    @functools.cached_property
    def cfg(self) -> dfn.ShutterCfg: #pyright:ignore 
        return dfn.ShutterCfg()

    @functools.cached_property
    def ctrl(self) -> dfn.ShutterCtrl: #pyright:ignore
        return dfn.ShutterCtrl()

    @functools.cached_property
    def info(self) -> dfn.ShutterInfo: #pyright:ignore
        return dfn.ShutterInfo()

    @functools.cached_property
    def stat(self) -> dfn.ShutterStat: #pyright:ignore
        return dfn.ShutterStat()

    # ===================== shortcuts ============================

    @property
    def config(self) -> ShutterSimConfig: 
        return self.engine.get_config()
    
    @property
    def log(self) -> dvs.core.ILogger: 
        return self.engine.log

    # ========================= Helpers  ==============================

    def ok(self) -> dfn.ShutterRpcError:
        """ reset rpc error and return OK code (should be 0) """
        self.set_rpc_error( dfn.ShutterRpcError.OK)
        return dfn.ShutterRpcError.OK
    
    def error(self,  err_code:dfn.ShutterRpcError, err_text:str|None=None)->dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error( err_code, err_text)
        return dvs.RpcException( self.stat.rpc_error_text, self.stat.rpc_error_code) 
        

    def set_error(self,  err_code:int, err_text:str="")->None:
        """ set error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text( dfn.ShutterError, err_code, err_text)
        self.stat.error_code = code
        self.stat.error_text = text
    
    def set_rpc_error(self,  err_code:int, err_text:str|None=None)->None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text( dfn.ShutterError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text

    def check_local(self)->None:
        """ Check if in Local mode, raise an RpcException if in local """
        # The RpcException is catched properly by the server and will return the error code 
        if self.stat.local:
            raise self.error(dfn.ShutterRpcError.LOCAL)  
    
    def set_last_command(self,  command: dfn.ShutterCommand)->None:
        """ Set the command (code & text) in engine stat """
        self.stat.last_command = command
        self.stat.last_command_text = command.name # Check if it has last_command_text ?
    
    def set_command(self, command: dfn.ShutterCommand)->None:
        """ Set the command in engine ctrl and fill stat accordingly """
        self.ctrl.command = command 
        self.ctrl.execute = True 
        self.set_last_command(command)
    
    def set_state(self, state: dfn.ShutterState, substate: dfn.ShutterSubstate) -> None:
        stat = self.stat
        stat.state = int(state) 
        stat.state_text =  dvs.core.get_enum_name(state)
        stat.substate = int(substate) 
        stat.substate_text = dvs.core.get_enum_name(substate)   

      
    #========================= RPC Methods Business Logic  =========================

    def reset(self)->int:
        """ reset Shutter """
        self.check_local()
        self.set_command( dfn.ShutterCommand.RESET )
        return self.ok()


    def stop(self)->int:
        """ stop Shutter """
        self.check_local()
        self.set_command(dfn.ShutterCommand.STOP)
        return self.ok() 

    def open(self)->int:
        """ open Shutter """
        self.set_command(dfn.ShutterCommand.OPEN)
        return self.ok() 

    def set_log(self, log:bool)->int:
        """ set_log Shutter """
        self.log.error("set_log received but not yet implemented")
        return 0


    def disable(self)->int:
        """ disable Shutter """
        self.check_local()
        stat = self.stat		
        if stat.state != dfn.ShutterState.OP:
            raise self.error( dfn.ShutterRpcError.NOT_OP)
        
        self.set_command(dfn.ShutterCommand.DISABLE )
        return self.ok()


    def enable(self)->int:
        """ enable Shutter """
        self.check_local()
        stat = self.stat		
        if stat.state != dfn.ShutterState.NOTOP:
            raise self.error(dfn.ShutterRpcError.NOT_NOTOP_NOTREADY)
        
        self.set_command(dfn.ShutterCommand.ENABLE )
        return self.ok()


    def close(self)->int:
        """ close Shutter """
        self.check_local()
        self.set_command(dfn.ShutterCommand.CLOSE)
        return self.ok() 

    def init(self)->int:
        """ init Shutter """
        self.check_local()
        stat = self.stat 		
        if stat.substate not in [
        	dfn.ShutterSubstate.NOTOP_NOTREADY,
        	dfn.ShutterSubstate.NOTOP_ERROR, 
            ]:
            raise self.error(dfn.ShutterRpcError.NOT_NOTOP_NOTREADY)
        
        self.set_command(dfn.ShutterCommand.INIT )
        return self.ok()


    def set_debug(self, debug:bool)->int:
        """ set_debug Shutter """
        self.log.error("set_debug received but not yet implemented")
        return 0



    async def initialise(self)->None:
        if self.config.auto_enter_op:
            self.set_command(dfn.ShutterCommand.INIT)
            self.log.info("AutoEnterOp feature activated - initiating initialisation ...")
    
    def handle_command(self)->None:
        if not self.ctrl.execute:
            return 
          
        ctrl = self.ctrl
        command = ctrl.command

        ctrl.execute = False
        ctrl.command = dfn.ShutterCommand.NONE 

        def schedule(e:str,s:str)->None:
            self.engine.schedule_event(sm.Event(e), s)

        if command == dfn.ShutterCommand.RESET:
            schedule(dfn.ShutterEvent.Reset, "Reset Shutter Device")
        elif command == dfn.ShutterCommand.INIT:
            schedule(dfn.ShutterEvent.Init, "Init Shutter Device")
        elif command == dfn.ShutterCommand.STOP:
            schedule(dfn.ShutterEvent.Stop, "Stop On-Going Operation")
        elif command == dfn.ShutterCommand.ENABLE:
            schedule(dfn.ShutterEvent.Enable, "Enable Shutter Device")
        elif command == dfn.ShutterCommand.DISABLE:
            schedule(dfn.ShutterEvent.Disable, "Disable Shutter Device")
        elif command == dfn.ShutterCommand.OPEN:
            schedule(dfn.ShutterEvent.Open, "Open Shutter")
        elif command == dfn.ShutterCommand.CLOSE:
            schedule(dfn.ShutterEvent.Close, "Close Shutter")
        else:
            self.log.error(f"Command {ctrl.command} is not implemented")


    async def next(self)->None:
        self.handle_command()
        self.stat.cycle_counter += 1

    # ==================== State Machine Business Logic ==========================

    def listen_event(self, event:sm.Event)->None:
        self.stat.event_desc = event.getId()

    def listen_state(self,  
          status: set[sm.State]
        ) -> None:
        # Inject SCXML state into engine state/substate data 
        for state in status:
            if isinstance(state, sm.StateAtomic):
                if state.getId() == "Off":
                    self.engine.exit(0) 
                    time.sleep(1.0)
                else:
                    try:
                        state, substate = shutter_state_mapping[state.getId()]
                    except KeyError:
                        self.log.error(f"Could not resolve state comming from state machine: {state.getId()}")
                    else:
                        self.set_state(state, substate)

    # ~~~~~~~~~~~~~~~~~ Activity Methods ~~~~~~~~~~~~~~~~~~ 

    def opening_activity(self,
          handler: sm.IActivityHandler
        )->None:
        start_time = time.time()
        sim_time = self.config.sim_delay 
        self.log.info( f"ActivityOpening: sim_time={sim_time}" )
        
        while (time.time() - start_time) < sim_time and handler.is_running():
            time.sleep(0.010)
        if not handler.is_running():
            return 

        handler.send_internal_event( sm.Event(dfn.ShutterEvent.IsOpen ))
        self.log.info(f"Shutter {handler.get_id()} opened")
        handler.stop()
       

    def closing_activity(self,
          handler: sm.IActivityHandler
        )->None:
        start_time = time.time()
        sim_time = self.config.sim_delay 

        self.log.info(f"ActivityClosing: sim_time={sim_time}")
        while (time.time() - start_time) < sim_time and handler.is_running():
            time.sleep(0.010)
        if not handler.is_running():
            return
        handler.send_internal_event(sm.Event(dfn.ShutterEvent.IsClosed))
        self.log.info(f"Shutter {handler.get_id()} closed")
        handler.stop() 

    def disabling_activity(self,
          handler: sm.IActivityHandler
        )->None:
        while handler.is_running():
            self.log.info(f"Activity {handler.get_id()} is running...")
            handler.send_internal_event(sm.Event(dfn.ShutterEvent.DisableClosed))
            handler.stop()
            break


    def initialising_activity(self,
          handler: sm.IActivityHandler
        )->None:

        while handler.is_running():
            self.log.info(f"Activity {handler.get_id()} is running ...")
            
            # If CfgInitialState=1, open the shutter.
            if self.cfg.initial_state:
                handler.send_internal_event(sm.Event(dfn.ShutterEvent.InitOpen))
            else:
                handler.send_internal_event(sm.Event(dfn.ShutterEvent.InitClosed))
            self.log.info(f"Activity {handler.get_id()} completed")

            if self.config.auto_enter_op:
                self.log.info("AutoEnterOp is activated - enabling Sevice Simulator")
                handler.send_internal_event(sm.Event(dfn.ShutterEvent.Enable))

            handler.stop()
            break


    # ~~~~~~~~~~~~~~~~~ Action Methods ~~~~~~~~~~~~~~~~~~ 
    #  to be re-defined case by case if needed 
    
    def set_action_description(self,
       handler:sm.IActionHandler, 
       context:sm.Context
      )->None:
        """ A Generic action method """
        del context # context not used 
        self.stat.event_desc = handler.get_id() 
	
    def init_abort_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def unexp_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def close_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def disable_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def init_complete_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def init_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def enable_complete_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def stop_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def reset_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def err_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def open_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def on_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	



if __name__=="__main__":
    _: type[IShutterBl] = ShutterBl


