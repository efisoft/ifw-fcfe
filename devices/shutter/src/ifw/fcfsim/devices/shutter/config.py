from __future__ import annotations

from dataclasses import dataclass

from ifw.fcfsim  import api as dvs

ShutterSimConfigLoader = dvs.SimConfigLoader 

@dataclass
class ShutterSimConfig(dvs.SimConfig):
    """ Shutter configuration """
    log: str = "devsim.shutter"
    state_machine_scxml: str = "config/fcfsim/devices/shutter/shutter.scxml.xml" 
    opcua_profile: str = "config/fcfsim/devices/shutter/shutter.namespace.yaml"   
    initial_state: bool = False


