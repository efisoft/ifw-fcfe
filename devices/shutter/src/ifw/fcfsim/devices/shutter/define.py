# File generated once by Tmc2Fcs @efisoft on 09 Oct 2024
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations

from dataclasses import dataclass

from ifw.fcfsim.devices.shutter.gen import define_auto as auto

# Normalise Names from Auto Generated Enumerators or Classes  
# Edit if necessary. This renamed classes are  most likely used 
# by the business logic. Edit with care
ShutterState = auto.E_SHUTTER_STATE
ShutterSubstate = auto.E_SHUTTER_SUBSTATE
ShutterCommand = auto.E_SHUTTER_COMMAND
ShutterError = auto.E_SHUTTER_ERROR
ShutterRpcError = auto.E_SHUTTER_RPC_ERROR
ShutterActivity = auto.ShutterActivity
ShutterAction = auto.ShutterAction
ShutterEvent = auto.ShutterEvent




##### Data Structures #####
# Transform some TC_* names into python class name 
# Warning: Do not remove or rename one of this class, they are used by the engine 
# Eventually add some custom properties 
@dataclass
class ShutterCfg(auto.T_SHUTTER_CFG):
    """ Cfg class of Shutter device """
    
@dataclass
class ShutterCtrl(auto.T_SHUTTER_CTRL):
    """ Ctrl class of Shutter device """
    
@dataclass
class ShutterInfo(auto.T_SHUTTER_INFO):
    """ Info class of Shutter device """
    date: str = "09 Oct 2024"            
    name: str = "FCF DeviceSimulatorShutter"            
    platform: str = "WS Simulator"        

@dataclass
class ShutterStat(auto.T_SHUTTER_STAT):
    """ Stat class of Shutter device """
    


# Set Error description 
# one can redefine error description here if not define in define_auto
# e.g.  dvs.set_enum_text( ShutterError.LOCAL, 'Control not allowed. Motor in Local mode.')



