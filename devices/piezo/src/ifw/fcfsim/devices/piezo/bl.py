
from __future__ import annotations

import functools
import random
import time
import typing

from ifw.fcfsim import api as dvs
from ifw.fcfsim.api import sm as sm
from ifw.fcfsim.devices.piezo import define as dfn
from ifw.fcfsim.devices.piezo.config import PiezoSimConfig
from ifw.fcfsim.devices.piezo.gen.iengine import IPiezoBl, IPiezoSimEngine

# ----------------------------------------------
# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.PiezoState, dfn.PiezoSubstate
piezo_state_mapping: dict[str,tuple[dfn.PiezoState, dfn.PiezoSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready": (S.NOTOP, SS.NOTOP_READY), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::Operational::Pos": (S.OP, SS.OP_POS), 
  "On::Operational::Auto": (S.OP, SS.OP_AUTO), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
}
del S, SS
# ----------------------------------------------


class PiezoBl(IPiezoBl):
    # This class must implement all abstract methods of IPiezoBl

    def __init__(self, engine: IPiezoSimEngine) -> None:
        self.engine = engine
        self._cfg = dfn.PiezoCfg()
        self._ctrl = dfn.PiezoCtrl() 
        self._stat = dfn.PiezoStat()
        self._info = dfn.PiezoInfo()

    # ===================== Data Model ===========================
    
    @property
    def cfg(self) -> dfn.PiezoCfg: 
        return self._cfg

    @property
    def ctrl(self) -> dfn.PiezoCtrl: 
        return self._ctrl 
    
    @property
    def stat(self) -> dfn.PiezoStat: 
        return self._stat

    @property
    def info(self) -> dfn.PiezoInfo: 
        return self._info

    # ===================== shortcuts ============================

    @property
    def config(self) -> PiezoSimConfig: 
        return self.engine.get_config()
    
    @property
    def log(self) -> dvs.core.ILogger: 
        return self.engine.log


    # ========================= Helpers  ==============================


    def ok(self) -> dfn.PiezoRpcError:
        """ reset rpc error and return OK code (should be 0) """
        self.set_rpc_error(dfn.PiezoRpcError.OK)
        return dfn.PiezoRpcError.OK

    def error(self, err_code: dfn.PiezoRpcError, err_text: str | None = None) -> dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error(err_code, err_text)
        return dvs.RpcException(self.stat.rpc_error_text, self.stat.rpc_error_code)

    def set_error(self, err_code: int, err_text: str = "") -> None:
        """ set error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(
            dfn.PiezoError, err_code, err_text)
        self.stat.error_code = code
        self.stat.error_text = text

    def set_rpc_error(self, err_code: int, err_text: str | None = None) -> None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(
            dfn.PiezoRpcError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text

    def check_local(self) -> None:
        """ Check if in Local mode, raise an RpcException if in local """
        # The RpcException is catched properly by the server and will return the error code
        if self.stat.local:
            raise self.error(dfn.PiezoRpcError.LOCAL)

    def set_last_command(self, command: dfn.PiezoCommand) -> None:
        """ Set the command (code & text) in engine stat """
        self.stat.last_command = command
        # Check if it has last_command_text ?
        self.stat.last_command_text = command.name

    def set_command(self, command: dfn.PiezoCommand) -> None:
        """ Set the command in engine ctrl and fill stat accordingly """
        self.ctrl.command = command
        self.ctrl.execute = True
        self.set_last_command(command)

    def set_state(self, state: dfn.PiezoState, substate: dfn.PiezoSubstate) -> None:
        stat = self.stat
        stat.state = int(state)
        stat.state_text = dvs.core.get_enum_name(state)
        stat.substate = int(substate)
        stat.substate_text = dvs.core.get_enum_name(substate)

    def schedule(self, event_name:str, description: str = "") -> None:
        self.engine.schedule_event(sm.Event(event_name), description)

    #========================= RPC Methods Business Logic  =========================

    def reset(self) -> int:
        """ reset Piezo """
        self.check_local()
        self.set_command(dfn.PiezoCommand.RESET)
        return self.ok()

    def set_debug(self, debug: bool) -> int:
        """ set_debug Piezo """
        self.log.error("set_debug received but not yet implemented")
        return 0

    def check_ready(self) -> None:
        stat = self.stat
        if stat.state == dfn.PiezoState.NOTOP and\
           stat.substate != dfn.PiezoSubstate.NOTOP_READY:
            raise self.error(dfn.PiezoRpcError.NOT_NOTOP_NOTREADY)

    def home(self) -> int:
        """ home Piezo """
        self.check_local()
        self.check_ready()
        self.set_command(dfn.PiezoCommand.HOME)
        return self.ok()

    def stop(self) -> int:
        """ stop Piezo """
        self.check_local()
        stat = self.stat
        if stat.state != dfn.PiezoState.OP:
            raise self.error(dfn.PiezoRpcError.NOT_OP)

        self.set_command(dfn.PiezoCommand.STOP)
        return self.ok()

    def auto(self) -> int:
        """ auto Piezo """
        self.check_local()
        self.check_ready()
        self.set_command(dfn.PiezoCommand.AUTO)
        return self.ok()

    def pos(self) -> int:
        """ pos Piezo """
        self.check_local()
        self.check_ready()
        self.set_command(dfn.PiezoCommand.POS)
        return self.ok()

    def disable(self) -> int:
        """ disable Piezo """
        self.check_local()
        stat = self.stat
        if stat.state != dfn.PiezoState.OP:
            raise self.error(dfn.PiezoRpcError.NOT_OP)

        self.set_command(dfn.PiezoCommand.DISABLE)
        return self.ok()

    def enable(self) -> int:
        """ enable Piezo """
        self.check_local()
        self.check_ready()
        self.set_command(dfn.PiezoCommand.ENABLE)
        return self.ok()

    def get_bits(self, index: int, user_value: float) -> int:
        offset = self.cfg.user_offset_bit_set[index]
        user2bit = self.cfg.user2_bit_set[index]
        return int(offset + user_value * user2bit)

    def get_pos(self, index: int, bit: int) -> float:
        offset = self.cfg.user_offset_bit_set[index]
        user2bit = self.cfg.user2_bit_set[index]
        return (bit - offset) / user2bit

    def move_user(self, pos1: float, pos2: float, pos3: float) -> int:
        """ move_user Piezo """
        self.check_local()
        stat, ctrl, cfg = self.stat, self.ctrl, self.cfg
        if stat.state == dfn.PiezoState.NOTOP and\
           stat.substate != dfn.PiezoSubstate.NOTOP_NOTREADY:
            raise self.error(dfn.PiezoRpcError.NOT_NOTOP_NOTREADY)

        for i, pos in zip(range(cfg.num_axes), (pos1, pos2, pos3)):
            bits_pos = self.get_bits(i, pos)
            self.log.debug(
                f"cfg_limit_low_node={cfg.limit_low[i]}, cfg_limit_high_node={cfg.limit_high[i]}, user_pos={pos}")
            if bits_pos < cfg.limit_low[i] or bits_pos > cfg.limit_high[i]:
                raise self.error(dfn.PiezoRpcError.MOVING_USER)

        ctrl.pos_user[:] = (pos1, pos2, pos3)
        self.set_command(dfn.PiezoCommand.MOVE_USER)
        return self.ok()

    def move_bit(self, pos1: int, pos2: int, pos3: int) -> int:
        """ move_bit Piezo """
        self.check_local()
        stat, ctrl, cfg = self.stat, self.ctrl, self.cfg
        if stat.state == dfn.PiezoState.NOTOP and\
           stat.substate != dfn.PiezoSubstate.NOTOP_NOTREADY:
            raise self.error(dfn.PiezoRpcError.NOT_NOTOP_NOTREADY)

        for i, pos in zip(range(cfg.num_axes), (pos1, pos2, pos3)):
            self.log.debug(
                f"cfg_limit_low_node={cfg.limit_low[i]}, cfg_limit_high_node={cfg.limit_high[i]}, user_pos={pos}")
            if pos < cfg.limit_low[i] or pos > cfg.limit_high[i]:
                raise self.error(dfn.PiezoRpcError.MOVING_BIT)
        ctrl.pos_bit[:] = (pos1, pos2, pos3)
        self.set_command(dfn.PiezoCommand.MOVE_BIT)
        return self.ok()

    def init(self) -> int:
        """ init Piezo """
        self.check_local()
        stat = self.stat
        if stat.substate not in [
        	dfn.PiezoSubstate.NOTOP_NOTREADY,
        	dfn.PiezoSubstate.NOTOP_ERROR,
        ]:
            raise self.error(dfn.PiezoRpcError.NOT_NOTOP_NOTREADY)

        self.set_command(dfn.PiezoCommand.INIT)
        return self.ok()

    def set_log(self, log: bool) -> int:
        """ set_log Piezo """
        self.log.error("set_log received but not yet implemented")
        return 0

    async def initialise(self) -> None:
        stat, ctrl, config = self.stat, self.ctrl, self.config
        stat.local = config.local_mode
        self.log.info("Local Mode is: {stat.local}")
        if config.auto_enter_op:
            ctrl.command = dfn.PiezoCommand.INIT
            ctrl.execute = True

    def handle_command(self) -> None:
        if not self.ctrl.execute:
            return
        stat, ctrl = self.stat, self.ctrl

        command = ctrl.command
        ctrl.execute = False
        ctrl.command = dfn.PiezoCommand.NONE
        
        if command == dfn.PiezoCommand.RESET:
            self.schedule(dfn.PiezoEvent.Reset, "Reset Piezo Device")
        elif command == dfn.PiezoCommand.INIT:
            self.schedule(dfn.PiezoEvent.Init, "Init Piezo Device")
        elif command == dfn.PiezoCommand.STOP:
            self.schedule(dfn.PiezoEvent.Stop, "Stop Piezo Device")
        elif command == dfn.PiezoCommand.ENABLE:
            self.schedule(dfn.PiezoEvent.Enable, "Enable Piezo Device")
        elif command == dfn.PiezoCommand.DISABLE:
            self.schedule(dfn.PiezoEvent.Disable, "Disable Piezo Device")
        elif command == dfn.PiezoCommand.POS:
            self.schedule(dfn.PiezoEvent.Pos, "Set Pos mode for Piezo Device")
        elif command == dfn.PiezoCommand.HOME:
            self.schedule(dfn.PiezoEvent.Home, "Set Home for Piezo Device")
        elif command == dfn.PiezoCommand.AUTO:
            self.schedule(dfn.PiezoEvent.Auto, "Set Auto mode for Piezo Device")
        elif command == dfn.PiezoCommand.MOVE_BIT:
            self.schedule(dfn.PiezoEvent.Pos, "Set Pos mode for Piezo Device")
            stat.act_pos_bit[:] = ctrl.pos_bit
            stat.act_pos_usr[:] = (self.get_pos(i, bit)
                                   for i, bit in enumerate(ctrl.pos_bit))
        elif command == dfn.PiezoCommand.MOVE_USER:
            self.schedule(dfn.PiezoEvent.Pos, "Set Pos mode for Piezo Device")
            stat.act_pos_bit[:] = (self.get_bits(i, pos)
                                   for i, pos in enumerate(ctrl.pos_user))
            stat.act_pos_usr[:] = ctrl.pos_user
        else:
            self.set_error(999, "UNKNWON COMMAND")
            self.log.error(f"Command {command} is unknown or not implemented")

    async def next(self) -> None:
        self.handle_command()
        stat, cfg = self.stat, self.cfg
        if stat.substate == dfn.PiezoSubstate.OP_AUTO:
            act_pos_bit = [abs(int(random.random() * frg))
                           for frg in cfg.full_range]
            act_pos_usr = [self.get_pos(i, b)
                           for i, b in enumerate(stat.act_pos_bit)]
        else:
            act_pos_bit = stat.act_pos_bit[:]
            act_pos_usr = stat.act_pos_usr[:]

        stat.mon_set_pos_bit_0 = act_pos_bit[0]
        stat.mon_set_pos_bit_1 = act_pos_bit[1]
        stat.mon_set_pos_bit_2 = act_pos_bit[2]

        stat.mon_act_pos_bit_0 = act_pos_bit[0]
        stat.mon_act_pos_bit_1 = act_pos_bit[1]
        stat.mon_act_pos_bit_2 = act_pos_bit[2]

        stat.mon_act_pos_usr_0 = act_pos_usr[0]
        stat.mon_act_pos_usr_1 = act_pos_usr[1]
        stat.mon_act_pos_usr_2 = act_pos_usr[2]

    # ==================== State Machine Business Logic ==========================

    def listen_event(self, event:sm.Event)->None:
        self.stat.event_desc = event.getId()

    def listen_state(self,  
          status: set[sm.State]
        ) -> None:
        # Inject SCXML state into engine state/substate data 
        for state in status:
            if isinstance(state, sm.StateAtomic):
                if state.getId() == "Off":
                    self.engine.exit(0) 
                    time.sleep(1.0)
                else:
                    try:
                        state, substate = piezo_state_mapping[state.getId()]
                    except KeyError:
                        self.log.error(f"Could not resolve state comming from state machine: {state.getId()}")
                    else:
                        self.set_state(state, substate)


    # ~~~~~~~~~~~~~~~~~ Activity Methods ~~~~~~~~~~~~~~~~~~ 

    def disabling_activity(self,
          handler: sm.IActivityHandler
        )->None:
        while handler.is_running():
            time.sleep( self.config.sim_delay )
            handler.send_internal_event( 
               sm.Event( dfn.PiezoEvent.DisableDone )
            )
            handler.stop()
            break

    def initialising_activity(self,
          handler: sm.IActivityHandler
        )->None:
        while handler.is_running():
            time.sleep( self.config.sim_init_time )
            handler.send_internal_event( 
               sm.Event(  dfn.PiezoEvent.InitDone )
            )
            if self.config.auto_enter_op:
                self.engine.schedule_event( 
                    sm.Event( dfn.PiezoEvent.Enable ), 
                    "AUTO OP Eneble"
                )
            handler.stop()
            break
    # ~~~~~~~~~~~~~~~~~ Action Methods ~~~~~~~~~~~~~~~~~~ 
    #  to be re-defined case by case if needed 
    # Note: Only action defined inside the scxml are exposed 
    # Action method of the TMC (PLC) are ignored
    
    def set_action_desc(self,
       handler: sm.IActionHandler, 
       context: sm.Context
      ) -> None:
        """ A Generic action method """
        del context # context not used 
        self.stat.event_desc = handler.get_id() 
    
    def reset_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def pos_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def init_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def auto_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def home_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def stop_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def err_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def init_reset_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)
	
    def init_stop_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_desc(handler, context)

if __name__=="__main__":
    if typing.TYPE_CHECKING:
        _: type[IPiezoBl] = PiezoBl


