# File generated once by Tmc2Fcs @efisoft on 13 Jan 2025
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations

from dataclasses import dataclass

from ifw.fcfsim  import api as dvs

PiezoSimConfigLoader = dvs.SimConfigLoader 

# This class represent device configuration 
# One may have to edit this one along with the config schema  
# !! Do not rename this class: used by th egenerated engine
@dataclass
class PiezoSimConfig(dvs.SimConfig):
    """ Piezo configuration """
    log: str = "devsim.piezo"
    state_machine_scxml: str = "config/fcfsim/devices/piezo/piezo.scxml.xml" 
    opcua_profile: str = "config/fcfsim/devices/piezo/piezo.namespace.yaml"   
    initial_state: bool = False


