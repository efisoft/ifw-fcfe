# File generated once by Tmc2Fcs @efisoft on 09 Oct 2024
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations

from dataclasses import dataclass

from ifw.fcfsim.devices.piezo.gen import define_auto as auto

# Normalise Names from Auto Generated Enumerators or Classes  
# Edit if necessary. This renamed classes are  most likely used 
# by the business logic. Edit with care
PiezoState = auto.E_PIEZO_STATE
PiezoSubstate = auto.E_PIEZO_SUBSTATE
PiezoCommand = auto.E_PIEZO_COMMAND
PiezoError = auto.E_PIEZO_ERROR
PiezoRpcError = auto.E_PIEZO_RPC_ERROR
PiezoActivity = auto.PiezoActivity
PiezoAction = auto.PiezoAction
PiezoEvent = auto.PiezoEvent




##### Data Structures #####
# Transform some TC_* names into python class name 
# Warning: Do not remove or rename one of this class, they are used by the engine 
# Eventually add some custom properties 
@dataclass
class PiezoCfg(auto.T_PIEZO_CFG):
    """ Cfg class of Piezo device """
    
@dataclass
class PiezoCtrl(auto.T_PIEZO_CTRL):
    """ Ctrl class of Piezo device """
    
@dataclass
class PiezoStat(auto.T_PIEZO_STAT):
    """ Stat class of Piezo device """
    
@dataclass
class PiezoInfo(auto.T_PIEZO_INFO):
    """ Info class of Piezo device """
    date: str = "09 Oct 2024"            
    name: str = "FCF DeviceSimulatorPiezo"            
    platform: str = "WS Simulator"        



# Set Error description 
# one can redefine error description here if not define in define_auto
# e.g.  dvs.set_enum_text( PiezoError.LOCAL, 'Control not allowed. Motor in Local mode.')



