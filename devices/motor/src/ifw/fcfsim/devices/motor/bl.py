
from __future__ import annotations
import functools
import math
import time 
import random
from ifw.fcfsim import api as dvs
from ifw.fcfsim.api import sm 
from ifw.fcfsim.devices.motor import define as dfn 
from ifw.fcfsim.devices.motor.config import MotorSimConfig
from ifw.fcfsim.devices.motor.gen.iengine import  IMotorSimEngine, IMotorBl
from ifw.fcfsim.devices.motor import utils
from ifw.fcfsim.devices.motor.control import control
 

# ----------------------------------------------
# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.MotorState, dfn.MotorSubstate
motor_state_mapping: dict[str,tuple[dfn.MotorState, dfn.MotorSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready": (S.NOTOP, SS.NOTOP_READY), 
  "On::NotOperational::Aborting": (S.NOTOP, SS.NOTOP_ABORTING), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::NotOperational::Initialising": (S.NOTOP, SS.NOTOP_INITIALISING), 
  "On::Operational::Standstill": (S.OP, SS.OP_STANDSTILL), 
  "On::Operational::Moving": (S.OP, SS.OP_MOVING), 
  "On::Operational::SettingPosition": (S.OP, SS.OP_SETTING_POS), 
  "On::Operational::Stopping": (S.OP, SS.OP_STOPPING), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
}
del S, SS
# ----------------------------------------------

class MotorBl(IMotorBl):
    # This class must implement all abstract methods of IMotorBl

    def __init__(self, engine: IMotorSimEngine):
        self.engine: IMotorSimEngine = engine
        self._stat = dfn.MotorStat()
        self._cfg = dfn.MotorCfg()
        self._ctrl = dfn.MotorCtrl()
        self._info = dfn.MotorInfo()
 
    # ===================== Data Model ===========================

    @property
    def stat(self) -> dfn.MotorStat:
        return self._stat
    
    @property
    def cfg(self) -> dfn.MotorCfg:
        return self._cfg 
    
    @property
    def ctrl(self) -> dfn.MotorCtrl:
        return self._ctrl

    @property
    def info(self) -> dfn.MotorInfo:
        return self._info

    # ===================== shortcuts ============================
    @property
    def config(self) -> MotorSimConfig: return self.engine.get_config()
    
    @property
    def log(self) -> dvs.core.ILogger: return self.engine.log
    

    # ===================== Controlers ============================
    @functools.cached_property
    def controler(self) -> control.IControler:
        if self.config.model is None:
            return control.new_basic_controler(self)
        else:
            return control.new_model_controler(self)

    @property
    def initialiser(self) -> control.IInitialiser:
        if self.config.model is None:
            return control.new_basic_initialiser(self)
        else:
            return control.new_model_initialiser(self)

    # ========================= Helpers  ==============================

    def ok(self) -> int:
        """ reset rpc error and return OK code (should be 0) """
        self.set_rpc_error( dfn.MotorRpcError.OK)
        return int(dfn.MotorRpcError.OK)
    
    def wait_substate_changes(self, start_substate: int, timeout: float = 1.0) -> int:
        stat = self.stat
        try:
            dvs.core.wait( lambda : stat.substate!=start_substate, period=0.01, timeout=timeout)
        except TimeoutError:
            self.log.error(f'Simulator bug: substate was expected to change after timeout={timeout}s')
            raise self.error(999, f'Simulator bug: substate was expected to change after timeout={timeout}s')
        return self.ok()
    
    def is_moving(self) -> bool:
        """True if motor is moving from a move command (exclude init)"""
        return self.stat.substate == dfn.MotorSubstate.OP_MOVING
    
    def error(self,
              err_code: int,
              err_text: str | None = None) -> dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error(err_code, err_text)
        return dvs.RpcException(self.stat.rpc_error_text, self.stat.rpc_error_code)
        

    def set_error(self,  err_code: int, err_text: str | None = None)->None:
        """ set error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(dfn.MotorError, err_code, err_text)
        self.stat.error_code = code
        self.stat.error_text = text
    
    def clear_error(self) -> None:
        self.set_error(dfn.MotorError.OK)

    def set_rpc_error(self,  err_code: int, err_text:str | None = None)->None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(dfn.MotorRpcError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text

    def check_local(self)->None:
        """ Check if in Local mode, raise an RpcException if in local """
        # The RpcException is catched properly by the server and will return the error code 
        if self.stat.local:
            raise self.error( dfn.MotorRpcError.LOCAL )  
    
    def set_last_command(self,  command: dfn.MotorCommand)->None:
        """ Set the command (code & text) in engine stat """
        self.stat.last_command = command
        self.stat.last_command_text = command.name # Check if it has last_command_text ?
    
    def set_command(self, command: dfn.MotorCommand, wait: bool =False) -> int:
        """ Set the command in engine ctrl and fill stat accordingly 
        If wait is True, we wait that the command has been taken into account
        by the state machine process
        """
        substate = self.stat.substate
        self.ctrl.command = command 
        self.ctrl.execute = True 
        self.set_last_command(command)
        if wait:
            return self.wait_substate_changes(substate)
        return self.ok()
     
    def set_state(self, state: dfn.MotorState, substate: dfn.MotorSubstate) -> None:
        stat = self.stat
        stat.state = int(state) 
        stat.state_text =  dvs.core.get_enum_name(state)
        stat.substate = int(substate) 
        stat.substate_text = dvs.core.get_enum_name(substate)   
    
    def schedule(self, event_name:str, description: str = "") -> None:
        self.engine.schedule_event(sm.Event(event_name), description)


    #========================= RPC Methods Business Logic  =========================

    def reset(self) -> int:
        """ reset Motor """
        self.check_local()
        stat = self.stat
        stat.initialised = False

        self.set_command(dfn.MotorCommand.RESET)
        return self.ok()

    def stop(self) -> int:
        """ stop Motor """
        self.check_local()
        self.set_command(dfn.MotorCommand.STOP)
        return self.ok()

    def move_vel(self, vel: float) -> int:
        """ move_vel Motor """
        self.check_local()
        self.check_move_velocity_condition(int(math.copysign(1,vel)))
        ctrl = self.ctrl

        ctrl.move_type = dfn.MotorMoveType.VELOCITY
        ctrl.position = 0.0
        ctrl.velocity = vel
        self.set_command(dfn.MotorCommand.MOTOR_MOVE)
        return self.ok()

    def set_log(self, log: bool) -> int:
        """ set_log Motor """
        self.log.error("set_log received but not yet implemented")
        return 0

    def enable(self) -> int:
        """ enable Motor """
        self.check_local()
        stat = self.stat
        if stat.state != dfn.MotorState.NOTOP:
            raise self.error(dfn.MotorRpcError.NOT_NOTOP_NOTREADY)

        self.set_command(dfn.MotorCommand.ENABLE)
        return self.ok()

    def disable(self) -> int:
        """ disable Motor """
        self.check_local()
        stat = self.stat
        if stat.state != dfn.MotorState.OP:
            raise self.error(dfn.MotorRpcError.NOT_OP)

        self.set_command(dfn.MotorCommand.DISABLE)
        return self.ok()

    def init(self) -> int:
        """ init Motor """
        self.check_local()
        stat = self.stat
        stat.mode = dfn.MotorMode.POS

        if stat.substate == dfn.MotorSubstate.NOTOP_INITIALISING:
            return self.ok()

        if stat.substate not in [
            dfn.MotorSubstate.NOTOP_NOTREADY,
            dfn.MotorSubstate.NOTOP_ERROR
        ]:
            raise self.error(dfn.MotorRpcError.NOT_NOTOP_READY)

        self.set_command(dfn.MotorCommand.INIT)
        return self.ok()

    def check_move_condition(self, pos: float) -> None:
        """ Check condition to go to that position 

        Raise RpcException if this is not possible 
        """
        stat, cfg = self.stat, self.cfg

        if stat.state == dfn.MotorState.NOTOP:
            raise self.error(dfn.MotorRpcError.NOT_OP)
        if cfg.min_position != 0.0 or cfg.max_position != 0.0:
            if pos < cfg.min_position:
                raise self.error(dfn.MotorRpcError.SW_LIMIT_LOWER)
            if pos > cfg.max_position:
                raise self.error(dfn.MotorRpcError.SW_LIMIT_UPPER)
    
    def check_move_velocity_condition(self, direction: int) -> None:
        """ Check condition to go to that position 

        Raise RpcException if this is not possible 
        """
        stat, cfg = self.stat, self.cfg

        if stat.state == dfn.MotorState.NOTOP:
            raise self.error(dfn.MotorRpcError.NOT_OP)
        if cfg.min_position != 0.0 or cfg.max_position != 0.0:
            if direction<0  and stat.pos_actual <= cfg.min_position:
                raise self.error(dfn.MotorRpcError.SW_LIMIT_LOWER)
            if direction>0 and stat.pos_actual >= cfg.max_position:
                raise self.error(dfn.MotorRpcError.SW_LIMIT_UPPER)


    def move_rel(self, pos: float, vel: float) -> int:
        """ move_rel Motor """
        self.check_local()
        self.check_move_condition(pos + self.stat.pos_actual)
        ctrl = self.ctrl

        ctrl.move_type = dfn.MotorMoveType.RELATIVE
        ctrl.position = pos
        ctrl.velocity = vel
        self.set_command(dfn.MotorCommand.MOTOR_MOVE, wait=not self.is_moving())
        return self.ok()

    def move_abs(self, pos: float, vel: float) -> int:
        """ move_abs Motor """
        self.check_local()
        self.check_move_condition(pos)
        ctrl = self.ctrl

        ctrl.move_type = dfn.MotorMoveType.ABSOLUTE
        ctrl.position = pos
        ctrl.velocity = vel

        self.set_command(dfn.MotorCommand.MOTOR_MOVE, wait=not self.is_moving())
        return self.ok()

    def set_debug(self, debug: bool) -> int:
        """ set_debug Motor """
        self.log.error("set_debug received but not yet implemented")
        return self.ok()

    def auto_enable(self) -> None:
        """Enable the current on the axis, if disabled."""
        if not self.stat.enabled:
            self.stat.enabled = True

    def auto_disable(self) -> None:
        """Disable the current on the axis if the "Disable After Move" is enabled."""
        if self.cfg.disable_after_move:
            self.stat.enabled = False

    async def initialise(self) -> None:
        stat, cfg, ctrl, config = self.stat, self.cfg, self.ctrl, self.config
        cfg.min_position = config.min_position
        cfg.max_position = config.max_position
        cfg.timeout_init = int(config.timeout_init * 1000)
        cfg.timeout_move = int(config.timeout_move * 1000)
        cfg.timeout_switch = int(config.timeout_switch * 1000)
        cfg.disable_after_move = config.disable_after_move
        cfg.default_velocity = config.default_velocity

        ctrl.velocity = config.default_velocity
        ctrl.command = dfn.MotorCommand.NONE
        ctrl.execute = False

        stat.axis_ready = True
        stat.enabled = False
        self.log.info(f"Local Mode is: {config.local_mode}")
        stat.local = config.local_mode
        if config.sim_start_pos == -1: #do not like this
            pos  =\
                round(random.random() * (cfg.max_position -
                      cfg.min_position) + cfg.min_position, 6)
            self.controler.set_position(pos)
        else:
            self.controler.set_position(config.sim_start_pos)

        self.log.debug(
            f"Min Pos: {cfg.min_position}. Max Pos: {cfg.max_position}. Start Pos: {stat.pos_actual}"
        )
        stat.scale_factor = config.scale_factor
        stat.axis.units = "Degree"  # TODO Really ?? Should be configurable

        if config.auto_enter_op:
            ctrl.command = dfn.MotorCommand.INIT
            ctrl.execute = True

    async def next(self) -> None:
        self.handle_command()
        try:
            self.controler.next()
        except dvs.core.ActionError as ex:
            self.set_error(ex.get_error_code())
            if self.stat.substate != dfn.MotorSubstate.OP_ERROR:
                self.engine.schedule_event(sm.Event(dfn.MotorEvent.ErrFault), 
                                       f"Error in Engine {ex.get_error_code()}")
        

    def handle_command(self) -> None:
        if not self.ctrl.execute:
            return
        ctrl = self.ctrl 
        command = ctrl.command
        # reset controler
        ctrl.command = dfn.MotorCommand.NONE
        ctrl.execute = False
        # ---

        if command == dfn.MotorCommand.INIT:
            self.schedule(dfn.MotorEvent.Init, "Motor Initialisation")
        elif command == dfn.MotorCommand.MOTOR_MOVE:
            self.schedule(dfn.MotorEvent.Move, "Motor Movement")
        elif command == dfn.MotorCommand.ENABLE:
            self.schedule(dfn.MotorEvent.Enable, "Enable command")
        elif command == dfn.MotorCommand.DISABLE:
            self.controler.stop()
            self.schedule(dfn.MotorEvent.Disable, "Disable command")
        elif command == dfn.MotorCommand.RESET:
            self.schedule(
                dfn.MotorEvent.Reset,
                "Reset command"
            )
        elif command == dfn.MotorCommand.STOP:
            self.controler.stop()
            self.schedule(
                dfn.MotorEvent.Stop,
                "Stop command"
            )
        else:
            self.log.error(f"BUG: not implemented command: {command}")


    # ==================== State Machine Business Logic ==========================


    def listen_event(self, event: sm.Event) -> None:
        self.stat.event_desc = event.getId()

    def listen_state(self,
          status: set[sm.State]
                     ) -> None:
        # Inject SCXML state into engine state/substate data
        for state in status:
            if isinstance(state, sm.StateAtomic):
                if state.getId() == "Off":
                    self.engine.exit(0)
                    time.sleep(1.0)
                else:
                    try:
                        state, substate = motor_state_mapping[state.getId()]
                    except KeyError:
                        self.log.error(
                            f"Could not resolve state comming from state machine: {state.getId()}")
                    else:
                        self.set_state(state, substate)

    # ~~~~~~~~~~~~~~~~~ Activity Methods ~~~~~~~~~~~~~~~~~~

    def moving_activity(self,
          handler: sm.IActivityHandler
                        ) -> None:
        # we can make it faster than the main period this assure switch detection 
        # when controler has a real model 
        period = max(1. / self.config.update_frequency / 10.0, 1/1000.)
        if self.stat.mode == dfn.MotorMode.POS:
            timeout = self.cfg.timeout_move / 1000.0
        else:
            timeout = None
        
        # execute movement and store error or OK in stat.error_code
        try:
            utils.wait_for_movement(
                    self.controler, period, timeout, handler.is_running)
        except dvs.core.ActionError as exc:
            self.set_error(exc.get_error_code())
            event_name = utils.resolve_event_name_after_move(exc.get_error_code())
            handler.send_internal_event(sm.Event(event_name))
        else:
            self.clear_error()
            handler.send_internal_event(sm.Event(dfn.MotorEvent.MoveDone))
        self.controler.stop() # in case of error
        self.auto_disable()
                
    def stopping_activity(self,
          handler: sm.IActivityHandler
                          ) -> None:

        while handler.is_running():
            time.sleep(self.config.sim_delay)
            try:
                self.controler.stop()
            except dvs.core.ActionError as exc:
                self.set_error(exc.get_error_code())
                handler.send_internal_event(
                    sm.Event(dfn.MotorEvent.ErrFault))
            else:
                self.clear_error()
                handler.send_internal_event(
                    sm.Event(dfn.MotorEvent.StopDone))
            self.auto_disable()
            break

    def initialising_activity(self,
          handler: sm.IActivityHandler
                              ) -> None:
        self.auto_enable()
        try:
            self.initialiser.execute(handler.is_running)
        except dvs.core.ActionError as exc:
            self.set_error(exc.get_error_code())
            handler.send_internal_event(
                sm.Event(dfn.MotorEvent.ErrInit)
            )
        else:
            self.clear_error()
            handler.send_internal_event(
                sm.Event(dfn.MotorEvent.InitDone)
            )
            self.stat.initialised = True
        self.auto_disable()
        
        if self.config.auto_enter_op:
            self.engine.schedule_event(
                sm.Event(dfn.MotorEvent.Enable),
                "AUTO OP Enabled"
            )
 
    def aborting_activity(self,
          handler: sm.IActivityHandler
                          ) -> None:
        while handler.is_running():
            time.sleep(self.config.sim_delay)
            self.controler.stop()
            handler.send_internal_event(
                sm.Event(dfn.MotorEvent.StopDone)
            )
            self.auto_disable()
            handler.stop()
            break

    # ~~~~~~~~~~~~~~~~~ Action Methods ~~~~~~~~~~~~~~~~~~
    #  to be re-defined case by case if needed 
    
    # Check define_auto.MotorAction. Some of this actions exists in the Twincat (tmc file) 
    # but does not exist in the scxml file. Meaning that they will never be reached by 
    # the state machine 
    def set_action_description(self,
         handler: sm.IActionHandler,
         context: sm.Context
                        ) -> None:
        """ A Generic action method """
        del context  # context not used
        self.stat.event_desc = handler.get_id()
    
    def move_execute_action(self, 
          handler: sm.IActionHandler, 
          context: sm.Context
                        ) -> None:
        self.auto_enable()
        ctrl, stat = self.ctrl, self.stat
        if ctrl.move_type == dfn.MotorMoveType.ABSOLUTE:
            stat.pos_target = ctrl.position
            self.move_abs_action()

        elif ctrl.move_type == dfn.MotorMoveType.RELATIVE:
            stat.pos_target = stat.pos_actual + ctrl.position
            self.move_abs_action()
        else:
            self.move_vel_action()
        self.set_action_description(handler, context)
    
    def move_abs_action(self) -> None:
        stat, ctrl = self.stat, self.ctrl
        stat.mode = dfn.MotorMode.POS
        self.controler.move_abs(ctrl.position, ctrl.velocity)
    
    def move_vel_action(self) -> None:
        stat, ctrl = self.stat, self.ctrl
        stat.pos_target = 0.0
        stat.mode = dfn.MotorMode.VEL
        self.controler.move_vel(ctrl.velocity) # Send the motor in movement 

        if ctrl.velocity >= 0:
            ctrl.direction = dfn.McDirection.MC_CURRENT_DIRECTION
        else:
            ctrl.direction = dfn.McDirection.MC_NEGATIVE_DIRECTION

    def reset_execute_action(self,
            handler: sm.IActionHandler, 
            context: sm.Context
        ) -> None:
        self.controler.stop()
        self.set_action_description(handler, context)
    
    def stop_execute_action(self, 
             handler: sm.IActionHandler, 
             context: sm.Context
        ) -> None:
        self.controler.stop()
        self.set_action_description(handler, context)

    def init_execute_action(self, 
             handler: sm.IActionHandler, 
             context: sm.Context
        ) -> None:
        self.auto_enable()
        self.set_action_description(handler, context)

    def init_abort_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def init_complete_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	
    def err_execute_action(self,
       handler: sm.IActionHandler, 
       context: sm.Context
    ) -> None:
        self.set_action_description(handler, context)
	


if __name__=="__main__":
    _: type[IMotorBl] = MotorBl


