
from enum import IntEnum
from typing import ClassVar

from ifw.fcfsim.api import core


class MaEvent:
    """
    Ma events 
    """
    Move: ClassVar[str] =  "Move"
    ResetDone: ClassVar[str] =  "ResetDone"
    MoveDone: ClassVar[str] =  "MoveDone"
    Init: ClassVar[str] =  "Init"
    Disable: ClassVar[str] =  "Disable"
    StopTrack: ClassVar[str] =  "StopTrack"
    StopDone: ClassVar[str] =  "StopDone"
    DisableDone: ClassVar[str] =  "DisableDone"
    Reset: ClassVar[str] =  "Reset"
    InitDone: ClassVar[str] =  "InitDone"
    Enable: ClassVar[str] =  "Enable"
    EnableDone: ClassVar[str] =  "EnableDone"
    PresetDone: ClassVar[str] =  "PresetDone"
    HwOk: ClassVar[str] =  "HwOk"
    Error: ClassVar[str] =  "Error"
    Stop: ClassVar[str] =  "Stop"
    StartTrack: ClassVar[str] =  "StartTrack"
    Timeout: ClassVar[str] =  "Timeout"

class MaActivity:
    """
    MA activities 
    """
    Moving: ClassVar[str] =  "Moving"
    Stopping: ClassVar[str] =  "Stopping"
    Presetting: ClassVar[str] =  "Presetting"
    Disabling: ClassVar[str] =  "Disabling"
    Resetting: ClassVar[str] =  "Resetting"
    Enabling: ClassVar[str] =  "Enabling"
    Tracking: ClassVar[str] =  "Tracking"
    Initialising: ClassVar[str] =  "Initialising"
    Aborting: ClassVar[str] =  "Aborting"

class MaAction:
    ...

class E_MA_STATE(IntEnum):
    """
    Enumeration for E_MA_STATE generated automaticcaly from PLC lib tmc file
    """
    NONE = 0
    NOTOP = 1
    OP = 2


class E_MA_SUBSTATE(IntEnum):
    """
    Enumeration for E_MA_SUBSTATE generated automaticcaly from PLC lib tmc file
    """
    NONE = 0
    NOTOP_NOTREADY = 100
    NOTOP_READY = 101
    NOTOP_INITIALISING = 102
    NOTOP_ABORTING = 107
    NOTOP_RESETTING = 109
    NOTOP_ENABLING = 110
    NOTOP_ERROR = 199
    OP_DISABLING = 205
    OP_STANDSTILL = 216
    OP_MOVING = 217
    OP_SETTING_POS = 218
    OP_STOPPING = 219
    OP_TRACKING = 220
    OP_PRESETTING = 221
    OP_ERROR = 299

class E_MA_COMMAND(IntEnum):
    """
    Enumeration for E_MA_COMMAND generated automaticcaly from PLC lib tmc file
    """
    NONE = 0
    #  Commands corresponding to events - MUST match E_MA_EVENT !!!
    RESET = 1
    INIT = 2
    STOP = 3
    ENABLE = 4
    DISABLE = 5
    MA_MOVE = 6
    START_TRACK = 7
    STOP_TRACK = 8

class E_MA_RPC_ERROR(IntEnum):
    """
    Enumeration for E_MA_RPC_ERROR generated automaticcaly from PLC lib tmc file
    """
    OK =  0
    NOT_OP = -1
    NOT_NOTOP_READY = -2
    NOT_NOTOP_NOTREADY = -3
    LOCAL = -4
    SW_LIMIT_LOWER = -5
    SW_LIMIT_UPPER = -6
    INIT_WHILE_MOVING = -7
    VEL_ZERO = -8
    VEL_NEG = -9
    VEL_MAX = -10
    NOT_TRACKING = -11
    UNSAFE = -12


core.set_enum_text( E_MA_RPC_ERROR.OK, "OK"  )
core.set_enum_text( E_MA_RPC_ERROR.NOT_OP, "Cannot control motor. Not in OP state."  )
core.set_enum_text( E_MA_RPC_ERROR.NOT_NOTOP_READY, "Call failed. Not in NOTOP_READY."  )
core.set_enum_text( E_MA_RPC_ERROR.NOT_NOTOP_NOTREADY, "Call failed. Not in NOTOP_NOTREADY/ERROR."  )
core.set_enum_text( E_MA_RPC_ERROR.LOCAL, "RPC calls not allowed in Local mode."  )
core.set_enum_text( E_MA_RPC_ERROR.SW_LIMIT_LOWER, "Move rejected. Target Pos < Lower SW Limit"  )
core.set_enum_text( E_MA_RPC_ERROR.SW_LIMIT_UPPER, "Move rejected. Target Pos > Upper SW Limit"  )
core.set_enum_text( E_MA_RPC_ERROR.INIT_WHILE_MOVING, "Cannot INIT moving motor. Motor stopped. Retry."  )
core.set_enum_text( E_MA_RPC_ERROR.VEL_ZERO, "Set velocity cannot be zero"  )
core.set_enum_text( E_MA_RPC_ERROR.VEL_NEG, "Set velocity not allowed (<=0)"  )
core.set_enum_text( E_MA_RPC_ERROR.VEL_MAX, "Set velocity not allowed (>Vel_max)"  )
core.set_enum_text( E_MA_RPC_ERROR.NOT_TRACKING, "Not in tracking"  )
core.set_enum_text( E_MA_RPC_ERROR.UNSAFE, "Unsafe to move the motor"  )


MaSubstate = E_MA_SUBSTATE
MaState = E_MA_STATE
MaRpcError = E_MA_RPC_ERROR
MaCommand = E_MA_COMMAND

