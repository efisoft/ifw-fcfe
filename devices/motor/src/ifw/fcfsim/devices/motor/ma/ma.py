""" 
Define a class to handle most Multy Axes features  
"""
from __future__ import annotations

from contextlib import suppress
import functools
import math
import time
from dataclasses import dataclass, field
from typing import Any, Callable, Iterable, Protocol

from ifw.fcfsim import api as dvs
from ifw.fcfsim.api import RpcException, sm as sm
from ifw.fcfsim.devices.motor import sim as motor
from ifw.fcfsim.devices.motor.ma import define as dfn


class _ISm(Protocol):
    state: int 
    state_text: str
    substate: int
    substate_text: str
    event_desc: str 

class _IMaStat(Protocol):
    @property
    def sm(self) -> _ISm:
        ...
    local: bool 
    status_text: str
    rpc_error_text: str 
    rpc_error_code: int 
    error_code: int 


class _IMaCtrl(Protocol):
    command: int 
    execute: bool

class _IMaTout(Protocol):
    init_timeout: int 
    move_timeout: int 
    stop_timeout: int 

class _IMaCfg(Protocol):
    @property
    def timeout(self) -> _IMaTout:
        ...



# Define the state machine state to device (state, substate) mapping 
S, SS = OP = dfn.MaState, dfn.MaSubstate
pil_state_mapping: dict[str,tuple[dfn.MaState, dfn.MaSubstate]] =  { 
  "On::NotOperational": (S.NOTOP, SS.NONE), 
  "On::Operational": (S.OP, SS.NONE), 
  "On::On": (S.NONE, SS.NONE), 
  "On::NotOperational::NotReady": (S.NOTOP, SS.NOTOP_NOTREADY), 
  "On::NotOperational::Ready": (S.NOTOP, SS.NOTOP_READY), 
  "On::NotOperational::Initialising": (S.NOTOP, SS.NOTOP_INITIALISING), 
  "On::NotOperational::Aborting": (S.NOTOP, SS.NOTOP_ABORTING), 
  "On::NotOperational::Resetting": (S.NOTOP, SS.NOTOP_RESETTING), 
  "On::NotOperational::Enabling": (S.NOTOP, SS.NOTOP_ENABLING), 
  "On::NotOperational::Error": (S.NOTOP, SS.NOTOP_ERROR), 
  "On::Operational::Disabling": (S.OP, SS.OP_DISABLING), 
  "On::Operational::Standstill": (S.OP, SS.OP_STANDSTILL), 
  "On::Operational::Moving": (S.OP, SS.OP_MOVING), 
  "On::Operational::Stopping": (S.OP, SS.OP_STOPPING), 
  "On::Operational::Tracking": (S.OP, SS.OP_TRACKING), 
  "On::Operational::Presetting": (S.OP, SS.OP_PRESETTING), 
  "On::Operational::Error": (S.OP, SS.OP_ERROR), 
}
del S, SS


@dataclass
class MaStandard:
    """ Helper handling most of Standard Multy-axes Business logic
    
    Reset, Init, Enable, Disable, Stop
    """
    motors: list[motor.MotorSimEngine] | tuple[motor.MotorSimEngine, ...]
    stat: _IMaStat
    ctrl: _IMaCtrl
    cfg: _IMaCfg
    log: dvs.core.ILogger = dvs.core.get_logger("MA")

    def set_state(self, state: int, substate: int) -> None:
        """Set state and substate to the stat structure"""
        state = dfn.MaState(state)
        substate = dfn.MaSubstate(substate)
        self.stat.sm.state = state 
        self.stat.sm.state_text = state.name 
        self.stat.sm.substate = substate 
        self.stat.sm.substate_text = substate.name
   
    def resolve_state(self) -> None:
        """ Edit the multy axis state from motor states"""
        stat = self.stat
        for m in self.motors:
            if (stat.sm.substate != dfn.MaSubstate.OP_DISABLING 
                and m.stat.state < stat.sm.state):
                if m.stat.state == motor.define.MotorState.NOTOP:
                    self.set_state(dfn.MaState.NOTOP, dfn.MaSubstate.NOTOP_ERROR)
                else:
                    self.set_state(dfn.MaState.OP, dfn.MaSubstate.OP_ERROR)
                stat.status_text = 'Unexpected motor state change.'
                break
            if (m.stat.substate == motor.define.MotorSubstate.OP_ERROR ):
                if stat.sm.state == dfn.MaState.NOTOP:
                    self.set_state(dfn.MaState.NOTOP, dfn.MaSubstate.NOTOP_ERROR)
                else:
                    self.set_state(dfn.MaState.OP, dfn.MaSubstate.OP_ERROR)
                stat.status_text = 'Unexpected motor error state'
                break 
    
    def check_motor_state(self, 
            state: motor.define.MotorState, substate: motor.define.MotorSubstate | None = None
        ) -> bool:
        """Return True if each motor matches the given state & substate"""
        if substate is None:
            return all( m.stat.state == state for m in self.motors)
        return all((m.stat.state, m.stat.substate) == (state, substate) for m in self.motors)

    def check_local(self) -> None:
        """ Check if in Local mode, raise an RpcException if in local """
        # The RpcException is catched properly by the server and will return the error code 
        if self.stat.local:
            raise self.error(dfn.MaRpcError.LOCAL)  
        for mot in self.motors:
            if mot.stat.local:
                raise self.error(dfn.MaRpcError.LOCAL)
    
    def error(self,  err_code: int, err_text: str|None=None) -> dvs.RpcException:
        """ Build an RpcException to be raised, also update stat """
        self.set_rpc_error(err_code, err_text)
        return dvs.RpcException(self.stat.rpc_error_text, self.stat.rpc_error_code) 
    
    def ok(self) -> dfn.MaRpcError:
        """Reset rpc error and return OK code (should be 0) """
        self.set_rpc_error(dfn.MaRpcError.OK)
        return dfn.MaRpcError.OK

    def set_error(self, err_code: int, err_text: str="") -> None:
        """ set error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(motor.define.MotorError, err_code, err_text)
        self.stat.error_code = code
    
    def set_rpc_error(self, err_code: int, err_text: str | None=None) -> None:
        """ set rpc error, code & text,  in engine stat """
        code, text = dvs.core.get_code_and_text(dfn.MaRpcError, err_code, err_text)
        self.stat.rpc_error_code = code
        self.stat.rpc_error_text = text
    
    def wait_substate_changes(self, start_substate: int, timeout: float = 1.0) -> int:
        sm = self.stat.sm
        try:
            dvs.core.wait( lambda : sm.substate!=start_substate, period=0.01, timeout=timeout)
        except TimeoutError:
            self.log.error(f'Simulator bug: substate was expected to change after timeout={timeout}s')
            raise self.error(999, f'Simulator bug: substate was expected to change after timeout={timeout}s')
        return self.ok()
    
    def set_command(self, command: int, wait: bool =False) -> int:
        """ Set the command in engine ctrl and fill stat accordingly """
        substate = self.stat.sm.substate
        self.ctrl.command = command 
        self.ctrl.execute = True 
        self.set_last_command(command) 
        if wait:
            return self.wait_substate_changes(substate)
        return self.ok()

    def set_last_command(self,  command: int) -> None:
        """ Set the command (code & text) in engine stat """
        ... # Nothing in stat for last command  for multy-axis ??? 
        
    def reset(self) -> int:
        """ reset Ma """
        self.check_local()
        self.set_command( dfn.MaCommand.RESET )
        return self.ok()
  
    def enable(self) -> int:
        """ enable Ma """
        self.check_local()
        stat = self.stat		
        if stat.sm.state != dfn.MaState.NOTOP:
            raise self.error(dfn.MaRpcError.NOT_NOTOP_NOTREADY)
        
        self.set_command(dfn.MaCommand.ENABLE )
        return self.ok()
    
    def disable(self) -> int:
        """ enable Ma """
        self.check_local()
        stat = self.stat		
        if stat.sm.state != dfn.MaState.OP:
            raise self.error(dfn.MaRpcError.NOT_NOTOP_READY)
        
        self.set_command(dfn.MaCommand.DISABLE)
        return self.ok()
    
    def stop(self) -> int:
        """ stop Ma """
        self.check_local()
        self.set_command(dfn.MaCommand.STOP)
        return self.ok() 

    def init(self) -> int:
        """ init Ma """
        self.check_local()
        stat = self.stat 		
        if stat.sm.substate not in [
        	dfn.MaSubstate.NOTOP_NOTREADY,
        	dfn.MaSubstate.NOTOP_ERROR, 
            ]:
            raise self.error(dfn.MaRpcError.NOT_NOTOP_NOTREADY)
        
        self.set_command(dfn.MaCommand.INIT )
        return self.ok()

    async def initialise(self) -> None:
        for mot in self.motors:
            await mot.initialise()
    
    async def next(self) -> None:
        for mot in self.motors:
            await mot.next()

    def handle_ma_command(self, 
          command: int, 
          schedule_event: Callable[[sm.Event, str], None]
        ) -> bool:
        if command == dfn.MaCommand.RESET:
            for m in self.motors:
                with suppress(RpcException): # let state machine handle error
                    m.reset()
            schedule_event(sm.Event(dfn.MaEvent.Reset), "Reset") 
            return True
        
        if command == dfn.MaCommand.INIT:
            for m in self.motors:
                with suppress(RpcException):
                    m.init()
            schedule_event(sm.Event(dfn.MaEvent.Init), "Init") 
            return True
        
        if command == dfn.MaCommand.ENABLE:
            for m in self.motors:
                with suppress(RpcException):
                    m.enable()
            schedule_event(sm.Event(dfn.MaEvent.Enable), "Enable") 
            return True
        
        if command == dfn.MaCommand.DISABLE:
            for m in self.motors: 
                with suppress(RpcException):
                    m.disable()
            schedule_event(sm.Event(dfn.MaEvent.Disable), "Disable") 
            return True 
        
        if command == dfn.MaCommand.STOP:
            for m in self.motors:
                with suppress(RpcException):
                    m.stop()
            schedule_event(sm.Event(dfn.MaEvent.Stop), "Stop")
            return True 

        return False

    def listen_event(self, event: sm.Event) -> None:
        self.stat.sm.event_desc = event.getId()
    
    def listen_state(self,  
          status: set[sm.State], 
          exit_method: Callable[[int], Any],
        ) -> None:
        # Inject SCXML state into engine state/substate data 
        self._last_status_listened = status
        for state in status:
            if isinstance(state, sm.StateAtomic):
                if state.getId() == "Off":
                    exit_method(0) 
                    time.sleep(1)
                else:
                    try:
                        state, substate = pil_state_mapping[state.getId()]
                    except KeyError:
                        self.log.error(f"Could not resolve state comming from state machine: {state.getId()}")
                    else:
                        self.set_state(state, substate)


    def stopping_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        time_stat = time.time()
        while handler.is_running():
            if self.check_motor_state(
                motor.define.MotorState.OP, motor.define.MotorSubstate.OP_STANDSTILL
            ):
                handler.send_internal_event(sm.Event(dfn.MaEvent.StopDone))
                break 
            elif (time.time()-time_stat) > self.cfg.timeout.stop_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout while stopping motors"
                handler.send_internal_event(sm.Event(dfn.MaEvent.StopDone))
                break

    def disabling_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        time_stat = time.time()
        state_target = (motor.define.MotorState.NOTOP, motor.define.MotorSubstate.NOTOP_READY)
        while handler.is_running():
            if self.check_motor_state(*state_target):
                handler.send_internal_event(sm.Event(dfn.MaEvent.DisableDone)) 
                break
            if (time.time()-time_stat) > self.cfg.timeout.stop_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout on Enable"
                handler.send_internal_event(sm.Event(dfn.MaEvent.DisableDone)) 
                break 

    def resetting_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        time_stat = time.time()
        state_target = (motor.define.MotorState.NOTOP, motor.define.MotorSubstate.NOTOP_NOTREADY)
        while handler.is_running():
            if self.check_motor_state(*state_target):
                handler.send_internal_event(sm.Event(dfn.MaEvent.ResetDone)) 
                break
            if (time.time()-time_stat) > self.cfg.timeout.stop_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout on Reset"
                handler.send_internal_event(sm.Event(dfn.MaEvent.ResetDone))
                break

    def enabling_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        time_stat = time.time()
        state_target = (motor.define.MotorState.OP, motor.define.MotorSubstate.OP_STANDSTILL)
        while handler.is_running():
            if self.check_motor_state(*state_target):
                handler.send_internal_event(sm.Event(dfn.MaEvent.EnableDone))
                break
            if (time.time()-time_stat) > self.cfg.timeout.stop_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout on Enable"
                handler.send_internal_event(sm.Event(dfn.MaEvent.Error))
                break

    def initialising_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        time_stat = time.time()
        state_target = (motor.define.MotorState.NOTOP, motor.define.MotorSubstate.NOTOP_READY)
        while handler.is_running():
            if self.check_motor_state(*state_target):
                handler.send_internal_event( sm.Event( dfn.MaEvent.InitDone) )
                break
            if (time.time()-time_stat) > self.cfg.timeout.init_timeout/1000.0: 
                self.set_error(motor.define.MotorError.TIMEOUT_INIT)
                handler.send_internal_event( sm.Event( dfn.MaEvent.Reset) )
                break 

    def aborting_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        time_start = time.time()
        while handler.is_running():
            if self.check_motor_state(
                motor.define.MotorState.NOTOP, motor.define.MotorSubstate.NOTOP_INITIALISING
                ):
                if (time.time()-time_start) > self.cfg.timeout.stop_timeout/1000.0: 
                    self.stat.status_text = "ERROR: Motors initialization timeout"
                    handler.send_internal_event(sm.Event(dfn.MaEvent.Reset))
                    break
            else:
                    handler.send_internal_event(sm.Event(dfn.MaEvent.StopDone))
                    break


    def set_action_description(self,
       handler: sm.IActionHandler, 
       context: sm.Context
      ) -> None:
        """ A Generic action method """
        del context # context not used 
        self.stat.sm.event_desc = handler.get_id() 


class _IMaMoveCtrl(_IMaCtrl,Protocol):
    target_pos: dvs.core.Array[float]
    target_vel: float



class _IMaPosTracker(Protocol):
    def init(self, pos: float) -> None:
        ...

    def next(self) -> float:
        ...

@dataclass
class MaSinusTracker:
    """ Just return a sinus movement """
    period: float 
    amplitude: float 
    start_time: float = field(default=0.0, init=False)
    start_pos: float = field(default=0.0, init=False)

    def init(self, pos: float) -> None:
        self.start_time = time.time()
        self.start_pos = pos
    
    def next(self) -> float:
        dt = time.time() - self.start_time
        pos = math.sin( dt/self.period * 2*math.pi ) * self.amplitude
        return self.start_pos + pos

@dataclass
class MaMove:
    """ Helper handling Multy axis Movement Business logic
    
    Handle, move_abs, move_rel, move_vel RPC 
    Handle MA_MOVE command 
    """
    motors: list[motor.MotorSimEngine] | tuple[motor.MotorSimEngine, ...]
    stat: _IMaStat
    ctrl: _IMaMoveCtrl
    cfg: _IMaCfg
    log: dvs.core.ILogger = dvs.core.get_logger("MA")
    
    @functools.cached_property
    def ma_std(self) -> MaStandard:
        return MaStandard( 
             self.motors,self.stat, self.ctrl, self.cfg, self.log)
    
    def move_abs(self, axes: Iterable[int], pos: float, vel: float) -> int:
        """ move_abs from a list of axes index """
        self.ma_std.check_local()
        ctrl = self.ctrl
        for i, mot in enumerate(self.motors):
            ctrl.target_pos[i] = mot.stat.pos_actual
        for axis in axes:
            ctrl.target_pos[axis] = pos 
        ctrl.target_vel = vel
        self.ma_std.set_command(dfn.MaCommand.MA_MOVE)
        return self.ma_std.ok()

    
    def move_rel(self, axes: Iterable[int], pos: float, vel: float) -> int:
        """ move_rel from a list of axes index """
        self.ma_std.check_local()
        ctrl = self.ctrl
        for i, mot in enumerate(self.motors):
            ctrl.target_pos[i] = mot.stat.pos_actual
        for axis in axes:
            ctrl.target_pos[axis] = pos + self.motors[axis].stat.pos_actual
        ctrl.target_vel = vel
        self.ma_std.set_command(dfn.MaCommand.MA_MOVE)
        return self.ma_std.ok()

    def move_vel(self, axes: Iterable[int], vel: float) -> int:
        """ move_vel from a list of axes index """
        # Like it is done on FB_ADC, directly drive motors
        self.ma_std.check_local()
        for axis in axes:
            self.motors[axis].move_vel(vel)
        
        self.ma_std.set_command(dfn.MaCommand.NONE)
        return self.ma_std.ok()        

    def start_track(self)->int:
        """ start_track Drot """
        self.ma_std.check_local()
        self.ma_std.set_command(dfn.MaCommand.START_TRACK)
        return self.ma_std.ok() 

    def stop_track(self)->int:
        """ stop_track Drot """
        self.ma_std.check_local()
        self.ma_std.set_command(dfn.MaCommand.STOP_TRACK)
        return self.ma_std.ok() 
    
    def stop(self) -> int:
        return self.ma_std.stop()

    def handle_ma_move_command(self, 
        command: int, 
        schedule_event: Callable[[sm.Event, str], None],
     ) -> bool:
        ctrl = self.ctrl
        if command == dfn.MaCommand.MA_MOVE:
            for mot,pos in zip(self.motors, ctrl.target_pos): 
                mot.move_abs(pos, ctrl.target_vel)
            schedule_event(sm.Event(dfn.MaEvent.Move), "Move")
            return True

        if command == dfn.MaCommand.STOP:
            for m in self.motors:
                m.stop()
            schedule_event(sm.Event(dfn.MaEvent.Stop), "Stop")
            return True 

        if command == dfn.MaCommand.START_TRACK:
            schedule_event(sm.Event(dfn.MaEvent.StartTrack), "Start Tracking")
            return True 

        if command == dfn.MaCommand.STOP_TRACK:
            schedule_event(sm.Event(dfn.MaEvent.StopTrack), "Stop Tracking")
            return True 

        return False

    def moving_activity(self,
          handler: sm.IActivityHandler
        ) -> None:

        time_stat = time.time()
        time.sleep(1)
        while handler.is_running():
            if self.ma_std.check_motor_state(
                motor.define.MotorState.OP, motor.define.MotorSubstate.OP_STANDSTILL
            ):
                handler.send_internal_event(sm.Event(dfn.MaEvent.MoveDone))
                break 
            elif (time.time()-time_stat) > self.cfg.timeout.move_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout while moving motors"
                handler.send_internal_event(sm.Event(dfn.MaEvent.MoveDone))
                break
            
    def stopping_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        self.ma_std.stopping_activity(handler)
    
    def tracking_activity(self,
            handler: sm.IActivityHandler, 
            trackers: Iterable[_IMaPosTracker],
            update_frequency: float = 10
        ) -> None:
        timer = dvs.core.Timer()
        period = 1.0 / update_frequency
        trackers = tuple(trackers) 
        for tracker, mot in zip(trackers, self.motors):
            tracker.init(mot.stat.pos_actual)

        while handler.is_running():
            timer.tick()
            for tracker, mot in zip(trackers, self.motors):
                mot.ctrl.move_type = motor.define.MotorMoveType.ABSOLUTE
                mot.ctrl.position = tracker.next()
                mot.ctrl.command = motor.define.MotorCommand.MOTOR_MOVE
                mot.ctrl.execute = True 
            timer.tock()
            timer.sleep(period)
                  
    def presetting_activity(self,
          handler: sm.IActivityHandler, 
          preset_resolver: Callable[[], bool]
        ) -> None:

        time_stat = time.time()
        time.sleep(1)
        while handler.is_running():
            if self.ma_std.check_motor_state(
                motor.define.MotorState.OP, motor.define.MotorSubstate.OP_STANDSTILL
            ):
                handler.send_internal_event(sm.Event(dfn.MaEvent.PresetDone))
                break 
            
            elif preset_resolver():
                handler.send_internal_event(sm.Event(dfn.MaEvent.PresetDone))
                break 

            elif (time.time()-time_stat) > self.cfg.timeout.move_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout while moving motors"
                handler.send_internal_event(sm.Event(dfn.MaEvent.MoveDone))
                break

   

class _IMaMoveOneAxisCtrl(_IMaCtrl,Protocol):
    target_pos: float
    target_vel: float

@dataclass
class MaMoveOneAxis:
    """ Helper handling Multy axis Movement Business logic
    
    Handle, move_abs, move_rel, move_vel RPC 
    Handle MA_MOVE command 
    """
    motor: motor.MotorSimEngine
    stat: _IMaStat
    ctrl: _IMaMoveOneAxisCtrl
    cfg: _IMaCfg
    log: dvs.core.ILogger = dvs.core.get_logger("MA")
    
    @functools.cached_property
    def ma_std(self) -> MaStandard:
        return MaStandard( 
             (self.motor,),self.stat, self.ctrl, self.cfg, self.log)
    
    def move_abs(self, pos: float, vel: float) -> int:
        """ move_abs from a list of axes index """
        self.ma_std.check_local()
        ctrl = self.ctrl
        ctrl.target_pos = pos 
        ctrl.target_vel = vel
        self.ma_std.set_command(dfn.MaCommand.MA_MOVE)
        return self.ma_std.ok()

    
    def move_rel(self, pos: float, vel: float) -> int:
        """ move_rel from a list of axes index """
        self.ma_std.check_local()
        ctrl = self.ctrl
        ctrl.target_pos = pos + self.motor.stat.pos_actual
        ctrl.target_vel = vel
        self.ma_std.set_command(dfn.MaCommand.MA_MOVE)
        return self.ma_std.ok()

    def move_vel(self, vel: float) -> int:
        """ move_vel from a list of axes index """
        # Like it is done on FB_ADC, directly drive motors
        self.ma_std.check_local()
        self.motor.move_vel(vel)
        self.ma_std.set_command(dfn.MaCommand.NONE)
        return self.ma_std.ok()        
    
    def start_track(self) -> int:
        """ start_track Drot """
        self.ma_std.check_local()
        self.ma_std.set_command(dfn.MaCommand.START_TRACK)
        return self.ma_std.ok() 

    def stop_track(self) -> int:
        """ stop_track Drot """
        self.ma_std.check_local()
        self.ma_std.set_command(dfn.MaCommand.STOP_TRACK)
        return self.ma_std.ok() 
   
    def stop(self) -> int:
        return self.ma_std.stop()

    def handle_ma_move_command(self, 
        command: int, 
        schedule_event: Callable[[sm.Event, str], None],
     ) -> bool:
        ctrl = self.ctrl
        if command == dfn.MaCommand.MA_MOVE:
            self.motor.move_abs(ctrl.target_pos, ctrl.target_vel)
            schedule_event(sm.Event(dfn.MaEvent.Move), "Move")
            return True

        if command == dfn.MaCommand.STOP:
            self.motor.stop()
            schedule_event(sm.Event(dfn.MaEvent.Stop), "Stop")
            return True 
        
        if command == dfn.MaCommand.START_TRACK:
            schedule_event(sm.Event(dfn.MaEvent.StartTrack), "Start Tracking")
            return True 

        if command == dfn.MaCommand.STOP_TRACK:
            schedule_event(sm.Event(dfn.MaEvent.StopTrack), "Stop Tracking")
            return True 

        return False

    def moving_activity(self,
          handler: sm.IActivityHandler
        ) -> None:

        time_stat = time.time()
        time.sleep(1)
        while handler.is_running():
            if self.ma_std.check_motor_state(
                motor.define.MotorState.OP, motor.define.MotorSubstate.OP_STANDSTILL
            ):
                handler.send_internal_event(sm.Event(dfn.MaEvent.MoveDone))
                break 
            elif (time.time()-time_stat) > self.cfg.timeout.move_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout while moving motors"
                handler.send_internal_event(sm.Event(dfn.MaEvent.MoveDone))
                break
            
    def stopping_activity(self,
          handler: sm.IActivityHandler
        ) -> None:
        self.ma_std.stopping_activity(handler)

    def tracking_activity(self,
            handler: sm.IActivityHandler, 
            tracker: _IMaPosTracker,
            update_frequency: float = 10
        ) -> None:
        timer = dvs.core.Timer()
        period = 1.0 / update_frequency
        mot = self.motor
        tracker.init(self.motor.stat.pos_actual)

        while handler.is_running():
            timer.tick()
            mot.ctrl.move_type = motor.define.MotorMoveType.ABSOLUTE
            mot.ctrl.position = tracker.next()
            mot.ctrl.command = motor.define.MotorCommand.MOTOR_MOVE
            mot.ctrl.execute = True 
            timer.tock()
            timer.sleep(period)
                  
    def presetting_activity(self,
          handler: sm.IActivityHandler, 
          preset_resolver: Callable[[], bool]
        ) -> None:

        time_stat = time.time()
        time.sleep(1)
        while handler.is_running():
            if self.ma_std.check_motor_state(
                motor.define.MotorState.OP, motor.define.MotorSubstate.OP_STANDSTILL
            ):
                handler.send_internal_event(sm.Event(dfn.MaEvent.PresetDone))
                break 
            
            elif preset_resolver():
                handler.send_internal_event(sm.Event(dfn.MaEvent.PresetDone))
                break 

            elif (time.time()-time_stat) > self.cfg.timeout.move_timeout/1000.0: 
                self.stat.status_text = "ERROR: Timeout while moving motors"
                handler.send_internal_event(sm.Event(dfn.MaEvent.MoveDone))
                break

