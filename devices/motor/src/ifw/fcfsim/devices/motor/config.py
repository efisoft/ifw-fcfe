from __future__ import annotations

from dataclasses import dataclass
from typing import Any, Iterator, Optional

from ifw.fcfsim import api as dvs
from ifw.fcfsim.devices.motor.control import imodel, model_config


MOTOR_SCXML_PATH = "config/fcfsim/devices/motor/motor.scxml.xml"
MOTOR_NAMESPACE_PATH = "config/fcfsim/devices/motor/motor.namespace.yaml"

class MotorSimConfigLoader(dvs.SimConfigLoader):
    def dispatch(self, values: dict[str, Any]) -> Iterator[tuple[str, Any]]:
        model_file = values.pop("model", None)
        if model_file:
            yield "model", _load_model(model_file)
        yield from super().dispatch(values)

@dataclass
class MotorSimConfig(dvs.SimConfig):
    """ Motor configuration """
    log: str = "sim.motor"
    state_machine_scxml: str = MOTOR_SCXML_PATH 
    opcua_profile: str = MOTOR_NAMESPACE_PATH   
    velocity_error: float = 0.0
    sim_pos_error: float = 0.0
    sim_tolerance: float = 1
    sim_start_pos: float = 0.0
    scale_factor: float = 1.0
    min_position: float = 0.0
    max_position: float = 0.0
    timeout_init: float = 90
    timeout_move: float = 90
    timeout_switch: float = 300
    disable_after_move: bool = False
    default_velocity: float = 1.0
    model: Optional[imodel.IMotorModelConfig] = None 

######################################################################

def _load_model(model_file: str) -> imodel.IMotorModelConfig | None:
    """Load a motor model from the specified file

    or return None if the file path is empty or None.
    """
    if not model_file: 
        return None 
    return model_config.motor_model_config_factory.from_cii_config(model_file)

