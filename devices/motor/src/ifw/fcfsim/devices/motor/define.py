
# File generated once by Tmc2Fcs @efisoft on 17 Jun 2024
# Purpose of this file is to normalise some class names and add some informations that 
# could not be generated (e.g. error text explanations, or configuration class 
#
from __future__ import annotations
import enum 
from ifw.fcfsim.devices.motor.gen import define_auto as auto
from ifw.fcfsim.core import api as core
from dataclasses import dataclass, field

# Normalise Names from Auto Generated Enumerators  
# Edit if necessary. This Enums most likely used by the business logic 
MotorCommand = auto.E_MOTOR_COMMAND 
MotorError = auto.E_MOTOR_ERROR
MotorRpcError = auto.E_MOTOR_RPC_ERROR 
MotorState = auto.E_MOTOR_STATE
MotorSubstate = auto.E_MOTOR_SUBSTATE
MotorMode = auto.E_MOTOR_MODE

MotorSignal = auto.T_MOTOR_SIGNAL
MotorInitStep = auto.T_MOTOR_INIT_STEP
##### State Machine #####

MotorActivity = auto.MotorActivity
MotorAction = auto.MotorAction
MotorEvent = auto.MotorEvent
McDirection = auto.MC_Direction

# Wasn't generated and not in TMC :( 
class MotorMoveType(enum.IntEnum):
    NONE  = 0
    ABSOLUTE = 1 
    RELATIVE = 2 
    VELOCITY = 3 
    TRACKING = 4 

class MotorInitAction(enum.IntEnum):
	END	= 0
	FIND_INDEX = enum.auto()
	FIND_REF_LE = enum.auto()
	FIND_REF_UE = enum.auto()
	FIND_LHW = enum.auto()
	FIND_UHW = enum.auto()
	DELAY = enum.auto()
	MOVE_ABS = enum.auto()
	MOVE_REL = enum.auto()
	CALIB_ABS = enum.auto()
	CALIB_REL = enum.auto()
	CALIB_SWITCH = enum.auto()

class MotorSwitchIndexes(enum.IntEnum):
	LSTOP	= 0
	LHW		= 1
	REF		= 2
	INDEX	= 3
	UHW		= 4
	USTOP	= 5


##### Data Structures #####
# Transform TC name into python class name 
# Warning: Do not remove or rename one of this class, they are used by the engine 
# Eventually add some custom properties 
@dataclass
class MotorCfg(auto.T_MOTOR_CFG):
    """ Cfg class of Motor device """
    
    
@dataclass
class MotorCtrl(auto.T_MOTOR_CTRL):
    """ Ctrl class of Motor device """
    
    
@dataclass
class MotorInfo(auto.T_MOTOR_INFO):
    """ Info class of Motor device """
    date: str = "17 Jun 2024"            
    name: str = "FCF DeviceSimulatorMotor"            
    platform: str = "WS Simulator"        

@dataclass
class MotorStat(auto.T_MOTOR_STAT):
    """ Stat class of Motor device """
        
    last_command_text: str = "" # not in T_MOTOR_STAT !
    
    # ================= Some helpers ===================
    @property
    @core.dependencies("substate")
    def is_standstill(self) -> bool:
        return self.substate == MotorSubstate.OP_STANDSTILL

    @property
    @core.dependencies("substate")
    def is_moving(self) -> bool:
        return self.substate == MotorSubstate.OP_MOVING


# Set Error description 
# one can redefine error description here if not define in define_auto
# e.g.  dvs.core.set_enum_text( MotorError.LOCAL, 'Control not allowed. Motor in Local mode.')



