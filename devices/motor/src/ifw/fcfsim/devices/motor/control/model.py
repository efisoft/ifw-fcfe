from __future__ import annotations

import random
import time
from dataclasses import dataclass, field
from typing import Protocol

from ifw.fcfsim.devices.motor.control.imodel import (
    IMotorModel,
    IMotorModelConfig,
    ISwitcheStatus,
)


def rand(amplitude: float, ndigits:int = 6)->float:
    return round((random.random()-0.5)*amplitude*2, ndigits)

def add_noise(value: float, proportional: float, white: float = 0.0) -> float:
    """Add proportional and white noise to a value"""
    return value + rand(value*proportional) + rand(white)


@dataclass
class SwitchStatus:
    index: int
    activated: bool = False 

@dataclass
class MotorModelStatus:
    time: float = 0.0 
    position: float  = 0.0 
    pos_diff: float = 0.0
    encoder_position: int = 0
    velocity: float = 0.0  
    switches: list[ISwitcheStatus] = field(default_factory=list)


class StandardMotorModel(IMotorModel):
    config: IMotorModelConfig
    start_position: float 
    start_velocity: float
    power: float 

    time_scale: float 
    start_time: float 
    status: MotorModelStatus
    slowing_down: bool = False 

    def __init__(self, config: IMotorModelConfig, time_scale:float = 1.0):
        
        self.config = config
        self.time_scale = time_scale 
        self.status = MotorModelStatus()
        self.reset()

    def reset(self) -> None:
        config = self.config
        self.start_position = config.initial_position
        self.start_velocity = 0.0 

        self.power = 0.0 

        self.start_time = time.time()
        
        self.status.switches = [SwitchStatus(sw.index) for sw in self.config.switches]

        middle = (self.config.low_stop_position
                 +self.config.high_stop_position)/2.0
        if not middle==middle: middle = 0.0 # Nan
        self.start_position = middle

    def get_delta_time(self, time:float) -> float:
        return (time-self.start_time) * self.time_scale
    
    def compute_pos_vel(self, time:float) -> tuple[float,float, float]:
        """Compute the position at given time 

        Return the position and a flag True is the motor is at a low or high limit
        """
        dt = self.get_delta_time(time)
        target_velocity = self.config.max_velocity * add_noise(self.power, 0.01)
        if self.slowing_down: 
            velocity = max( -self.config.max_acceleration * dt + self.start_velocity, 
                            target_velocity)
        else:
            velocity = min(self.config.max_acceleration * dt + self.start_velocity, 
                            target_velocity)
        pos = self.start_position + velocity * dt
        
        #theoreticall position
        tpos = self.start_position + self.config.max_velocity*self.power * dt

        if pos > self.config.high_stop_position:
            return (self.config.high_stop_position, 
                    0.0, 
                    tpos-self.config.high_stop_position
                    )
        if pos < self.config.low_stop_position:
            return (self.config.low_stop_position,
                    0.0, 
                    tpos-self.config.low_stop_position 
                    )
        return pos, velocity, tpos-pos

    def drive(self, power: float) -> None:
        (self.start_position, 
         self.start_velocity, _) = self.compute_pos_vel(time.time())
        self.slowing_down = power< self.power
        self.power = min(1.0, max(power,-1.0))
        self.start_time  = time.time()

    def stop(self) -> None:
        self.drive(0.0)
    
    def get_status(self) -> MotorModelStatus:
        status = self.status 
        status.time = time.time()
        status.position, vel, pos_diff = self.compute_pos_vel(status.time)
        status.velocity = vel
        status.pos_diff = pos_diff 

        for sw_status, sw_conf in zip(status.switches, self.config.switches):
            # increase the switches window with the time scale 
            sw_status.activated =  abs(status.position-sw_conf.position) <= (sw_conf.window*self.time_scale)
        
        enc = status.position / self.config.encoder_size + self.config.encoder_offset
        enc = enc % self.config.encoder_overflow
        status.encoder_position = int(enc)
        return status 


# ########################################################################
#   Factory 

class IBlForModelConfig(Protocol):
    model: IMotorModelConfig

class IBlForModel(Protocol):
    config: IBlForModelConfig

def new_model(bl: IBlForModel) -> StandardMotorModel:
    """Create a new Model from a Motor business logic"""
    return StandardMotorModel(bl.config.model)

