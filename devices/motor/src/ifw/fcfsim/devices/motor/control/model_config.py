from __future__ import annotations

import math
from dataclasses import dataclass, field

from ifw.fcfsim.api import core
from ifw.fcfsim.devices.motor.control.imodel import ISwitchConfig
import typing 
from typing import Any
@dataclass
class SwitchConfig:
    index: int
    position: float 
    window: float


class MotorModelConfigLoader(core.ConfigLoader):
    def dispatch(self, values: dict[str, Any]) -> typing.Iterator[tuple[str, Any]]:
        if 'switches' in values:
            yield 'switches', [SwitchConfig(**d) for d in values.pop('switches')]
        yield from super().dispatch(values)


@dataclass
class MotorModelConfig:
    switches: list[ISwitchConfig] = field(default_factory=list)
    encoder_size: float = 1/1000 # size of one encoder in UU
    encoder_overflow: int = 2**16-1 # 16 bits encoder 
    encoder_offset: int = 500
    
    max_velocity: float = +math.inf
    max_acceleration: float = +math.inf
    max_deceleration: float = +math.inf
    
    low_stop_position: float = -math.inf 
    high_stop_position: float = +math.inf
     
    initial_position: float = 0.0 


motor_model_config_factory = MotorModelConfigLoader().new_factory(MotorModelConfig)
