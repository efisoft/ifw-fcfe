from __future__ import annotations
from abc import abstractmethod
from enum import IntEnum
import enum
from typing import Protocol

class ISwitcheStatus(Protocol):
    index: int
    activated: bool


class ISwitchConfig(Protocol):
    index: int
    position: float
    window: float

class IMotorModelConfig(Protocol):
    switches: list[ISwitchConfig]
    encoder_size: float
    encoder_overflow: int
    encoder_offset: int = 500
    max_velocity: float
    max_acceleration: float 
    low_stop_position: float 
    high_stop_position: float 

    initial_position: float


class IMotorModelStatus(Protocol):
    time: float 
    position: float 
    pos_diff: float
    encoder_position: int
    velocity: float 
    switches: list[ISwitcheStatus]


class IMotorModel(Protocol):
    
    @abstractmethod
    def reset(self) -> None:
        """Reset the model"""
        ...

    @abstractmethod
    def drive(self, power: float) -> None:
        """Move motor with a given velocity"""
        ...
    
    @abstractmethod
    def get_status(self) -> IMotorModelStatus: 
        """Return the modeled motor and switches status"""
        ...


class EPositionerMode(enum.IntEnum):
    NONE = 0
    POS = 1 
    VEL = 2
