from __future__ import annotations

import math
import random
import time
from dataclasses import dataclass, field
from typing import Literal, Protocol

from ifw.fcfsim.api import core
from ifw.fcfsim.devices.motor import define as dfn
from ifw.fcfsim.devices.motor.control.imodel import EPositionerMode


def rand(amplitude: float, ndigits:int = 6)->float:
    return round((random.random()-0.5)*amplitude*2, ndigits)

class IBasicControlerStat(Protocol):
    """Necessary Stat variables used by the Basic Controler"""
    pos_actual: float 
    vel_actual: float 
    pos_error: float
    mode: int 

class IBasicControlerCfg(Protocol):
    """Necessary cfg variables used by the Basic Controler"""
    min_position: float 
    max_position: float 

class IBasicControlerConfig(Protocol):
    """Necessary Simulator config used by the basic Controler"""
    sim_pos_error: float 
    velocity_error: float 
    sim_acceleration: float 
    sim_tolerance: float 

@dataclass 
class ControlInstance:
    direction: Literal[1] | Literal[-1] = 1
    move_start_time: float = field(default_factory=time.time)
    start_pos: float = 0.0
    target_position: float = 0.0
    velocity: float = 0.0
    move_type: EPositionerMode = EPositionerMode.NONE
    def time_elapsed(self)->float:
        return time.time() - self.move_start_time
    
    
@dataclass
class BasicControler:
    """A simple Positioner Compatible to what was done old simulator"""
    stat: IBasicControlerStat
    cfg: IBasicControlerCfg
    config: IBasicControlerConfig 
    
    control_instance: ControlInstance = field(default_factory=ControlInstance)

    def move_abs(self, 
         position: float, 
         velocity: float
        ) -> None:
        self.control_instance = ControlInstance(
            direction =  1 if position > self.stat.pos_actual else -1, 
            start_pos = self.stat.pos_actual, 
            target_position = position, 
            move_type = EPositionerMode.POS, 
            velocity = velocity, 
        )
    

    def move_vel(self, velocity: float) -> None:
        self.control_instance = ControlInstance(
            start_pos = self.stat.pos_actual, 
            direction = 1 if velocity>=0 else -1, 
            target_position = 0.0, 
            velocity = velocity, 
            move_type = EPositionerMode.VEL, 
        )

    def stop(self)->None:
        self.control_instance = ControlInstance() 
    
    def is_movement_done(self)->bool:
        return self.control_instance.move_type == EPositionerMode.NONE
  
    def set_position(self, position: float) ->  None:
        self.stat.pos_actual = position

    def next(self) -> None:
        if self.is_movement_done():
            return 

        stat,  config = self.stat, self.config

        ci = self.control_instance
        
        time_diff = ci.time_elapsed()

        stat.pos_error = rand(config.sim_pos_error)*\
                stat.pos_actual / 100.0
        vel_error =rand(config.velocity_error)*\
                stat.vel_actual / 100.0 
        stat.vel_actual = config.sim_acceleration * ci.velocity + vel_error 
        
        if ci.move_type == EPositionerMode.POS:
            stat.pos_actual = ci.start_pos + ci.direction * stat.vel_actual*time_diff 
        else:
            stat.pos_actual = ci.start_pos + stat.vel_actual*time_diff
        

        # Check if positioner has finished 
        if ci.move_type == EPositionerMode.POS:
            if abs(stat.pos_actual - ci.target_position) < config.sim_tolerance:
                self.stop() 
            else:
                # Backstop in case the velocity is too high for the given update frequency to detect
                # when the motor is in position (within the tolerance).
                if ci.direction == 1:
                    if stat.pos_actual > (ci.target_position - config.sim_tolerance):
                        self.stop()
                    
                elif ci.direction == -1:
                    if stat.pos_actual < (ci.target_position + config.sim_tolerance):
                        self.stop()
        
        if self.is_movement_done():
            stat.pos_actual = ci.target_position + stat.pos_error 
            stat.vel_actual = 0.0 

        if self.cfg.min_position != 0.0 or self.cfg.max_position!=0.0:
            if ci.direction<0 and stat.pos_actual <= self.cfg.min_position:
                self.stop()
                raise  core.ActionError(dfn.MotorError.SW_LIMIT_LOWER)

            if ci.direction>0 and stat.pos_actual >= self.cfg.max_position:
                self.stop()
                raise  core.ActionError(dfn.MotorError.SW_LIMIT_UPPER)


# ########################################################################
#   Factory 

class IBlForBasicControl(Protocol):
    """Protocol to build a basic controler"""
    @property
    def stat(self) -> IBasicControlerStat: ...
    @property
    def cfg(self) -> IBasicControlerCfg: ...
    @property
    def config(self) -> IBasicControlerConfig: ...

def new_basic_controler(bl: IBlForBasicControl) -> BasicControler:
    return BasicControler(bl.stat, bl.cfg, bl.config)



