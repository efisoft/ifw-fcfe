from __future__ import annotations

import functools
import math
import time
from dataclasses import dataclass, field
from typing import Callable, Protocol

from ifw.fcfsim.api import core
from ifw.fcfsim.devices.motor import define as dfn
from ifw.fcfsim.devices.motor.control.icontrol import IControler


class IModelInitialiserStat(Protocol):
    pos_actual: float 
    init_step: int 
    init_action: int 
    signals: core.Array[dfn.MotorSignal] 

class IModelInitialiserCfg(Protocol):
     timeout_init: int  
     str_arr_init_seq: core.Array[dfn.MotorInitStep]

@dataclass
class InitialisationInstance:
    is_alive: Callable[[], bool] = lambda : True
    start_time: float = field(default_factory=time.time)
    last_switch_pos: float | None = None


@dataclass
class ModelInitialiser:
    """Executor of the Initialisation sequence"""
    stat: IModelInitialiserStat
    cfg: IModelInitialiserCfg
    controler: IControler
    period: float 

    
    def find_switch_status(self, 
       velocity: float, 
       check_switch: Callable[[],bool], 
       init_instance: InitialisationInstance|None = None
                           ) -> None:
        """Move untils a switch change its particular status
        
        The targeted status of the switch is resolved from the 
        chek_switch() -> bool method.
        Velocity sign matters
        """    
        timeout = self.cfg.timeout_init/1000.
        if init_instance is None: 
            init_instance = InitialisationInstance() 
            
        timer = core.Timer()
        
        self.controler.move_vel(velocity)
        while init_instance.is_alive():
            timer.tick() 
            self.controler.next()
            if check_switch():
                init_instance.last_switch_pos = self.stat.pos_actual 
                self.controler.stop()
                return 

            if self.controler.is_movement_done(): # could have been stopped
                raise core.ActionError(dfn.MotorError.STOP)

            if (time.time()-init_instance.start_time) > timeout:
                self.controler.stop()
                raise core.ActionError(dfn.MotorError.TIMEOUT_INIT)
            timer.tock()
            timer.sleep(self.period)

    
    def find_switch(self, 
        switch_index: int, 
        velocity: float,
        init_instance: InitialisationInstance | None = None
                    ) -> None:
        """Move untils the switch is active""" 
        def check_switch() -> bool: return self.stat.signals[switch_index].active
        self.find_switch_status(velocity, check_switch,init_instance)
    
    def exit_switch(self, 
        switch_index: int, 
        velocity: float,
        init_instance: InitialisationInstance | None = None 
                    ) -> None:
        """Move until the switch is un-active""" 
        def check_switch() -> bool: return not self.stat.signals[switch_index].active
        self.find_switch_status(velocity, check_switch, init_instance)
    
    def find_uhw(self, 
        vel1: float, vel2: float, 
        init_instance: InitialisationInstance | None = None
        ) -> None:
        self.find_switch(dfn.MotorSwitchIndexes.UHW,  abs(vel1), init_instance=init_instance) 
        self.exit_switch(dfn.MotorSwitchIndexes.UHW, -abs(vel2), init_instance=init_instance) 

    def find_lhw(self, 
        vel1: float, vel2: float, 
        init_instance: InitialisationInstance | None = None
        ) -> None:
        self.find_switch(dfn.MotorSwitchIndexes.LHW,  -abs(vel1), init_instance=init_instance) 
        self.exit_switch(dfn.MotorSwitchIndexes.LHW,  +abs(vel2), init_instance=init_instance)
    
    def find_ref_le(self, 
        vel1: float, vel2: float, 
        init_instance: InitialisationInstance | None = None
        ) -> None:
        vel2 = -math.copysign(vel2, vel1) # vel2 is always oposite sign of v1
        self.find_switch(dfn.MotorSwitchIndexes.REF, vel1, init_instance=init_instance) 
        self.exit_switch(dfn.MotorSwitchIndexes.REF, vel2, init_instance=init_instance)

    def find_ref_ue(self, 
        vel1: float, vel2: float, 
        init_instance: InitialisationInstance | None = None
        ) -> None:
        # TODO : check the difference with find_ref_le
        vel2 = -math.copysign(vel2, vel1) # vel2 is always oposite sign of v1
        self.find_switch(dfn.MotorSwitchIndexes.REF, vel1, init_instance=init_instance) 
        self.exit_switch(dfn.MotorSwitchIndexes.REF, vel2, init_instance=init_instance)
    
    def find_index(self,
        vel1: float, vel2: float, 
        init_instance: InitialisationInstance | None = None
        ) -> None:
        # Not yet implemented 
        return 

    def delay(self, delay: float) -> None:
        time.sleep(delay)


    def move_abs(self, 
        position: float, velocity: float, 
        init_instance: InitialisationInstance | None = None) -> None:
        if init_instance is None: init_instance = InitialisationInstance()

        self.controler.move_abs(position, velocity)
        timer = core.Timer()

        timeout = self.cfg.timeout_init/1000.
        while init_instance.is_alive():
            timer.tick()
            self.controler.next() 
            if self.controler.is_movement_done():
                return 
            if (time.time()-init_instance.start_time) > timeout:
                raise core.ActionError(dfn.MotorError.TIMEOUT_INIT)
            timer.tock()
            timer.sleep(self.period)

    def move_rel(self, 
        position: float, velocity: float, 
        init_instance: InitialisationInstance | None = None) -> None:
        self.move_abs(self.stat.pos_actual+position, velocity, init_instance)
    

    def calib_abs(self, position: float) -> None:
        self.controler.set_position(position)

    def calib_rel(self, position: float) -> None:
        self.controler.set_position(self.stat.pos_actual + position) 
    
    def calib_switch(self, init_instance: InitialisationInstance) -> None:
        if init_instance.last_switch_pos:
            self.controler.set_position(init_instance.last_switch_pos)
        else:
            raise core.ActionError(dfn.MotorError.SWITCH_NOT_USED)

    def execute(self, is_alive: Callable[[],bool]) -> None:

        init_instance = InitialisationInstance(is_alive)
        
        for i,step in enumerate(self.cfg.str_arr_init_seq, start=1):
            self.stat.init_step = i 
            self.stat.init_action = step.action
            if step.action == dfn.MotorInitAction.END:
                return 

            elif step.action == dfn.MotorInitAction.FIND_INDEX:
                self.find_index(step.value1, step.value2, init_instance)
            elif step.action == dfn.MotorInitAction.FIND_REF_LE:
                self.find_ref_le(step.value1, step.value2, init_instance)
            elif step.action == dfn.MotorInitAction.FIND_REF_UE:
                self.find_ref_ue(step.value1, step.value2, init_instance)
            elif step.action == dfn.MotorInitAction.FIND_LHW:
                self.find_lhw(step.value1, step.value2, init_instance)
            elif step.action == dfn.MotorInitAction.FIND_UHW:
                self.find_uhw(step.value1, step.value2, init_instance)
            elif step.action == dfn.MotorInitAction.DELAY:
                self.delay(step.value1)
            elif step.action == dfn.MotorInitAction.MOVE_ABS:
                self.move_abs(step.value1, step.value2, init_instance)
            elif step.action == dfn.MotorInitAction.MOVE_REL:
                self.move_rel(step.value1, step.value2, init_instance)
            elif step.action == dfn.MotorInitAction.CALIB_ABS:
                self.calib_abs(step.value1)
            elif step.action == dfn.MotorInitAction.CALIB_REL:
                self.calib_rel(step.value1)
            elif step.action == dfn.MotorInitAction.CALIB_SWITCH:
                self.calib_switch(init_instance)


# ########################################################################
#   Factory 

class IBlForModelInitialiserConfig(Protocol):
    update_frequency: float 

class IBlForModelInitialiser(Protocol):
    @property 
    def stat(self) ->  IModelInitialiserStat : ...
    @property 
    def cfg(self) ->  IModelInitialiserCfg: ...
    @property 
    def config(self) ->  IBlForModelInitialiserConfig: ...
    @functools.cached_property
    def controler(self) -> IControler:
        ...

def new_model_initialiser(bl: IBlForModelInitialiser) -> ModelInitialiser:
    return ModelInitialiser( 
            stat = bl.stat, cfg = bl.cfg , controler= bl.controler, 
            period = max( 1.0 / bl.config.update_frequency / 50, 1/1000.) 
        )
