#####
#  API for the controler sub package 
#  This could be at motor root, but this is just a way to organise 
#  packages.

from ifw.fcfsim.devices.motor.control.imodel import ( 
    IMotorModel as IMotorModel,
    IMotorModelConfig as IMotorModelConfig,
)

from ifw.fcfsim.devices.motor.control.icontrol import ( 
    IControler as IControler, 
    IInitialiser as IInitialiser, 
)

from ifw.fcfsim.devices.motor.control.basic_controler import ( 
   BasicControler as BasicControler, 
   new_basic_controler as new_basic_controler,
)

from ifw.fcfsim.devices.motor.control.basic_initialiser import ( 
   BasicInitialiser as BasicInitialiser, 
   new_basic_initialiser as new_basic_initialiser
)

from ifw.fcfsim.devices.motor.control.model_controler import ( 
   ModelControler as ModelControler, 
   new_model_controler as new_model_controler,
)

from ifw.fcfsim.devices.motor.control.model_initialiser import ( 
   ModelInitialiser as ModelInitialiser, 
   new_model_initialiser as new_model_initialiser
)

from ifw.fcfsim.devices.motor.control.model_config import ( 
   SwitchConfig as SwitchConfig, 
   MotorModelConfig as MotorModelConfig, 
)

from ifw.fcfsim.devices.motor.control.model import ( 
   StandardMotorModel as StandardMotorModel, 
   SwitchStatus as SwitchStatus, 
   new_model as new_model
)

from ifw.fcfsim.devices.motor.control.utils import ( 
    wait_for_movement as wait_for_movement
)
