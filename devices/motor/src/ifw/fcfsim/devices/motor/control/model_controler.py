from __future__ import annotations

import math
import time
from dataclasses import dataclass, field
from typing import Literal, Protocol, cast

from ifw.fcfsim.api import core
from ifw.fcfsim.devices.motor import define as dfn
from ifw.fcfsim.devices.motor.control.icontrol import EPositionerMode
from ifw.fcfsim.devices.motor.control.imodel import (
    IMotorModel,
    IMotorModelConfig,
    IMotorModelStatus,
)
from ifw.fcfsim.devices.motor.control.model import MotorModelStatus, StandardMotorModel


class IModelControlerStat(Protocol):
    pos_actual: float 
    vel_actual: float 
    pos_error: float
    mode: int 
    enabled: bool
    signals: core.Array[dfn.MotorSignal] 

class IModelControlerCfg(Protocol):
    min_position: float 
    max_position: float 

class IModelControlerSimConfig(Protocol):
    sim_pos_error: float 
    sim_tolerance: float 

@dataclass 
class MovementInstance:
    move_type: EPositionerMode = EPositionerMode.NONE
    target_position: float = 0.0
    start_time: float = field(default_factory=time.time)
    direction: Literal[-1] | Literal[1] = 1
    

@dataclass
class ModelControler:
    """A positioner using a Motor Model"""
    model: IMotorModel
    stat: IModelControlerStat
    cfg:  IModelControlerCfg
    config:  IModelControlerSimConfig 
    max_velocity: float

    model_status: IMotorModelStatus = field(default_factory=MotorModelStatus)
    movement_instance: MovementInstance = field(default_factory=MovementInstance) 

    def move_abs(self, 
          position: float, 
          velocity: float, 
        ) -> None:
        self.movement_instance = MovementInstance( 
            target_position = position,
            move_type = EPositionerMode.POS,
            direction =  1 if position > self.stat.pos_actual else -1, 
        )
        self.model.drive(self.movement_instance.direction * velocity/self.max_velocity)
      
    def move_vel(self, velocity: float) -> None:
        self.movement_instance = MovementInstance( 
           direction = 1 if velocity >= 0.0  else -1, 
           move_type = EPositionerMode.VEL
        )
        self.model.drive(velocity/self.max_velocity)

    def is_movement_done(self) -> bool:
        return self.movement_instance.move_type == EPositionerMode.NONE
    
    def stop(self) -> None:
        self.model.drive(0.0)
        self.movement_instance = MovementInstance()

    def set_position(self, position: float) -> None:
        self.position_offset = -self.model.get_status().position + position

    def next(self) -> None:
        stat, config = self.stat, self.config
        ms = self.model.get_status()
        self.model_status = ms
        mi = self.movement_instance 
        
        stat.pos_actual = ms.position + self.position_offset
        stat.vel_actual = ms.velocity 
        stat.pos_error  = ms.pos_diff

        for switch in ms.switches:
            stat.signals[switch.index].active = switch.activated

        if stat.signals[dfn.MotorSwitchIndexes.USTOP].active and mi.direction>0:
            self.stop()
            return None 
        
        if stat.signals[dfn.MotorSwitchIndexes.LSTOP].active and mi.direction<0:
            self.stop()
            return None 
        
        if self.cfg.min_position != 0.0 or self.cfg.max_position!=0.0:
            if mi.direction<0  and stat.pos_actual <= self.cfg.min_position:
                self.stop()
                raise core.ActionError(dfn.MotorError.SW_LIMIT_LOWER)

            if mi.direction>0 and stat.pos_actual >= self.cfg.max_position:
                self.stop()
                raise core.ActionError(dfn.MotorError.SW_LIMIT_UPPER)
        

        if mi.move_type == EPositionerMode.POS:
            if abs(stat.pos_actual - mi.target_position) < config.sim_tolerance:
                self.stop()
            elif self.stat.enabled:
                # we went to far, go to the other direction with 
                # a half the speed 
                if mi.direction == 1:
                    if stat.pos_actual > (mi.target_position - config.sim_tolerance):
                        self.model.drive(-ms.velocity/2.0 / self.max_velocity)
                        mi.direction = -mi.direction
                elif mi.direction == -1:
                    if stat.pos_actual < (mi.target_position + config.sim_tolerance):
                        self.model.drive(-ms.velocity/2.0/ self.max_velocity)
                        mi.direction = -mi.direction
            

# ########################################################################
#   Factory 

class IBlForModelControlerConfig(IModelControlerSimConfig, Protocol):
    model: IMotorModelConfig | None


class IBlForModelControler(Protocol):
    @property
    def stat(self) -> IModelControlerStat: ...
    @property
    def cfg(self) -> IModelControlerCfg: ...
    @property
    def config(self) -> IBlForModelControlerConfig: ...

def new_model_controler(bl: IBlForModelControler) -> ModelControler:
    if bl.config.model is None:
        raise ValueError("Noe Model configuration found")
    model = StandardMotorModel(bl.config.model)
    return ModelControler( 
            model, 
            stat = bl.stat, cfg = bl.cfg, 
            config = cast( IModelControlerSimConfig, bl.config), 
            max_velocity= model.config.max_velocity
        )
