from __future__  import annotations
from dataclasses import dataclass
from typing import Callable, Protocol
import time

@dataclass
class BasicInitialiser:
    """Motor Basic initialiser.

    It just wait for a given amount of time 
    """
    init_time: float =  0.0

    def execute(self, is_alive: Callable[[],bool]) -> None:
        del is_alive
        time.sleep(self.init_time)


# ########################################################################
#   Factory 

class IBasicInitialiserConfig(Protocol):
    """Config protocol to run a Basic Initialiser"""
    sim_init_time: float 

class IBlForBasicInitialiser(Protocol):
    """Protocol to build a basic Initialiser"""
    @property 
    def config(self) -> IBasicInitialiserConfig: ...
    
def new_basic_initialiser(bl: IBlForBasicInitialiser) -> BasicInitialiser:
    return BasicInitialiser(bl.config.sim_init_time)
