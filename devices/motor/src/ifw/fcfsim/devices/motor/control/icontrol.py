from __future__ import annotations
import enum
from typing import Callable, Protocol


class EPositionerMode(enum.IntEnum):
    NONE = 0
    POS = 1
    VEL = 2


class IControler(Protocol):
    """Interface for Controler object used in Motor Devsim business logic"""

    def move_abs(self, position: float, velocity: float) -> None:
        ...

    def move_vel(self, velocity: float) -> None:
        ...

    def set_position(self, position: float) -> None:
        ...

    def next(self) -> None:
        ...

    def is_movement_done(self) -> bool:
        ...

    def stop(self) -> None:
        ...


class IInitialiser(Protocol):
    """Used for motor initialisation"""

    def execute(self, is_alive: Callable[[], bool]) -> None:
        """Return None but raise an core.ActionError

        The raised ActionError.error hold a MotorError enum code 
        """
        ...
