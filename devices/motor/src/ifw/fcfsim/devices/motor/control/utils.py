from __future__ import annotations
import time
from typing import Callable
from ifw.fcfsim.devices.motor.control import icontrol
from ifw.fcfsim.devices.motor import define as dfn 
from ifw.fcfsim.api import core 


def wait_for_movement(
     controler: icontrol.IControler, 
     period: float,
     timeout: float | None = None,
     is_running: Callable[[],bool] = lambda: True
    ) -> None:
    """Check Periodicaly if a movement is done 
    
    Return when movement is done 
    Raise a ActionError if  timeout  
    """
    timer = core.Timer()
    start_time = time.time()
    if timeout is None:
        def check_timeout() -> bool: return False 
    else:
        def check_timeout() -> bool: return (time.time()-start_time) > timeout

    while is_running():
        timer.tick()
        controler.next()
        if controler.is_movement_done():
            return 
        if check_timeout():
            raise core.ActionError(dfn.MotorError.TIMEOUT_MOVE)
        timer.tock()
        timer.sleep(period)    



