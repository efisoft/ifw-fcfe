from __future__ import annotations
import time
from typing import Callable
from ifw.fcfsim.devices.motor.control import control
from ifw.fcfsim.devices.motor import define as dfn 
from ifw.fcfsim.core import api as core 


def wait_for_movement(
     controler: control.IControler, 
     period: float,
     timeout: float | None = None,
     is_running: Callable[[],bool] = lambda: True
    ) -> None:
    """Check Periodicaly if a movement is done 
    
    Return when movement is done 
    Raise a ActionError if  timeout  
    """
    timer = core.Timer()
    start_time = time.time()
    if timeout is None:
        def check_timeout() -> bool: return False 
    else:
        def check_timeout() -> bool: return (time.time()-start_time) > timeout

    while is_running():
        timer.tick()
        controler.next()
        if controler.is_movement_done():
            return 
        if check_timeout():
            raise core.ActionError(dfn.MotorError.TIMEOUT_MOVE)
        timer.tock()
        timer.sleep(period)   



def resolve_event_name_after_move(error: int) -> str:
    """Return an Event name resolved from a MotorError, result of a movement"""
    if error == dfn.MotorError.OK:
        return dfn.MotorEvent.MoveDone
    elif error == dfn.MotorError.TIMEOUT_MOVE:
        return dfn.MotorEvent.Timeout
    elif error == dfn.MotorError.SW_LIMIT_UPPER:
        return dfn.MotorEvent.ErrUpperLimit
    elif error == dfn.MotorError.SW_LIMIT_LOWER:
       return dfn.MotorEvent.ErrLowerLimit
    elif error in (
            dfn.MotorError.BRAKE_ENGAGE, 
            dfn.MotorError.BRAKE_ACTIVE,
            dfn.MotorError.BRAKE_DISENGAGE):
        return dfn.MotorEvent.ErrBrake
    elif error == dfn.MotorError.IN_POS:
        return dfn.MotorEvent.ErrInPos
        
    elif error == dfn.MotorError.TIMEOUT_USER_PREMOVE:
        return dfn.MotorEvent.ErrUserPreMove
    elif error == dfn.MotorError.TIMEOUT_USER_POSTMOVE:
         return dfn.MotorEvent.ErrUserPostMove
    else:
        return dfn.MotorEvent.ErrFault

