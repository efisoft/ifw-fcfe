#  ____   ___    _   _  ___ _____   _____ ____ ___ _____ 
# |  _ \ / _ \  | \ | |/ _ \_   _| | ____|  _ \_ _|_   _|
# | | | | | | | |  \| | | | || |   |  _| | | | | |  | |  
# | |_| | |_| | | |\  | |_| || |   | |___| |_| | |  | |  
# |____/ \___/  |_| \_|\___/ |_|   |_____|____/___| |_|  
#
# This file was automaticaly generated by Tmc2Fcs @efisoft on 10 Feb 2025
# You should normaly be abble to code all business logic by edditing bl.py and define.py
# which are safely protected. 
# If an edit is however needed (it shouldn't be) one must rename this files (and related imports) 
# so they are safelly protected 
#


from __future__ import annotations
from dataclasses import dataclass, field
import functools
from ifw.fcfsim import api as dvs
from ifw.fcfsim.devices.motor import define as dfn 

log = dvs.get_logger("devsim.motor")

@dvs.core.attrconnect 
@dataclass
class MotorClientEngine:
    """ Client Engine object for the Motor """
    log: dvs.core.ILogger = log   
    ua_info: dvs.opcua.UaInfo = field(default_factory=dvs.opcua.UaInfo) 

    cfg: dfn.MotorCfg = field(default_factory=dfn.MotorCfg) 
    ctrl: dfn.MotorCtrl = field(default_factory=dfn.MotorCtrl) 
    info: dfn.MotorInfo = field(default_factory=dfn.MotorInfo) 
    stat: dfn.MotorStat = field(default_factory=dfn.MotorStat) 
    
    def get_signals(self) -> dvs.core.ISignals:
        """Signals handler listen by the client connection application"""
        return dvs.core.get_global_signals()
    # ============= Commands ========================

    def reset(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute reset method."
                         " Waitinng for a client connection")

    def stop(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute stop method."
                         " Waitinng for a client connection")

    def move_vel(self, vel: float) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute move_vel method."
                         " Waitinng for a client connection")

    def set_log(self, log: bool) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute set_log method."
                         " Waitinng for a client connection")

    def enable(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute enable method."
                         " Waitinng for a client connection")

    def disable(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute disable method."
                         " Waitinng for a client connection")

    def init(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute init method."
                         " Waitinng for a client connection")

    def move_rel(self, pos: float, vel: float) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute move_rel method."
                         " Waitinng for a client connection")

    def move_abs(self, pos: float, vel: float) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute move_abs method."
                         " Waitinng for a client connection")

    def set_debug(self, debug: bool) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute set_debug method."
                         " Waitinng for a client connection")

    async def async_reset(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute reset method."
                         " Waitinng for a client connection")
       
    async def async_stop(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute stop method."
                         " Waitinng for a client connection")
       
    async def async_move_vel(self, vel: float) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute move_vel method."
                         " Waitinng for a client connection")
       
    async def async_set_log(self, log: bool) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute set_log method."
                         " Waitinng for a client connection")
       
    async def async_enable(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute enable method."
                         " Waitinng for a client connection")
       
    async def async_disable(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute disable method."
                         " Waitinng for a client connection")
       
    async def async_init(self) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute init method."
                         " Waitinng for a client connection")
       
    async def async_move_rel(self, pos: float, vel: float) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute move_rel method."
                         " Waitinng for a client connection")
       
    async def async_move_abs(self, pos: float, vel: float) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute move_abs method."
                         " Waitinng for a client connection")
       
    async def async_set_debug(self, debug: bool) -> int:
        # this method will be overided on instance when connected to Client
        raise ValueError("Cannot execute set_debug method."
                         " Waitinng for a client connection")
       



