from __future__ import annotations
from dataclasses import dataclass, field
import functools
from queue import Queue
from typing import Any, Callable,  Iterator, Optional,  overload
import scxml4py.action
import scxml4py.event 

from ifw.fcfsim.sm.iaction import (
        IAction,
        IActionEngine,
        IActionExecuteFunction,
        IActionExecuteMethod,
        IActionHandler, 
    )

# ######
# Inerit from scaml4py action but bring some composition architechture 
class Action(scxml4py.action.Action): # type:ignore[misc] # mypy is confused
    """ Scml4py Action compatible using user defined IActionEngine 
    
    This use a composition rather then class innerithance architecture 
    In order to define an activity. 

    The action engine must provide execute and eveluate methods
    """
    action_engine: IActionEngine  
    def __init__(self, 
            id: str, 
            action_engine: IActionEngine | IActionExecuteFunction, 
            event_queue: Queue[scxml4py.event.Event]|None = None
        )->None:
        super().__init__(id, event_queue)
        self.action_engine = parse_action(action_engine) 

    def execute(self, context:scxml4py.context.Context)->None:
        self.action_engine.execute(self, context)

    def evaluate(self, context:scxml4py.context.Context)->bool: 
        return self.action_engine.evaluate(self, context)
    
    # standardise some methods so this class is compatible with ActionHandler  
    def get_id(self)->str:
        """ aslias of getId(), return Action Id """
        return self.getId() # type:ignore[no-any-return]
    
    def send_internal_event(self, event:scxml4py.event.Event)->None:
        self.sendInternalEvent(event)

def set_action_method(id:str, method: IActionExecuteMethod)->IActionExecuteMethod:
    method.__sm_action__ = True # type:ignore 
    method.__sm_id__ = id #type:ignore  
    return method

def is_action_method(obj:Any)->bool:
    """ True if input object is a method decorated with action_method """
    try:
        return obj.__sm_action__ # type:ignore[no-any-return]
    except AttributeError:
        return False

@overload
def action_method(id:str)-> Callable[ [IActionExecuteMethod], IActionExecuteMethod]:
    ...

@overload
def action_method(id:str, method:IActionExecuteMethod)->  IActionExecuteMethod:
    ...

def action_method(
        id: str, 
        method: Optional[IActionExecuteMethod] = None
    )-> Callable[ [IActionExecuteMethod], IActionExecuteMethod] | IActionExecuteMethod:

    if method is None:
        # This is a decorator, return a function 
        # to add sm properties to the method directly 
        def create_action(method:IActionExecuteMethod)->IActionExecuteMethod:
            return set_action_method( id, method)
        return create_action
    else:
        # The wrapping is necessary to avoid erasing
        # already set method
        @functools.wraps(method)
        def wraped_method(*args:Any, **kwargs:Any)->Any:
            return method(*args, **kwargs)
        return set_action_method( id, wraped_method) 


def action(
     id: str,  
     function: IActionExecuteFunction, 
     queue: Queue[scxml4py.event.Event]|None = None
    )->Action:
    """ Create an action from a method """
    if hasattr( function, "__call__"):
        return Action( id, ActionWrapper(function), queue )
    raise ValueError( f"Expecting a callable as action executor got a {type(function)}")


def collect_actions(
      obj:object, 
      event_queue: Queue[scxml4py.event.Event]|None=None
    )->Iterator[IAction]:
    """ Iterator on actions present in the input object 
    
    @action_method decorated method will be transformed to Action 
    """
    for attr in dir( obj ):
        try:
            member = getattr(obj, attr)
        except AttributeError:
            continue
              
        if isinstance(member, scxml4py.action.Action):
            yield member 
        elif is_action_method(member):
            yield action(member.__sm_id__, member, event_queue)

@dataclass
class ActionWrapper:
    """ Wrap a function/method into an ActionRunner """
    func: IActionExecuteFunction

    def execute(self, handler:IActionHandler, context: scxml4py.context.Context)->None:
        return self.func(handler, context)

    def evaluate(self, handler:IActionHandler, context: scxml4py.context.Context)->bool:
        del handler, context # Not used here
        return False 

def parse_action(action:IActionEngine|IActionExecuteFunction)->IActionEngine:
    """ Parse an ActionExecutor 
    
    A function will be wrapped inside an ActionExecutor 
    """
    if isinstance( action, IActionEngine):
        return action 
    if hasattr(action, "__call__"):
        return ActionWrapper(action)
    raise ValueError(f"Action must be conform to ActionExecutor Protocol or a callable, got a {type(action)}" )

@dataclass
class ActionManager:
    actions: list[IAction] = field(default_factory=list)
    
    def add(self, action: IAction)->None:
        self.actions.append(action)

    def collect(self, obj: Any, queue: Queue[scxml4py.event.Event]|None = None)->None:
        self.actions.extend( collect_actions(obj, queue) )

    def get_actions(self)->list[IAction]:
        return self.actions


