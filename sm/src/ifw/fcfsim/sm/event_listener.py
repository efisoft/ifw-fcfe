from __future__ import annotations

from dataclasses import dataclass, field
import functools
from typing import Any, Callable, Iterator,  Union, overload
import scxml4py.listeners 
import scxml4py.event 

from ifw.fcfsim.sm.ievent_listener import (
    IEventListener,
    IEventListenerEngine,
    IEventListenerNotifyFunction,
    IEventListenerNotifyMethod,
)

class EventListener(scxml4py.listeners.EventListener): #type:ignore[misc] # mypy confusion 
    listener_engine: IEventListenerEngine
    
    def __init__(self, 
         listener_engine: IEventListenerEngine | IEventListenerNotifyFunction
        ):
        self.listener_engine = parse_event_listener( listener_engine )
        super().__init__()

    def notify(self, event:scxml4py.event.Event)->None:
        self.listener_engine.notify( event )

@dataclass 
class EventListenerWrapper:
    func: IEventListenerNotifyFunction
    def notify(self, event: scxml4py.event.Event)->None:
        return self.func(event)

def parse_event_listener(
        listener:IEventListenerEngine|IEventListenerNotifyFunction
    )->IEventListenerEngine:
    """ Parse an Event Listener engine 

    A function will be wrapped inside an EventListenerNotifier
    """
    if isinstance( listener, IEventListenerEngine):
        return listener 
    if hasattr(listener, "__call__"):
        return EventListenerWrapper(listener)
    raise ValueError(f"Event Listener must be conform to IEventListenerEngine Protocol or a callable, got a {type(listener)}" )


def set_event_listener_method(method: IEventListenerNotifyMethod)->IEventListenerNotifyMethod:
    method.__sm_event_listener__ = True # type:ignore 
    return method

def is_event_listener_method( obj:Any)->bool:
    try:
        return obj.__sm_event_listener__ # type:ignore[no-any-return]
    except AttributeError:
        return False

@overload
def event_listener_method()->Callable[ [IEventListenerNotifyMethod], IEventListenerNotifyMethod]:
    """decorator signature """
    ...

@overload
def event_listener_method(method:IEventListenerNotifyMethod)->IEventListenerNotifyMethod:
    ...


def event_listener_method(
     method:IEventListenerNotifyMethod|None=None
    )->Union[Callable[ [IEventListenerNotifyMethod], IEventListenerNotifyMethod],IEventListenerNotifyMethod]:
    if method is None:
        # This is a decorator, return a function 
        # to add sm properties to the method directly 
        def create_action(method:IEventListenerNotifyMethod)->IEventListenerNotifyMethod:
            return set_event_listener_method( method)
        return create_action
    else:
        # The wrapping is necessary to avoid erasing
        # already set method
        @functools.wraps(method)
        def wraped_method(*args:Any, **kwargs:Any)->Any:
            return method(*args, **kwargs)
        return set_event_listener_method(wraped_method) 

def collect_event_listener(obj:Any)->Iterator[IEventListener]:
    """ Iterator on actions present in the input object 
    
    @action_method decorated method will be transformed to Action 
    """
    for attr in dir( obj ):
        try:
            member = getattr(obj, attr)
        except AttributeError:
            continue
              
        if isinstance(member, scxml4py.listeners.EventListener):
            yield member 
        elif is_event_listener_method(member):
            yield event_listener(member)

def event_listener( method: IEventListenerNotifyFunction)->IEventListener:
    """ Create an action from a method """
    if hasattr( method, "__call__"):
        return EventListener( EventListenerWrapper(method) )
    raise ValueError( f"Expecting a callable as event listener got a {type(method)}")

@dataclass
class EventListenerManager:
    event_listeners: list[IEventListener] = field(default_factory=list)

    def add(self, event_listener: IEventListener)->None:
        self.event_listeners.append(event_listener)

    def collect(self, obj: Any)->None:
        self.event_listeners.extend( collect_event_listener(obj) )
    
    def get_event_listeners(self)->list[IEventListener]:
        return self.event_listeners

