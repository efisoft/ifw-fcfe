from __future__ import annotations
from typing import Any, Callable, Protocol, runtime_checkable
from typing_extensions import TypeAlias
import scxml4py.context
import scxml4py.action
import scxml4py.event
from queue import Queue


IAction: TypeAlias = scxml4py.action.Action

@runtime_checkable
class IActionEngine(Protocol):
    def execute(self, 
          handler: IActionHandler, 
          context: scxml4py.context.Context
        )->None:
        ...
    
    def evaluate(self, 
          handler: IActionHandler, 
          context: scxml4py.context.Context
        )->bool:
        ...

class IActionHandler(Protocol):
    """ Action handler as """
    
    def get_id(self)->str: 
        """ Return action Id """ 
        ... 

    def send_internal_event(self, event: scxml4py.event.Event)->None:
        """ queue a new internal event """
        ...

IActionExecuteFunction = Callable[[IActionHandler, scxml4py.context.Context], None]
""" Function accepted as a ActionEngine.execute replacement """
IActionExecuteMethod = Callable[[Any, IActionHandler, scxml4py.context.Context], None]
""" Method accepted as a ActionEngine.execute replacement """


class IActionManager(Protocol):
    def add(self, action: IAction)->None:
        ... 

    def collect(self, obj: Any, queue:Queue[scxml4py.event.Event]|None = None)->None:
        ...
    
    def get_actions(self)->list[IAction]:
        ...

