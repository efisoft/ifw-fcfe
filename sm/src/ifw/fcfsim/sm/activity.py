from __future__ import annotations
from asyncio import  iscoroutinefunction
from queue import Queue
from dataclasses import dataclass, field
from typing import Any, Callable, Iterator, Union, overload
import functools
import scxml4py.activity
import scxml4py.event

from ifw.fcfsim.sm.iactivity import (
        IActivity,
        IActivityEngine,
        IActivityHandler, 
        IActivityRunFunction,
        IActivityRunMethod,
        IAsyncActivityEngine,
        IAsyncActivityRunFunction, 
)


class ThreadedActivity(scxml4py.activity.ThreadedActivity): # type:ignore[misc] # mypy
    activity_engine: IActivityEngine

    def __init__(self,
          id: str,
          activity: IActivityEngine | IActivityRunFunction,
          event_queue: Queue[scxml4py.event.Event] | None = None
        ) -> None:
        super().__init__(id, event_queue)
        self.activity_engine = parse_activity(activity)

    def run(self) -> None:
        self.activity_engine.run(self)

    def stop(self) -> None:
        # redefine stop here. In original devsim code
        # this was hidden by doing setRunning
        # The super().stop raise RuntimError at self.mThread.join()
        if self.mThread and self.isRunning():  # pyright:ignore
            self.setRunning(False)

    # Standardise some method to be compatible with codding standrds in IActivityHandler
    def is_running(self) -> bool:
        """ True if activity is running """
        return self.isRunning()  # type:ignore[no-any-return]

    def send_internal_event(self, event: scxml4py.event.Event) -> None:
        """ alias of sendInternalEvent() """
        self.sendInternalEvent(event)

    def get_id(self) -> str:
        """ alias of getId() """
        return self.getId()  # type:ignore[no-any-return]

class CoroActivity(scxml4py.activity.CoroActivity): #type:ignore[misc] # mypy does not know
    activity_engine: IAsyncActivityEngine

    def __init__(self, 
          id:str, 
          activity_engine: IAsyncActivityEngine| IAsyncActivityRunFunction, 
          event_queue:Queue[scxml4py.event.Event]|None = None
        )->None:
        super().__init__(id, event_queue)
        self.activity_engine = parse_async_activity(activity_engine) 
        
    async def run(self)->None:
        await self.activity_engine.run(self)  
    
    def is_running(self)->bool:
        """ True if activity is running """
        return self.isRunning() # type:ignore[no-any-return]

    def send_internal_event(self, event:scxml4py.event.Event)->None:
        """ alias of sendInternalEvent() """
        self.sendInternalEvent( event ) 
    
    def get_id(self)->str:
        """ alias of getId() """
        return self.getId() # type:ignore[no-any-return]

@dataclass
class ActivityWrapper:
    """ Wrapp a callable into a valid IActivityEngine """
    func: IActivityRunFunction
    def run(self, handler:IActivityHandler)->None:
        self.func(handler)

@dataclass
class AsyncActivityWrapper:
    """ Wrapp a callable into a valid Activity """
    coroutine: IAsyncActivityRunFunction

    async def run(self, handler:IActivityHandler)->None:
        await self.coroutine(handler)

def parse_activity(activity: IActivityEngine|IActivityRunFunction)->IActivityEngine:
    """ Parse an activity object or a function into activity object """
    if isinstance(activity, IActivityEngine):
        return activity
    
    if hasattr( activity, "__call__"):
        return ActivityWrapper(activity)
    raise  ValueError(f"activity must be conform to  ActivityRunner Protocol or a callable, got a {type(activity)}")

def parse_async_activity( activity: IAsyncActivityRunFunction|IAsyncActivityEngine)->IAsyncActivityEngine:
    """ Parse an activity object or a async function into activity object """
    if isinstance(activity, IAsyncActivityEngine):
        if not iscoroutinefunction(activity.run):
            raise ValueError("An activity with a run coroutine is expected, got a regular run method")
        return activity

    if hasattr( activity, "__call__"):
        if not iscoroutinefunction(activity):
            raise ValueError(f"A coroutine or an Activity was expected got a {type(activity)}")
        return AsyncActivityWrapper(activity)
    raise  ValueError(f"activity must be conform to  ActivityRunner Protocol or a callable, got a {type(activity)}")

def set_activity_method(id: str, method: IActivityRunMethod)->IActivityRunMethod:
    method.__sm_activity__ = True #type:ignore
    method.__sm_id__ = id #type:ignore 
    return method

@overload
def activity_method(id:str )->Callable[[IActivityRunMethod], IActivityRunMethod] :
    ...

@overload
def activity_method(id:str,method: IActivityRunMethod)->IActivityRunMethod:
    ...

def activity_method(
        id: str, 
        method: IActivityRunMethod|None = None
    )-> Callable[[IActivityRunMethod], IActivityRunMethod]| IActivityRunMethod:

    if method is None:
        # This is a decorator, return a function 
        # to add sm properties to the method directly 
        def create_action(method:IActivityRunMethod)->IActivityRunMethod:
            return set_activity_method( id, method)
        return create_action
    else:
        # The wrapping is necessary to avoid erasing
        # already set method
        @functools.wraps(method)
        def wraped_method(*args:Any, **kwargs:Any)->Any:
            return method(*args, **kwargs)
        return set_activity_method( id, wraped_method) 

def is_activity_method(obj:Any) -> bool:
    """ Ture object is a method decorated with activity_method """
    try:
        return obj.__sm_activity__ # type:ignore[no-any-return]
    except AttributeError:
        return False

def is_activity_async_method(obj:Any) -> bool:
    """ Ture object is a method decorated with activity_method """
    try:
        return obj.__sm_activity__ and iscoroutinefunction(obj)
    except AttributeError:
        return False

def collect_activities(
      obj:Any, 
      event_queue: Queue[scxml4py.event.Event] | None = None
    ) -> Iterator[IActivity]:
    """ Iterator on activities present in the input object 
    
    @activity_method decorated method will be transformed to ThreadedActivity or CoroActivity 
    if they are async method.
    """
    for attr in dir( obj ):
        try:
            member = getattr(obj, attr)
        except AttributeError:
            continue
        if isinstance(member, (scxml4py.activity.ThreadedActivity, scxml4py.activity.CoroActivity)):
            yield member 
        
        elif is_activity_method(member):
            yield activity(member.__sm_id__, member, event_queue)
@overload
def activity(
        id: str, 
        method: IActivityRunFunction,
        event_queue: Queue[scxml4py.event.Event]|None = None
    ) -> ThreadedActivity:
    ...

@overload
def activity(
        id: str, 
        method: IAsyncActivityRunFunction,
        event_queue: Queue[scxml4py.event.Event]|None = None
    ) -> CoroActivity:
    ...


def activity(
        id: str, 
        method: IActivityRunFunction|IAsyncActivityRunFunction,
        event_queue: Queue[scxml4py.event.Event]|None = None
    ) -> Union[ThreadedActivity, CoroActivity]:
    """ Create an ThreadedActivity or CoroActivity from a function 
    
    """
    if  iscoroutinefunction(method):
        return CoroActivity( id, AsyncActivityWrapper(method), event_queue)
    elif  hasattr(method, "__call__"):
        return ThreadedActivity( id, ActivityWrapper(method), event_queue) #type:ignore #lsp don't catch iscoroutin
    raise ValueError( f"expecting a function or a coroutine function got a {type(method)}" )


@dataclass
class ActivityManager:
    """ Collection of activities """
    activities: list[IActivity] = field(default_factory=list)
    
    def add(self, activity: IActivity) -> None:
        self.activities.append(activity)

    def collect(self, obj: Any, event_queue: Queue[scxml4py.event.Event] | None = None) -> None:
        self.activities.extend(collect_activities(obj, event_queue))

    def get_activities(self) -> list[IActivity]:
        return self.activities
