from __future__ import annotations
from typing import Any, Callable, Protocol, runtime_checkable
from typing_extensions import TypeAlias

import scxml4py.event
import scxml4py.listeners


IEventListener: TypeAlias = scxml4py.listeners.EventListener

@runtime_checkable
class IEventListenerEngine(Protocol):
    def notify(self, event:scxml4py.event.Event)->None:
        ...

IEventListenerNotifyFunction = Callable[[scxml4py.event.Event], None]
IEventListenerNotifyMethod = Callable[[Any, scxml4py.event.Event], None]


class IEventListenerManager(Protocol):
    def add(self, event_listener: IEventListener)->None:
        ...

    def collect(self, obj: Any)->None:
        ... 
    
    def get_event_listeners(self)->list[IEventListener]:
        ...
