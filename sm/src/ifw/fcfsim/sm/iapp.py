from __future__ import annotations
import queue
from typing import Any, Protocol
from ifw.fcfsim.sm.iaction import IAction
from ifw.fcfsim.sm.iactivity import IActivity
from ifw.fcfsim.sm.ievent_listener import IEventListener
from ifw.fcfsim.sm.istatus_listener import IStatusListener
from ifw.fcfsim.core import api as core 
from typing_extensions import TypeAlias
import scxml4py.stateMachine
import scxml4py.executor


IExecutor: TypeAlias = scxml4py.executor.Executor
IStateMachineModel: TypeAlias = scxml4py.stateMachine.StateMachine


class IStateMachineLink(Protocol):
    def collect(self,
            obj: Any,
            event_queue: queue.Queue[scxml4py.event.Event] | None = ...
                ) -> None:
        ...

    def get_activities(self) -> list[IActivity]:
        ...

    def get_actions(self) -> list[IAction]:
        ...

    def get_event_listeners(self) -> list[IEventListener]:
        ...

    def get_status_listeners(self) -> list[IStatusListener]:
        ...

    def add_activity(self, *activities: IActivity) -> None:
        ...

    def add_action(self, *actions: IAction) -> None:
        ...

    def add_event_listener(self, *listeners: IEventListener) -> None:
        ...

    def add_status_listener(self, *listeners: IStatusListener) -> None:
        ...

    def make_model(self, file: str) -> IStateMachineModel:
        ...

    def make_executor(self,
            file: str,
            context: scxml4py.context.Context | None = ...,
            model: IStateMachineModel | None = ...
            ) -> IExecutor:
        ...


class IStateMachineManager(Protocol):
    links: IStateMachineLink
    
    def collect(self, *objs: object) -> None:
        ...
    
    def new_task(self, *objs: object) -> core.ITask:
        ...

    def new_runner(self, looper: core.ILooper, *objs: object) -> core.IRunner:
        ...
    
    def new_session(self, looper: core.ILooper, *objs: object) -> core.ISession:
        ...

class IStateMachineHolder(Protocol):
    def get_activities(self) -> list[IActivity]:
        ...

    def get_actions(self) -> list[IAction]:
        ...


class IStateMachineModelFactory(Protocol):
    def make_model(self, sm_manager: IStateMachineHolder) -> IStateMachineModel:
        ...

class IExecutorHolder(Protocol):
    def get_event_listeners(self) -> list[IEventListener]:
        ...

    def get_status_listeners(self) -> list[IStatusListener]:
        ...

