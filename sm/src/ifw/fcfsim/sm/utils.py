from __future__  import annotations
from enum import Enum
from typing import Any, Iterable, TypeVar
import warnings 

T = TypeVar("T")


def iter_member_of_class(obj:Any, cls:type[T])->Iterable[T]:
    """ iter on class instances on members of any object """
    for attr in dir(obj):
        member = getattr(obj, attr, None)
        if isinstance( member, cls):
            yield member 



def build_state_mapping( State: Iterable[Enum] , Substate: Iterable[Enum], warning:bool=False)->dict[str,tuple[Enum,Enum]]:
    """ Attempt to build a state machine mapping

    The state machine mapping looks like this : 
        {"On::NotOperational::NotReady":     (LampState.NOTOP, LampSubstate.NOTOP_NOTREADY),
        "On::NotOperational::Initialising": (LampState.NOTOP, LampSubstate.NOTOP_INITIALISING),
        "On::NotOperational::Ready::Off" :   (LampState.NOTOP, LampSubstate.NOTOP_READY_OFF), 
        "On::NotOperational::Ready::On":     (LampState.NOTOP, LampSubstate.NOTOP_READY_ON),
    }
    
    It links StateMachine states to State/Substate from Enumerator 
    """
    map = {}
    def parse_state(name:str)->str:
        if name == "OP": return "Operational"
        if name == "NOTOP": return "NotOperational" 
        raise ValueError(f"Expecting OP or NOTOP for state, got {name!r}")
    
    def parse_substate(name:str)->str:
        name = name.removeprefix("OP_").removeprefix("NOTOP_")
        return "".join(a.lower().capitalize() for a in  name.split("_"))

    for StateMember in State:
        if not StateMember.value: 
            continue 
        for SubstateMember in Substate:
            if not SubstateMember: 
                continue 
            
            key = f"On::{parse_state(StateMember.name)}::{parse_substate(SubstateMember.name)}"
            map[key] = (StateMember, SubstateMember)
    
    if warning:
        texts =  [f"{k!r} : ({s.__class__.__name__}.{s.name}, {ss.__class__.__name__}.{ss.name})," for k, (s, ss) in map.items()]
        text = "\n{\n   "+"\n   ".join(texts)+"\n}\n"
        
        warnings.warn("State Machine mapping is automaticaly made. Considers to check and hardcode it")
        warnings.warn(text)

    return map 





