from __future__ import annotations 

from dataclasses import dataclass, field
import queue
from typing import Any, Generic, Iterator, TypeVar

from ifw.fcfsim.core.exceptions import StopTask
import scxml4py.action 
import scxml4py.activity 
import scxml4py.stateMachine 
import scxml4py.reader 
import scxml4py.context 
import scxml4py.executor 
import scxml4py.helper 
import scxml4py.listeners 
import scxml4py.event

import ifw.core.utils.utils

from ifw.fcfsim.sm.iaction import IAction, IActionManager
from ifw.fcfsim.sm.iactivity import IActivity, IActivityManager
from ifw.fcfsim.sm.ievent_listener import IEventListener, IEventListenerManager
from ifw.fcfsim.sm.istatus_listener import IStatusListener, IStatusListenerManager

from ifw.fcfsim.sm.action import ActionManager
from ifw.fcfsim.sm.activity import ActivityManager
from ifw.fcfsim.sm.event_listener import EventListenerManager
from ifw.fcfsim.sm.status_listener import StatusListenerManager

from ifw.fcfsim.core import api as core

from ifw.fcfsim.sm.iapp import (
        IExecutor, IExecutorHolder, IStateMachineHolder,  IStateMachineModel, 
        IStateMachineLink
)

log = core.get_logger("devsim.sm")

_smid_counter = 0
def _next_smid() -> str:
    """Automatic stateMachine ID generator"""
    global _smid_counter
    _smid_counter += 1
    return f"SM{_smid_counter:02d}"

@dataclass
class StateMachineLink: 
    """A collector and a factory of a state Machine model and executor"""
    
    action_manager: IActionManager = field(default_factory=ActionManager)
    """ handle a list of actions """

    activity_manager: IActivityManager = field(default_factory=ActivityManager)
    """ handle a list of activities """

    event_listener_manager: IEventListenerManager = field(default_factory=EventListenerManager)
    """  handle a list of event listeners """ 

    status_listener_manager: IStatusListenerManager = field(default_factory=StatusListenerManager)
    """ hanlde a list of status listeners """ 

    def collect(self, 
            obj:Any, 
            event_queue: queue.Queue[scxml4py.event.Event]|None = None
        )->None:
        """ Collect actions, activities event and state listener from an object 

        Check all methods decorated with action_method, activity_method, 
        event_listener_method, status_listener_method and create the scxml 
        appropriate objects.
        """
        self.action_manager.collect(obj, event_queue)
        self.activity_manager.collect(obj, event_queue)
        self.event_listener_manager.collect(obj)
        self.status_listener_manager.collect(obj)
    
    def get_activities(self) -> list[IActivity]:
        """Return a list of all Activities collected"""
        return self.activity_manager.get_activities()
    
    def get_actions(self) -> list[IAction]:
        """Return a list of all Actions collected"""
        return self.action_manager.get_actions()
    
    def get_event_listeners(self) -> list[IEventListener]:
        """Return a list of all event listeners collected"""
        return self.event_listener_manager.get_event_listeners()

    def get_status_listeners(self) -> list[IStatusListener]:
        """Return a list of all status listener collected"""
        return self.status_listener_manager.get_status_listeners()
    
    def add_activity(self, *activities: IActivity) -> None:
        """Add one or several activity object"""
        for activity in activities:
            self.activity_manager.add(activity)
    
    def add_action(self, *actions: IAction) -> None:
        """Add one or several action object"""
        for action in actions:
            self.action_manager.add(action)
    
    def add_event_listener(self, *listeners: IEventListener) -> None:
        """Add one or several event listener object"""
        for listener in listeners:
            self.event_listener_manager.add(listener)

    def add_status_listener(self, *listeners: IStatusListener) -> None:
        """Add one or several state listener object"""
        for listener in listeners:
            self.status_listener_manager.add(listener)

    def make_model(self, file: str) -> IStateMachineModel:
        """ Make a full scxml4py StateMachine model """
        return StateMachineModelFactory(file).make_model(self)
    
    def make_executor(self,
          file: str,
          context: scxml4py.context.Context | None = None,
          model: IStateMachineModel | None = None) -> IExecutor:
        """ Make a full scxml4py Executor 

        if model is None it will be created with self.make_model(file)
        """
        if model is None:
            model = self.make_model(file)
        if context is None:
            context = scxml4py.context.Context()
        return ExecutorFactory( model,  context=context).make_executor(self)



@dataclass
class StateMachineManager: 
    """Handy high level object to make a State Machine model runner"""
    
    file: str
    """SCXML resource file path""" 
    
    smid: str = field(default_factory=_next_smid)
    """State machine ID""" 
    
    event_queue: queue.Queue[scxml4py.event.Event] = field(
            default_factory=queue.Queue)
    """A Queue of Event listened for the state machine executor"""

    links: IStateMachineLink = field(default_factory=StateMachineLink)
    """contained all collected state machine objects"""

    context: scxml4py.context.Context = field(
            default_factory=scxml4py.context.Context)
    
    log: core.ILogger = core.get_logger("StateMachine")

    def collect(self, *objs: object) -> None:
        """Add objects containing state machine decorated methods 
        
        It is expected that the objects will schedule event on the 
        event queue of this state machine manager. 

        For finer details of Actions, Activity, ... inclusion. 
        use the self.links Linker object.
        """
        for obj in objs:
            self.links.collect(obj, self.event_queue)

    def new_task(self, *objs: object) -> core.ITask:
        """Build a new Task to run the state machine 
        
        Optional objects can be included here if this wasn't done
        with .collect method 
        """
        self.collect(*objs)
        executor = self.links.make_executor(self.file, context=self.context)
        return StateMachineTask(
              self.smid, executor,  event_queue = self.event_queue, log=self.log
        )

    def new_runner(self, looper: core.ILooper, *objs: object) -> core.IRunner:
        """Build a new Runner for the state machine 
        
        Args:
            looper: a Looper for the runner 
            \\*objects: Optional objects can be included here if this 
                wasn't done with .collect method 
        """
        task = self.new_task(*objs)
        return core.TaskRunner(task, looper, log = self.log)
    
    def new_session(self, looper: core.ILooper, *objs: object) -> core.ISession:
        """Build a new Runner Thread for the state machine 
        
        Args:
            looper: a Looper for the runner 
            \\*objects: Optional objects can be included here if this 
                wasn't done with .collect method 
        """
        self.collect(*objs)
        executor = self.links.make_executor(self.file, self.context)
        task  = StateMachineTask(
              self.smid, executor,  event_queue = self.event_queue, log=self.log
        )
        return core.Session(core.TaskRunner(task, looper, self.log))
 

@dataclass 
class StateMachineModelFactory:
    file: str 
    """sxml resource file path containing the state machine definition"""

    def make_model(self, sm_manager:IStateMachineHolder) -> IStateMachineModel:
        """ Make a full scxml4py StateMachine """
        file = ifw.core.utils.utils.find_file(self.file)
        return scxml4py.reader.Reader().read(
                file, 
                sm_manager.get_actions(), 
                sm_manager.get_activities() 
            )


@dataclass
class ExecutorFactory:
    model: scxml4py.stateMachine.StateMachine 
    """state machine model"""
    
    context: scxml4py.context.Context = field(default_factory=scxml4py.context.Context)
    """Context instance use in the executor"""
    
    def make_executor(self, executor_manager: IExecutorHolder) -> IExecutor:
        executor = scxml4py.executor.Executor(
                self.model, self.context
            )
        for el in executor_manager.get_event_listeners():
            executor.addEventListener(el)
        for sl in executor_manager.get_status_listeners():
            executor.addStatusListener(sl)
        return executor


@dataclass 
class StateMachineTask(core.BaseTask):
    """Task for a scaml4py Executor"""
    
    id: str
    """State machine identifier"""
    
    executor: IExecutor 
    """ scxml4py Executor"""
    
    event_queue: queue.Queue[scxml4py.event.Event] = field(default_factory=queue.Queue)
    """ A Queue from which event are processed """
   
    log: core.ILogger = core.get_logger("StateMachine")
    """ logger """
    
    _alive_flag: bool = field(default=False, init=False) 
    
    async def initialise(self)->None:
        # do nothing but conform to ITask interface 
        ...

    async def start(self) -> None:
        model_id = self.executor.getModel().getId()
        self.log.info(f"{self.id}: Starting execution of {model_id}")
        self._alive_flag = True
        self.executor.start()
        formated_status = scxml4py.helper.formatStatus(
            self.executor.getStatus()
        )
        self.log.info(f"{self.id}: Status: {formated_status}")

    async def next(self) -> None:
        try:
            # queue should no block otherwise it will block
            # other concurent tasks
            event = self.event_queue.get(False)
        except queue.Empty:
            return
        self.log.info(
            f"{self.id}: Application received event = {event} <{type(event)}>")

        self.executor.processEvent(event)
        if event.getId() == "Exit":
            self.log.debug(f"{self.id}: Application terminating...")
            raise StopTask('Exit event', 0)
        
    async def end(self) -> None:
        self.executor.stop()
        model_id = self.executor.getModel().getId()
        self.log.info(f"{self.id}: Stopping execution of {model_id}")
        formated_status = scxml4py.helper.formatStatus(
            self.executor.getStatus())
        self.log.info(f"{self.id}: Status: {formated_status}")


T = TypeVar('T', bound=Any)


@dataclass
class StateMachine(Generic[T]):
    """High level object to build a state machine session"""
    file: str
    sm_engine: T
    event_queue: queue.Queue[scxml4py.event.Event] = field(
        default_factory=queue.Queue)
    update_frequency: float = 10.0

    def get_engine(self) -> T:
        return self.sm_engine

    def iter_runners(self, excludes: set[str] = set()) -> Iterator[core.IRunner]:
        if "sm" not in excludes:
            sm_mgr = StateMachineManager(
                self.file, event_queue=self.event_queue)
            sm_mgr.collect(self.sm_engine)
            yield sm_mgr.new_runner(core.Looper(1. / self.update_frequency))

