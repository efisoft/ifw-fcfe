from __future__ import annotations
from typing import Any, Callable, Protocol, runtime_checkable
from typing_extensions import TypeAlias

import scxml4py.event
import scxml4py.state
import scxml4py.listeners


IStatusListener: TypeAlias = scxml4py.listeners.StatusListener

@runtime_checkable
class IStatusListenerEngine(Protocol):
    def notify(self, status: set[scxml4py.state.State])->None:
        ...

IStatusListenerNotifyFunction = Callable[[set[scxml4py.state.State]], None]
IStatusListenerNotifyMethod = Callable[[Any, set[scxml4py.state.State]], None]


class IStatusListenerManager(Protocol):
    def add(self, event_listener:IStatusListener)->None:
        ...

    def collect(self, obj: Any)->None:
        ... 
    
    def get_status_listeners(self)->list[IStatusListener]:
        ...
