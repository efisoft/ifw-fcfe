from __future__ import annotations
from typing import Any, Awaitable, Callable, Protocol, Union, runtime_checkable

from queue import Queue
from typing_extensions import TypeAlias
from scxml4py.event import Event
import scxml4py.activity

IActivity: TypeAlias = Union[ scxml4py.activity.ThreadedActivity, scxml4py.activity.CoroActivity] 



@runtime_checkable
class IActivityEngine(Protocol):
    def run(self, handler: IActivityHandler)->None:
        """ Activity runner, should include loop 
        
        Ending this function, ends the Activity Thread 
        """
        ...

@runtime_checkable
class IAsyncActivityEngine(Protocol):
    async def run(self, handler: IActivityHandler)->None:
        """ Activity runner, should include loop 
        
        Ennding this function, ends the Activity Task
        """
        ...

class IActivityHandler(Protocol):
    """ Activity Thread handler """
    def get_id(self)->str:
        """ Return current activity id """
        ...
    
    def stop(self)->None:
        """ stop activity thread """
        ... 

    def is_running(self)->bool:
        """ Check if Activity thread is running """
        ...
    
    def send_internal_event(self, event: Event)->None:
        """ queue a new internal event """
        ... 


IActivityRunMethod = Callable[ [Any, IActivityHandler] , None]
""" Alternative method replacing ActivityRunner """
IActivityRunFunction = Callable[[IActivityHandler], None]
IAsyncActivityRunFunction = Callable[[IActivityHandler], Awaitable[None]]

class IActivityManager(Protocol):
    def add(self, activity: IActivity)->None:
        ... 

    def collect(self, obj: Any, event_queue:Queue[Event]|None = None)->None:
        ...
    
    def get_activities(self)->list[IActivity]:
        ...


