from __future__ import annotations

from dataclasses import dataclass, field
import functools
from typing import Any, Callable, Iterator, Optional, Union, overload
import scxml4py.listeners 
import scxml4py.event 
import scxml4py.state 

from ifw.fcfsim.sm.istatus_listener import (
    IStatusListener,
    IStatusListenerEngine,
    IStatusListenerNotifyFunction, 
    IStatusListenerNotifyMethod, 
)

class StatusListener(scxml4py.listeners.StatusListener): #type:ignore[misc] # mypy confusion
    listener_engine: IStatusListenerEngine
    def __init__(self, 
          listener_engine: IStatusListenerEngine | IStatusListenerNotifyFunction
        ):
        self.listener_engine = parse_status_listener( listener_engine )
        super().__init__()

    def notify(self, status: set[scxml4py.state.State])->None:
        return self.listener_engine.notify( status )


@dataclass
class StatusListenerWrapper:
    func: IStatusListenerNotifyFunction
    def notify(self, status: set[scxml4py.state.State])->None:
        return self.func(status)


def parse_status_listener(
        listener:IStatusListenerEngine|IStatusListenerNotifyFunction
    )->IStatusListenerEngine:
    """ Parse an IStatusListenerEngine

    A function will be wrapped inside an IStatusListenerEngine
    """
    if isinstance( listener, IStatusListenerEngine):
        return listener 
    if hasattr(listener, "__call__"):
        return StatusListenerWrapper(listener)
    raise ValueError(f"Status Listener must be conform to IStatusListenerEngine Protocol or a callable, got a {type(listener)}" )


def set_status_listener_method(method: IStatusListenerNotifyMethod)->IStatusListenerNotifyMethod:
    method.__sm_status_listener__ = True # type:ignore 
    return method

def is_status_listener_method( obj:Any)->bool:
    try:
        return obj.__sm_status_listener__ # type:ignore[no-any-return]
    except AttributeError:
        return False


@overload
def status_listener_method()-> Callable[ [IStatusListenerNotifyMethod] , IStatusListenerNotifyMethod ]: 
    """ Decorator signature """ 
    ... 
@overload
def status_listener_method(method: IStatusListenerNotifyMethod )->IStatusListenerNotifyMethod:
    ...

def status_listener_method(
     method: Optional[ IStatusListenerNotifyMethod ] = None
    )->Union[Callable[ [IStatusListenerNotifyMethod] , IStatusListenerNotifyMethod ], IStatusListenerNotifyMethod]:

    if method is None:
        # This is a decorator, return a function 
        # to add sm properties to the method directly 
        def create_action(method:IStatusListenerNotifyMethod)->IStatusListenerNotifyMethod:
            return set_status_listener_method( method)
        return create_action
    else:
        # The wrapping is necessary to avoid erasing
        # already set method
        @functools.wraps(method)
        def wraped_method(*args:Any, **kwargs:Any)->Any:
            return method(*args, **kwargs)
        return set_status_listener_method(wraped_method) 

def collect_status_listener(obj:Any)->Iterator[IStatusListener]:
    """ Iterator on actions present in the input object 
    
    @action_method decorated method will be transformed to Action 
    """
    for attr in dir( obj ):
        try:
            member = getattr(obj, attr)
        except AttributeError:
            continue
              
        if isinstance(member, scxml4py.listeners.StatusListener):
            yield member 
        elif is_status_listener_method(member):
            yield status_listener(member)

def status_listener( method: IStatusListenerNotifyFunction)->StatusListener:
    """ Create an action from a method """
    if hasattr( method, "__call__"):
        return StatusListener( StatusListenerWrapper(method) )
    raise ValueError( f"Expecting a callable as status listener got a {type(method)}")


@dataclass
class StatusListenerManager:
    status_listeners: list[IStatusListener] = field(default_factory=list)

    def add(self, event_listener: IStatusListener)->None:
        self.status_listeners.append(event_listener)

    def collect(self, obj: Any)->None:
        self.status_listeners.extend( collect_status_listener(obj) )
    
    def get_status_listeners(self)->list[IStatusListener]:
        return self.status_listeners

