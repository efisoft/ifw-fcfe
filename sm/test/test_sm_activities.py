from asyncio import iscoroutinefunction
from ifw.fcfsim.state_machine.activity import activity_method, parse_activity, parse_async_activity
from ifw.fcfsim.state_machine.iactivity import IActivityEngine
import pytest


def test_parsing_of_activity():
    class MyGoodActivity:
        def run(self, handler):
            ... 
    good= MyGoodActivity()
    
    class MyBadActivity:
        def not_run(self, handler):
            ... 
    bad = MyBadActivity()

    class MyGoodAsyncActivity:
        async def run(self, handler):
            ... 
    agood= MyGoodAsyncActivity()
    

    assert parse_activity(good) is good
    assert parse_async_activity(agood) is agood

    with pytest.raises( ValueError ):
        parse_activity( bad )#type: ignore 

    with pytest.raises( ValueError ):
        parse_async_activity( good)#type: ignore 
    
    def func(handler):
        pass 
    def afunc(handler):
        pass 

    assert isinstance( parse_activity(func), IActivityEngine)
    assert isinstance( parse_activity(afunc), IActivityEngine)


