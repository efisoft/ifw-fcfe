
# DOC IN PROGRESS 

# What is this ? 

This is a prototype of a cleaner version of ifw-fcf devsim. 

The goal here is to try to improve code reliability and 
makes it more robust to changes and evolutions. 

Also an important new feature is the ability to run several device simulator in one process.

The core of devsim is completely re-written. 

Install: 

```shell
> git clone https://gitlab.lam.fr/efisoft/ifw-fcfsim.gitlab 
> cd ifw-fcfsim
> waf configure 
> waf build 
> waf install
```

## Note on type checking mypy: 

The package is fully type checked, it means that tools such as mypy can be run after any edit or when a new simulator has been written. 

When installed, for now we need to manually add the py.typed file because not installed by the eso waf framework so far: 

```shell
> touch ~/INTROOT/lib/python3.11/site-packages/ifw/fcfsim/py.typed
```

Mypy in strict mode can be run with the following options.  

```shell
mypy -p ifw.fcfsim --ignore-missing-imports --strict 
```

Or when working on specific submodule: 

```shell
mypy -p ifw.fcfsim.opcua --ignore-missing-imports --strict
```

For Continus Integration it might be better to remove some checks: 

```shell
mypy -p ifw.fcfsim --ignore-missing-imports --strict --no-warn-unused-ignores
```

