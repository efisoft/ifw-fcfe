
Introduction 
============

The main principle of this opcua package is to free the user from any 
OPCUA business inside the business logic. It was designed for the device 
simulator library. 

The idea is to have an "Engine" and "Runner" side by side

The engine is a normal python object with attributes and methods. It is
handling business logic in the python space. 

The runner takes care of the link between engine and the OPCUA 
server or  client. 

The only thing the user has to do is to provide an OPCUA profile which 
links python object into OPCU nodes (data or methods).


