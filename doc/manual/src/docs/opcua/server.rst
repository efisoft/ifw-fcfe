OPCUA Server
============

Now that we now everything about profile. Let us build a server. 
Also it might be usefull to read documentation of data node in the core package.

The steps are: 

- make an Engine class which will handle the business logic 
  without taking care of the OPCUA (like in TwinCat).
- build a profile, mapping the Engine data/methods into ua nodes 
- build an interface or use a generic one 
- attach the engine thruogh its interface to the server


Let us make a dummy lamp like object: 

.. code-block:: python 

   from ifw.fcfsim.core import api as core 
   from dataclasses import dataclass 
   import enum

   class MyLampError(enum.IntEnum): 
       OK = 0 
       NOT_INITIALIZED = 1


   @core.attrconnect
   @dataclass
   class MyLampEngine:
       intensity: float = 0.0
       initialized: bool = False
       
       def init(self) -> int:
           self.initialized = True 
           return MyLampError.OK 
           
       def switch_on(self, intensity: float) -> int:
           if not self.initialized:
               raise core.RpcException(
                    'Lamp is not initialized', MyLampError.NOT_INITIALIZED)
           self.intensity = intensity 
           return MyLampError.OK
        
       def switch_off(self) -> int:
           if not self.initialized:
               raise core.RpcException(
                    'Lamp is not initialized', MyLampError.NOT_INITIALIZED)
           self.intensity = 0.0 
           return MyLampError.OK


We than create a Server Interface, we use :class:`ifw.fcfsim.opcua.api.ServerInterface` it is a generic interface which should work in most cases: 



The interface must be conform the Protocol : :class:`ifw.fcfsim.opcua.itf.IServerInterface`

.. code-block:: python 

   from ifw.fcfsim.opcua import api as opcua

   my_lamp_profile = opcua.mapping_to_profile({
      'intensity': 'stat.lrIntensity(Double)', 
      'initialized': 'stat.bInitialized(Boolean)', 
      
      'init': 'method.RPC_Init(O:ReturnValue(Int16))', 
      'switch_on': 'method.RPC_On(I:in_lrIntensity(Double), O:ReturnValue(Int16))', 
      'switch_off': 'method.RPC_Off(O:ReturnValue(Int16))', 
   })
   my_lamp_interface = opcua.ServerInterface(
      MyLampEngine(), 
      my_lamp_profile, 
      prefix="MAIN.Lamp1",
      namespace = 4
   )


Then we create a server, attach the lamp and run it: 

.. code-block:: python 

   from ifw.fcfsim.opcua import api as opcua
   import asyncio
   server = opcua.UaServer("opc.tcp://localhost:4840")
   engine = server.attach(my_lamp_interface)
   asyncio.run(server.run()) # blocking Ctrl-C to end


Or server can be execute into a thread: 

.. code-block:: python 

   session = server.start()
   # you can still interact with the "engine". Can be handy for debugging puposes
   print(engine.intensity)
   engine.init()
   session.stop() # stop the thread 


Principles 
----------

To go deeper in the implementation details.

We believe the code is quite simple and self explanatory for the majority of it. 

But, here is the basice principles: 

- The :class:`ifw.fcfsim.opcua.api.UaServer` is a high level class containing
   - an asyncua.Server
   - object containing opcua links :class:`ifw.fcfsim.opcua.links.UaLinks`
     It does link opcua node and engine data 
   - a Node builder which collect nodes and build them on the server when 
     it is started. 


- The Session Instance hold the engine and all runners. In our above example there is only one runner which takes carre of building the OPC-UA Server and handle comminucation between engine and server. In case of device simulator for instance we have 2 more runners, one for the state machine, an other one for simulation thread.
- The OPC-UA Server Runner is made thanks to a :class:`ifw.fcfsim.opcua.api.ServerManager`. It holds an ``asyncua.Server``, the links between engine to opcua thanks to the profile input and the factory of server nodes and rpc. 

In the above example what is done is basically: 


.. code-block:: python 

   server_manager = opcua.ServerManager(endpoint="opc.tcp://localhost:4840")
   engine = MyLampEngine()
   # build the links between engine and server 
   server_manager.add_profile(my_lamp_profile, engine, namespace=4, prefix="MAIN.Lamp1")
   runner = server_manager.new_runner(core.Looper(1/10.))
   await runner.initialise()
   await runner.run()
   # or core.run_int_thread(runner)


 
