#####################################################
Opcua Package
#####################################################

This Package provide tools to easily build an OPCUA server and client  


.. toctree::
   :maxdepth: 1
   :caption: Contents:

   introduction.rst
   profile.rst
   server.rst 
   client.rst
