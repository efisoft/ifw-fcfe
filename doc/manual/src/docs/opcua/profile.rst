OPC-UA Profile
==============

The goal of a OPCUA profile object is to provide the mapping between 
variables and the OPCUA session as well as define some OPCUA variable 
types for the server.   

They are several way to load a profile: 

From a yaml file: 

.. code-block:: python
    
   from ifw.fcfsim.opcua import api as opcua 
   profile = opcua.read_profile("my_namespace_profile.yaml")

From a dictionary: 

.. code-block:: python 

   from ifw.fcfsim.opcua import api as opcua 
   profile = opcua.mapping_to_profile({ 
      'stat.intensity': 'stat.lrIntensity(Double)', 
      'cfg.timeout': 'cfg.nTimeout(Int16)', 
      'switch_on': 'method.RPC_SwitchOn(I:in_lrIntensity(Double), I:in_nTimeOnLimit(UInt32), O:ReturnValue(Int16))'
      # etc ...
   })

Or, for the braves, directly from python objects:  

.. code-block:: python 

   from ifw.fcfsim.opcua import api as opcua 
   profile = opcua.OpcuaProfile(
                 nodes={  
                    'stat.intensity': opcua.NodeProfile('stat.lrIntensity', 'Double'), 
                    'cfg.timeout': opcua.NodeProfile('cfg.nTimeout', 'Int16') # 'Int16' can be replaced by 4
                 }, 
                 rpc_methods = {
                    'switch_on': opcua.RpcProfile("", "RPC_SwitchOn", 
                                       inputs=[opcua.RpcArgumentProfile('in_lrIntensity', 'Double'),
                                               opcua.RpcArgumentProfile('in_nTimeOnLimit', 'UInt32')], 
                                        outputs=[opcua.RpcArgumentProfile('ReturnValue','Int16' )] 
                                )
                 }
        )
   # node can be added after    
   profile.add('stat.time_left', opcua.NodeProfile('stat.nTimeLeft', 'UInt32'))


Profile Handling 
----------------

Profile can be copied with optional added prefix to the OPCUA Space and to 
python data namespace: 

.. code-block:: python 

   from ifw.fcfsim.opcua import api as opcua 
   profile = opcua.mapping_to_profile({ 
      'stat.intensity': 'stat.lrIntensity(Double)', 
      'cfg.timeout': 'cfg.nTimeout(Int16)', 
      'switch_on': 'method.RPC_SwitchOn(I:in_lrIntensity(Double), I:in_nTimeOnLimit(UInt32), O:ReturnValue(Int16))'
   })

   prefixed_profile = profile.copy( opcua_prefix="MAIN.Lamp1", data_prefix='lamp1' ) 
   
   assert profile.nodes['stat.intensity'].path.format(4) == 'ns=4;s=stat.lrIntensity'
   assert prefixed_profile.nodes['lamp1.stat.intensity'].path.format(4) == 'ns=4;s=MAIN.Lamp1.stat.lrIntensity'


They can also be merged, which is usefull to compose with several profie files: 

.. code-block:: python 

   profile = opcua.OpcuaProfile() 
   profile.merge(
       opcua.read_profile('config/fcfsim/devices/motor/motor.namespace.yaml'), 
       opcua_prefix = "MAIN.Motor1", 
       data_prefix = "motor1"
   )
   profile.merge(
       opcua.read_profile('config/fcfsim/devices/lamp/lamp.namespace.yaml'), 
       opcua_prefix = "MAIN.Lamp1", 
       data_prefix = "lamp1"
   )


Dumping Profile to a XML NodeSet 
--------------------------------

Conventionaly in the OPCUA world the profile is defined inside NodeSet in 
a xml file type. We can create such file from a profile. 


.. code-block:: python 

   profile = opcua.read_profile('config/fcfsim/devices/motor/motor.namespace.yaml')
   node_set = opcua.XmlNodeSetManager(prefix="MAIN.Motor1", namespace=6)
   node_set.add_profile(profile) 
   # print result
   print(node_set.export())
   # save in a file 
   node_set.write("/tmp/my_nodeset.xml")

Note: They are some assumptions about the type of intermediate objects which are not defined in the profile such as "MAIN" (assumed to be a programm) and "Motor1" assumed to be a "device" e.i. a Function Block. This can be changed with ``program_level`` and ``device_level`` optional arguments. 

Note2: The prefix is here added to the :class:`ifw.fcfsim.opcua.XmlNodeSetManager`, it can also be added to a profile.copy() has show above. 

Note3: If a node already exists in the NodeSet it is ignored silently


Namespace file 
--------------

ToDo Doc on the profile parser 



