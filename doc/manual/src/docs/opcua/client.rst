OPCUA Client
============ 

The wrapper around OPC-UA client work similarly to the Server. The same "Engine" and profile can be used for both client and server. The Engine is then used to manipulate OPC-UA data and send method.

Following the same example for Server: 

.. code-block:: python 

   from ifw.fcfsim.core import api as core 
   from ifw.fcfsim.opcua import api as opcua
   from dataclasses import dataclass 
   import enum

   class MyLampError(enum.IntEnum): 
       OK = 0 
       NOT_INITIALIZED = 1


   @core.attrconnect
   @dataclass
   class MyLampEngine:
       intensity: float = 0.0
       initialized: bool = False
       
       def init(self) -> int:
           self.initialized = True 
           return MyLampError.OK 
           
       def switch_on(self, intensity: float) -> int:
           if not self.initialized:
               raise core.RpcException(
                    'Lamp is not initialized', MyLampError.NOT_INITIALIZED)
           self.intensity = intensity 
           return MyLampError.OK
        
       def switch_off(self) -> int:
           if not self.initialized:
               raise core.RpcException(
                    'Lamp is not initialized', MyLampError.NOT_INITIALIZED)
           self.intensity = 0.0 
           return MyLampError.OK
   
   my_lamp_profile = opcua.mapping_to_profile({
      'intensity': 'stat.lrIntensity(Double)', 
      'initialized': 'stat.bInitialized(Boolean)', 
      
      'init': 'method.RPC_Init(O:ReturnValue(Int16))', 
      'switch_on': 'method.RPC_On(I:in_lrIntensity(Double), O:ReturnValue(Int16))', 
      'switch_off': 'method.RPC_Off(O:ReturnValue(Int16))', 
   })


We can similarly run a client (assuming that the server is running). 


.. code-block:: python 
 
   from ifw.fcfsim.opcua import api as opcua
   client_config = opcua.UaClientConfig(
                     prefix="MAIN.Lamp", 
                     endpoint="opc.tcp://localhost:4840",
                )
              
   client = opcua.UaClient(client_config, MyLampEngine(), my_lamp_profile)
   
   client_session = client.new_session()
   l1 = client_session.new_thread().start()

   l1.init()
   l1.switch_on(50.0)
   
   client_session.stop()


One can also use a context manager on the session, the client is connect at enter and closed at exit:


.. code-block:: python 

   with client_session as l1:
        l1.init()
        l1.switch_on(50.0)

.. note:: 

    One important thing to notice is that the linked OPC-UA methods are overwritten to a client call when the client session stars. If one wants to have a dedicated Client engine it is probably better to explicitely write method with a NotimplementedError: 

    .. code-block:: python 

       def init(self) -> int:
           raise NotimplementedError("Cannot execute init: waiting for a client connection")






