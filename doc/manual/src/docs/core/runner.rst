Session, Runner, Task, ...
==========================

Introduction
------------

The core package offers tools to run business logic in loops. 
From the highest level to the lower :

- A Session: :class:`ifw.fcfsim.core.api.ISession`
     High level object handling the execution of a runner and holding 
     the business logic class (called "Engine")

- A Runner: :class:`ifw.fcfsim.core.api.IRunner` 
     Dedicated to run asynchronous (or synchronous) things 
     in a loop. 

- A Task: :class:`ifw.fcfsim.core.api.ITask`
   Hold the execution of one cycle and can be used by Runner 
   The task method is also responsible to handle the start and the end of 
   a process.

- A Looper: :class:`ifw.fcfsim.core.api.ILooper` 
   Tool use to loop the execution of one asynchronous method. Most likely used by Runner.

- Timer: :class:`ifw.fcfsim.core.api.ITimer`
   Tool to handle cycle time. Most likely used by Looper.

- Signals: :class:`ifw.fcfsim.core.api.ISignals`
    An object holding signals (integers) coming from business logic and which 
    can be used to interrupt a program properly.

This is typically used to: 

- Run an OPCUA-Server or Client 
- Run a scxml4py State Machine 
- Run device simulator business logics ...

Usage
-----

Most likely the only class to make is a Task, for all other object types base classes 
are offered.  

Here an example with the inclusion of an optional "Engine" which take cares of business logics:
 
.. code-block:: python 


        from ifw.fcfsim.core import api as core 
        from dataclasses import dataclass , field

        @dataclass
        class MyEngine: # can be anything 
            signals: core.ISignals = field(default_factory=core.get_global_signals)
            counter: int = 0 
            def reset(self) -> None:
                self.counter = 0 
                self.signals.reset()
                
            def increment(self) -> None:
                self.counter += 1
                print(f"Step {self.counter}")
                if self.counter>10:
                    self.signals.exit(0)

        class MyTask(core.BaseTask):
            def __init__(self, engine: MyEngine):
                self.engine =  engine
                
            async def initialise(self) -> None:
                # Initialise some object, build 
                ...
        
            async def start(self) -> None:
                # Start a session, maybe establish a client/server connection 
                self.engine.reset()

            async def next(self) -> None:
                # Execute cycle business logic
                self.engine.increment()
            
            async def end(self) -> None:
                # Close any connections or delete some objects etc ...
                ...

Once a task class was settled it can be used with a TaskRunner :

.. code-block:: python 

   engine = MyEngine()
   runner = core.TaskRunner(MyTask(engine),  core.Looper(0.1, signals=engine.signals))
   await runner.initialise()
   await runner.run()

In the example above the task runs at 10Hz until the exit signals has been receive (``signals.exit(0)``)
 coming from somewhere in the business logic (here from the MyEngine instance). 

The signals object returned by :func:`ifw.fcfsim.core.api.get_global_signals()` is also listening to 
SIGTERM and SIGINT (Ctrl-C) and is sending ``signals.exit(<SIG_CODE>)``.


One can also run a runner in a thread : 

.. code-block:: python 

   core.run_in_thread(runner)


One can group several tasks with the class :class:`ifw.fcfsim.core.api.TaskGroup` which respect the ITask Interface.

Also runners can be grouped with :class:`ifw.fcfsim.core.api.RunnerGroup`

Finally a Session is the highest level which can be used as standard interface to execute an app:



.. code-block:: python 

    session = core.Session(engine, runner)
    session.new_thread().start()

Session understand context manager:


.. code-block:: python 

   with session as obj:
        print("App running ...")
   print("App ended")

In our dummy example this should stop the runner after the first step. But it can be handy to send message to a server for instance. 

For a realiastic see devsim and OpcuUa Client: 

.. code-block:: python 

   from ifw.fcfsim.devices.lamp import api as lamp
   from ifw.fcfsim.opcua import api as opcua 
   client_config = opcua.UaClientConfig(endpoint="opc.tcp://127.0.0.1:4840", prefix="MAIN.Lamp1", namespace=4) 
   lamp_cl = lamp.LampUaClient(client_config) 

   with lamp_cl.new_session() as lamp1:
       print("Intensity: ", lamp1.stat.intensity)
   with lamp_cl.new_session() as lamp1:
       lamp1.on(30.0,2) # 30%, 2 seconds
   # etc ...

   
Typing Interface
----------------

Available on the :mod:`ifw.fcfsim.core.api` API module.

.. automodule:: ifw.fcfsim.core.api
   :members: ISession, IRunner, ITask, ILooper, ITimer, ISignals
   :undoc-members:

Index
-----

Available on the :mod:`ifw.fcfsim.core.api` API module.

.. automodule:: ifw.fcfsim.core.api
   :members: Session, TaskRunner, RunnerGroup,  BaseTask,  TaskGroup, Looper, Timer, AppSignals, get_global_signals, run_in_thread
   :undoc-members:


