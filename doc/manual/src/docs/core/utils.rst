Core Utils
==========

Here is a collection of usefull tools from the core package not covered in other doc section. All available in the core API package.



IO
--

.. automodule:: ifw.fcfsim.core.api
   :members: load_cii_config, CiiConfigReader, YamlReader
   :undoc-members:


Enums 
-----

.. automodule:: ifw.fcfsim.core.api
   :members: get_enum_text, get_error_text, set_enum_text, get_enum_name, get_code_and_text


Log
---

.. automodule:: ifw.fcfsim.core.api
   :members: get_logger, LogConfig, print_existing_loggers, init_loggers, init_config_logger


Generic 
-------

.. automodule:: ifw.fcfsim.core.api
   :members: wait, join_path, split_path, resolve_data_path, is_linkable, callback
   :synopsis: Generic

