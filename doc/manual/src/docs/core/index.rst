#####################################################
Core Package
#####################################################

This core package is meant to provide tools for the development of python application 
as an opcua server with a state machine engine or a device simulator. 

Contrary to other previous packages, it provides base tools rather than a bunch of 
base classes to inherit from. 

The package is fully annotated and rely on code linters such as mypy or pyright with 
the strict constrains that everything has to be type annotated for reliability and test 
purposes. 

Most of classes have an Interface class used for the programmer to annotate its own code 
and make abstraction of concrete classes. 

**Some Conventions**

The package itself is built using composition rather than inheritance and is meant to be used 
this way. This imply that it has a bunch of small classes with restricted functionality (typically one). 
Therefore it is useful to follow some naming rules to quickly understand the components. 

- A class starting by **I** and followed by a capital leter is an *interface*, (a python Protocol), and shall 
  not be instancied but used for `typing <https://en.wikipedia.org/wiki/Duck_typing>`_  annotations or concrete class construction. 
  For instance : :class:`ifw.fcfsim.core.ISignals`


.. toctree::
   :maxdepth: 1
   :caption: Contents:
   
   config.rst
   exceptions.rst 
   data.rst 
   runner.rst
   utils.rst 

.. only:: html
        Indices and tables
        ==================

        * :ref:`genindex`
        * :ref:`modindex`


