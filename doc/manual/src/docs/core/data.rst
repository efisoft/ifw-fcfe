Data Node
=========

The idea behind this abstraction layer of how to get,set and connect data was 
born when trying to mimic what is done in TwinCat/PLC project: the ability to handle 
an OPC-UA server and its values by simply manipulating a data structure receiving methods.

However, this can be used as well to make clean abstraction of server/client 
publish/subscribe connection. 

We introduce here a new type of object a DataNode with the interface defined in 
:class:`ifw.fcfsim.core.IDataNode`. This is basically a getter/setter of a value 
with callback connection (subscription) capability. The real value is located somewhere 
than the DataNode, for instance inside a python object but can be virtually anything. 

So, we want to interact with normal python structure and background processes 
are taking care of publishing or fetching data. This ia what the opcua module is doing.
The user code can then focus on business logic and forgot about 
all the data transfer.

Let us considers this data structure : 

.. code-block:: python 

   from dataclasses import dataclass 

   @dataclass
   class Position:
        x: float = 0.0 
        y: float = 0.0 

We would like that each times ``x`` and ``y`` are modified in a data instance a 
callback method is triggered (for instance to update an OPCUA server with the new value).
For this we only need one decorator on the class to allow its attribute connection. This 
decorator only change the ``__setattr__`` method. The python Class has no dependency or 
inheritance: 

.. code-block:: python 

   from dataclasses import dataclass, field
   from ifw.fcfsim.core import api as core 
   
   @core.attrconnect 
   @dataclass
   class Position:
        x: float = 0.0 
        y: float = 0.0 

Connection can be made to a callback using a "Data Node" and as 
returned by the generic :func:`ifw.fcfsim.core.data_node` function. 

.. code-block:: python 
   
   def callback(value):
       print( "new value", value )
   pos = Position()
   core.data_node(pos, "x").connect(callback)
   
   pos.x = 10.0 
   # Prints:  new value 10  

On top of connect method data nodes have get/set methods and carry 
simple metadata informations.

.. code-block:: python 

   node = core.data_node(pos, "x")
   node.set(11)
   # Prints:  new value 11
   assert 11 == pos.x
   assert 11 == node.get()
   node.trigger()
   # Prints:  new value 11
   
   last_modification_datetime = node.get_metadata().timestamp
   

As mentioned above, the ``attrconnect`` decorator make any classes a valid 
Data Container for Data Node with connection capabilities.  
On top of this, exists two other builtin Data Container with connection:

- ``Array`` is basicaly a list with a fixed size 
- ``Map`` is a dictionary. 

They both are data container with data node connection capabiliies. 
They  can be combined into a structured data: 

.. code-block:: python 

    @core.attrconnect 
    @dataclass
    class Status:
        position: core.Array[float] = field(default_factory=lambda: core.Array(2, float))
        keywords: core.Map[str] = field(default_factory=core.Map)
        n: int = 0 
    
    @dataclass
    class Device:
        status: Status = field(default_factory=Status)
    
    def callback(value):
       print( "new value", value )

    device = Device()
    device.status.keywords['my_key'] = ""
    core.data_node(device, "status.position[0]").connect(callback)
    core.data_node(device, "status.keywords[my_key]").connect(callback) 

    device.status.position[0] = 10.0 
    #prints new value 10.0 

    device.status.keywords['my_key'] = "Hello"  
    #prints new value Hello 


Note that device is not decorated with ``attrconnect``, it works because
``Device`` doesn't own attribute to be connected. The connection of ``n`` 
for instance is saved inside the ``Status`` instance, connection of ``[0]``
is stored inside the position ``Array`` instance, etc. 

This has important implications: if ``status`` is replace by a new ``Status()``
instance on the device structure the connections will be lost ! 
In other word, **do not do:** ``device.status = Status(n=10)``, unless you want 
to drop all connections intentionally. 

This will conserve connections: 

.. code-block:: python 
   
        device.status.n = 10
        device.status.position[:] = [0.0]*len(device.status.position)
        #Prints: new value 0.0 


Note : 

.. code-block:: python 
   
   node = core.data_node(device, "status.position[0]")
   assert node == core.data_node(device.status, "position[0]")
   assert node == core.data_node(device.status.position, "[0]")


Alias and dependencies
----------------------

Let us considers this class with a property: 

.. code-block:: python 

   from ifw.fcfsim.core import api as core 
   from dataclasses import dataclass
   import math 
   
   @core.attrconnect
   @dataclass
   class Position:
       x: float = 0.0 
       y: float = 0.0 
       
       @property 
       def radius(self) -> float:
           return math.sqrt(self.x**2 + self.y**2) 

The problem with the example above is that any connection to 
``radius`` will not be triggered when ``x`` or ``y`` is triggered.

To fix this we can decorate the ``radius`` property method with 
``dependencies`` to tell which attribute modification shall 
trigger a ``radius`` connection: 

.. code-block:: python 

   from ifw.fcfsim.core import api as core 
   from dataclasses import dataclass
   import math 
   
   @core.attrconnect
   @dataclass
   class Position:
       x: float = 0.0 
       y: float = 0.0 
       
       @property 
       @core.dependencies("x", "y")
       def radius(self) -> float:
           return math.sqrt(self.x**2 + self.y**2) 

   def callback(v): print("Radius " , v)

   pos = Position()
   core.data_node(pos, "radius").connect(callback, False)  
   pos.x = 4.0
   #Prints: Radius 4.0 
   pos.y = 3.0
   #Prints: Radius 5.0


Also an alias node can be created outside of the data structure using :func:`ifw.fcfsim.core.api.node_alias`. 


.. code-block:: python 

    pos = Position(x=4.0, y=3.0)
    node_x = core.data_node(pos, 'x')
    node_y = core.data_node(pos, 'y')

    radius_node = core.node_alias( 
          (node_x, node_y), 
          lambda x,y: math.sqrt(x*x + y*y)
          )

    radius_node.connect(callback, False)
    
    pos.x = 0.0 
    # Prints: Radius 3.0

This can be handy for instance when the data node is linked to a GUI or 
to make computation on the fly on an OPCUA-server.

Note that if we alias to a single node, the "transfer function" can be omitted and 
is considered as ``lambda v:v`` 


Classes
-------

.. automodule:: ifw.fcfsim.core.node.node
   :imported-members:
   :members:
   :undoc-members:



