
Exceptions
==========

Core Provide a few specific Exceptions


Index
-----

Available on the :mod:`ifw.fcfsim.core.api` API module.

.. automodule:: ifw.fcfsim.core.exceptions
   :members: 
   :undoc-members:



