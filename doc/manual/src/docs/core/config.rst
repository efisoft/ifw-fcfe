Configuration
=============

Introduction
------------

Toolkits and Protocols to load configuration like object.

When building application we want to rely on a clear python class 
which hold all configuration parameters. We do not want to rely, for instance, 
on a dictionary or other dynamic container loaded from a configng file ; it would 
be difficult to maintain and a source of bugs. 

Core offer some tools to parse config file and/or command line arguments and 
load some parameters into a python object. 

This may seem a bit complicated at first but it has great benefice to have a solid 
configuration object and avoid problems during run time. 

Example of a config class, using dataclass : 

.. code-block:: python 

   from dataclasses import dataclass 
   
   @dataclass
   class MyConfig:
        endpoint: str = "opc.tcp://localhost:4840"
        prefix: str = "MAIN"
        namespace: int = 4 

Config loader 
-------------

The :class:`ifw.fcfsim.core.ConfigLoader` is an helpers to load a config instance 
from Cii config. 
It loads parameters inside the config file namespace into a python object 

By default the :class:`ifw.fcfsim.core.ConfigLoader` try to loads parameters as they
are into an attribute of the config object. 


.. code-block:: python 

   from dataclasses import dataclass 
   from ifw.fcfsim.core import api as core  

   @dataclass
   class MyConfig:
        endpoint: str = "opc.tcp://localhost:4840"
        prefix: str = "MAIN"
        namespace: int = 4 

   my_config = MyConfig() 
   loader = core.ConfigLoader(extras=core.Extras.ignore) 
   # and / or 
   loader.load_dict(my_config, {'prefix': "MAIN.MyStuff", "not_for_this_config": 0})
   

In the example above ``extras='ignore'`` implies that the loader will ignore attribute 
not defined in the config object. Other values are 'allow' -> try to set the attribute 
anyway, and 'forbid' -> an error is raised.

One can quickly add some basic parsing rules into the ConfigLoader : 

.. code-block:: python 

    loader = core.ConfigLoader(parsers = {'namespace': int})
    loader.load_dict(my_config, {'namespace': "6"})
    assert my_config.namespace == 6

For other more complex parsing the ``dispatch`` method can be override, for instance:

.. code-block:: python 
   
   from typing import Any, Iterator

   class MyConfigLoader(core.ConfigLoader): 
       def dispatch(self, values: dict[str, Any]) -> Iterator[tuple[str,Any]]:
           if 'address' in values:
               if 'endpoint' in values: 
                   raise ValueError("'address' and 'value' are both set")
               else: 
                   print("Warning: 'address' is deprecated use 'endpoint' instead")
                   yield 'endpoint' , values.pop('address')
           yield from super().dispatch(values) 

   loader = MyConfigLoader()
   loader.load_dict(my_config, {'address':'opc.tcp://somewhere:4840'}) 
   assert my_config.endpoint == 'opc.tcp://somewhere:4840'

For high level application using command line arguments,
the ``load_argv`` function can be defined to parse a list of 
arguments (as returned by ``sys.argv[1:]``) into the python config object. 


.. code-block:: python 

        import typing 
        from ifw.fcfsim.core import api as core 
        import argparse

        class IUaClientCmdArgs(typing.Protocol):
            """Expected properties comming out from the args parser"""
            address: str
            port: str 
            prefix: str
            ns: int
        
        def get_argument_parser(
            arg_parser: argparse.ArgumentParser | None = None
        ) -> argparse.ArgumentParser:
            """ Return the application argument parser """
            if arg_parser is None:
                    arg_parser = argparse.ArgumentParser(description="My App")
            arg_parser.add_argument(
                "-a", "--address", 
                type=str, required=True, help="target ip Address e.g. 134.171.59.98"
            )
            arg_parser.add_argument(
                "-p", "--port", 
                type=int, required=False, default=4840, help="OPCUA port e.g.: 4840"
            )

           
            arg_parser.add_argument(
                "-x", "--prefix",
                type=str, required=True, 
                help="Device prefix, e.g. MAIN.Motor1",
            )

            arg_parser.add_argument(
                "-n", "--ns",
                type=int, required=False, 
                help="OPCUA namespace, e.g. 4", default=4
            )
            return arg_parser
        
        class MyConfigLoader(core.ConfigLoader):
            def load_argv(self, config: typing.Any,  argv:list[str]) -> None:
                """ Update configuration from an argv list 

                Warning: This method can trigger a SystemExit(0) (e.g. in case of --help)
                """
                args_parser = get_argument_parser()
        
                if "-h" in argv or "--help" in argv:
                    args_parser.print_help()
                    raise SystemExit(0)

                args = typing.cast(IUaClientCmdArgs, args_parser.parse_args(argv))
                config.prefix = args.prefix 
                config.endpoint = f"opc.tcp://{args.address}:{args.port}"
                config.namespace = args.ns
        
        loader = MyConfigLoader()
        loader.load_argv( my_config, ["-a", "127.0.0.1", "-x", "MAIN", "-p", "4850"])

        assert my_config.endpoint == 'opc.tcp://127.0.0.1:4850'


Config Factory 
--------------

From a configLoader and a python class one can create a config_factory object allowing 
a generic interface for applications to instantiate a config object: 


.. code-block:: python 

   my_config_factory = MyConfigLoader(extras='forbid').new_factory(MyConfig)

Usage : 

.. code-block:: python 

   config = my_config_factory.from_cii_config('path/to/resource/my_config.cfg.yaml')
   # or 
   config = my_config_factory.from_argv(sys.argv[1:])
   # or 
   config = my_config_factory.from_dict({'namespace':6})



Typing Interface
----------------

Available on the :mod:`ifw.fcfsim.core.api` API module.

.. automodule:: ifw.fcfsim.core.iconfig
   :members: IConfigLoader, IConfigFactory
   :undoc-members:

Index
-----

Available on the :mod:`ifw.fcfsim.core.api` API module.

.. automodule:: ifw.fcfsim.core.config
   :members: ConfigLoader, ConfigFactory, Extras, parse_values   
   :undoc-members:



