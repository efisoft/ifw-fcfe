#####################################################
State Machine Package
#####################################################


This package is a wrapper around scxml4py in order to facilitate the creation of state machine business logics. 

It redefine ``Action``, ``Activity``, ``EventListener`` and ``StatusListener`` classes in order to work more nicely with composition rather than inheritance. 

Example: 

.. code-block:: python 

   from ifw.fcfsim.state_machine import state_machine as sm 

   def my_activity_function(handler: sm.IActivityHandler) -> None:
       while handler.is_running(): 
           ... # So something
   my_activity = sm.ThreadedActivity("MyActivity", my_activity_function)

Again, in the example above we have nicely separated business logics to the scxml4py business using composition. 

The ugly inheritance side would be equivalent to do this in pure scxml4py: 

.. code-block:: python 

    import scxml4py.activity
    import scxml4py.event
    from queue import Queue

    class MyActivity(scxml4py.activity.ThreadedActivity): 
        def __init__(self, event_queue: Queue[scxml4py.event.Event] | None = None) -> None:
            super().__init__("MyActivity", event_queue)
            
        def run(self) -> None:
            while self.isRunning(): 
                ... # Do Something 
    
    my_activity = MyActivity()


But this package shine with the use of decorators and the :class:`ifw.fcfsim.state_machine.state_machine.StateMachineManager`

Here is an example: 

.. code-block:: python 

    from dataclasses import dataclass, field
    from ifw.fcfsim.state_machine import state_machine as sm 
    import queue 
    import time
    import typing
     
    @dataclass 
    class MySmBusiness:
        event_queue: queue.Queue[sm.Event] = field(default_factory=queue.Queue) 
        """event_queue is probably the only thing we need inside the Business logic 
        in order to push event in the queue 
        """ 
        some_data: dict[str,typing.Any] = field(default_factory=dict)
        """Dummy data for illustration purposes"""
     
@sm.event_listener_method
        def listen_event(self, event: sm.Event) -> None:
            print(f"Receive event {event!r}")

        @sm.status_listener_method
        def listen_state(self, states: set[sm.State]) -> None:
            ids = [state.getId() for state in states]
            print(f"states has changed to {ids}")
      
          
        @sm.action_method("Init.Execute")
        def init_execute_action(self, 
           handler: sm.IActionHandler, 
           context: sm.Context
        ) -> None:
            print(f"Receive Action {handler.get_id()!r} ")
            # do something 

        @sm.activity_method("Initialising")  
        def activity_initialising(self, handler: sm.IActivityHandler) -> None:
            while handler.is_running(): 
                time.sleep(2.0) 
                handler.send_internal_event(sm.Event("InitDone"))


The decorators are very simple, they are not altering the methods.  
They just add the state machine kind (activity, action, event_listener, status_listener) to 
the python function object as well as their ID for activity and action.
The IDs must be as they appear in the scxml file.  

What we have done above is just regular python code to run a scxml4py state machine over it 
we will use :class:`ifw.fcfsim.state_machine.state_machine.StateMachineManager` and its collect method. Or a even higher class :class:`ifw.fcfsim.state_machine.state_machine.StateMachine` which will do the job if all business logic is written on one object:

.. code-block:: python 

    event_queue = queue.Queue() 
    my_business = MySmBusiness(event_queue=event_queue) 
    sm_session = sm.StateMachine("path/to/sm.scxml", my_business, event_queue).new_session()
    sm_session.new_thread().start() # to run on thread 
    # or using async methos
    # await session.run()

If more control is needed we can build the state machine runner from 
:class:`ifw.fcfsim.state_machine.state_machine.StateMachineManager` ::

.. code-block:: python 
     
    event_queue = queue.Queue() 
    my_business = MySmBisness(event_queue=event_queue) 

    sm_manager = sm.StateMachineManager("path/to/sm.scxml", event_queue=event_queue)
    sm_manager.collect(my_business) 
    sm_manager.collect( an_other_business_object )
    sm_manager.links.add_activity( MyActivity(...) ) # We can add an old fashion activity instances 
    # etc ...

    # to run it in a thread  
    from ifw.fcfsim.core import api as core
    core.run_in_thread(sm_manager.new_runner(core.Looper(0.1)))



One can also use an even lower class to manage the scxml4py state machine handling : 

.. code-block:: python 

   sm_links = StateMachineLink()
   sm_links.collect( my_business, event_queue)
   sm_links.add_activity( ... )
   model = sm_links.make_model("path/to/sm.scxml.py") # make scxml4py.stateMachine.StateMachine 
   executor = sm_links.make_model("path/to/sm.scxml.py", model=model) # make a scxml4py.executor.Executor
   ... 
    


   

      
       
