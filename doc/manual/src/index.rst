.. ifwe-core documentation master file, created by
   sphinx-quickstart on Wed Jun 12 09:00:11 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


#####################################################
ELT ICS Devsim2 and dependencies  Package description
#####################################################


.. Warning::

   This doc is meant to be reshuffled and included into ESO documentations. 


.. toctree::
   :maxdepth: 1
   :caption: Contents:
   
   docs/core/index.rst
   docs/opcua/index.rst 
   docs/state_machine/state_machine.rst 

.. only:: html
        Indices and tables
        ==================

        * :ref:`genindex`
        * :ref:`modindex`






















* :ref:`search`
