from __future__ import annotations
import functools
from typing import  Any, Iterable, Protocol, runtime_checkable
import typing

import  asyncua 
from ifw.fcfsim.opcua.ilinks import ILinkModeResolver, ISessionLinker
from ifw.fcfsim.opcua.iprofile import INodeProfile, IOpcuaProfile, IRpcProfile
from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua.server_mgr import ServerManager 


T = typing.TypeVar('T', bound=Any, covariant=True)

class IServerInterface(Protocol[T]):
    def get_engine(self) -> T:
        ...
    
    def add_to_server(self, server_manager: ServerManager, /) -> None:
        ...



