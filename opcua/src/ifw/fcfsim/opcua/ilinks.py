from __future__ import annotations
from enum import IntEnum
from typing import Any, Callable, Protocol,  Union
from ifw.fcfsim.opcua.iprofile import INodeProfile, IOpcuaProfile, IRpcProfile
from typing_extensions import TypeAlias
from ifw.fcfsim.core import api as core 
import asyncua 
from asyncua import ua

PUSH_BYTE: int = 0 
PULL_BYTE: int = 1 
SUBSCRIBE_BYTE: int = 2

ISession: TypeAlias = Union[asyncua.Server, asyncua.Client]
IUaValue: TypeAlias = Any # TODO make the Union of python type compatible with opcua 
IIdInput: TypeAlias = Union[str, INodeProfile, IRpcProfile, asyncua.Node, ua.NodeId]

ILinkModeResolver = Callable[[INodeProfile, bool], int]
"""Callable to resolve a link mode from profile """


class LinkMode(IntEnum):
    PUSH = 2**PUSH_BYTE
    PULL = 2**PULL_BYTE
    SUBSCRIBE = 2**SUBSCRIBE_BYTE
    PUSH_PULL = 2**PUSH_BYTE + 2**PULL_BYTE
    PUSH_SUBSCRIBE = 2**PUSH_BYTE + 2**SUBSCRIBE_BYTE
    PULL_SUBSCRIBE = 2**PULL_BYTE + 2**SUBSCRIBE_BYTE
    PUSH_PULL_SUBSCRIBE = 2**PUSH_BYTE + 2**PULL_BYTE + 2**SUBSCRIBE_BYTE

class IIdResolver(Protocol):
    def resolve_id(self, 
          id_input:IIdInput
        ) -> ua.NodeId:
        """ Build a ua.NodeId from various input arguments """
        ...
    def resolve_method(self, name:str) -> str:
        """ Resolve s tring method name """
        ...

class IPushConnector(Protocol):
    def connect(self, data_node: core.IDataNode[Any], stack: bool = True) -> None:
        """Connect a data Node to push 

        Data node value change pushed on session"""
        ...
    
    def disconnect(self, data_node: core.IDataNode[Any]) -> None:
        """Disconnect from a data node"""
        ...

class IPullConnector(Protocol):
    def connect(self, data_node: core.IDataNode[Any]) -> None:
        """Connect a data Node to pull
        
        Connected UaNode value are pulled into the data node 
        """
        ...
    
    def disconnect(self, data_node: core.IDataNode[Any]) -> None:
        """Disconnect data node from pull"""
        ...

class ISubscriptionConnection(Protocol):
    datanode: core.IDataNode[Any]
    log: core.ILogger
    def receive(self, value:Any, data_value: ua.DataValue)->None:
        ...

class ISubscriptionConnector(Protocol):
    def connect(self, data_node: core.IDataNode[Any]) -> None:
        """Connect a data Node for subscription 
        
        Connected UaNode value are pulled automatically into the 
        data node when changed on server or client
        """
        ...
    
    def disconnect(self, data_node: core.IDataNode[Any]) -> None:
        """Disconnect data node from subscription"""
        ...
  
class IRpcConnector(Protocol):
        def connect(self, 
         imethod:Callable[...,Any] | core.IMethodNode[Callable[..., Any]], 
         rpc_profile: IRpcProfile | None
        ) -> None:
            ...


class IUaLinks(Protocol):
    def new_push_connector(self, nodeid: ua.NodeId) -> IPushConnector:
        ...

    def new_pull_connector(self, nodeid: ua.NodeId) -> IPullConnector:
        ...

    def new_subscription_connector(self, 
            nodeid: ua.NodeId, 
            Connection: Callable[[core.IDataNode[Any]], ISubscriptionConnection] = ...
        ) -> ISubscriptionConnector:
        ...
    
    def new_rpc_connector(self, nodeid: ua.NodeId) -> IRpcConnector:
        ...
    
    def new_coro_connector(self, nodeid: ua.NodeId) -> IRpcConnector:
        ...   
    
    def clear(self) -> None:
        ...

    async def push(self, session: ISession) -> None:
        ...

    async def pull(self, session: ISession) -> None:
        ...
    
    async def pull_check(self, session: ISession,  raise_error: bool = False) -> None:
        ...

    async def subscribe(self, session: ISession) -> None:
        ...

    async def link_method(self, session: ISession) -> None:
        ...
    
    async def execute_coro(self) -> None:
        ...
    
    async def relink(self, session: ISession) -> None:
        ...


class ISessionLinker(Protocol):
    def link_rpc(self,
          imethod: Callable[..., Any] | core.IMethodNode[Callable[..., Any]],
          rpc_profile: IRpcProfile) -> None:
        ...

    def link_node(self,
          datanode: core.IDataNode[Any],
          profile: INodeProfile,
          mode: int | None = None,
          stack: bool = True) -> None:
        ...

    def link_profile(self,
            obj: Any,
            profile: IOpcuaProfile,
            stack: bool = True) -> None:
        """ Link data object with an OpcuaProfile """
        ...

class ILinkEngine(Protocol):
    async def push(self) -> None:
        ...
    
    async def pull(self) -> None:
        ...
    
    async def execute_coro(self) -> None:
        ...

class IUaInfo(Protocol):
    connected: bool
    endpoint: str
