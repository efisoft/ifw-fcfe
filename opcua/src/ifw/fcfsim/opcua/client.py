from __future__ import annotations

import abc
import typing
from dataclasses import InitVar, dataclass

import asyncua
from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import defaults
from ifw.fcfsim.opcua.client_mgr import ClientManager
from ifw.fcfsim.opcua.iclient import IClientInterface
from ifw.fcfsim.opcua.iprofile import IOpcuaProfile
from ifw.fcfsim.opcua.profile_parser import read_profile

log = core.get_logger("opcua.client")

@core.attrconnect
@dataclass 
class UaInfo: # conform to IUaInfo
    """Data class used to hold Ua connection information"""
    endpoint: str = ""
    """server endpoint"""
    connected: bool = False 
    """True if connected to server"""
    
    @property
    def endpoint_node(self) -> core.IDataNode[str]:
        return core.node.AttributeNode(self, 'endpoint')

    @property 
    def connected_node(self) -> core.IDataNode[bool]:
        return core.node.AttributeNode(self, 'connected')


@dataclass
class BaseClientInterface(abc.ABC):
    prefix: str = ""
    namespace: int = defaults.namespace 
    identifier: str = defaults.identifier

    @abc.abstractmethod 
    def get_engine(self) -> typing.Any:
        ...
    @abc.abstractmethod 
    def add_to_client(self, client_mgr: ClientManager, /) -> None:
        ...

T = typing.TypeVar('T', bound=typing.Any)

@dataclass
class ClientInterface(typing.Generic[T]):
    """ A Generic Interface for UaClient """
    
    engine: T 
    """ Engine object containing linked data and methods """
    
    profile: InitVar[IOpcuaProfile | str]
    """ Opcua Profile to link the engine """
    
    prefix: str = ""
    """ Optional prefix added to profile when linked """
    
    namespace: int = defaults.namespace 
    """ opcua namespace default is 4"""
    
    identifier: str = defaults.identifier
    """ OPCUA identifier, e.g. 'PLC1' """

    def __post_init__(self, profile: IOpcuaProfile | str) -> None:
        if isinstance(profile, str):
            self.uaprofile: IOpcuaProfile = read_profile(profile)
        else:
            self.uaprofile = profile

    def get_engine(self) -> T:
        return self.engine 

    def add_to_client(self, client_mgr: ClientManager,/) -> None:
        engine = self.get_engine()
        client_mgr.add_profile( 
                self.uaprofile, engine, 
                prefix=self.prefix,
                namespace=self.namespace, 
                identifier=self.identifier
        ) 
        if hasattr(engine, "ua_info"):
            client_mgr.add_ua_info(engine.ua_info)


class UaClient:
    """High level Client class"""

    update_frequency: float 
    """Client main loop frequency"""
    
    def __init__(self, 
        endpoint: str = defaults.endpoint, 
        timeout: float  = 4.0, 
        keep_alive: bool = False , 
        keep_alive_period: float = 2.0 , 
        update_frequency: float = 20.0
    ) -> None:
        self._client_mgr =  ClientManager(
                    endpoint=endpoint, 
                    timeout=timeout, 
                    keep_alive=keep_alive, 
                    keep_alive_period=keep_alive_period
                )
        self.update_frequency = update_frequency
    
    @property 
    def client_mgr(self) -> ClientManager:
        return self._client_mgr
    
    @property 
    def client(self) -> asyncua.Client:
        return self.client_mgr.client
 
    @property
    def endpoint(self) -> str:
        return self.client_mgr.endpoint
    
    @endpoint.setter 
    def endpoint(self, endpoint: str) -> None:
        self.client_mgr.endpoint = endpoint

    @property
    def keep_alive(self) -> bool:
        return self.client_mgr.keep_alive
    
    @keep_alive.setter 
    def keep_alive(self, keep_alive: bool) -> None:
        self.client_mgr.keep_alive = keep_alive

    @property
    def keep_alive_period(self) -> float:
        return self.client_mgr.keep_alive_period
    
    @keep_alive_period.setter 
    def keep_alive_period(self, keep_alive: float) -> None:
        self.client_mgr.keep_alive_period = keep_alive

    def attach(self, interface: IClientInterface[T]) -> T:
        interface.add_to_client(self.client_mgr)
        return interface.get_engine()

    def new_looper(self) -> core.ILooper:
        return core.Looper( 1./self.update_frequency)


    def new_runner(self) -> core.IRunner:
        return self.client_mgr.new_runner(self.new_looper())


    def iter_runners(self,excludes: set[str] = set()) -> typing.Iterator[core.IRunner]:
        if "opcua" not in excludes:
            yield self.new_runner()

    def new_session(self, 
            excludes: set[str] = set()
        ) -> core.ISession:
        runner = core.RunnerGroup(*self.iter_runners(excludes=excludes))
        return core.Session(runner)
    
    def connect(self, timeout: float = 5.0) -> core.ISession:
        """Start client session and wait for link and connection"""
        session = self.start()
        core.wait(session.is_ready, timeout=timeout, period=0.01, lag=0.1)
        return session

    def start(self) -> core.ISession:
        session = self.new_session()
        session.start()
        return session

    async def run(self) -> int:
        session = self.new_session()
        return await session.run()


class ClientInterfaceGroup:# TODO: To be removed ?? 
    def __init__(self, **interfaces: IClientInterface[T]):
        self._interfaces = interfaces 
        self._engine: dict[str,typing.Any] = {name:itf.get_engine() for name,itf in interfaces.items()} 

    def get_engine(self) -> dict[str, typing.Any]:
        return self._engine 

    def add_to_client(self, client_manager: ClientManager) -> None:
        for itf in self._interfaces.values():
            itf.add_to_client(client_manager)

