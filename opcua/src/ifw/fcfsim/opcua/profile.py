""" 
Define some data interfaces for Node and Rpc. 
The goal here is to separate data from its source (e.g. *namespace.yaml file)
and so have a common data interface for the module.
"""
from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any
from ifw.fcfsim.opcua.iprofile import (
    IDataType, INodeProfile, IRpcProfile, IOpcuaProfile, IRpcArgumentProfile, IUaPath
)
from ifw.fcfsim.opcua.base_profile import (
    join_path,
    parse_data_type,
    default_access,
    split_path
)

########################################################
#
# UaPath are use to iddentify an opcua point. Contrary 
# to UaNodeId the path is not "instanciated" with prefix and 
# name space.
# The first naive attempt was to use, as path, a string separated with '.' 
# But it does not cover all case (e.g. Numerical Id)  

def ua_path(path: str | int | None | IUaPath) -> IUaPath:
    if isinstance(path, int):
        return UaNumericalPath(path)
    elif path is None:
        return UaRootPath() 
    elif isinstance(path, str):
        if path.startswith("i="):
            return UaNumericalPath(int(path[2:]))
        else:
            return UaStringPath(path)
    else:
        return path

@dataclass(frozen=True)
class UaRootPath:
    """This Path is a dummy to represent the root"""
    def __eq__(self, right: object) -> bool:
        return isinstance(right, UaRootPath)
    
    def is_numerical(self) -> bool:
        return False
    
    def get_path(self) -> str | int:
        return ""

    def format(self, namespace: int) -> str:
        return ""
    
    def get_name(self) -> str:
        return ""
    
    def copy(self, prefix: str | int | IUaPath | None = None) -> IUaPath:
        return ua_path(prefix or None)

    def get_root(self) -> None:
        return None

    def get_level(self) -> int:
        return 0

@dataclass(frozen=True)
class UaStringPath:
    """Path to an opcua point represented by a String 

    These path will lead to a String ID
    """
    path: str 
    root: IUaPath = UaRootPath()
    force_namespace: int | None = None # Place holder for some special cases

    def __post_init__(self) -> None:
        root, name = split_path(self.path)
        if root:
            self.__dict__['root'] = self.root.copy(root)
            self.__dict__['path'] = name 

    def __eq__(self, right: object) -> bool:
        try:
            return (right.get_path() == self.get_path() # type:ignore
                    and right.get_root() == self.root) # type:ignore
        except (AttributeError, ValueError):
            return False

    def is_numerical(self) -> bool:
        return False
    
    def get_path(self) -> str:
        root_path = self.root.get_path()
        if isinstance(root_path, str):
            return join_path(root_path, self.path)
        else:
            return self.path

    def format(self, namespace: int) -> str:
        if self.force_namespace is not None:
            namespace = self.force_namespace 
        return f"ns={namespace};s={self.get_path()}"
    
    def get_name(self) -> str:
        _, name = split_path(self.path)
        return name 

    def copy(self, prefix: str | int | IUaPath | None = None) -> IUaPath:
        return UaStringPath(self.path, self.root.copy(prefix))
    
    def get_root(self) -> IUaPath:
        return self.root
        
    def get_level(self) -> int:
        return 1 + self.root.get_level()
    

@dataclass(frozen=True)
class UaNumericalPath:
    id: int 
    root: IUaPath = UaRootPath() 
    force_namespace: int | None = None # Place holder for some special cases

    def __eq__(self, right: object) -> bool:
        try:
            return right.get_path() == self.id # type:ignore
        except (AttributeError, ValueError):
            return False

    def is_numerical(self) -> bool:
        return True 
    
    def get_path(self) -> int:
        return self.id

    def format(self, namespace: int) -> str:
        if self.force_namespace is not None:
            namespace = self.force_namespace 
        return f"ns={namespace};i={self.id}"
    
    def get_name(self) -> str:
        return str(self.id)

    def copy(self, prefix: str | int | IUaPath | None = None) -> IUaPath:
        return UaNumericalPath(self.id, self.root.copy(prefix))
    
    def get_root(self) -> IUaPath:
        return self.root
    
    def get_level(self) -> int:
        return self.root.get_level() + 1


@dataclass(init=False)
class NodeProfile:
    """ Contain server or client Node definition """
    path: IUaPath
    """ Node relative path """
    data_type: IDataType
    """ Node Data Type """
    access: int 
    """ Acces level of the node """
    default: Any
    """ default, if None this will be the data_type default """
    name: str
    """ Qualified name (Used as BrowseName in Opcua) """
    def __init__(self, 
         path: IUaPath | str | int, 
         data_type: str | int | IDataType | type, 
         access: int = default_access, 
         default: Any = None, 
         name: str | None = None
        ) -> None:
        self.path = ua_path(path)
        self.data_type = parse_data_type(data_type)
        self.access = access
        if default is None:
            default = self.data_type.default
        self.default = default 
        if name is None:
            resolved_path = self.path.get_path()
            if isinstance(resolved_path, str):
                _, name = split_path(resolved_path)
            else:
                name = str(resolved_path)
        self.name = name 
    
    def copy(self, prefix: str | int | IUaPath | None = "") -> INodeProfile:
        """Copy the node and eventually add an OPCU prefix"""
        return NodeProfile(
            path=self.path.copy(prefix),
            data_type=self.data_type,
            access=self.access,
            default=self.default
        )


@dataclass
class RpcArgumentProfile:
    """ Contain a Rpc Argument """
    name: str
    data_type: IDataType


@dataclass(init=False)
class RpcProfile:
    """ Contain definition of a RPC method """
    path: IUaPath
    """ parent RPC node relative path (without method name) """
    name: str
    """ method name, e.g. 'RPC_Init' """
    inputs: list[IRpcArgumentProfile] = field(default_factory=list)
    """ input argument profiles """
    outputs: list[IRpcArgumentProfile] = field(default_factory=list)
    """ output argument profiles """
    error_output: bool = False 
    """If True, the firt output argument is expected to be an error code"""
    
    def __init__(self, 
         path: str | int | IUaPath, 
         name: str, 
         inputs:  list[IRpcArgumentProfile] | None = None, 
         outputs: list[IRpcArgumentProfile] | None = None, 
         error_output: bool = False
        ) -> None:

        self.path = ua_path(path)
        self.name = name 
        self.inputs = [] if inputs is None else inputs 
        self.outputs = [] if outputs is None else outputs
        self.error_output = error_output

    def copy(self, prefix: str = "") -> IRpcProfile:
        return RpcProfile(
            path=self.path.copy(prefix),
            name=self.name,
            inputs=list(self.inputs),
            outputs=list(self.outputs),
            error_output=self.error_output,
        )


def copy_profile(
    origin: IOpcuaProfile,
    target: IOpcuaProfile,
    opcua_prefix: str = "", data_prefix: str = "", 
) -> None:
    for name, node in origin.nodes.items():
        target.nodes[join_path(data_prefix, name)] = node.copy(opcua_prefix)

    for name, rpc in origin.rpc_methods.items():
        target.rpc_methods[join_path(
            data_prefix, name)] = rpc.copy(opcua_prefix)

def reduce_profile(
    origin: IOpcuaProfile,
    target: IOpcuaProfile,
    data_prefix: str 
    ) -> None:
    if not data_prefix.endswith("."):
        data_prefix = data_prefix + "."

    for name, node in origin.nodes.items():
        if name.startswith(data_prefix):
            target.nodes[ name.removeprefix(data_prefix) ] = node.copy()
    
    for name, rpc in origin.rpc_methods.items():
        if name.startswith(data_prefix):
            target.rpc_methods[name.removeprefix(data_prefix)] = rpc.copy()
        



@dataclass
class OpcuaProfile:
    """ A full OPCUA interface containing nodes and RPCs """
    nodes: dict[str, INodeProfile] = field(default_factory=dict)
    rpc_methods: dict[str, IRpcProfile] = field(default_factory=dict)

    def add(self, name: str, element: INodeProfile | IRpcProfile) -> None:
        if isinstance(element, IRpcProfile):
            self.rpc_methods[name] = element
        else:
            self.nodes[name] = element

    def copy(self, opcua_prefix: str = "", data_prefix: str = "") -> IOpcuaProfile:
        new_profile = OpcuaProfile()
        copy_profile(self, new_profile, opcua_prefix=opcua_prefix,
                     data_prefix=data_prefix)
        return new_profile

    def reduce(self, prefix: str) -> IOpcuaProfile:
        new_profile = OpcuaProfile()
        reduce_profile(self, new_profile, prefix)
        return new_profile

    def merge(self,
            *profiles: IOpcuaProfile,
            opcua_prefix: str = "",
            data_prefix: str = "") -> None:
        for profile in profiles:
            copy_profile(profile, self, opcua_prefix, data_prefix)
    
if __name__ == "__main__":
    # check interfaces 
    _: type[INodeProfile] = NodeProfile
    __: type[IRpcProfile] = RpcProfile
    ___: type[IRpcArgumentProfile] = RpcArgumentProfile
    ____: type[IOpcuaProfile] = OpcuaProfile 
    

        
