from __future__ import annotations

import asyncio
import functools
import queue
from dataclasses import dataclass, field
from datetime import datetime, timezone
from typing import Any, Awaitable, Callable, Iterable

import asyncua
from asyncua import ua
from asyncua.common import ua_utils
from asyncua.common.subscription import DataChangeNotif
from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua.base_profile import AccessLevel, join_path
from ifw.fcfsim.opcua.ilinks import (
    PULL_BYTE,
    PUSH_BYTE,
    SUBSCRIBE_BYTE,
    IIdInput,
    IIdResolver,
    ILinkModeResolver,
    IPullConnector,
    IPushConnector,
    IRpcConnector,
    ISession,
    ISubscriptionConnection,
    ISubscriptionConnector,
    IUaInfo,
    IUaLinks,
    IUaValue,
)
from ifw.fcfsim.opcua.iprofile import (
    INodeProfile,
    IOpcuaProfile,
    IRpcArgumentProfile,
    IRpcProfile,
)

log = core.get_logger("devsim.opcua")

_variant_type_to_python: dict[ua.VariantType | None, Callable[[Any], Any]] = {
    ua.VariantType(1): bool,
    ua.VariantType(4): int,
    ua.VariantType(5): int,
    ua.VariantType(6): int,
    ua.VariantType(7): int,
    ua.VariantType(8): int,
    ua.VariantType(9): int,
    ua.VariantType(10): float,
    ua.VariantType(11): float,
    ua.VariantType(12): str,
}


def parse_val_with_variant_type(
        varianttype: ua.VariantType | None, 
        val: Any
) -> Any:
    try:
        tpe = _variant_type_to_python[varianttype]
    except KeyError:
        return val
    return tpe(val)


def value_to_datavalue(
        val: Any,
        variant_type: ua.VariantType | None = None
) -> ua.DataValue:
    """
    convert anything to a DataValue using the varianttype
    """
    if isinstance(val, ua.DataValue):
        return val

    val = parse_val_with_variant_type(variant_type, val)

    # the pyright: ignore are due to a bad type annotation in ua pkg
    if isinstance(val, ua.Variant):
        return ua.DataValue(val)  # pyright: ignore

    return ua.DataValue(ua.Variant(val, variant_type)) # pyright: ignore

def value_to_datavalue_with_timestamp(
        val: Any,
        variant_type: ua.VariantType | None = None
) -> ua.DataValue:
    """
    convert anything to a DataValue using the varianttype
    A TimeStamp is added, which is probably wanted only if we 
    are witing on a server session and not a client
    """
    if isinstance(val, ua.DataValue):
        return val

    val = parse_val_with_variant_type(variant_type, val)

    # the pyright: ignore are due to a bad type annotation in ua pkg
    if isinstance(val, ua.Variant):
        return ua.DataValue(
            val, SourceTimestamp=datetime.now(timezone.utc))  # pyright: ignore

    return ua.DataValue(
        ua.Variant(val, variant_type),  # pyright: ignore
        SourceTimestamp=datetime.now(timezone.utc))  # pyright: ignore



class Empty:
    """ Empty something (other than None) """
    ...


def is_new_value_after_data_change(
    data_timestamp: datetime,
    data_value: ua.DataValue
) -> bool:
    """ Compare a timestamp to the timestamp contain in a ua.DataValue 

    Return True if the DataValue is past the data timestamp 
    """
    if data_value.ServerTimestamp:
        return bool(data_value.ServerTimestamp.replace(tzinfo=timezone.utc)
                    > data_timestamp)
    else:
        return True


def get_member(obj: Any, path: str) -> Any:
    """ Return the data member following the path 
    
    get_member(data,  'a.b.c') -> data.a.b.c 
    get_member(data,  'a.b[0].c') -> data.a.b[0].c 
    get_member(data,  'a.b.c[0]') -> data.a.b.c[0] 
    etc ...
        
    """
    obj, attr_or_index = core.resolve_data_path(obj, path)
    if isinstance(attr_or_index, (int, slice)):
        return obj[attr_or_index]
    elif isinstance(attr_or_index, core.node.Item):
        return obj[attr_or_index.key]
    else:
        return getattr(obj, attr_or_index)


# to be replaced by Abstract ?
def resolve_session(session: asyncua.Server | asyncua.Client) -> Any:
    """ return a base server or client for read_nodes and write_nodes functions """
    try:
        return session.iserver.isession  # pyright:ignore # part of the test
    except AttributeError:
        return session.uaclient  # pyright:ignore # part of the test

def is_client(session: asyncua.Server | asyncua.Client) -> bool:
    """True if session is a client, false otherwise"""
    try:
        session.uaclient# pyright:ignore # part of the test
    except AttributeError:
        return False 
    else:
        return True

async def read_attributes(
    session: asyncua.Server | asyncua.Client,
    nodeids: Iterable[ua.NodeId],
    attribute: ua.IntegerId
) -> list[Any]:
    """ read a list of nodes attributes 
    
    Nodes are identified from node ids  
    """
    result = await _read_attributes(session, nodeids, attribute)
    for r,n in zip(result, nodeids):
        try:
            r.StatusCode.check()  # pyright:ignore # asyncua typing for StatusCode is optional
        except ua.uaerrors.BadNodeIdUnknown as exc:
            raise ValueError(f"Failed to read attribute of node {n}") from exc
    return [r.Value.Value for r in result] # pyright:ignore # asyncua typing for Value is optional

async def _read_attributes(
    session: asyncua.Server | asyncua.Client,
    nodeids: Iterable[ua.NodeId],
    attribute: ua.IntegerId
) -> Iterable[Any]: # Not sure whats asyncua result as Result
    to_read = []
    for nodeid in nodeids:
        rv = ua.ReadValueId()
        rv.NodeId = nodeid
        rv.AttributeId = attribute
        rv.IndexRange = None  # pyright:ignore # problem on asyncua, None is however accepted
        to_read.append(rv)
    params = ua.ReadParameters()
    params.NodesToRead = to_read

    result = await (resolve_session(session)).read(params)
    return result # type:ignore

async def check_nodes(
    session: asyncua.Server | asyncua.Client,
    nodeids: Iterable[ua.NodeId]
) -> list[ua.NodeId]:
    """Check reading of a list of nodes 

    Return a list of bad node ids (cannot be red) 
    Empty if all nodes can be red 
    """
    result = await _read_attributes(session, nodeids, ua.IntegerId(ua.AttributeIds.Value))
    bad_nodes = []
    for r,n in zip(result, nodeids):
        try:
            r.StatusCode.check()  # pyright:ignore # asyncua typing for StatusCode is optional
        except ua.uaerrors.BadNodeIdUnknown:
            bad_nodes.append(n)
    return bad_nodes

async def read_nodes(
    session: asyncua.Server | asyncua.Client,
    nodeids: Iterable[ua.NodeId]
) -> list[Any]:
    """ read node values in one go """
    return await read_attributes(session, nodeids, ua.IntegerId(ua.AttributeIds.Value))


async def read_attribute(
    session: asyncua.Server | asyncua.Client,
    nodeid: ua.NodeId,
    attribute: ua.IntegerId
) -> Any:
    """ Read one single attribute of a node with its node id 

        read_attribute( session, nodeid, attr) 
    is equivalent to 
        session.get_node( nodeid ).read_attribute(attr) 
    """
    value, = await read_attributes(session, [nodeid], attribute)
    return value


async def read_variant_types(
    session: asyncua.Server | asyncua.Client,
    nodeids: Iterable[ua.NodeId]
) -> dict[ua.NodeId, ua.VariantType]:
    """ Read Variant types for given node ids 
    
    return result in a nodeid/variant_type dictionary 
    """
    data_types = await read_attributes(
        session,
        nodeids,
        ua.IntegerId(ua.AttributeIds.DataType))
     
    return {nid: await ua_utils.data_type_to_variant_type(
                 asyncua.Node(resolve_session(session), dt)) 
            for nid, dt in zip(nodeids, data_types)}


async def write_nodes(
        session: asyncua.Server | asyncua.Client, 
        nodeids: dict[ua.NodeId, Any]
) -> None:
    """ Write several node value defined in a NodeId->Value dictionary

    This is optimized to be slightly faster than looping on node.set_value()
    """
    if not nodeids:
        return
    nodeids = nodeids.copy() # copy prevent the dictionary change by bacground task  
    to_write = []
    if is_client(session):
        value_parser = value_to_datavalue
    else:
        value_parser = value_to_datavalue_with_timestamp

    # TODO shall we cache the types ?
    datatypes = await read_variant_types(session, nodeids.keys())
    # list the node items in case dictionary size changes (new node value)
    # during iteration
    for nodeid, value in list(nodeids.items()):
        attr = ua.WriteValue()
        attr.NodeId = nodeid
        attr.AttributeId = ua.AttributeIds.Value  # pyright:ignore
        attr.Value = value_parser(value, datatypes[nodeid])
        attr.IndexRange = None  # pyright:ignore
        to_write.append(attr)
    params = ua.WriteParameters()
    params.NodesToWrite = to_write
    result = await (resolve_session(session)).write(params)

    for r in result:
        try:
            r.check()
        except ua.uaerrors.BadNodeIdUnknown as exc:
            raise ValueError(f"Failed to write attribute node for one of {nodeids}") from exc
            raise ua.uaerrors.BadNodeIdUnknown(f"Failed to write attribute node for one of {nodeids}") from exc


#  ____            _     
# |  _ \ _   _ ___| |__  
# | |_) | | | / __| '_ \ 
# |  __/| |_| \__ \ | | |
# |_|    \__,_|___/_| |_|


@dataclass
class PushConnector:
    """ Push Connection from one data attribute change to opcua server/client """
    nodeid: ua.NodeId
    stack: PushStack     
    # receive is conform to core.ILinkCallback
    def receive(self, value:IUaValue, event:core.IDataEvent)->None:
        del event # not used here
        # just add it to the push stack, pushed at next push async method
        self.stack.add(self.nodeid, value)
    
    def connect(self, data_node: core.IDataNode[Any], stack: bool = True) -> None:
        """Connect a data node to the UaNode for 'push'

        Changes in the data node will be wrtien on Server or Client
        """
        
        try:
            data_node.connect_callback(self.receive)
        except ValueError as err:
            raise ValueError(f"Impossible to link data {data_node}"
                             f" and node {self.nodeid}") from err 
        
        if stack: 
            # if a value is set to data_node add it to the push stack 
            # expected that the value is UA compatible
            value = data_node.get(None)
            if value is not None:
                self.stack.add(self.nodeid, value)

    def disconnect(self, data_node: core.IDataNode[Any]) -> None:
        data_node.disconnect_callback(self.receive)


@dataclass
class PushStack:
    values: dict[ua.NodeId,IUaValue] = field(default_factory=dict)
    """ Stack of nodeid/value to be pushed (cleaned after a push) """
    
    def add(self,  nodeid:ua.NodeId, value:IUaValue)->None:
        """ Add a new node/value pair to be pushed on server """
        self.values[nodeid] = value
    

    def clear(self)->None:
        """Clear the stack of values"""
        self.values.clear()
    
    async def push(self, session: asyncua.Server | asyncua.Client) -> int:
        """Push the stack of node values into the OPC-Ua session"""
        if not self.values:
            return 0
        await write_nodes(session, self.values)
        n = len(self.values)
        self.clear()
        return n 
    
#  ____        _ _ 
# |  _ \ _   _| | |
# | |_) | | | | | |
# |  __/| |_| | | |
# |_|    \__,_|_|_|
                 

@dataclass
class PullConnection:
    """Pull Connection, received when a pull of values is triggered on sessions"""
    datanode: core.IDataNode[Any]

    def receive(self, value: Any)->None:
        if self.datanode.get(Empty) != value: 
            self.datanode.set(value)

@dataclass
class PullConnector:
    nodeid: ua.NodeId
    stack: PullStack 

    def connect(self, data_node: core.IDataNode[Any]) -> None:
        """Connect a data node to be pulled from session 
        
        Pull is usually called manually it will ovewrite data node values
        from OPC-UA Session
        """
        connection = PullConnection(data_node) 
        self.stack.add(self.nodeid, connection)
    
    def disconnect(self, data_node: core.IDataNode[Any]) -> None:
        self.stack.remove(self.nodeid, data_node)


@dataclass
class PullStack:
    connections: dict[ua.NodeId,list[PullConnection]] = field(default_factory=dict)
    
    def add(self, nodeid: ua.NodeId, connection: PullConnection)->None:
        self.connections.setdefault(nodeid, []).append(connection)   
    
    def find(self, nodeid: ua.NodeId, data_node: core.IDataNode[Any]) -> PullConnection:
        """Find the connection between ua nodeid and a data node"""
        try:
            lst = self.connections[nodeid]
        except KeyError:
            raise ValueError(f"No connection found for {nodeid}")
        for c in lst:
            if c.datanode == data_node:
                return c 
        raise ValueError(f"No connection found for {nodeid} and {data_node}")
    
    def remove(self, nodeid: ua.NodeId, data_node: core.IDataNode[Any]) -> None:
        """Remove the connection between ua nodeid and a data node"""
        try:
            c = self.find(nodeid, data_node)
        except ValueError:
            return None 
        else:
            self.connections[nodeid].remove(c)  

    async def pull(self, session: asyncua.Server | asyncua.Client) -> int:
        nodeids = list(self.connections.keys())
        values = {nids:val \
                for nids,val in zip(nodeids, await read_nodes(session, nodeids))}
        for nodeid, node_connections in self.connections.items():
            value = values[nodeid]
            for connection in node_connections:
                connection.receive(value) 
        return len(nodeids) 

    async def check(self, session: asyncua.Server | asyncua.Client) -> None:
        nodeids = list(self.connections.keys())
        bad_nodes = await check_nodes(session, nodeids)
        if len(bad_nodes):
            raise ValueError("Problem with reading nodes: \n"
                             +"\n".join(str(n) for n in bad_nodes))
            
        


#  ____        _                   _       _   _             
# / ___| _   _| |__  ___  ___ _ __(_)_ __ | |_(_) ___  _ __  
# \___ \| | | | '_ \/ __|/ __| '__| | '_ \| __| |/ _ \| '_ \ 
#  ___) | |_| | |_) \__ \ (__| |  | | |_) | |_| | (_) | | | |
# |____/ \__,_|_.__/|___/\___|_|  |_| .__/ \__|_|\___/|_| |_|
#                                   |_|                      

@dataclass
class SubscriptionServerConnection:
    """ Handle a connection from server/client subscription """
    datanode: core.IDataNode[Any]
    log: core.ILogger = log

    def receive(self, value:Any, data_value: ua.DataValue)->None:
        if self.datanode.get(Empty) != value:
            ts = self.datanode.get_metadata().get_timestamp()
            if is_new_value_after_data_change(ts, data_value):
                # change value only if it is new compare to the data_node 
                # The idea is that internal business logic has priority 
                # over server/client changes
                self.log.debug(f"Receiving new value for {self.datanode!r} value={value} ")
                self.datanode.set(value)

@dataclass
class SubscriptionServerConnectionSkipOne:
    # This Mockup a SubscriptionServerConnection except that the first 
    # reveived that is trashed. Used because in async when a subscription
    # is established the callback is called. For many applications we
    # should trust more the data inside the datanode (python business logics)
    # rather than the default values inside the built server. 
    # see also SessionLinker class and the trust_server_values property
    """ Handle a connection from server/client subscription """
    datanode: core.IDataNode[Any]
    log: core.ILogger = log

    def receive(self, value:Any, data_value: ua.DataValue)->None:
        del value, data_value
        self.__class__ = SubscriptionServerConnection #type:ignore[assignment]

@dataclass
class SubscriptionClientConnection:
    """ Handle a connection from client subscription """
    datanode: core.IDataNode[Any]
    log: core.ILogger = log

    def receive(self, value:Any, data_value: ua.DataValue)->None:
        self.log.debug(f"Receiving new value for {self.datanode!r} value={value} ")
        self.datanode.set(value) 

@dataclass
class DataSubscriptionHandler:   
    """ Opc-Ua Subscription Handler for incoming changes 
    
    Its role is to write incoming node change into data nodes through
    established connections 
    """
    connections: dict[ua.NodeId,list[ISubscriptionConnection]] 

    def datachange_notification(self, 
          node: asyncua.Node, 
          val: Any, 
          data: DataChangeNotif
        )->None:
        """ Triggered when monitored node are changed on server 
        
        This will change new value on connected data 
        """
        try:
            connections = self.connections[node.nodeid]
        except KeyError:
            pass 
        else:
            data_value = data.monitored_item.Value
            for connection in connections:
                connection.receive(val, data_value)

@dataclass 
class SubscriptionConnector:
    nodeid: ua.NodeId 
    stack: SubscriptionStack
    Connection: Callable[[core.IDataNode[Any]], ISubscriptionConnection] = SubscriptionServerConnection
    log: core.ILogger = log 

    def connect(self, data_node: core.IDataNode[Any]) -> None:
        connection = self.Connection(data_node)
        connection.log = self.log
        self.stack.add(self.nodeid, connection)

    def disconnect(self,  data_node: core.IDataNode[Any]) -> None:
        self.stack.remove(self.nodeid, data_node) 

@dataclass
class SubscriptionStack:
    period: int = 50
    """Subscription engine period (ms)"""
    log: core.ILogger = log

    connections: dict[ua.NodeId,list[ISubscriptionConnection]] = field(default_factory=dict)
    subscription_queue: list[ua.NodeId] = field(default_factory=list)
    _subscription = None
    
    def add(self, 
            nodeid:ua.NodeId, 
            connection: ISubscriptionConnection
            )->None:
        """ Add a new nodeid to be subscribed and connected to a data node """
        self.subscription_queue.append(nodeid)
        self.log.debug( f"node {nodeid} is subscribed to data changed" )
        self.connections.setdefault(nodeid, []).append(connection)

    def find(self,
          nodeid:ua.NodeId, 
          datanode: core.IDataNode[IUaValue]
        ) -> ISubscriptionConnection:
        try:
            lst = self.connections[nodeid]
        except ValueError:
            raise ValueError(f"no subscription found for {nodeid}")
        for c in lst:
            if c.datanode == datanode:
                return c 
        raise ValueError(f"no subscription found fo {nodeid} and {datanode}")

    def remove(self, nodeid:ua.NodeId, datanode: core.IDataNode[IUaValue]) -> None:
        try:
            c = self.find( nodeid, datanode)
        except ValueError:
            return None 
        else:
            self.connections[nodeid].remove(c)
    
    async def resubscribe(self, session : asyncua.Server | asyncua.Client) -> None:
        self._subscription = None 
        self.subscription_queue.extend(self.connections.keys())
        await self.subscribe(session)

    async def subscribe(self, session : asyncua.Server | asyncua.Client) -> None:
        if not self.subscription_queue:
            return 

        if self._subscription is None:
            subscription_handler = DataSubscriptionHandler(self.connections)
            self._subscription = await session.create_subscription(self.period, subscription_handler)
        await self._subscription.subscribe_data_change(
                (session.get_node(nodeid) for nodeid in self.subscription_queue), 
            ) 
        self.subscription_queue.clear()   



#  ____  ____   ____ 
# |  _ \|  _ \ / ___|
# | |_) | |_) | |    
# |  _ <|  __/| |___ 
# |_| \_\_|    \____|


def rpc_outputs_formater(
    output_profiles: Iterable[IRpcArgumentProfile] | IRpcArgumentProfile | None
) -> Callable[[Any], list[ua.Variant]]:
    """ Build a function to format opcua compatible ouputs of a RPC method 

    The built function will receive some python method output and transform it 
    to a list of UaVariant output according to the given list of  
    output argument profiles.
    """
    if output_profiles is None:
        # return an empty list of UaVariant 
        # (assuming a `return None` on python function side)
        def format_outputs(result: Any) -> list[ua.Variant]:
            del result
            return []
    elif isinstance(output_profiles, Iterable):
        def format_outputs(result: Any) -> list[ua.Variant]:
            return [ua.Variant(output, ua.VariantType(op.data_type.num))\
                    for output, op in zip(result, output_profiles)]
    else:
        output_profile: IRpcArgumentProfile = output_profiles
        if "Int" in output_profile.data_type.name:
            # allows None result to be converted to ua.IntXX(0) 
            # (most probably this is an error code)
            def format_outputs(result: Any) -> list[ua.Variant]:
                return [ua.Variant(result or 0, ua.VariantType(output_profile.data_type.num))]
        else:
            def format_outputs(result: Any) -> list[ua.Variant]:
                return [ua.Variant(result, ua.VariantType(output_profile.data_type.num))]

    return format_outputs


def rpc_input_formater(
    input_profiles: Iterable[IRpcArgumentProfile] | IRpcArgumentProfile | None 
) -> Callable[[Iterable[Any]], list[Any]]:
    """Return a function that properly format input arguments into ua.Variant 

    The first N positional argument with profile defined are parsed into Variant 
    The others are left unchanged
    """
    if input_profiles is None:
        def format_inputs(args: Iterable[Any]) -> list[Any]:
            return list(args)
    else:
        if not isinstance(input_profiles, Iterable):
            input_profiles = [input_profiles]
        else:
            input_profiles = list(input_profiles)
        def format_inputs(args: Iterable[Any]) -> list[Any]:
            args = list(args)

            ua_args = args[:len(input_profiles)]
            extra_args = args[len(input_profiles):]
            return [ua.Variant(input, ua.VariantType(op.data_type.num))\
                    for input, op in zip(ua_args, input_profiles)]+extra_args
    return format_inputs 


def rpc_method(
        func:Callable[...,Any], 
        output_profiles: Iterable[IRpcArgumentProfile]|IRpcArgumentProfile|None
    )->Callable[...,list[ua.Variant]]:
    """ Wrap a python method to an opcua compatible RPC method 
    
    The inputs are converted from ua.Variant to regular python types
    and are sent to `func`
    The outputs of `func` are converted from regular python types to 
    ua.Variant. 
    
    If the python wrapped method raises a RpcError, it will be cached by 
    the wrapper and the RpcError.rpc_ouputs will be returned on server
    without the exception to be raised. 
    This allows to transform RpcException into an error code for instance. 
    This way we can cleanly catch errors when it was called by server.
    But still have exception raised when the method was called directly 
    from a python instance.

    Args:
        output_profiles: can be 
            - a RpcArgumentProfile (scalar output)
            - a list of RpcArgumentProfile 
            - None (No arguments and Nothing is returned) 
    Returns:
        rpc_wrapper a function which can be safely connected to an Opcua Server
    """
    format_outputs = rpc_outputs_formater(output_profiles) 

    def rpc_wrapper(parent:Any, *args:Any)->list[ua.Variant]:
        del parent # not used 
        func_args = [arg.Value for arg in args] 
        
        try:
            result  = func(*func_args)
        except core.RpcException as rpc_ex:
            log.error(f"RpcError {rpc_ex}, {rpc_ex.rpc_outputs}")
            result = rpc_ex.rpc_outputs
        except (TypeError, RuntimeError, ValueError) as er:
            log.error( f"Error when calling wrapped method: {er}")
            result = None
        if result is None:
            return []
        return format_outputs(result)
    return rpc_wrapper 



@dataclass
class ServerRpcConnector:
    nodeid: ua.NodeId
    stack: RpcStack
    
    def connect(self, 
         imethod:Callable[...,Any] | core.IMethodNode[Callable[..., Any]], 
         rpc_profile: IRpcProfile | None
        ) -> None:
        """Connect a python method to a OPCUA-RPc method"""
        if isinstance(imethod, core.IMethodNode):
            func: Callable[..., Any] = imethod.get_method()
        else:
            func = imethod 
        if rpc_profile is None:
            wrapped_method: Callable[...,Any] = asyncua.uamethod(func)
        else:
            # A single output is considered as a scalar output. 
            # This is not generic but what is wanted here (and in 99% of cases)
            if len(rpc_profile.outputs)==1:
                outputs: list[IRpcArgumentProfile] | IRpcArgumentProfile = rpc_profile.outputs[0]
            else:
                outputs = rpc_profile.outputs
            wrapped_method = rpc_method(func, outputs)
        
        self.stack.add(self.nodeid,  wrapped_method)
    
    def disconnect(self, method:Callable[...,Any] | core.IMethodNode[Callable[..., Any]]) -> None:
        raise ValueError("Cannot disconnect a method. Sorry")



# We need a RpcManager in order to stack connection before the server
# is built 
@dataclass
class RpcStack:
    """ A Stack of RPC method to be linked to server """
    stack: dict[ua.NodeId,Callable[..., Any]] = field(default_factory=dict)
    
    def add(self,nodeid: ua.NodeId, func:Callable[...,Any])->None:
        self.stack[nodeid] = func 
    
    def clear(self) -> None:
        self.stack.clear() 

    async def link_method(self, session: asyncua.Server | asyncua.Client) -> int:
        """ runtime updater 

        If new method has been added it will be installed here.
        Return the number of methods linked
        """
        # async not needed here but to be similar than other update async methods  
        if not self.stack:
            return 0
        if isinstance(session, asyncua.Server):
            for nodeid, method in self.stack.items():
                session.link_method(session.get_node(nodeid), method)
        n = len(self.stack)
        self.stack.clear()
        return n 



def _set_coro_rpc_method(
        session: asyncua.Client | asyncua.Server, 
        nodeid: ua.NodeId, 
        imethod: core.IMethodNode[Callable[...,Any]],
        rpc_profile: IRpcProfile,
        stack: CoroStack
) -> None:
    """Overide a method (contained in `imethod`) by the result of client RPC"""
    method = imethod.get_method()
    # remove the method name from node 
    snid,_, method_name = str(nodeid.Identifier).partition("#")
    nid = ua.NodeId( snid, nodeid.NamespaceIndex, ua.NodeIdType.String  )#pyright:ignore # asyncua problem
    method_name = f"{nid.NamespaceIndex}:{method_name}"
    input_formater = rpc_input_formater(rpc_profile.inputs)

    @functools.wraps(method)
    async def async_new_method(*args:Any, **kwargs:Any)->Any:
        n = session.get_node(nid)
        return await n.call_method( method_name, *input_formater(args), **kwargs)

    if asyncio.iscoroutinefunction(method):
        imethod.overide_method(async_new_method)
    else:
        imethod.overide_method(async_new_method, prefix="async_")

        @functools.wraps(method)
        def new_method(*args:Any, **kwargs:Any)->Any:
            nargs = len(args)+len(kwargs)
            if nargs<len(rpc_profile.inputs):
                raise ValueError(f"Expecting {len(rpc_profile.inputs)} arguments got {nargs}")
            n = session.get_node(nid)
            coro = n.call_method(method_name, *input_formater(args), **kwargs)
            stack.put_coro(coro)
            res, ext = stack.get_result()
            if ext:
                raise ext 
            if rpc_profile.error_output and res != 0:
                raise core.RpcException(f"RPC returned error code {res}", [res]) 
            return res   
        imethod.overide_method(new_method)


@dataclass
class ClientRpcConnector:
    nodeid: ua.NodeId 
    stack: CoroStack
    
    
    def connect(self, 
        imethod:Callable[...,Any] | core.IMethodNode[Callable[..., Any]], 
        rpc_profile: IRpcProfile | None
        ) -> None:
        """Connect a method for client applications

        Warning: This type of connection overide the targeted method, 
            mostlikely a method of an object. Meaning that the business
            logic inside the method is replaced by the result of the client.
            
        If the targeted function is an synchronous normal function it is 
        expected that a process is running to handled the method call in 
        an asynchronious way to the server. Sess CoroStack().execute method.
        """
        if not isinstance(imethod, core.IMethodNode):
            raise ValueError("Client RPC connection needs a IMethodNode"
                             " got a regular function")
        if rpc_profile is None:
            raise ValueError("Missing RPC profile")
        
        method = imethod.get_method()
        
        # We overide methods with dummy one 
        # The method will be overided when linked to a client 
        # see _set_coro_rpc_method for the real implementation 
        # made with a session instance 

        @functools.wraps(method)
        async def async_new_method(*args:Any, **kwargs:Any)->Any:
            raise RuntimeError("Waiting for UA client")
        
        if asyncio.iscoroutinefunction(method):
            imethod.overide_method(async_new_method)

        else:
            imethod.overide_method(async_new_method, prefix="async_")
            
            @functools.wraps(method)
            def new_method(*args:Any, **kwargs:Any)->Any:
                raise RuntimeError("Waiting for UA client")
            imethod.overide_method(new_method)
            
        
        self.stack.add(
                functools.partial(_set_coro_rpc_method, 
                    imethod = imethod, 
                    nodeid= self.nodeid,
                    rpc_profile = rpc_profile, 
                    stack = self.stack
                ))

    
@dataclass
class CoroStack:
    """ Handle execution of Coroutine 

    This is handy to execute async func inside sync function. 
    The coroutine are sent to a queue and result back to an other queue 
    To works correctly a Thread or task needs to call the execute async 
    method periodicly.   
    """
    timeout: float = 15
    method_stack: list[Callable[[ISession], None]] = field(default_factory=list)
    connections: list[Callable[[ISession], None]] = field(default_factory=list)
    coro_queue: queue.Queue[Awaitable[Any]] = field(default_factory=queue.Queue)
    result_queue: queue.Queue[Any] = field(default_factory=queue.Queue)
    lock: asyncio.Lock = field(default_factory=asyncio.Lock)

    def add(self, session_setter: Callable[[ISession], None]) -> None:
        self.method_stack.append(session_setter)
        self.connections.append(session_setter)

    def put_coro(self, coro: Awaitable[Any]) -> None:
        """ Put a new Coroutine in the queue to be executed """
        self.coro_queue.put(coro, timeout=self.timeout)

    def get_result(self) -> Any:
        """ Get result of last coroutine call. """
        return self.result_queue.get(True, timeout=self.timeout)
    
    async def relink(self, session: asyncua.Server | asyncua.Client) -> None:
        self.method_stack.clear()    
        self.method_stack.extend(self.connections)
        await self.link_coro(session)

    async def link_coro(self,
                session: asyncua.Server | asyncua.Client) -> None:
        if not self.method_stack: 
            return 
        for session_setter in self.method_stack:
            session_setter(session)

        self.method_stack.clear()

    async def execute(self) -> None:
        try:
            coro = self.coro_queue.get(False)
        except queue.Empty:
            return
        async with self.lock:
            try:
                result = await coro
            except Exception as ex:
                # put the exception in queue
                # this is the caller job to raise it
                self.result_queue.put((None,ex))
            else:
                self.result_queue.put((result, None))


@dataclass
class UaSessionInfoStack:
    """Hold OPC-UA session information and a list of IUaInfo to be synchronized"""
    endpoint: str = ""
    connected: bool = False

    infos: list[IUaInfo] = field(default_factory=list)
    """ List of info structure to be updated """
    
    callbacks: list[Callable[[],None]] = field(default_factory=list) 
    """ Callbacks listening to new info plugged """ 

    def set_connection(self, connection_flag: bool) -> None:
        self.connected = connection_flag
        self.update_connection()

    def update_connection(self) -> None:
        for info in self.infos:
            info.connected = self.connected
    
    def set_endpoint(self, endpoint: str) -> None:
        self.endpoint = endpoint
        self.update_endpoint()

    def update_endpoint(self) -> None:
        for info in self.infos:
            info.endpoint = self.endpoint

    def add(self, *infos: IUaInfo) -> None:
        for info in infos: 
            info.endpoint = self.endpoint 
            info.connected = self.connected
        self.infos.extend(infos)
        



@dataclass
class UaLinks:
    log: core.ILogger = log
    
    push_stack: PushStack = field(default_factory=PushStack)
    pull_stack: PullStack = field(default_factory=PullStack)
    subscription_stack: SubscriptionStack = field(default_factory=SubscriptionStack) 
    rpc_stack: RpcStack = field(default_factory=RpcStack)
    coro_stack: CoroStack = field(default_factory=CoroStack)
    

    def new_push_connector(self, nodeid: ua.NodeId) -> IPushConnector:
        return PushConnector(nodeid, self.push_stack)

    def new_pull_connector(self, nodeid: ua.NodeId) -> IPullConnector:
        return PullConnector(nodeid, self.pull_stack)

    def new_subscription_connector(self, 
            nodeid: ua.NodeId,
            Connection: Callable[[core.IDataNode[Any]], ISubscriptionConnection] =\
                    SubscriptionServerConnection
        ) -> ISubscriptionConnector:
        return SubscriptionConnector(nodeid, self.subscription_stack, Connection)
    
    def new_rpc_connector(self, nodeid: ua.NodeId) -> IRpcConnector:
        return ServerRpcConnector(nodeid, self.rpc_stack)
    
    def new_coro_connector(self, nodeid: ua.NodeId) -> IRpcConnector:
        return ClientRpcConnector(nodeid, self.coro_stack)
    
    def clear(self) -> None:
        self.push_stack.clear()

    async def push(self, session: ISession) -> None:
        await self.push_stack.push(session)

    async def pull(self, session: ISession) -> None:
        await self.pull_stack.pull(session)
    
    async def pull_check(self, session: ISession, raise_error: bool = False) -> None:
        """ Check if all linked nodes are pullable 
        
        By default log an error unless raise_error is True (ValueError raised)
        """
        try:
            await self.pull_stack.check(session)
        except ValueError as err:
            if raise_error: 
                raise err
            else:
                log.error("Problem when pulling nodes")
                log.error(str(err))

    async def subscribe(self, session: ISession) -> None:
        await self.subscription_stack.subscribe(session)
    
    async def link_method(self, session: ISession) -> None:
        await self.rpc_stack.link_method(session)
        await self.coro_stack.link_coro(session)

    async def execute_coro(self) -> None:
        await self.coro_stack.execute() 
    
    async def relink(self, session: ISession) -> None:
        await self.subscription_stack.resubscribe(session)
        await self.link_method(session)
        await self.coro_stack.relink(session)

@dataclass
class LinkEngine:
    """Execute established links actions"""
    session: ISession
    links: IUaLinks
    async def push(self) -> None:
        await self.links.push(self.session)
    
    async def pull(self) -> None:
        await self.links.pull(self.session)
    
    async def execute_coro(self) -> None:
        await self.links.execute_coro()

@dataclass
class LinkInstaller:
    """"Hold method to settup all links in session"""
    session: ISession
    links: IUaLinks
    async def subscribe(self) -> None:
        await self.links.subscribe(self.session)

    async def link_method(self) -> None:
        await self.links.link_method(self.session)


@dataclass
class LinkTask(core.BaseTask):
    session: ISession
    links: IUaLinks
    async def initialise(self) -> None:
        await self.links.link_method(self.session) 
        await self.links.subscribe(self.session)

    async def next(self) -> None:
        await self.links.push(self.session)
        await self.links.execute_coro()



def is_subscrible_by_access(profile:INodeProfile)->bool:
    """ Define from a node profile if it is subscrible 
    
    this is done by access level information  
    """
    return bool( profile.access & AccessLevel.CurrentWrite )

def build_node_id(
        prefix:str, 
        namespace:int, 
        nodeid:str|INodeProfile|IRpcProfile) -> ua.NodeId:
    """ build a node id from prefix namespace, string or profile """
    if isinstance(nodeid, INodeProfile):
        path =  nodeid.path.copy(prefix)
        return ua.NodeId(path.get_path(),  ua.Int16(namespace ))  #pyright:ignore # bad usage of dataclass annotation in asyncua
       
    if isinstance(nodeid, IRpcProfile):
        path =  nodeid.path.copy(prefix)
        return ua.NodeId(f"{path.get_path()}#{nodeid.name}" , ua.Int16(namespace), ua.NodeIdType.String) #pyright:ignore # see above  

    if isinstance(nodeid, str):
        root, s, name = nodeid.partition("#")
        return ua.NodeId(f"{join_path(prefix, root)}{s}{name}", ua.Int16(namespace), ua.NodeIdType.String) #pyright:ignore # see above 
    raise ValueError(f'Invalid nodeid argument: {nodeid}')


@dataclass
class PrefixedIdResolver:
    """A Factory for ua.NodeId using prefixed OPCUA path 

    It is mostlikely used to convert relative NodeProfile or RpcProfile 
    into an absolute nodeid 
    
    Conform to IIdResolver
    """
    prefix: str = ""
    """ Node Id prefix """
    namespace: int = 4 
    """ Ua namespace (default is 4) """

    def resolve_id(self, id_input:IIdInput) -> ua.NodeId:
        """ Build a ua.NodeId from various input arguments """
        if isinstance(id_input, ua.NodeId):
            return id_input 
        elif isinstance(id_input, asyncua.Node):
            return id_input.nodeid 
        else:
            return build_node_id(self.prefix, self.namespace, id_input) 
    
    def resolve_method(self, name:str) -> str:
        return f"{self.namespace}:{name}"


def default_link_mode_resolver(node_profile: INodeProfile, client_links: bool) -> int:
    w = int(bool(node_profile.access & AccessLevel.CurrentWrite))
    r = int(bool(node_profile.access & AccessLevel.CurrentRead))
   
    if client_links:
        return int(w*2**PUSH_BYTE
                   + r*2**PULL_BYTE
                   + r*2**SUBSCRIBE_BYTE)
    else:
        # we can and must push anything to server 
        # Pull is for manual use (not in task engine) we can include 
        # all writable node to pull, but subscription is the "automatic" 
        # way to do it 
        return int(2**PUSH_BYTE
                   + w*2**PULL_BYTE
                   + w*2**SUBSCRIBE_BYTE) # int ncessary for mypy 
  


@dataclass
class SessionLinker:
    """ Object used to build links between Ua session (Server or Client) and data structure """
    
    links: IUaLinks
    """Contains all links to server or client"""
    
    id_resolver: IIdResolver = field(default_factory=PrefixedIdResolver)
    """Object with method resolve to build node ids"""
    
    client_links: bool = False
    """Must be True if links are intended for a client session"""
   
    link_mode_resolver: ILinkModeResolver = default_link_mode_resolver 
    """Method returning link mode when is not given by user"""
    
    trust_server_values: bool = False 
    """Decide to trust the default value (as built) of server when subscribing

    If False, at subscription the values are not puled from Server immediatly
    But are still pulled when changed on server after
    """

    log: core.ILogger = log  

    def _get_nodeid(self, node:IIdInput)->ua.NodeId:
        """ reolve the UaId from multiple input types """
        return self.id_resolver.resolve_id(node)

    def _push_link( self, 
          datanode: core.IDataNode[Any],             
          identifier: IIdInput, 
          stack: bool = True
        ) -> None:
        nid = self._get_nodeid(identifier)
        self.links.new_push_connector(nid).connect(datanode,stack)
    
    def _subscribe_link(self, 
          datanode: core.IDataNode[Any], 
          identifier: IIdInput
        ) -> None:
        Connection: Callable[[core.IDataNode[Any]], ISubscriptionConnection]
        
        if self.client_links:
           Connection = SubscriptionClientConnection
        else:
            if self.trust_server_values:
                Connection = SubscriptionServerConnection
            else:
                Connection = SubscriptionServerConnectionSkipOne

        self.links.new_subscription_connector(
                self._get_nodeid(identifier), Connection
        ).connect(datanode)
    
   
    def _pull_link(self, 
          datanode: core.IDataNode[Any],  
          identifier: IIdInput
        ) -> None:
        self.links.new_pull_connector(
                self._get_nodeid(identifier)
        ).connect(datanode)


    def link_rpc(self, 
          imethod: Callable[...,Any] | core.IMethodNode[Callable[...,Any]], 
          rpc_profile: IRpcProfile 
        ) -> None:
        if self.client_links:
             self.links.new_coro_connector(
                self._get_nodeid(rpc_profile)
            ).connect(imethod, rpc_profile)
        else:
            self.links.new_rpc_connector(
                self._get_nodeid(rpc_profile)
            ).connect(imethod, rpc_profile)
        
    def link_node(self,
          datanode: core.IDataNode[Any],
          profile: INodeProfile,
          mode: int | None = None,
          stack: bool = True) -> None:

        if mode is None:
            mode = self.link_mode_resolver(profile, self.client_links)

        if mode & 2**PUSH_BYTE:
            self._push_link(datanode, profile, stack=stack)
        if mode & 2**PULL_BYTE:
            self._pull_link(datanode, profile)
        if mode & 2**SUBSCRIBE_BYTE:
            self._subscribe_link(datanode, profile)

    def link_profile(self,
            obj:Any, 
            profile: IOpcuaProfile, 
            stack: bool = True, 
        ) -> None:
        """ Link data object with an OpcuaProfile 

        Args:
            obj: any object with an attrconnect property
            profile: OpcuaProfile. Define mapping of data path vs node path 
                and mapping of method path vs RPC 
            
            rpcs: optional, an alternative object use to link rpc method 
                if not given, obj is used 
        """
        for path, node_profile in profile.nodes.items():
            self.link_node(core.data_node(obj,path), node_profile, stack=stack)
        
        for path, rpc_profile in profile.rpc_methods.items():
            self.link_rpc(core.method_node(obj,path), rpc_profile)
 

