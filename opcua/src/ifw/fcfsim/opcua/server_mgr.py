from __future__ import annotations

import functools
import xml.etree.ElementTree as ET
from dataclasses import dataclass, field
from typing import Iterable

import asyncua
from asyncua import ua
from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import defaults
from ifw.fcfsim.opcua.ilinks import ILinkModeResolver, ISessionLinker, IUaLinks
from ifw.fcfsim.opcua.iprofile import INodeProfile, IOpcuaProfile, IRpcProfile
from ifw.fcfsim.opcua.iserver_mgr import IServerBuilder
from ifw.fcfsim.opcua.links import (
    PrefixedIdResolver,
    SessionLinker,
    UaLinks,
    default_link_mode_resolver,
)
from ifw.fcfsim.opcua.xml_profile import XmlNodeSetManager, create_node_set

log = core.get_logger("devsim.opcua")

async def _is_server_initialised(server: asyncua.Server) -> bool:
    """True if the server instance has been initialised"""
    try:
        _node_check = server.get_node(ua.NodeId(ua.ObjectIds.Server_NamespaceArray))# pyright:ignore #ua to blame
        await _node_check.read_value()
    except ua.uaerrors.BadNodeIdUnknown:
        return False 
    else:
        return True


@dataclass
class ServerBuilder:
    """Object to collect node and rpc profile in order to build server nodes"""
    # all nodes are stored in a XML (xml.etree.ElementTree) Element 
    # The intermediate OPCUA-nodes (program, FB, Structures, ...) 
    # are added if they do not exists in the nodeset. 

    nodeset: ET.Element = field(default_factory=create_node_set)

    def add_nodes(self,
         nodes: Iterable[INodeProfile],
         prefix: str = "",
         namespace: int = defaults.namespace,
         identifier: str = defaults.identifier
                  ) -> None:

        XmlNodeSetManager(
            prefix=prefix,
            namespace=namespace,
            identifier=identifier,
            nodeset=self.nodeset
        ).add_nodes(nodes)

    def add_rpcs(self, rpc_methods: Iterable[IRpcProfile],
          prefix: str = "",
          namespace: int = defaults.namespace,
          identifier: str = defaults.identifier
                 ) -> None:
        XmlNodeSetManager(
            prefix=prefix,
            namespace=namespace,
            identifier=identifier,
            nodeset=self.nodeset
        ).add_rpc_methods(rpc_methods)

    async def build_server(self, server: asyncua.Server) -> None:
        if not await _is_server_initialised(server):
            await server.init()
        await server.import_xml(None, ET.tostring(self.nodeset))

@dataclass
class ServerManager:
    """ Dedicated to setup an Opcua server and its links from an OpcuaProfile """
    
    endpoint: str = defaults.endpoint
    """ server endpoint address """

    builder: IServerBuilder = field(default_factory=ServerBuilder)
    """ Collect node and rpc profiles to build the server """
    
    links: IUaLinks = field(default_factory=UaLinks)
    """Containing links (push, pull, subscribe, etc ...)"""
            
    log: core.ILogger = log
    """server logging object""" 

    @functools.cached_property # !cached_property is important
    def server(self) ->  asyncua.Server:
        """asyncua Server Instance"""
        # Server is not initialised yet this is done by the builder
        server = asyncua.Server()
        server.set_endpoint(self.endpoint)
        return server

    def new_linker(self, 
          prefix: str = "",
          namespace: int  = defaults.namespace, 
          link_mode_resolver: ILinkModeResolver = default_link_mode_resolver, 
        ) -> ISessionLinker:
        """ Return a new Linker object

        The Linker is used to establish links between structured data (engine) 
        and server's node or rpc 
        
        Args:
            prefix: optional prefix added to each nodes 
            namespace: int default is 4 
            link_mode_resolver: a function resolving the link mode 
                (a 3 digit integer) for push, puul and subscribe
                The function takes 2 arguments, the Node Profile and 
                a flag which must be True in case of client.
        """
        return SessionLinker(
                self.links, 
                PrefixedIdResolver(prefix, namespace), 
                link_mode_resolver=link_mode_resolver 
        )

    def add_profile(self, 
          profile: IOpcuaProfile, 
          engine: object | None= None, 
          prefix: str = "", 
          namespace: int = 4, 
          identifier: str = "PLC1",
          link_mode_resolver: ILinkModeResolver = default_link_mode_resolver, 
        ) -> None:
        """Add new OPCUA profile containing Node and RPC definitions 
        
        Link node and rpc to an optional object ('engine') and set 
        everything to build the server

        Args:
            profile: input profile 
            engine (Optional): Linkable object, members to link are defined
                               in profile 
             
            prefix: optional prefix added to each nodes 
            namespace: default is 4 
            identifier (optional): default is 'PLC1', PLC identifier   
            link_mode_resolver: a function resolving the link  mode 
                    (a 3 digit integer)
                    from a node profile and a session kind.
                    Digits are in this order:  push, pull, subscribe 
        """
        if engine is not None:
            linker = self.new_linker(
                    link_mode_resolver=link_mode_resolver, 
                    namespace=namespace, prefix=prefix)

            linker.link_profile(engine, profile, stack=True)
        
        self.builder.add_nodes(
                profile.nodes.values(), prefix,  namespace, identifier)
        self.builder.add_rpcs(
                profile.rpc_methods.values(), prefix, namespace, identifier)
                
    def new_task(self) -> ServerTask:
        """ Return a new ServerTask """
        return ServerTask(self.server, self.builder, self.links, log=self.log)

    def new_runner(self, looper: core.ILooper) -> core.IRunner:
        """ Return a new ServerRunner """
        return core.TaskRunner(self.new_task(), looper)
    
    # Provide some handy method if someone wants to use the server manager 
    # manually (e.i. not within a runner)
    async def build(self) -> None:
        """ Init and build server from defined profile """

        await self.builder.build_server(self.server)

        self.log.info(
            f"Server built, ready to be started on {self.endpoint}"
        )

    async def push(self) -> None:
        await self.links.push(self.server)

    async def pull(self) -> None:
        await self.links.pull(self.server)

    async def update(self) -> None:
        await self.links.subscribe(self.server)
        await self.links.link_method(self.server)

    async def execute_coro(self) -> None:
        await self.links.execute_coro()


@dataclass
class ServerTask(core.BaseTask):
    """ Cycling Task for an OPCUA Server 

    Mostlikely create by new_task method of ServerManager
    """

    server: asyncua.Server 
    """ Instanciated Asyncua server (should not be build yet)"""

    builder: IServerBuilder = field(default_factory=ServerBuilder)
    """ Collect node and rpc profiles to build the server """
    
    links: IUaLinks = field(default_factory=UaLinks)
    """ object containing links (push,pull, and subscribe) """
            
    log: core.ILogger = log
    """ server logging system """ 


    async def initialise(self) -> None:
        await self.builder.build_server(self.server) 

        self.log.info( 
            f"Server built, ready to be started on {self.server}"
        )
    
    async def start(self) -> None:
        self.log.info("Entering OPCUA Server Process")
        await self.server.start()
       
    async def next(self) -> None:
        await self.links.push(self.server)
        await self.links.execute_coro()
        await self.links.subscribe(self.server)
        await self.links.link_method(self.server)

    async def end(self) -> None:
        await self.server.stop()



