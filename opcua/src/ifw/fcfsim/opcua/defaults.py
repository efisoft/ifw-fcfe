"""Some OPCUA package defaults"""
endpoint: str = "opc.tcp://127.0.0.1:4840"
namespace: int = 4 
identifier: str = "PLC1"
update_frequency: float = 20.0

