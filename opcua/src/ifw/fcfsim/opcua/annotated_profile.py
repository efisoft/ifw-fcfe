from __future__ import annotations
# #############################################
#
#  Exploration. These are not (yet) in API 
#

from dataclasses import Field, dataclass, field
import re
from typing import Any, Callable, Protocol
import typing
import inspect

from ifw.fcfsim.opcua.iprofile import IDataType, IOpcuaProfile
from ifw.fcfsim.opcua.base_profile import AccessLevel, split_path

from ifw.fcfsim.opcua.profile import  NodeProfile, OpcuaProfile, RpcArgumentProfile, RpcProfile
from ifw.fcfsim.opcua.base_profile import parse_data_type
from ifw.fcfsim.opcua.profile_parser import find_type_from_prefix 

class _DataClass(Protocol):
    __dataclass_fields__: dict[str, Field[Any]]


@dataclass
class UaNodeExtraction:
    path: str = ""
    ua_path: str = ""
    access: int = AccessLevel.CurrentRead + AccessLevel.CurrentWrite
    bind_all: bool = False

    def new(self, suffix: str, ua_suffix: str) -> UaNodeExtraction:
        return self.__class__(
            path=join_path(self.path, suffix),
            ua_path=join_path(self.ua_path, ua_suffix),
            access=self.access,
            bind_all=self.bind_all
        )


@dataclass
class Ua:
    suffix: str | None = None
    data_type: str | None = None
    namespace: int | None = None
    access: int | None = None
    bind_all: bool | None = None

    def populate_ua_profiles(self, 
            profile: IOpcuaProfile, 
            name: str, 
            type_: type, 
            rules: UaNodeExtraction
        ) -> None:

        if self.data_type is None:
            data_type = get_data_type(type_)
        else:
            data_type = parse_data_type(self.data_type)

        if data_type is None:
            new_rules = rules.new(name, self.suffix or name)
            _populate_class_profile(profile, type_, new_rules)
        else:
            node = NodeProfile(
                path=join_path(rules.ua_path, self.suffix or name),
                data_type=data_type,
                access=rules.access if self.access is None else self.access
            )
            profile.nodes[join_path(rules.path, name)] = node


@dataclass
class UaMethod:
    suffix: str|None = None
    is_method: bool = True 

    def populate_ua_profiles(self,
          profile:IOpcuaProfile, 
          name:str, 
          method: Callable[...,Any], 
          rules:UaNodeExtraction
        )->None:
        profile.rpc_methods[join_path(rules.path, name)] = self.get_rpc_profile(name,  method, rules)


    def get_rpc_profile(self, name:str, method: Callable[...,Any], rules:UaNodeExtraction)->RpcProfile:
        annotations = typing.get_type_hints( method, include_extras=True ) 
        if not "return" in annotations:
            raise ValueError("Invalid UaMethod, return type must be decorated")
    
        parameters = inspect.signature(method).parameters
        for pname, parameter in parameters.items():
            if parameter.kind in (parameter.VAR_KEYWORD, parameter.VAR_POSITIONAL):
                raise ValueError(f"argument {pname} is invalid for a uamethod, all parameters should be named")
        n_params = len(parameters)
        n_params -= self.is_method
        n_annotation = len(annotations) - 1 # remove the return 
        if n_params != n_annotation:
            raise ValueError("All method arguments must be annotated")
        
        if self.suffix:
            ua_name = self.suffix
        else:
            ua_name = name
        
        rpc_profile = RpcProfile(path=rules.ua_path, name = ua_name)
        
        ua_default = Ua()
        for name, annotation in annotations.items():
            type_, ua = _resolve_ua_definition(annotation, ua_default)
            if ua.data_type is None:
                data_type = get_data_type( type_ )
            else:
                data_type = parse_data_type( ua.data_type )
            
            if data_type is None:
                if name == "return" and type_ is type(None):
                    continue # allow None as return annotation  

                raise ValueError(f"Argument {name} as an invalid data type, got {type_}")
            if name == "return":
                rpc_profile.outputs.append( RpcArgumentProfile("Output", data_type))
            else:
                rpc_profile.inputs.append( RpcArgumentProfile(name, data_type))

        return rpc_profile


def uamethod(suffix:str | None = None) -> Callable[...,Any]:
    def decorate_ua_method(method:Callable[...,Any])->Callable[...,Any]:
        method.__ua_method__ = UaMethod(suffix) #type:ignore # assignation to callable
        return method
    return decorate_ua_method


def get_ua_args(args: tuple[Any, ...]) -> Ua | None:
    for arg in args:
        if isinstance(arg, Ua):
            return arg
        if isinstance(arg, str) and arg.startswith("ua:"):
            return string_to_ua(arg.removeprefix("ua:"))
    return None


_node_path_with_type = re.compile( '^([^(]+)[(]([^)]*)[)]') #   match abcd(Type)


def string_to_ua(s: str) -> Ua:
    """ Convert a String into Ua binding """
    if not s.strip():
        return Ua()

    root, name = split_path(s)

    match = _node_path_with_type.search(name)
    if match:
        name, data_type = match.group(1).strip(), match.group(2).strip()
    else:
        try:
            data_type = find_type_from_prefix(name)
        except ValueError:
            data_type = None
    return Ua(join_path(root, name), data_type=data_type)
    

def get_data_type(type_: type) -> IDataType | None:
    try:
        return parse_data_type(type_)
    except ValueError:
        return None

def join_path(*args:str)->str:
    return ".".join( (a for a in args if a) )


def create_ua_profile(
    cls: type,
    path: str = "",
    ua_path: str = "",
    access: int = AccessLevel.CurrentRead + AccessLevel.CurrentWrite,
    bind_all: bool = False
) -> OpcuaProfile:
    profile = OpcuaProfile()
    _populate_class_profile(profile, cls,
                            UaNodeExtraction(path=path, ua_path=ua_path,
                                             access=access, bind_all=bind_all
                                             )
                            )
    return profile
    


T = typing.TypeVar('T', bound=typing.Union[Ua,None])
def _resolve_ua_definition( annotation:Any, default: T)->tuple[type,Ua|T]:
    if typing.get_origin( annotation ) is typing.Annotated:
        args = typing.get_args(annotation)
        if len(args)<2: 
            return args[0], default 
        ua = get_ua_args(args)
        if ua is None:
            return args[0], default  
        return args[0], ua
    return annotation, default  




def _populate_class_profile(profile: IOpcuaProfile, cls:type, rules: UaNodeExtraction )->None:
    """ Add Nodes and Rpc profiles inside the global profile and from class """ 
    annotations: dict[str,Any] = typing.get_type_hints(cls, include_extras=True)
    ua_default = Ua() if rules.bind_all else None

    for name, annotation in annotations.items():
        type_, ua = _resolve_ua_definition( annotation, ua_default)
        if ua is None:
            continue
        ua.populate_ua_profiles( profile, name, type_, rules) 

    for attr in dir(cls):
        if rules.bind_all and not attr.startswith("_"):
            try:
                method: Callable[...,Any] = getattr(cls, attr) 
            except AttributeError:
                continue 
            else:
                if callable(method):
                    ua_method = UaMethod(attr, is_method=True)
                else:
                    continue
        else:
            try:
                method = getattr(cls, attr)
                ua_method: UaMethod = method.__ua_method__#type:ignore #checked at runtime 
            except AttributeError:
                continue 
        ua_method.populate_ua_profiles(profile,  attr, method,  rules)



if __name__ == "__main__":
    class A:

        t: typing.Annotated[str, "ua:sMessage"] = ""
        x: typing.Annotated[float, Ua("lrPos")] = 0.0
        x2: typing.Annotated[float, Ua()] = 0.0

        y: float = 0.0 
        z: typing.Annotated[float, "ua:rPos"] = 0.0
        n: typing.Annotated[int, "ua:nIter(Int16)"] = 0

        @uamethod("RPC_ChangeX")
        def change_x(self, val:int)->typing.Annotated[int, Ua(data_type="Int16")]:
            self.x = val
            return 0
        
        @uamethod("RPC_toto")
        def toto(self, val: typing.Annotated[int, Ua(data_type="Int16")])->None:
            self.x = val

   
    @dataclass
    class B:
        a1: typing.Annotated[A, Ua(bind_all=True)] = field(default_factory=A)
        a2: typing.Annotated[A, Ua(bind_all=False)] = field(default_factory=A)

    # _extract_profile( p,  B )
    print( create_ua_profile( B, bind_all=True) )
     
    # def toto(self, a:int,b:float)->typing.Annotated[int, Ua(data_type="Int16")]:
    #     return 0 
    # p = _extract_method_profile("toto", toto)
    # print( p ) 
        



    

