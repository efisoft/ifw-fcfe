from __future__ import annotations
from typing import Protocol
from ifw.fcfsim.core import api as core 

class IUaServerConfig(core.ILogConfig, Protocol):
    prefix: str
    """Device Prefix (e.g. 'MAIN.Motor1')"""
    
    endpoint: str
    """Opcua server end point"""
    
    namespace: int 
    """opcua namespace"""

    identifier: str
    """PLC object identifier"""

    update_frequency: float
    """Server update frequency"""

