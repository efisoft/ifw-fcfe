from __future__ import annotations

import argparse
from dataclasses import dataclass
from typing import Any, Protocol, cast

import ifw.core.utils.utils
from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import defaults


def new_endpoint(port:int, use_ext_ip: bool = False) -> str:
    """build a endpoint from port"""
    if use_ext_ip:
        address = ifw.core.utils.utils.get_ip_address()
    else:
        address = "127.0.0.1"
    return f"opc.tcp://{address}:{port}"


@dataclass
class UaServerConfig(core.LogConfig):
    prefix: str = ""
    """Device Prefix (e.g. 'MAIN.Motor1')"""
    
    endpoint: str = defaults.endpoint
    """Opcua server end point"""
    
    namespace: int = defaults.namespace 
    """opcua namespace"""

    identifier: str = defaults.identifier # not used yet
    """PLC object identifier"""

    update_frequency: float = 10.0
    """Server update frequency"""
    

class IUaServerCmdArgs(core.ILogCmdArgs, Protocol):
    """ Expected vars comming from devsim args parser """
    port: int 
    use_ext_ip: bool 
    verbose: bool  
    debug: bool 

def get_ua_server_argument_parser(
        arg_parser: argparse.ArgumentParser | None = None
) -> argparse.ArgumentParser:
    """ Return the Device Simulator application argument parser.

    Args:
       arg_parser (optional): an existing argument parser for which elements 
           will be added. If None a fresh one is created. 

    Returns:
       arg_parser: arg_parser input if given or a new one 
    """
    if arg_parser is None:
        arg_parser = argparse.ArgumentParser(description="OPCUA Server")
    arg_parser.add_argument(
        "--port", type=int, required=True, help="server port number"
    )
    arg_parser.add_argument(
        "--use-ext-ip",
        action="store_true",
        help="use the external IP address of deployment host",
        default=False,
    )
    arg_parser.add_argument(
         "--debug",
         action="store_true",
         help="Enter in debug iptyhon shell",
         default=False
    )
    arg_parser.add_argument("--server-name", required=False, help="server name")

    # Add Log command line arguments --list-loggers, --loggers, --log-level, --verbose
    core.get_log_argument_parser(arg_parser)
    return arg_parser 

class UaServerConfigLoader(core.ConfigLoader):
    """ Class to update a UaServerConfig object from argv and/or config files"""
        
    def load_argv(self, config:Any,  argv:list[str])->None:
        """ Update configuration from an argv list 

        Warning: This method can trigger a SystemExit(0) (e.g. in case of --help)
        """
        args_parser = get_ua_server_argument_parser()
        if "-h" in argv or "--help" in argv:
            args_parser.print_help()
            raise SystemExit(0)
        
        args = cast(IUaServerCmdArgs, args_parser.parse_args(argv)) 
        self.load_args(config, args)

    def load_args(self, config:Any, args:IUaServerCmdArgs)->None:
        """ Update configuration from an argument structure (e.i. parsed argvs) """
        # Load the log config business    
        # WARNING: Application can stop here if --list-loggers is True
        core.LogConfigLoader().load_args(config, args)  
        config.endpoint = new_endpoint(args.port, args.use_ext_ip)
        config.debug = args.debug

