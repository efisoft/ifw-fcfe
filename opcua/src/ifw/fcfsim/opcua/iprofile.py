from __future__ import annotations
from typing import Any, ClassVar, Protocol, runtime_checkable


class IUaPath(Protocol):
    def is_numerical(self) -> bool:
        ...

    def format(self, namespace: int) -> str:
        ...

    def copy(self, prefix: str | int | IUaPath | None = ...) -> IUaPath:
        ...
    
    def get_name(self) -> str:
        ... 
    
    def get_path(self) -> str | int:
        ...


    def get_root(self) -> IUaPath | None:
        ...

    def get_level(self) -> int:
        ...


class IDataType(Protocol):
    """ Define name variant number and default of a data type """
    # In context of Interface property means read-only (frozen)
    @property 
    def name(self)->str:
        ...
    @property 
    def num(self)->int:
        ...
    @property
    def pytype(self)->type:
        ...
    @property
    def default(self)->Any:
        ...

@runtime_checkable
class INodeProfile(Protocol):
    path: IUaPath
    """ Node relative path """
    data_type: IDataType
    """ Node Data Teyp Profile """
    access: int  
    """ Acces level of the node """
    default: Any 
    """ default, if None this will be the data_type default """
    def copy(self, prefix: str = ...) -> INodeProfile:
        ...

@runtime_checkable
class IRpcProfile(Protocol):
    path: IUaPath
    """ parent RPC node relative path (without method name) """
    name: str
    """ method name, e.g. 'RPC_Init' """
    inputs: list[IRpcArgumentProfile] 
    """ input argument profiles """
    outputs: list[IRpcArgumentProfile] 
    """ output argument profiles """
    error_output: bool 
    """True if the first output argument is an error code"""
    def copy(self, prefix: str = ...) -> IRpcProfile:
        ...

class IRpcArgumentProfile(Protocol):
    name: str 
    data_type: IDataType

class IOpcuaProfile(Protocol):
    """ A full OPCUA interface containing nodes and RPCs """
    nodes: dict[str, INodeProfile] 
    rpc_methods: dict[str, IRpcProfile] 
    
    def add(self, name: str, element: INodeProfile|IRpcProfile) -> None:
        ...

    def copy(self, opcua_prefix:str="", data_prefix:str="")->IOpcuaProfile:
        ...
    
    def reduce(self, prefix: str) -> IOpcuaProfile:
        ...

    def merge(self, 
            *profiles: IOpcuaProfile, 
            opcua_prefix: str = ...,
            data_prefix: str =...) -> None:
        ...
