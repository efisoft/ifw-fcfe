from typing import Protocol


class IClientAppConfig(Protocol):
    prefix: str 
    """Device Prefix (e.g. 'MAIN.Motor1')"""
    
    endpoint: str    
    """Opcua server end point"""
    
    namespace: int
    """opcua namespace"""

    identifier: str 
    """PLC object identifier"""

    update_frequency: float
    """House keeping client update frequency"""
    
    keep_alive: bool
    """If True the client running task will be kept alive 

    Attempt of reconection will be done every `keep_alive_period`
    """
    
    keep_alive_period: float
    """Period for which a reconnection try is done"""
    
    timeout: float
    """Connection timeout in seconds"""

