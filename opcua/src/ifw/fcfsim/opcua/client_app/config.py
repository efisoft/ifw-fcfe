from __future__ import annotations
import argparse
from dataclasses import dataclass
from typing import Any, Protocol, cast
from ifw.fcfsim.core import api as core 
from ifw.fcfsim.opcua import defaults

@dataclass
class ClientAppConfig:
    prefix: str = ""
    """Device Prefix (e.g. 'MAIN.Motor1')"""
    
    endpoint: str = defaults.endpoint
    """Opcua server end point"""
    
    namespace: int = defaults.namespace
    """opcua namespace"""

    identifier: str = defaults.identifier # actually not used yet
    """PLC object identifier"""

    update_frequency: float = 10.0
    """House keeping client update frequency"""
    
    keep_alive: bool = False 
    """If True the client running task will be kept alive 

    Attempt of reconection will be done every `keep_alive_period`
    """
    
    keep_alive_period: float = 2.0 
    """Period for which a reconnection try is done"""
    
    timeout: float = 4.0 
    """Connection timeout in seconds"""
    
class IClientAppCmdArgs(Protocol):
    """Expected properties comming from the args parser"""
    address: str
    prefix: str
    ns: int
    loglevel: str 

def get_client_app_argument_parser() -> argparse.ArgumentParser:
    """ Return the UaClient application argument parser """

    arg_parser = argparse.ArgumentParser(description="UA Client")
    arg_parser.add_argument(
        "-a", "--address", 
        type=str, required=True, help="OPCUA Address e.g. opc.tcp://134.171.59.98:4840"
    )
   
    arg_parser.add_argument(
        "-p", "--prefix",
        type=str, required=True, 
        help="Device prefix, e.g. MAIN.Motor1",
    )

    arg_parser.add_argument(
        "-n", "--ns",
        type=int, required=False, 
        help="OPCUA namespace, e.g. 4", default=4
    )

    arg_parser.add_argument(
        "-l", "--loglevel",
        choices=("CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"),
        help="logging level",
        default="WARNING",
    )

    return arg_parser 



@dataclass
class ClientAppConfigLoader(core.ConfigLoader):
    """ Class to update a UaClient config object from argv and/or config files

    Example::
    
        ConfigLoader().load_argv(my_config, sys.argv[1:] )

    The above exemple will modify my_config in place
    """


    def load_argv(self, config: Any,  argv:list[str]) -> None:
        """ Update configuration from an argv list 

        Warning: This method can trigger a SystemExit(0) (e.g. in case of --help)
        """
        args_parser = get_client_app_argument_parser()
        if "-h" in argv or "--help" in argv:
            args_parser.print_help()
            raise SystemExit(0)
        
        args = cast(IClientAppCmdArgs, args_parser.parse_args(argv)) 
        self.load_args(config, args)

    def load_args(self, config: Any, args:IClientAppCmdArgs) -> None:
        """ Update configuration from an argument structure (e.i. parsed argvs) """
        config.prefix = args.prefix 
        config.address = args.address 
        config.namespace = args.ns


