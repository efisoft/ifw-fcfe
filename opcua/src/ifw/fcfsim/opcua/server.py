from __future__ import annotations
from dataclasses import InitVar, dataclass
import typing
import asyncua
from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import defaults
from ifw.fcfsim.opcua.iprofile import IOpcuaProfile
from ifw.fcfsim.opcua.iserver import IServerInterface
from ifw.fcfsim.opcua.profile_parser import read_profile
from ifw.fcfsim.opcua.server_mgr import ServerManager


T = typing.TypeVar('T', bound=typing.Any)


@dataclass
class ServerInterface(typing.Generic[T]):
    """ A Generic Interface for UaServer """
    
    engine: T 
    """ Engine object containing linked data and methods """
    
    profile: InitVar[IOpcuaProfile | str]
    """ Opcua Profile to link the engine """
    
    prefix: str = ""
    """ Optional prefix added to profile when linked """
    
    namespace: int = defaults.namespace 
    """ opcua namespace default is 4"""
    
    identifier: str = defaults.identifier
    """ OPCUA identifier, e.g. 'PLC1' """

    def __post_init__(self, profile: IOpcuaProfile | str) -> None:
        if isinstance(profile, str):
            self.uaprofile: IOpcuaProfile = read_profile(profile)
        else:
            self.uaprofile = profile

    def get_engine(self) -> T:
        return self.engine 

    def add_to_server(self, server_mgr: ServerManager, /) -> None:
        engine = self.get_engine()
        server_mgr.add_profile( 
                self.uaprofile, engine, 
                prefix=self.prefix,
                namespace=self.namespace, 
                identifier=self.identifier
        ) 
        





class UaServer:
    """High level Server class"""

    update_frequency: float 
    """Client main loop frequency"""
    
    def __init__(self, 
        endpoint: str = defaults.endpoint, 
        update_frequency: float = 20.0
    ) -> None:
        self._server_mgr =  ServerManager(
                    endpoint=endpoint, 
                )
        self.update_frequency = update_frequency
    
    @property 
    def server_mgr(self) -> ServerManager:
        return self._server_mgr
    
    @property 
    def server(self) -> asyncua.Server:
        return self.server_mgr.server
 
    @property
    def endpoint(self) -> str:
        return self.server_mgr.endpoint
    
    @endpoint.setter 
    def endpoint(self, endpoint: str) -> None:
        self.server_mgr.endpoint = endpoint


    def attach(self, interface: IServerInterface[T]) -> T:
        interface.add_to_server(self.server_mgr)
        return interface.get_engine()

    def new_looper(self) -> core.ILooper:
        return core.Looper( 1./self.update_frequency)


    def new_runner(self) -> core.IRunner:
        return self.server_mgr.new_runner(self.new_looper())

    def iter_runners(self, excludes: set[str] = set()) -> typing.Iterator[core.IRunner]:
        if "opcua" not in excludes:
            yield self.new_runner()

    def new_session(self, 
            excludes: set[str] = set()
        ) -> core.ISession:
        runner = core.RunnerGroup(*self.iter_runners(excludes=excludes))
        return core.Session(runner)
    
    def start(self) -> core.ISession:
        session = self.new_session()
        session.start()
        return session

    async def run(self) -> int:
        session = self.new_session()
        return await session.run()


