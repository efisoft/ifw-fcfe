


from typing import Any, Protocol, TypeVar
from ifw.fcfsim.opcua.iclient import IClientInterface
from ifw.fcfsim.opcua.client_mgr import ClientManager
from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import defaults

from ifw.fcfsim.opcua.client import UaClient
from ifw.fcfsim.opcua.profile_parser import read_profile

T = TypeVar('T', bound=Any)

def start_client(
    interface: IClientInterface[T],
    endpoint: str = defaults.endpoint, 
    timeout: float = 4.0, 
    keep_alive: bool = False, 
    keep_alive_period: float = 2.0 ,
    update_frequency: float = defaults.update_frequency 
    ) -> tuple[core.ISession,T]:
    """Build, attach interface and connect client in one go"""
    client = UaClient(
            endpoint=endpoint,
            timeout=timeout, 
            keep_alive=keep_alive,
            keep_alive_period=keep_alive_period, 
            update_frequency=update_frequency
    )
    engine = client.attach(interface)
    return client.start(), engine


class _InterfaceForAddToClient(Protocol):
    prefix: str
    namespace: int 
    identifier: str
    def get_engine(self) -> Any:
        ...

def add_to_client(
    interface: _InterfaceForAddToClient, 
    client_manager: ClientManager, 
    namspace_file: str 
    ) -> None:
    """ Using a standard client interface, add nodes to a client manager
    
    The interface must have get_engine() method as well as prefix, namespace and 
    identifier properties.
    """
    engine = interface.get_engine()

    client_manager.add_profile( 
        read_profile(namspace_file), 
        engine,  
        namespace=interface.namespace, 
        identifier=interface.identifier, 
        prefix= interface.prefix
    )
    if hasattr(engine, "ua_info"):
        client_manager.add_ua_info(engine.ua_info)



   
