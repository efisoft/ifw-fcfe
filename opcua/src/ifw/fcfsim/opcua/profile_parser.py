""" Tools to parse a yaml file into opcua profile interface """
from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any, Callable, Iterable
import re

from ifw.fcfsim.opcua.profile import (
        NodeProfile, 
        OpcuaProfile, 
        RpcArgumentProfile, 
        RpcProfile,
        ua_path, 
)
from ifw.fcfsim.opcua.base_profile import ( 
        AccessLevel,
        join_path,
        parse_data_type, 
        split_path, 
        default_access
)
from ifw.fcfsim.opcua.iprofile import (
    INodeProfile, IOpcuaProfile, IRpcArgumentProfile, IRpcProfile
)
from ifw.fcfsim.core.io import YamlReader

from ifw.fcfsim.opcua.iprofile_parser import (
    ProfileEntry,     
    ProfileMapping, 
    IProfileParser
)

from ifw.fcfsim.core.iio import IIoReader


# re to match node name with type defined 
_node_path_with_type = re.compile( '^([^(]+)[(]([^)]*)[)]') #   match abcd(Type)
# TODO so far cases like lrval instead of lrValue will be interpreted as Double. Check if this is ok 


_rpc_def_with_args = re.compile( r'^([^(]+)[(](.*)[)]\s*$') #  match abcd( arg1(Type), arg2(Type) ) 

def find_type_from_prefix( name: str)->str:
    """ convert a TC3 Hungarian notation prefix into profile type """
    if name.startswith('s'): 
        return 'String'
    if name.startswith('b'):
        return 'Boolean'
    if name.startswith('n'): # TODO check if 'n' is a INT32
        return 'Int32'
    if name.startswith('lr'):
        return 'Double'
    if name.startswith('li'):
        return 'Int64'
    if name.startswith('lui'):
        return 'UInt64'
    if name.startswith('r'):
        return 'Float'
    if name.startswith('ui'):
        return 'UInt32'
    if name.startswith('i'):
        return 'Int32'

    raise ValueError( f"Illegal type specification contained in variable: {name}")

def guess_access_level(path:str, write_prefixes:Iterable[str] = ("ctrl","cfg") )->int:
    """ Guess the access level of the node 

    If the path contains the write_prefixes , default is 'ctrl' or 'cfg'
    element it will be Read and Write access, read otherwise 
    """
    names = path.split(".") 
    for prefix in write_prefixes:
        if prefix in names:
            return AccessLevel.CurrentRead + AccessLevel.CurrentWrite 
    return int(AccessLevel.CurrentRead)

def is_rpc(path:str)->bool:
    """ Guess from a path string if the node is an RPC method """
    names = path.split(".")
    if names and names[0].upper() == "METHOD":
        return True 
    return False

def parse_access( access: int | str)->int:
    """ Transform a string to opcua access level integer """
    if isinstance(access, str):
        access = access.lower()
        if access not in ('r','w','rw','wr'):
            raise ValueError( f"Access must be 'r','w','rw', or 'wr' got {access!r}")
        return AccessLevel.CurrentRead*('r' in access) + AccessLevel.CurrentWrite*('w' in access) 
    return int(access)


@dataclass
class NamespaceProfileParser:
    """ Parser of string which define OPCUA path and type """
    access_level_getter: Callable[[str],int] = guess_access_level
    def can_parse(self, inputs:Any)->bool:
        """ True if input can be parsed """
        if isinstance(inputs, str):
            return True
        return False

    def parse_element(self, definition: str)->INodeProfile|IRpcProfile:
        if is_rpc(definition):
            return self.parse_rpc(definition)
        else:
            return self.parse_node(definition)

    def parse_node(self, node_def: str)->INodeProfile:
        root, name  = split_path(node_def)  

        access = self.access_level_getter(node_def)

        match = _node_path_with_type.search( name )
        if match:
            name, data_type = match.group(1).strip(), match.group(2).strip()
        else: 
            data_type = find_type_from_prefix(name)
        return NodeProfile(ua_path(join_path(root, name)), parse_data_type(data_type), access=access)
    
    def parse_rpc(self, rpc_def:str)->IRpcProfile:
        rpc_def = rpc_def.removeprefix("method.")
        root, name = split_path(rpc_def) 
        
        match = _rpc_def_with_args.search( name )
        if match:
            name = match.group(1).strip()
            inputs, outputs = self._parse_rpc_arguments(match.group(2).strip())
        else:
            inputs, outputs = [], []
        error_output = False 
        if len(outputs) == 1:
            if outputs[0].data_type.name in ["Int16"]:
                error_output = True 
        # if is_numerical_id_name(name): 
        #     true_name = name
        # else:
        #     true_name = "RPC_"+name 
        true_name = name
        return RpcProfile(root, true_name, inputs, outputs, error_output=error_output)  

    def _parse_rpc_argument(self, arg_string: str)->tuple[str,IRpcArgumentProfile]:
        cat, _, arg_def = arg_string.partition(":")
        cat = cat.strip()
        match = _node_path_with_type.search( arg_def.strip() )
        if match:
            name = match.group(1).strip()
            sdata_type = match.group(2).strip() 
        else:
            raise ValueError(f"{arg_string!r} is not a valid RCP argument definition")
        if cat not in ("I","O"):
            raise ValueError( f'Illegal argument category: {cat!r} in {arg_string!r}. Must be "I" or "O"')
        return cat, RpcArgumentProfile(name, parse_data_type(sdata_type))

    def _parse_rpc_arguments(self, args_string: str)->tuple[list[IRpcArgumentProfile],list[IRpcArgumentProfile]]:
        inputs: list[IRpcArgumentProfile] = [] 
        outputs: list[IRpcArgumentProfile] = []

        if not args_string.strip():
            return inputs, outputs
        args =  args_string.split(",")
        for arg in args:
            cat, rpc_argument = self._parse_rpc_argument( arg )
            if cat == "O":
                outputs.append( rpc_argument )
            elif cat == "I":
                inputs.append( rpc_argument)
        return inputs, outputs

class DictProfileParser:
    """ A Profile parser using dict node/rpc definitions 
    
    This is not used yet but act has a place holder. 
    """
    def can_parse(self, inputs:Any) -> bool:
        """ True if input can be parsed """
        return isinstance(inputs, dict)

    def parse_element(self, definition: dict[str, Any]) -> INodeProfile | IRpcProfile:
        try:
            elem_type = definition['type']
        except KeyError:
            raise ValueError(f"'type' keyword is missing in {definition}")

        if elem_type == "Method":
            return self.parse_rpc(definition)
        else:
            return self.parse_node(definition)

    def parse_node(self, node_def: dict[str, Any]) -> INodeProfile:
        try:
            data_type = node_def['type']
        except KeyError:
            raise ValueError(f"'type' keyword is missing in {node_def}")
        try:
            path = node_def['path']
        except KeyError:
            raise ValueError(f"'path' keyword is missing in {node_def}")

        default = node_def.get('default', None)
        access = parse_access(node_def.get('access', default_access))
        return NodeProfile(ua_path(path), data_type, access=access, default=default)

    def parse_rpc(self, definition: dict[str, Any]) -> IRpcProfile:
        try:
            path = definition['path']
        except KeyError:
            raise ValueError(f"'path' keyword is missing in {definition}")
        try:
            name = definition['name']
        except KeyError:
            raise ValueError(f"'name' keyword is missing in {definition}")


        inputs = [self._parse_dict_rpc_arguments(arg_def) for arg_def in  definition.get('inputs',[])]
        outputs = [self._parse_dict_rpc_arguments(arg_def) for arg_def in  definition.get('outputs',[])]
        return RpcProfile(path, name, inputs, outputs)
   
    def _parse_dict_rpc_arguments(self, arg_def: dict[str, Any]) -> IRpcArgumentProfile:
        try:
            data_type = arg_def['type']
        except KeyError:
            raise ValueError(f"'type' keyword is missing in {arg_def}")
        try:
            name = arg_def['name']
        except KeyError:
            raise ValueError(f"'name' keyword is missing in {arg_def}")

        return RpcArgumentProfile(name, data_type)

def default_parsers()->list[IProfileParser]:
    """ A List of default opcua parsers """
    return [NamespaceProfileParser(), DictProfileParser()]

@dataclass
class ProfileParser:
    """ A Profile parser using several sub-parser to switch from one to another 

    This is used in, for instance, a yaml file mixing string and dictionary definition 
    for nodes.
    """
    parsers: list[IProfileParser] = field(default_factory=default_parsers)
    
    def can_parse(self, inputs:ProfileEntry)->bool:
        for parser in self.parsers:
            if parser.can_parse(inputs):
                return True 
        return False 
    
    def parse_element(self, definition:ProfileEntry)->IRpcProfile|INodeProfile:
        for parser in self.parsers:
            if parser.can_parse( definition ):
                break
        else:
            raise ValueError(f"Cannot parse: {definition} ")
        return parser.parse_element( definition )


def mapping_to_profile(
    mapping: ProfileMapping,
    opcua_prefix: str = "",
    data_prefix: str = "",
    parser: IProfileParser = ProfileParser()
) -> IOpcuaProfile:
    """Load an OPCUA profile from a map

    Args:
        mapping: dictionary with data key / opcua definition pairs
        opcua_prefix (optional): Add this prefix to opcua node names 
        data_prefix (optional): Add this prefix to data keys 
        parser (optional): alternative parser to read Node and Rpc 
            profiles.

    Returns:
        profile: an IOpcuaProfile compatible object
    """
    profile = OpcuaProfile()

    for name, definition in mapping.items():
        try:
            obj = element_profile(definition, opcua_prefix, parser) 
        except ValueError as er:
            raise ValueError(f"When parsing {name}: {er}") from er

        profile.add(join_path(data_prefix, name), obj)

    return profile


@dataclass
class ProfileReader:
    """A Reader for OpcUaProfile"""

    opcua_prefix: str = ""
    """Add this prefix to opcua path"""
    data_prefix: str = ""
    """Add this prefix to the data (python) namespace"""
    reader: IIoReader[dict[str,ProfileEntry]] = field(default_factory=YamlReader) 
    """Reader use to extract the raw dictionary"""

    def read(self, filename: str) -> IOpcuaProfile:
        return mapping_to_profile( 
                self.reader.read(filename), 
                opcua_prefix=self.opcua_prefix, 
                data_prefix=self.data_prefix
        )

################################################################
#
#   Helpers Functions
#
################################################################

def element_profile(
        definition: ProfileEntry,
        prefix: str = "",
        parser: IProfileParser = ProfileParser()
) -> INodeProfile | IRpcProfile:
    """Parse a string or dict into a Node or RPC Profile
    
    A prefix can be added to the node path
    """
    elem = parser.parse_element(definition)
    if prefix:
        elem = elem.copy(prefix)
    return elem


def node_profile(
        definition: ProfileEntry,
        prefix: str = "",
        parser: IProfileParser = ProfileParser()
) -> INodeProfile:
    """Parse a valid definition (string or dict) of a node into a NodeProfile"""
    node_profile = element_profile( definition, prefix, parser)
    if not isinstance(node_profile, INodeProfile):
        raise ValueError("This is not a value node but rpc")
    return node_profile

def rpc_profile(
        definition: ProfileEntry,
        prefix: str = "",
        parser: IProfileParser = ProfileParser()
) -> IRpcProfile:
    """Parse a valid definition (string or dict) of a rpc into a RpcProfile"""
    if isinstance(definition, str) and not definition.startswith("method."):
        definition = "method."+definition
    rpc_profile = element_profile(definition, prefix, parser)
    if not isinstance(rpc_profile, IRpcProfile):
        raise ValueError("This is not a method node but a regular node")
    return rpc_profile


def read_profile(
    filename: str,
    mapping_path: str = "",
    opcua_prefix: str = "",
    data_prefix: str = ""
) -> IOpcuaProfile:
    """ Read an Opcua Profile from a yaml file """
    return ProfileReader(
        opcua_prefix=opcua_prefix,
        data_prefix=data_prefix,
        reader=YamlReader(mapping_path)
    ).read(filename)


