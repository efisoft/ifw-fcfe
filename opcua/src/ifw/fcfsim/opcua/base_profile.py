from __future__ import annotations
from enum import Enum
from typing import Any, NamedTuple
from asyncua import ua 
from ifw.fcfsim.opcua.iprofile import IDataType
from datetime import datetime, timezone

def split_path(path: str)->tuple[str,str]:
    """ Split a node path into (root,name) 
    e.g. :  'a.b.c' -> ('a.b', 'c') 
    """
    *root, elem = path.split(".")
    return ".".join(root), elem 

def is_numerical_id_name(name: str) -> bool:
    """True if the string name is iddentified as a numerical id"""
    return name.startswith("i=")

def format_numerical_id_name(namespace:int, name: str) -> str:
    return f"ns={namespace};{name}"

def format_string_id_name(namespace: int, name: str) -> str:
    return f"ns={namespace};s={name}"

def join_path(*paths:str)->str:
    if paths and is_numerical_id_name(paths[-1]):
        return paths[-1]
    return ".".join( p.strip() for p in paths if p.strip())

class AccessLevel(int, Enum):
    """ 8-bit unsigned integer 
    see https://node-opcua.github.io/api_doc/0.1.0/files/packages_node-opcua-address-space_src_ua_variable.js.html?#l262
    """
    CurrentRead=   2**0  #  Indicates if the current value is readable
    CurrentWrite=  2**1  #  Indicates if the current value is writable
    HistoryRead=   2**2  #  Indicates if the history of the value is readable
    HistoryWrite=  2**3  #  Indicates if the history of the value is writable
    SemanticChange= 2**4 #  Indicates if the Variable used as Property generates SemanticChangeEvents (see 9.31).
    Reserved1 = 2**5  #   5:7    Reserved for future use. Shall always be zero.
    Reserved2 = 2**6
    Reserved3 = 2**7

default_access = AccessLevel.CurrentWrite + AccessLevel.CurrentRead

class DataType(NamedTuple):
    """ Define name variant number and default of a data type """
    name: str 
    """OPC-Ua type name"""
    num: int 
    """OPC-Ua Type ID """
    pytype: type
    """Python type"""
    default: Any 
    """Python default"""

_data_types_list: list[IDataType] = [
    DataType("Boolean", 1, bool,  False), 
    DataType("SByte", 2, bytes, b''), 
    DataType("Byte", 3, bytes, b''), 
    DataType("Int16", 4, int, int()), 
    DataType("UInt16", 5,int,  int()), 
    DataType("Int32", 6, int, int()), 
    DataType("UInt32", 7, int, int()), 
    DataType("Int64", 8, int, int()), 
    DataType("UInt64", 9, int, int()), 
    DataType("Float", 10, float, float()), 
    DataType("Double", 11, float, float()), 
    DataType("String", 12, str, ""), 
    DataType("DateTime", 13, datetime,  datetime(1970,1,1, tzinfo=timezone.utc)), 
    DataType("ByteString", 15, bytes, b''), 
]
# suported DataType 
_data_types_loockup: dict[Any, IDataType] = {
                        **{dt.name: dt for dt in _data_types_list}, 
                        **{dt.num: dt for dt in _data_types_list}, 
                        **{ua.VariantType(dt.num): dt for dt in _data_types_list} 
                       }
_data_types_loockup[str] = _data_types_loockup["String"] 
_data_types_loockup[float] = _data_types_loockup["Double"]
_data_types_loockup[int] = _data_types_loockup["Int64"]
_data_types_loockup[bytes] = _data_types_loockup["SByte"]
_data_types_loockup[datetime] = _data_types_loockup["DateTime"]



def parse_data_type(dp: str|int|ua.VariantType|IDataType|type)->IDataType:
    """ Return a DataType 
    Input argument can be a string of a DataType class 
    if A DataType it is returned as it is 
    If a string, it must be registered 
    """
    if isinstance(dp, DataType): 
        return dp 
    try:
        return _data_types_loockup[dp]
    except KeyError:
        raise ValueError(f"data type {dp!r} is not supported") 


