from __future__ import annotations
from dataclasses import dataclass , field
import functools
import time
from typing import Callable, ClassVar, overload
import asyncua
from ifw.fcfsim.opcua.links import PrefixedIdResolver, SessionLinker, UaLinks, UaSessionInfoStack, default_link_mode_resolver
from ifw.fcfsim.opcua.ilinks import ILinkModeResolver, ISessionLinker, IUaInfo, IUaLinks
from ifw.fcfsim.opcua.iprofile import  IOpcuaProfile
from ifw.fcfsim.core import api as core 
from ifw.fcfsim.opcua import defaults

log = core.get_logger("opcua.client")


@dataclass(init=True) 
class ClientManager:
    """ Dedicated to handle an OpcuaClient and make connection """

    endpoint: str = defaults.endpoint
    """ server endpoint address """
    
    timeout: float = 4.0
    """ client to server request timeout in second """
    
    links: IUaLinks = field(default_factory=UaLinks)
    """ object containing links (push,pull, and subscribe) """

    keep_alive: bool = False 
    """If True the client running task will be kept alive
    
    Attempt of reconection will be done every `keep_alive_period`
    """
    
    keep_alive_period: float = 2.0 
    """Period for which a reconnection try is done"""

    log: core.ILogger = log
    
    @functools.cached_property
    def client(self) -> asyncua.Client:
        return asyncua.Client(self.endpoint, self.timeout)
    
    @functools.cached_property
    def ua_info_stack(self) -> UaSessionInfoStack:
        return UaSessionInfoStack(self.endpoint)

    def new_linker(self, 
          prefix: str = "", 
          namespace: int = defaults.namespace, 
          link_mode_resolver: ILinkModeResolver = default_link_mode_resolver, 
        ) -> ISessionLinker:

        return SessionLinker(
                self.links, 
                PrefixedIdResolver(prefix, namespace), 
                client_links=True, 
                link_mode_resolver=link_mode_resolver, 
        )
    
    def add_profile(self, 
          profile: IOpcuaProfile, 
          obj: object, 
          prefix: str = "",
          namespace: int = defaults.namespace, 
          identifier: str = defaults.identifier, 
          link_mode_resolver: ILinkModeResolver = default_link_mode_resolver, 
        ) -> None:
        del identifier # This is not used for client 
        linker = self.new_linker(namespace=namespace, prefix=prefix,
                   link_mode_resolver=link_mode_resolver)
        linker.link_profile(obj, profile, stack=False)
    
    def add_ua_info(self, 
            *infos: IUaInfo
        ) -> None:
        """Add one or several IUaInfo structure 
        They will be updated by the client taks 
        """
        self.ua_info_stack.add(*infos)
        
    def new_task(self) -> ClientTask | KeepaliveClientTask:
        """ Return a new Client Task """
        client_task = ClientTask(
             self.client, self.links, ua_info_stack=self.ua_info_stack)
        if self.keep_alive:
            return KeepaliveClientTask(client_task, try_period= self.keep_alive_period)
        else:
            return client_task
    
    def new_runner(self, looper: core.ILooper) -> core.IRunner:
        """ Return a new ClientRunner with a looper """
        return core.TaskRunner(self.new_task(), looper)

    async def push(self)->None:
        """ Push modified python data on ua client """
        await self.links.push(self.client)
    
    async def pull(self)->None:
        """ Pull client ua nodes into python data """
        await self.links.pull(self.client)
    
    async def update(self)->None:
        """ Update links if new connection was added or removed """
        await self.links.subscribe(self.client)
        await self.links.link_method(self.client)
    
    async def execute_coro(self)->None:
        await self.links.execute_coro()


@dataclass
class ClientTask(core.BaseTask):
    """Cycling task for an OPCUA client"""
    client: asyncua.Client 
    """ Asyncua Client (not yet connected)"""

    links: IUaLinks = field(default_factory=IUaLinks)
    """Contains all links with python engine(s)"""
   
    ua_info_stack: UaSessionInfoStack = field(default_factory=UaSessionInfoStack)
    """A Stack of IUaInfo structure to be updated"""
    
    pull_first: bool = False 
    """ Pull all nodes when the connection is established"""

    pull: bool = False
    """If True force to pull all connected nodes at each cycle 
    This is normally not needed because of the subscriptions running
    """

    log: core.ILogger = log
    """logging object"""
    
    connection_flag: bool = False 

    def set_connection(self, connection_flag: bool) -> None:
        """Set the sonnection status"""
        self.connection_flag =  connection_flag 
        self.update_connection_info()

    def update_connection_info(self) -> None:
        self.ua_info_stack.set_connection(self.connection_flag)

    async def initialise(self)->None:
        # Nothing to do to initialise the client 
        pass  
    
    async def start(self)->None:
        self.log.info("Entering OPCUA Client Process")

        await self.links.link_method(self.client)
        await self.client.connect()
        await self.links.subscribe(self.client)
        # pull all nodes when client starts
        if self.pull_first:
            await self.links.pull(self.client) 
        else:
            await self.links.pull_check(self.client, raise_error= self.pull)
        self.set_connection(True)
        
 
    async def next(self) -> None:
        await self.links.execute_coro() 
        await self.links.push(self.client)
        await self.links.subscribe(self.client)
        await self.links.link_method(self.client)
        if self.pull:
            await self.links.pull(self.client)
            # we do not pull by default, subscribe shall be enough 
       
    async def end(self) -> None:
        await self.client.disconnect() 
        self.links.clear()
        self.connection_flag = False
        self.set_connection(False)
        self.log.info("Ending OPCUA Client Process") 


class _ProtocolState:
    INITIALIZED: ClassVar[str] = 'initialized'
    OPEN: ClassVar[str] = 'open'
    CLOSED: ClassVar[str] = 'closed'


def check_connection(client: asyncua.Client) -> None:
    try:
        if client.uaclient.protocol.state == _ProtocolState.CLOSED:
            raise ConnectionError()
    except AttributeError:
        raise ConnectionError()

@dataclass
class KeepaliveClientTask(core.BaseTask):
    """A cycling client task which never die when connection brokes

    If the client connection is broken an new attempt is done every 
    `try_period` seconds. 
    """

    client_task: ClientTask
    """Child Client task to be ran"""

    try_period: float = 2.0
    """Re-try connection period"""

    _last_attempt_time: float = field(default=0.0, init=False)

    async def initialise(self)->None:
        await self.client_task.initialise()
    
    async def start(self)->None:
        await self.client_task.start()
        
    async def end(self)->None:
        await self.client_task.end() 
    
    async def next(self)->None:
        await self._next()

    async def _connected_next(self) -> None:
        try:
            check_connection(self.client_task.client)        
            await self.client_task.next() 
        except ConnectionError:
            self._next = self._disconnected_next # type:ignore[method-assign]
            self.client_task.set_connection(False)

            self._last_attempt_time = time.time()
            self.client_task.log.error(
                 "Client has been deconnected." 
                 " A reconnection will be attemped" 
                f" every {self.try_period} seconds"
                )

    async def _next(self) -> None: # _next method is overwriten in case of deconnection
        await self._connected_next()
       
    async def _disconnected_next(self) -> None:
        if (time.time()-self._last_attempt_time) > self.try_period:
            try:
                # Din't succeed to re-establish connection and subscriptions 
                # from an instancied client connection. 
                # Try to do it on a new client. TODO: check ghost threads ?
                client = asyncua.Client(self.client_task.client.server_url.geturl())
                await client.connect()
            except OSError:
                self._last_attempt_time = time.time()
            else:
                if self.client_task.client.uaclient.protocol:
                    if self.client_task.client.uaclient.protocol.state != _ProtocolState.CLOSED:
                        await self.client_task.client.disconnect()
                self.client_task.client = client # set a new client 
                await self.client_task.links.relink(client) # re-establish the links
                # chenge _next method to the connected one
                self._next = self._connected_next # type:ignore[method-assign]
                self.client_task.set_connection(True)
                self.client_task.log.warning("Connection re-established")

