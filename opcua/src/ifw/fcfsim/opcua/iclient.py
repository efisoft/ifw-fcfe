from __future__ import annotations
from typing import Any, Protocol
import typing

from ifw.fcfsim.opcua.client_mgr import ClientManager


T = typing.TypeVar('T', bound=Any, covariant=True)

class IClientInterface(Protocol[T]):
    def get_engine(self) -> T:
        ...
    
    def add_to_client(self, client_manager: ClientManager, /) -> None:
        ...


class IConnectionInfo(Protocol):
    connected: bool 
    
