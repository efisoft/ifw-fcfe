from typing import Iterable, Protocol, runtime_checkable
import asyncua
from ifw.fcfsim.opcua.iprofile import INodeProfile, IRpcProfile


@runtime_checkable
class IServerBuilder(Protocol):
    async def build_server(self,
            server: asyncua.Server, 
        )->None:
        """ Build nodes and rpc on a server """
        ...
    
    def add_nodes(self, 
                  nodes:Iterable[INodeProfile], 
                  prefix: str = ..., 
                  namespace: int = ..., 
                  identifier: str = ...
                )->None:
        """ Add new OPCUA nodes information to build server """
        ...

    def add_rpcs(self, rpc_methods:Iterable[IRpcProfile], 
                  prefix: str = ..., 
                  namespace: int = ..., 
                  identifier: str = ...
            )->None:
        """ Add new OPCUA rpcs information to build server """
        ...

