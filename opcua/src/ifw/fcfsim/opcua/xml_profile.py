"""
Class and functions dedicated to the creation of a XML NodeSet 
containing all OPCUA-information. 
This uses ifw.fcfsim.opcua.OpcuaProfile as inputs
"""
from __future__ import annotations
from dataclasses import dataclass, field
from typing import Iterable
import xml.etree.ElementTree as ET

from ifw.fcfsim.opcua.iprofile import (
    INodeProfile, 
    IRpcArgumentProfile, 
    IRpcProfile,
    IUaPath, 
    IOpcuaProfile, 
)

from ifw.fcfsim.opcua.profile import UaStringPath, ua_path
from ifw.fcfsim.opcua import defaults

IDENTIFIER_ID = 20737

def create_node_set() -> ET.Element:
    """Create a new Nodeset XML Element with necessary attributes"""
    nodeset = ET.Element("UANodeSet", attrib =  {
        'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
        'xmlns:uax': "http://opcfoundation.org/UA/2008/02/Types.xsd",
        'xmlns':  "http://opcfoundation.org/UA/2011/03/UANodeSet.xsd",
        'xmlns:xsd': "http://www.w3.org/2001/XMLSchema"
    })
    uris = ET.SubElement(nodeset, "NamespaceUris")
    #TODO: SGU. what to put as uri ? I believe asyncua is handling the NamespaceUris 
    ET.SubElement(uris, "Uri").text = "http://www.eso.org/xml" 
    return nodeset

def add_device_state(
      nodeset: ET.Element, 
      namespace: int = defaults.namespace, 
      identifier: str = defaults.identifier
    ) -> None:
    """ Write the DeviceState into a nodestate"""
    devstate = ET.SubElement(nodeset, "UAObject", 
                            NodeId = f"ns={namespace};i={IDENTIFIER_ID}", 
                            BrowseName = f"{namespace};DeviceState"
            )
    
    ET.SubElement(devstate, "DisplayName").text = "DeviceState"

    references = ET.SubElement(devstate, "References")
    ET.SubElement(references, "Reference", 
               ReferenceType="HasTypeDefinition"
            ).text = "i=58"
    ET.SubElement(references, "Reference", 
            ReferenceType="Organizes",
            IsForward = "false"
        ).text = ua_path(identifier).format(namespace)
    

def add_plc_definition(
        nodeset: ET.Element, 
        namespace: int = defaults.namespace, 
        identifier: str = defaults.identifier
    ) -> None:
    """Write the Identifier and DeviceState in a nodeset"""
    path = ua_path(identifier)
    obj = ET.SubElement(nodeset, "UAObject",
            NodeId = path.format(namespace),
            BrowseName = f"{namespace}:{identifier}"
        )
    ET.SubElement(obj, "DisplayName").text = identifier
    references = ET.SubElement(obj, "References")
    ET.SubElement(references, "Reference", 
               ReferenceType="HasTypeDefinition"
            ).text = "i=58"
    ET.SubElement(references, "Reference", 
               ReferenceType="Organizes",
    ).text = f"ns={namespace};i={IDENTIFIER_ID}"

    ET.SubElement(references, "Reference", 
               ReferenceType="Organizes",
            IsForward = "false"
            ).text = "i=85"
    
   

@dataclass
class XmlNodeSetManager:
    prefix: str = ""
    namespace: int = defaults.namespace
    identifier: str = defaults.identifier
    nodeset: ET.Element = field(default_factory=create_node_set) 
    
    program_level: int = 1 
    """ The Path level for which we considers that the node is a program"""
    device_level: int = 2
    """ The Path level for which we considers that the node is device (FB)"""
    
    def __post_init__(self) -> None:
        identifier_id = ua_path(self.identifier).format(self.namespace)
        if self.nodeset.find(f'./*[@NodeId="{identifier_id}"]') is None:
            add_plc_definition(self.nodeset, self.namespace, self.identifier)
        devstate_id = ua_path(IDENTIFIER_ID).format(self.namespace)
        if self.nodeset.find(f'./*[@NodeId="{devstate_id}"]') is None:
            add_device_state(self.nodeset, self.namespace, self.identifier)

    def is_defined(self, path: IUaPath) -> bool:
        nid = path.format(self.namespace)
        el = self.nodeset.find(f'./*[@NodeId="{nid}"]')
        return el is not None

    def provide_root_object(self, path: IUaPath) -> IUaPath:
        """Return the root object of a path
        
        The root is set in the NodeSet if not already defined
        """
        root = path.get_root()
        if root is None or root.get_level()==0:
            root = UaStringPath(self.identifier)
        elif not self.is_defined(root):
            self.add_object(root)
        return root 


    def add_object(self, path: IUaPath) -> None:
        """Add a new object according to path 
        
        Do nothing if the object is already in the nodeset 
        """
        # TODO: Decide what to do with already defined nodes: error, warning, ignore ?
        if self.is_defined(path):
            return 

        root = self.provide_root_object(path)
        name = path.get_name()
        level = path.get_level()

        obj = ET.SubElement(self.nodeset, "UAObject", 
                NodeId = path.format(self.namespace),
                BrowseName =   f"{self.namespace}:{name}"
        )

        ET.SubElement(obj, "DisplayName").text = name

        references = ET.SubElement(obj, "References")
        
        type_def = ET.SubElement(references, "Reference", ReferenceType="HasTypeDefinition")
        type_def.text = "i=58" if level == self.device_level else "i=61" 
        
        orga = ET.SubElement(references, "Reference", ReferenceType="Organizes", IsForward="false")
        orga.text = root.format(self.namespace) 

        if level == self.program_level: 
            ET.SubElement(references, "Reference", 
                          ReferenceType="Organizes", IsForward="false"
            ).text = f"ns={self.namespace};i={IDENTIFIER_ID}" 

    def add_node(self, node: INodeProfile) -> None:
        """Add a value node definition into the nodeset 
        
        If needed intermediate objects are added to the nodeset 
        If a node with iddentical id is set, a ValueError is raised 
        """
        if self.prefix:
            node = node.copy(prefix=self.prefix)

        if self.is_defined(node.path):
            raise ValueError(f"Node with id {node.path.format(self.namespace)} Is already installed")
        root = self.provide_root_object(node.path)
        name = node.path.get_name()
        access = int(node.access)
        
        variable = ET.SubElement(self.nodeset, "UAVariable", 
                        DataType =  node.data_type.name, 
                        NodeId = node.path.format(self.namespace), 
                        BrowseName = f"{self.namespace}:{name}", 
                        UserAccessLevel = str(access), 
                        AccessLevel = str(access), 
                    )
        dn = ET.SubElement(variable, "DisplayName")
        dn.text = name
        references = ET.SubElement(variable, "References")
        
        type_def = ET.SubElement(references, "Reference", ReferenceType="HasTypeDefinition")
        type_def.text = "i=63" 

        orga = ET.SubElement(references, "Reference", ReferenceType="Organizes", IsForward="false")
        orga.text = root.format(self.namespace) 
        
        value = ET.SubElement(variable, "Value")
        ET.SubElement(value, f"uax:{node.data_type.name}").text = str(node.default)

    def add_rpc_method(self,rpc: IRpcProfile) -> None:
        """ Add the RPC method into the nodeset 

        If needed intermediate objects are added to the nodeset
        Method Arguments are added to the nodeset 
        If an RPC method already exists with the same id a ValueError is raised
        """
        if self.prefix:
            rpc = rpc.copy(prefix=self.prefix)

        root = self.provide_root_object(rpc.path) # build intermetdiate objects
        path = rpc.path 
        if path.get_level()>0:
            path_id = path.format(self.namespace)
        else:
            path_id = UaStringPath(self.identifier).format(self.namespace)

        method = ET.SubElement(self.nodeset, "UAMethod", 
                        NodeId = f"{path_id}#{rpc.name}", 
                        BrowseName = f"{self.namespace}:{rpc.name}", 
                )
        dn = ET.SubElement(method, "DisplayName")
        dn.text = rpc.name
        
        references = ET.SubElement(method, "References")

        if rpc.inputs:
            nid = f"{path.format(self.namespace)}.{rpc.name}.inputs"
            self.add_rpc_argument( nid, "Input", path, rpc.name, rpc.inputs)
            ET.SubElement(references, "Reference", 
                  ReferenceType="HasProperty"
            ).text = nid  
        if rpc.outputs:
            nid = f"{path.format(self.namespace)}.{rpc.name}.outputs"
            self.add_rpc_argument( nid, "Output", path, rpc.name, rpc.outputs)
            ET.SubElement(references, "Reference", 
                  ReferenceType="HasProperty"
            ).text = nid 
        
        ET.SubElement(references, "Reference", 
            ReferenceType="HasComponent", IsForward="false"
        ).text = path_id

    def add_rpc_argument(self, 
          nid: str, 
          kind:str, 
          path:IUaPath, 
          name:str, 
          args:list[IRpcArgumentProfile]
    ) -> None: 
        variable = ET.SubElement(self.nodeset, "UAVariable", 
                     NodeId=nid, 
                     BrowseName=f"{kind}Arguments", 
                     DataType="i=296", 
                     ValueRank="-1"
                    )
        dn = ET.SubElement( variable, "DisplayName")
        dn.text = f"{kind}Arguments"

        references = ET.SubElement(variable, "References")
        ET.SubElement(references,"Reference", 
                      ReferenceType="HasTypeDefinition"
                    ).text = "i=68"

        ET.SubElement(references,"Reference", 
                      ReferenceType="HasProperty",
                      IsForward="false",
                    ).text = f"{path.format(self.namespace)}#{name}"

        value = ET.SubElement(variable, "Value")
        lst_object = ET.SubElement(value,"ListOfExtensionObject",  
                      xmlns="http://opcfoundation.org/UA/2008/02/Types.xsd")
        
        for arg in args:
           obj = ET.SubElement(lst_object,"ExtensionObject")
           ET.SubElement( ET.SubElement(obj, "TypeId"), "Identifier").text = "i=297"
           body = ET.SubElement(obj, "Body")
           argument = ET.SubElement(body, "Argument")
           ET.SubElement(argument, "Name").text = arg.name 
           ET.SubElement( 
              ET.SubElement(argument, "DataType"), "Identifier"
           ).text = f"i={arg.data_type.num}"
           ET.SubElement(argument, "ValueRank").text = "1"
           ET.SubElement(argument, "ArrayDimensions")
           ET.SubElement(argument, "Description", attrib={
               "xmlns:p5":"http://www.w3.org/2001/XMLSchema-instance",  "p5:nil":"true"}
           )

    def add_nodes(self, nodes: Iterable[INodeProfile]) -> None:
        """ Add several nodes into the nodeset"""
        for node in nodes:
            self.add_node(node)
    
    def add_rpc_methods(self, rpcs: Iterable[IRpcProfile]) -> None:
        """ Add several rpc methods into the nodeset"""
        for rpc in rpcs:
            self.add_rpc_method(rpc)

    def add_profile(self, profile: IOpcuaProfile) -> None: 
        """Add an Opcua Profile to the nodeset"""
        self.add_nodes(profile.nodes.values())
        self.add_rpc_methods(profile.rpc_methods.values())

    def export(self) -> str:
        """Export the XML nodeset into a string"""
        ET.indent(self.nodeset)
        return ET.tostring(self.nodeset).decode("utf-8")
    
    def write(self, filename: str) -> None:
        """Write the document into a file"""
        with open(filename, "w") as f:
            f.write('<?xml version="1.0" encoding="utf-8"?>\n')
            f.write(self.export())

