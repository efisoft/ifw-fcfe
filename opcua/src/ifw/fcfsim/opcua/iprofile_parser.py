from __future__ import annotations
from typing import Any, Protocol, Union, runtime_checkable
from typing_extensions import TypeAlias

from ifw.fcfsim.opcua.iprofile import INodeProfile, IRpcProfile, IOpcuaProfile

ProfileEntry: TypeAlias = Union[str,dict[str,Any]]
ProfileMapping: TypeAlias = dict[str,ProfileEntry]


class IProfileParser(Protocol):
    """ A Profile Parser can parse an object into a rpc or data node """
    def can_parse(self, inputs:Any)->bool:
        """ True if the given input is parsable by this Parser """
        ... 
    def parse_element( self, definition:Any)->INodeProfile|IRpcProfile:
        """ Parse element definition into a INodeProfile or IRpcProfile """
        ...


