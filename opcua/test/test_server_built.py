from asyncio import sleep
from dataclasses import dataclass, field
import asyncua
from ifw.fcfsim.opcua import api as opcua
import time 
import pytest 


pytest_plugins = ('pytest_asyncio',)
@pytest.mark.asyncio
async def test_basic_server():
    
    profile = opcua.mapping_to_profile({'pos': 'lrPosition'})
    server = asyncua.Server() 
    
    await server.init()
    await opcua.build_server( server, opcua.ServerConfig( namespace=6), profile, prefix='MAIN',) 

    @opcua.attrconnect
    @dataclass
    class Data:
        pos: float  = 0.0
    data = Data()
    
    linker = opcua.link_server(server, data, profile, prefix='MAIN', namespace=6 ) 
    async with server:
        pos_node = server.get_node('ns=6;s=MAIN.lrPosition')
        assert (await pos_node.get_value() == 0.0 )  
        
        data.pos = 10.0
        await linker.push()
        assert (await pos_node.get_value() == 10.0 )  

@pytest.mark.asyncio
async def test_server_link_subscription():
    
    profile = opcua.OpcuaProfile() 

    profile = opcua.mapping_to_profile({'pos': 'lrPosition'})
    profile.nodes['pos'].access = opcua.AccessLevel.CurrentRead + opcua.AccessLevel.CurrentWrite

    server = asyncua.Server() 
    
    await server.init()
    await opcua.build_server( server, opcua.ServerConfig(prefix='MAIN', namespace=6), profile) 

    @opcua.attrconnect
    @dataclass
    class Data:
        pos: float  = 0.0
    data = Data()
    
    linker = opcua.link_server( server, [(data,profile.nodes)], 'MAIN', namespace=6 ) 
    async with server:
        pos_node = server.get_node('ns=6;s=MAIN.lrPosition')
        assert (await pos_node.get_value() == 0.0 )  
        
        data.pos = 10.0
        await linker.push()
        assert (await pos_node.get_value() == 10.0 )  
        
        await pos_node.set_value(20.0)
        await sleep(0.5)
        assert data.pos == 20.0
        
@pytest.mark.asyncio
async def test_server_with_arrays_of_structure():
    
    profile = opcua.mapping_to_profile({
                   'pos_x': 'position[0].lrValue',
                   'pos_y': 'position[1].lrValue'
                })
    server = asyncua.Server() 
    
    @opcua.attrconnect
    @dataclass
    class Data:
        pos_x: float  = 0.0
        pos_y: float  = 0.0 
    data = Data()
    
    linker = opcua.link_server( server, [(data,profile.nodes)], 'MAIN', namespace=6 ) 

    await server.init()
    await opcua.build_server( server, opcua.ServerConfig(prefix='MAIN', namespace=6), profile) 
    
    async with server:
        pos_node = server.get_node('ns=6;s=MAIN.position')
        x_pos_node = server.get_node('ns=6;s=MAIN.position[0].lrValue')
        y_pos_node = server.get_node('ns=6;s=MAIN.position[1].lrValue')

        assert (await x_pos_node.get_value() == 0.0 )  
        
        data.pos_x = 10.0
        data.pos_y = 11.0

        await linker.push()
        assert (await x_pos_node.get_value() == 10.0 )  
        assert (await y_pos_node.get_value() == 11.0 )  




        
