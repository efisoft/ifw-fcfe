from ifw.fcfsim.opcua import api as opcua 
import pytest 


def test_parse_node_string():

    parser = opcua.NamespaceProfileParser( ) 

    node: opcua.NodeProfile =  parser.parse_node( 'stat.lrPos' )
    assert node.data_type.num == 11

    assert parser.parse_node( 'stat.rPos' ).data_type.num == 10 
    assert parser.parse_node( 'stat.pos(Float)' ).data_type.num == 10 
    
    assert parser.parse_node( 'cfg.sText').data_type.num == 12 
    
    assert parser.parse_node( 'some.long.path.to.bFlag').data_type.num == 1
    assert parser.parse_node( 'some[3].long[1].path[1].to.bFlag').data_type.num == 1
    assert parser.parse_node( 'some.long.path[1].to.flag(Boolean)').data_type.num == 1
    
    assert parser.parse_node( 'liCounter' ).data_type.num == 8  
    assert parser.parse_node( 'iCounter' ).data_type.num == 6   
    assert parser.parse_node( 'stat.counter(Int32)' ).data_type.num == 6   

    assert parser.parse_node( 'uiCounter' ).data_type.num == 7
    assert parser.parse_node( 'stat.counter(UInt32)' ).data_type.num == 7
    
    with pytest.raises( ValueError) :
        parser.parse_node( 'unknown' )    
    with pytest.raises( ValueError) :
        parser.parse_node( 'lrunknown()' )    

def test_parse_guessed_access_level():
    # by default access level is tested with prefix :( 
    # this can be changed in the parser 
    parser = opcua.NamespaceProfileParser( ) 
    
    assert parser.parse_node( 'cfg.lrValue').access & opcua.AccessLevel.CurrentWrite
    assert parser.parse_node( 'cfg.lrValue').access & opcua.AccessLevel.CurrentRead

    assert parser.parse_node( 'ctrl.lrValue').access & opcua.AccessLevel.CurrentWrite
    assert parser.parse_node( 'ctrl.lrValue').access & opcua.AccessLevel.CurrentRead
    
    assert not (parser.parse_node( 'info.lrValue').access & opcua.AccessLevel.CurrentWrite)
    assert parser.parse_node( 'info.lrValue').access & opcua.AccessLevel.CurrentRead
    
    
    assert parser.parse_node( 'motor1.cfg.lrValue').access & opcua.AccessLevel.CurrentWrite
    assert parser.parse_node( 'motor1.cfg.array[1].lrValue').access & opcua.AccessLevel.CurrentWrite

    assert not (parser.parse_node( 'motor1.cfgvalue(Double)').access & opcua.AccessLevel.CurrentWrite),\
        "should not match a word begining by cfg"

def test_parse_rpc_string():
    parser = opcua.NamespaceProfileParser( ) 

    rpc = parser.parse_rpc( 'rpc.Enable(O:ReturnValue(Int16))' )
    assert not rpc.inputs
    assert rpc.outputs[0].data_type.num == 4  

    # test 2 outputs 
    rpc = parser.parse_rpc( 'rpc.Enable(O:ReturnValue(Int16), O:NoName(Boolean))' )
    assert not rpc.inputs
    assert rpc.outputs[0].data_type.num == 4  
    assert rpc.outputs[1].data_type.num == 1
    assert rpc.outputs[1].name == "NoName"
    
    # should pass if rpc.is not defined 
    rpc = parser.parse_rpc( 'Enable(O:ReturnValue(Int16), O:NoName(Boolean))' )
    assert rpc.outputs[0].data_type.num == 4  
    assert rpc.outputs[1].data_type.num == 1
    assert rpc.outputs[1].name == "NoName"
    
    rpc = parser.parse_element( 'rpc.Enable(O:ReturnValue(Int16))' )
    with pytest.raises( ValueError) :
        rpc = parser.parse_element( 'Enable(O:ReturnValue(Int16))' ) # element cannot be resolved without rpc 
    
    rpc = parser.parse_rpc( 'rpc.Move(I:lrPos1(Double), I:nMode(Int32), O:ReturnValue(Int16))' )
    assert len(rpc.inputs) == 2
    assert rpc.inputs[0].data_type.num == 11 
    assert rpc.inputs[1].data_type.num == 6 # INT32 

    







      
