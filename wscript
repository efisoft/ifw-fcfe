"""
@ingroup devsim_lib
@copyright ESO - European Southern Observatory
@defgroup devsim_lib Device Simulator Library
@ingroup devsim
@brief Device Simulator Library
"""
from wtools.project import declare_project


declare_project('ifw-fcfsim',
                version="1.0.0",
                requires='cxx python pytest sphinx cii qt5 pyqt5',
		recurse='core opcua simlib sm api devices mgr doc',
		cxx_std='c++1z'
        )

