from __future__ import annotations
from typing import Any, Iterator, Protocol, TypeVar
from ifw.fcfsim.core import api as core 
from ifw.fcfsim.opcua import api as opcua

TEngine = TypeVar('TEngine', bound=Any, covariant=True)

class ISimInterface(Protocol[TEngine]):
    @classmethod
    def get_config_factory(cls) -> core.IConfigFactory[Any]:
        ...

    @classmethod
    def from_config(cls, config: Any) -> ISimInterface[TEngine]:
        ...
    
    def get_engine(self) -> TEngine:
        ...

    def iter_runners(self, excludes: set[str] = ...) -> Iterator[core.IRunner]:
        ...
    
    def add_to_server(self, server_manager: opcua.ServerManager, /) -> None:
        ...

    
