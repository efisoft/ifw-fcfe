from __future__ import annotations
import argparse

from dataclasses import dataclass, field
from typing import Any, Callable, Protocol, TypeVar, runtime_checkable, cast
from ifw.fcfsim.core import api as core 
import ifw.core.utils.utils
from ifw.fcfsim.simlib.isimulator import ISimInterface


@dataclass
class SimConfig:
    """ Device Simulator Base Configuration """
    device_name: str = "Devsim"
    sim_init_time: float = 0.0
    sim_delay: float = 0.0 
    sim_acceleration: float = 1.0
    auto_enter_op: bool = False 
    
    local_mode: bool = False     
    update_frequency: float = 5
    
    state_machine_scxml: str = "" # required !!! 
    
    opcua_prefix: str = ""
    opcua_namespace: int = 4 
    opcua_identifier: str = "PLC1"
    opcua_profile: str = "" # required !!!

    log: str = "ifw.devsim"
    debug: bool = False 
    
    def validate_config(self) -> None:
         ... 

class SimConfigLoader(core.ConfigLoader):
    ...


###############################################################
#
#   Config for the Standalone Application 
#



def new_endpoint(port:int, use_ext_ip: bool = False) -> str:
    """build a endpoint from port"""
    if use_ext_ip:
        address = ifw.core.utils.utils.get_ip_address()
    else:
        address = "127.0.0.1"
    return f"opc.tcp://{address}:{port}"


@dataclass
class SimAppConfig(core.LogConfig):
    """ Device Simulator Base Configuration """
    interface: ISimInterface = None # type:ignore # None should happen temporarly
    device_name: str = "Devsim"
    update_frequency: float = 10
    endpoint: str = new_endpoint(4840)
    """ server endpoint address """
    log: str = "ifw.devsim"
    debug: bool = False 
    

    def validate_config(self) -> None:
        if self.interface is None:
            raise ValueError("missing interface")
        core.LogConfig.validate_config(self)



@runtime_checkable
class ArgsProtocol(core.ILogCmdArgs, Protocol):
    """ expected vars comming from devsim args parser """
    port: int 
    cfg: str
    use_ext_ip: bool 
    verbose: bool  
    debug: bool 

def get_devsim_argument_parser(
        arg_parser: argparse.ArgumentParser | None = None
) -> argparse.ArgumentParser:
    """ Return the Device Simulator application argument parser 
        
        Args:
           arg_parser (optional): an existing argument parser for which elements 
               will be added. If None a fresh one is created. 

        Returns:
           arg_parser: arg_parser input if given or a new one 

    """
    if arg_parser is None:
        arg_parser = argparse.ArgumentParser(description="SCXML/OPC UA Server")
    arg_parser.add_argument(
        "--port", type=int, required=True, help="server port number"
    )
    arg_parser.add_argument("--cfg", required=True, help="configuration")
    arg_parser.add_argument(
        "--use-ext-ip",
        action="store_true",
        help="use the external IP address of deployment host",
        default=False,
    )
    arg_parser.add_argument(
         "--debug",
         action="store_true",
         help="Enter in debug iptyhon shell",
         default=False
    )
    arg_parser.add_argument("--server-name", required=False, help="server name")
    # Add Log command line arguments --list-loggers, --loggers, --log-level, --verbose
    core.get_log_argument_parser(arg_parser)

    return arg_parser 


@dataclass
class SimAppConfigLoader(core.ConfigLoader):
    """ Class to update a config object from argv and/or config files

    Example::
    
        ConfigLoader().load_argv(my_config, sys.argv[1:] )

    The above exemple will modify my_config in place
    """
    InterfaceClass: type[ISimInterface[Any]] | None = None

    def load_argv(self, config:Any, argv:list[str])->None:
        """ Update configuration from an argv list 

        Warning: This method can trigger a SystemExit(0) (e.g. in case of --help)
        """
        args_parser = get_devsim_argument_parser()
        if "-h" in argv or "--help" in argv:
            args_parser.print_help()
            raise SystemExit(0)
        
        args = cast(ArgsProtocol, args_parser.parse_args(argv)) 
        self.load_args(config, args)

    def load_args(self, config:Any, args: ArgsProtocol)->None:
        """ Update configuration from an argument structure (e.i. parsed argvs) """
        if self.InterfaceClass is None:
            raise ValueError("Missing Interface class ")
        else:
            cfg_factory = self.InterfaceClass.get_config_factory()
            cfg =  cfg_factory.from_cii_config(args.cfg)
            config.interface = self.InterfaceClass.from_config(cfg)

        # Load the log config business    
        # WARNING: Application can stop here if --list-loggers is True
        core.LogConfigLoader().load_args(config, args)  
        if hasattr(cfg, "log"):
            core.init_loggers(cfg.log, config.log_level, config.log_verbose)
        # update only things that wasn't load on config file
        config.endpoint =  new_endpoint(args.port, args.use_ext_ip)
        config.debug = args.debug 



