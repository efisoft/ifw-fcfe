from __future__ import annotations

from dataclasses import dataclass
from typing import Any, TypeVar

from ifw.fcfsim.core import api as core
from ifw.fcfsim.simlib.config import SimAppConfig, SimAppConfigLoader
from ifw.fcfsim.simlib.isimulator import ISimInterface
from ifw.fcfsim.simlib.simulator import Simulator
 
T = TypeVar('T', bound=Any)
@dataclass
class SimulatorApp:
    config: SimAppConfig
    
    @property
    def interface(self) -> ISimInterface[Any]:
        return self.config.interface

    async def run(self) -> int:
        return await self.new_simulator().run()
    
    def start(self) -> core.ISession:
        return self.new_simulator().start() 

    def new_session(self) -> core.ISession:
        return self.new_simulator().new_session()
  
    def new_simulator(self) -> Simulator:
        simulator = Simulator(
            endpoint=self.config.endpoint, 
            update_frequency=self.config.update_frequency, 
            log = core.get_logger(self.config.log)
        )
        if self.config.interface is not None:
            simulator.attach(self.config.interface)
        return simulator
    
    @classmethod
    def get_config_factory(cls, InterfaceClass: type[ISimInterface[T]]) -> core.IConfigFactory[SimAppConfig]:
        return SimAppConfigLoader(InterfaceClass=InterfaceClass).new_factory(SimAppConfig)

    @classmethod 
    def from_config(cls, config: SimAppConfig) -> SimulatorApp:
        return cls(config)

 

   
