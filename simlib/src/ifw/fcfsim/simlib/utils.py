""" A bunch of usefull helper function using a DevSim Config instance """

from __future__ import annotations

import asyncio
import queue
import sys
import typing
from dataclasses import dataclass, field
from types import ModuleType
from typing import Any, ClassVar, Iterator, Protocol

from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import api as opcua
from ifw.fcfsim.simlib.iconfig import ISimServerConfig
from ifw.fcfsim.simlib.isimulator import ISimInterface
from ifw.fcfsim.simlib.task import DevsimTask
from ifw.fcfsim.sm import api as sm

from ifw.fcfsim.simlib.app import SimulatorApp


class Excludes:
    engine: ClassVar[str] = "engine"
    sm: ClassVar[str] = "sm"
    opcua: ClassVar[str] = "opcua"

TEngine = typing.TypeVar('TEngine', bound=Any, covariant=True)


@typing.runtime_checkable
class _EngineForTask(Protocol):
    """Engine with a task: a runner can be created from it"""
    async def initialise(self) -> None:
        ...
    async def next(self) -> None:
        ...

T = typing.TypeVar('T', bound=Any)

@dataclass
class DevsimUtils:
    """Utility object to create a bunch of method for DevSim
    
    The Utility implies some Protocol to be respected on the
    given engine and config object.
    """
     
    opcua_identifier: str = "PLC1"
    opcua_namespace: int = 4 
    base_frequency: float = 10.0  
    opcua_frequency: float | None = None
    sm_frequency: float | None = None
    state_machine_scxml: str = ""
    name: str = ""
    signals: core.ISignals = field(default_factory=core.get_global_signals)
    event_queue: queue.Queue[sm.Event] | None = None
    log: core.ILogger = core.get_logger("fcfsim")

    def __post_init__(self) -> None:
        if self.state_machine_scxml and self.event_queue is None:
            raise ValueError("There is a state machine xml file but event queue is None")
        if self.event_queue is not None and not self.state_machine_scxml:
            raise ValueError("There is an event queue but no scxml file")

    def looper(self, frequency: float | None = None) -> core.Looper:
        """Implement a new looper with engine signals 
        
        If frequency is None, self.base_frequency is taken.
        
        """
        if frequency is None:
            frequency = self.base_frequency

        return core.Looper(1./frequency,
                        signals = self.signals
                    )
    
    def get_engine_runner(self, engine: _EngineForTask) -> core.IRunner:
        """Build a runner for the engine Background process """
        task =  DevsimTask(engine, log=self.log)
        return core.TaskRunner(task,self.looper())
 
    
    def create_state_machine_manager(self, engine: object | None = None) -> sm.IStateMachineManager:
        """ Create a State Machine Manager 

        If an engine object is given the state machine manager will collect 
        all actions, activities and listener defined in the object 
        """
        if self.event_queue is None:
            raise ValueError("Cannot make a state machine runner, event_queue is None")
        sm_mgr = sm.StateMachineManager(
            self.state_machine_scxml, 
            event_queue = self.event_queue, 
            smid = self.name,
            log = self.log
        )
        if engine is not None:
            sm_mgr.collect(engine)
        return sm_mgr

    def create_state_machine_runner(self, engine: object) -> core.IRunner:
        """Build a runner for the state machine"""
        sm_mgr = self.create_state_machine_manager(engine)
        # State machine runs 4 times faster than main loop to be more reactive
        return sm_mgr.new_runner(self.looper(self.sm_frequency))
    
    def iter_runners(self,
            interface: ISimInterface[T], 
            excludes: set[str] | None = None
        ) -> Iterator[core.IRunner]:
        """Iterator on runners 

        excludes is a set of runner to exclude from iterator. 
        It can contains "engine", "sm"

        Note: the opcua runner is not included in the list of runners 
        """
        excludes = excludes or set()
        engine = interface.get_engine()
        if Excludes.engine not in excludes:
            if isinstance(engine, _EngineForTask):
                self.log.info(f"{self.name}: Engine runner created")
                yield self.get_engine_runner(engine)
            else:
                self.log.info(f"{self.name}: Skipping Engine runner : "
                               "not initialise and/or next method")
             
        if Excludes.sm not in excludes: 
            # Ignore the state machine runner if no xml file 
            if self.state_machine_scxml: 
                self.log.info("State Machine runner created")
                yield self.create_state_machine_runner(engine)
            else:
                self.log.warning("No XML file is set. Skeeping the state machine system")
            
#############################################################################
#
#  Standard Utils 
#


class _IConfigForUtils(Protocol):
    """All config keys necessary for the standard_utils function"""
    opcua_prefix: str
    opcua_identifier: str 
    opcua_namespace: int
    update_frequency: float  
    state_machine_scxml: str 
    device_name: str
    log: str 

class _IEngineForUtils(Protocol):
    """ Protocol for standard_utils function"""
    def get_config(self) -> _IConfigForUtils:
        ... 

    def get_signals(self) -> core.ISignals:
        ... 

    def get_event_queue(self) -> queue.Queue[sm.Event]:
        ...

def standard_utils(engine: _IEngineForUtils) -> DevsimUtils:
    """Build a SimUtils class from an engine object 
    
    The engine must respect some protocols.
    """
    config = engine.get_config()
    return DevsimUtils(
        opcua_identifier= config.opcua_identifier,
        opcua_namespace= config.opcua_namespace, 
        base_frequency = config.update_frequency, 
        opcua_frequency=  config.update_frequency, 
        sm_frequency = config.update_frequency * 4.0,
        state_machine_scxml = config.state_machine_scxml, 
        name = config.device_name, 
        signals = engine.get_signals(), 
        event_queue= engine.get_event_queue(), 
        log = core.get_logger(config.log),
    )

#############################################################################

def run_simulator(Interface: type[ISimInterface[Any]], module: ModuleType | None = None) -> int:
    """Run a tandard Devim application
    
    Args:
        Devsim: The IDevim interface class 
        module: optional, is used in the debug shell to expose the
            main devsim module
    """
    config = SimulatorApp.get_config_factory( Interface ).from_argv(sys.argv[1:])
    simapp = SimulatorApp.from_config(config) 
    devsim = config.interface

    session = simapp.new_session()

    if config.debug:
        start_session_in_shell(
                devsim.get_engine(), 
                session, Interface.__name__, module
        )
        return 0 
    else:
        return sys.exit(asyncio.run(session.run()))

class _ConfigForAddToServer(Protocol):
    opcua_prefix: str
    opcua_identifier: str
    opcua_namespace: int
    opcua_profile: str 

def add_to_server(
      server_manager: opcua.ServerManager,
      config: _ConfigForAddToServer, 
      engine: object | None
    ) -> None:
    """Add to an existing server manager opcua nodes and it connection 
    
    Namespace Profile file, and other Ua config are given in the 
    standard device simulator config.
    """
    profile = opcua.read_profile(
                   config.opcua_profile, 
                   opcua_prefix= config.opcua_prefix
                ) 
    server_manager.add_profile( 
        profile, 
        engine, 
        namespace = config.opcua_namespace, 
        identifier = config.opcua_identifier
    )

def iter_runners(
    interface: ISimInterface[_IEngineForUtils], 
    excludes: set[str] | None = None
) -> Iterator[core.IRunner]:
    """Generator of Runner using a DevSim Interface object 
    
    the engine given by interface.get_engine() should be not None 
    and it shall respect some standard protocols. 
    """
    engine = interface.get_engine()
    yield from standard_utils(engine).iter_runners(interface, excludes)
    
def read_profile(config: ISimServerConfig) -> opcua.IOpcuaProfile:
    """Read the profile using information in config 
    
    Opcua Prefix is added to each nodes and rpcs   
    """
    return opcua.read_profile(
            config.opcua_profile, 
            opcua_prefix=config.opcua_prefix
        )


_debug_msg = """
############################################################################
#                    Ipythin Debug shell for {device}
#
# Available args:
#    - module: api module (or None)
#    - dfn: Define sub module (contains all enumerators, etc...)
#    - session: simulator session
#    - e: is the {device} engine (e.g.: e.init() , e.stat.state, ...)
#
# Use session.stop() to terminate threads quit() to also terminate shell
#
############################################################################
"""
def get_shell_message(device: str) -> str:
    return _debug_msg.format(device=device)


def start_session_in_shell(
        engine: Any, 
        session: core.ISession,
        device_name: str,
        module: ModuleType | None = None) -> None:
    """Start Application with thread and return hands in a Ipython shell"""
    locals()['dfn'] = getattr(module, "define", None)
    from ifw.fcfsim import api as dvs
    from IPython.terminal.embed import embed
    with session:
        e = engine
        embed(header=get_shell_message(device_name))# type:ignore[no-untyped-call,unused-ignore]
