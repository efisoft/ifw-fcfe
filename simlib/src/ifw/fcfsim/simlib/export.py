#
# !!!!!!!!!!!!! EXPERIMENTAL !!!!!!!!!!!!
# Do not include in api

from __future__ import annotations
from dataclasses import dataclass, field
from datetime import datetime
import typing
from typing_extensions import TypeAlias
from ifw.fcfsim.opcua import api as opcua
from ifw.fcfsim.core import api as core


def export_header() -> str:
    txt = []
    txt.append("from __future__ import annotations")
    txt.append("import typing")
    txt.append("import functools")
    txt.append("from dataclasses import dataclass, field")
    txt.append("from ifw.fcfsim import api as dvs")
    return "\n".join(txt)

def export_engine(name: str, profile: opcua.IOpcuaProfile) -> str:
    output: dict[str,CollectedItem] = {}
    collect_params(output, profile)
    txt: list[str] = [] 
    header = [
       f"    config: {name.capitalize()}SimConfig = field(default_factory={name.capitalize()}SimConfig)", 
       f"    def get_config(self) -> {name.capitalize()}SimConfig: return self.config",
        "    def get_signals(self) -> dvs.core.ISignals: return dvs.core.get_global_signals()",
       ""
    ]
    footer = [
            "    async def initialise(self) -> None:", 
            "        ... ", "", 
            "    async def next(self) -> None:", 
            "        ... ",
    ]
    _export_engine_walk(name.capitalize(), txt, output, header, footer)
    return "\n".join(txt)

def _export_engine_walk(Name: str, txt: list[str], collection: dict[str, CollectedItem], header: list[str] | None = None, footer: list[str] | None = None) -> None:
    for path, data in collection.items():
        if isinstance(data, CollectedClass):
            _export_engine_walk(Name+data.name.capitalize(), txt, data.children)
    
    txt.append("\n@dvs.core.attrconnect")
    txt.append("@dataclass")
    txt.append(f"class {Name}:")
    if header: 
        txt.extend(header)
    for path, data in collection.items():
        if isinstance(data, CollectedParameter):
            txt.append( f"    {data.name}: {data.pytype_name} = {data.default}")
        elif isinstance(data, CollectedClass):
            txt.append(f"    {data.name}: {Name}{data.name.capitalize()} = field(default_factory={Name}{data.name.capitalize()})")
    for path, data in collection.items():
        if isinstance(data, CollecMethod):
            txt.append("")
            txt.append(f"    def {data.name}(self{data.args_str}) -> {data.outputs_str}:")
            txt.append("        raise NotImplementedError()")
    if footer:
       txt.extend(footer) 

def export_config(name: str, namespace: str = "<-- PATH TO OPCUA NAMESPACE -->") -> str:
    txt = [] 
    txt.append(f"class {name.capitalize()}SimConfig(dvs.DevsimConfig):") 
    txt.append(f'    """{name.capitalize()} Configuration"""')
    txt.append(f'    log: str = "fcfsim.{name}"')
    txt.append(f'    opcua_profile: str = "{namespace}"')

    txt.append( "    @classmethod")
    txt.append(f"    def get_factory(cls) -> dvs.core.IConfigFactory[{name.capitalize()}SimConfig]:")
    txt.append( "        return super().get_factory()")    
    return "\n".join(txt)

def export_interface(name: str) -> str:
    txt = []
    Cfg = f"{name.capitalize()}SimConfig"
    Engine = f"{name.capitalize()}"
    txt.append( "@dataclass")
    txt.append(f"class {name.capitalize()}Sim(dvs.BaseDevsim):") 

    txt.append(f"    engine: {Engine} = field(default_factory={Engine})")
    txt.append( "    @functools.cached_property")
    txt.append( "    def _utils(self) -> dvs.utils.DevsimUtilsNoScxml:")
    txt.append( "        return dvs.utils.DevsimUtilsNoScxml(self.engine, self.engine.get_config(), self.get_opcua_profile())")

    txt.append( "    @classmethod")
    txt.append(f"    def get_config_factory(cls) -> dvs.core.IConfigFactory[{Cfg}]:")
    txt.append(f"        return {Cfg}.get_factory()")
    txt.append( "    @classmethod")
    txt.append(f"    def from_config(cls, config: {Cfg}) -> {name.capitalize()}Sim:")
    txt.append(f"        return cls({name.capitalize()}(config))")
    txt.append( "    def get_opcua_profile(self, prefixed: bool = False) -> dvs.opcua.IOpcuaProfile:")
    txt.append( "        config = self.engine.get_config()")
    txt.append( "        profile = dvs.opcua.read_profile(config.opcua_profile)")
    txt.append( "        if prefixed:")
    txt.append( "            profile = profile.copy(opcua_prefix = config.opcua_prefix)")
  
    txt.append( "        return profile")
    txt.append( "    def add_to_server(self, server_manager: dvs.opcua.ServerManager) -> None:")
    txt.append( "        self._utils.add_to_server(server_manager)")
    txt.append( "    def iter_runners(self, excludes: set[str] = set()) -> typing.Iterator[dvs.core.IRunner]:")
    txt.append( "        return self._utils.iter_runners(excludes)")
    txt.append( "    def new_session(self, excludes: set[str] = set()) -> dvs.core.ISession:")
    txt.append( "        runner = dvs.core.RunnerGroup(*self.iter_runners(excludes))")
    txt.append( "        return dvs.core.Session(runner)")

   

    

    return "\n".join(txt)
    


# only python base type supported see opcua.base_profile 
_type_name_loockup: dict[type,str] = {
   int: "int", 
   float: "float", 
   bool: "bool", 
   str: "str", 
   datetime: "datetime",
   bytes: "bytes"
}
def base_type_name(pytype: type) -> str:
    return _type_name_loockup[pytype]

def path_level(path: str) -> int:
    return len([s for s in path.split(".") if s.strip()])



@dataclass
class CollectedClass:
    name: str
    level: int
    children: dict[str, CollectedItem] = field(default_factory=dict)

@dataclass
class CollectedParameter:
    name: str 
    pytype: type
    default: typing.Any 
    @property 
    def pytype_name(self) -> str:
        return base_type_name(self.pytype)

@dataclass 
class MethodArg:
    name: str 
    pytype: type
    @property 
    def pytype_name(self) -> str:
        return base_type_name(self.pytype)

@dataclass 
class CollecMethod:
    name: str 
    args: list[MethodArg] = field(default_factory=list)
    outputs: list[MethodArg] = field(default_factory=list)
    
    @property
    def args_str(self) -> str:
        if not self.args: 
            return ",/"
        return ", " +(", ".join( f"{a.name}: {a.pytype_name}"  for a in self.args))+",/"
    
    @property 
    def outputs_str(self) -> str:
        if not self.outputs:
            return "None"
        if len(self.outputs)==1:
            o, = self.outputs
            return o.pytype_name
        else:
            return "tuple["+(", ".join( o.pytype_name for o in self.outputs ))+"]"

CollectedItem: TypeAlias = typing.Union[CollectedParameter, CollectedClass, CollecMethod]


def collect_params(output: dict[str, CollectedItem], profile: opcua.IOpcuaProfile) -> None:
    for path, node_profile in profile.nodes.items():
        root, name = core.split_path(path)
        param = CollectedParameter(name, node_profile.data_type.pytype, node_profile.default)

        if root:
            cls = provide_class(output, root)
            cls.children[name] =param      
        else:
            output[name] = param 
    
    for path, rpc_profile in profile.rpc_methods.items():
        root, name = core.split_path(path)
        method = CollecMethod(name)
        method.args  = [MethodArg(i.name, i.data_type.pytype) for i in rpc_profile.inputs]   
        method.outputs  = [MethodArg(o.name, o.data_type.pytype) for o in rpc_profile.outputs]
        
        if root:
            cls = provide_class(output, root)
            cls.children[name] = method      
        else:
            output[name] = method 
    

def provide_class(output: dict[str, CollectedItem], path: str) -> CollectedClass:
    root, name = core.split_path(path)
    cls = typing.cast(CollectedClass,  output.setdefault(path, CollectedClass(name, path_level(path))))
    if root:
        provide_class(cls.children, root) 
    return cls 

if __name__ == "__main__":

    profile = opcua.read_profile("config/fcfsim/devices/psu8600/psu8600.namespace.yaml")
    # print(export_engine("lamp", profile))
    print(export_header())
    print(export_config("psu8600"))
    print(export_engine("psu8600", profile))   
    print(export_interface("psu8600"))
    print("if __name__ == '__main__':")
    print("    import sys")
    print("    sys.exit(dvs.utils.run_simulator(Psu8600Sim, None))")

