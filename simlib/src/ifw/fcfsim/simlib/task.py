from __future__ import annotations
from dataclasses import dataclass, field
from ifw.fcfsim.core import api as core 
from typing import Protocol


class _ISimEngine(Protocol):
    """ Engine object to be used By an DevsimTask """

    async def initialise(self) -> None:
        ...

    async def next(self) -> None:
        ...


@dataclass
class DevsimTask(core.BaseTask):
    """Encapsulate an engine with initialise and next method into a Task"""

    engine: _ISimEngine
    """ Device Simulator engine (with next and initialise async method) """
    log: core.ILogger = core.get_logger("DevSimEngine")

    async def start(self) -> None:
        pass

    async def end(self) -> None:
        pass

    async def next(self) -> None:
        await self.engine.next()

    async def initialise(self) -> None:
        await self.engine.initialise()
