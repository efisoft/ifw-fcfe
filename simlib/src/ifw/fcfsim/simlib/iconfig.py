from __future__ import annotations
from typing import Protocol, TypeVar
from ifw.fcfsim.core import api as core

class ISimServerConfig(Protocol):
    """ necessary config inputs to build a ServerConfig """
    opcua_prefix: str
    device_name: str
    log: str
    opcua_identifier: str
    opcua_namespace: int
    update_frequency: float
    opcua_profile: str 

 
class IStateMachineConfig(Protocol):
    """ Necessary config for State Machine """
    device_name: str 
    state_machine_scxml: str 
    update_frequency: float
    log: str 


C = TypeVar('C', bound="ISimConfig")
class ISimConfig(ISimServerConfig, IStateMachineConfig, Protocol):
    """ Devsim Configuration must be conform with these attributes """
    ...
    
