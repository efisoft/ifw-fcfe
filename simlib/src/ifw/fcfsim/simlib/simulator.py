from  __future__ import annotations
import abc
from dataclasses import InitVar, dataclass, field
from typing import Any, Iterator, TypeVar

from ifw.fcfsim.core import api as core
from ifw.fcfsim.opcua import api as opcua
from ifw.fcfsim.simlib.isimulator import ISimInterface


TT = TypeVar('TT', bound='ISimInterface[Any]')
class BaseSimInterface(abc.ABC):
    
    @classmethod
    @abc.abstractmethod
    def get_config_factory(cls) -> core.IConfigFactory[Any]:
        ...

    @classmethod
    @abc.abstractmethod
    def from_config(cls: type[TT], config: Any) -> ISimInterface[Any]:
        ...

    @abc.abstractmethod
    def get_engine(self) -> Any:
        ...

    @abc.abstractmethod
    def add_to_server(self, server_manager: opcua.ServerManager, /) -> None:
        ... 

    @abc.abstractmethod
    def iter_runners(self, excludes: set[str] = ...) -> Iterator[core.IRunner]:
        ...
    

T = TypeVar('T', bound=Any)
@dataclass
class Simulator:
    endpoint: InitVar[str] = opcua.defaults.endpoint
    """opcua endpoint address, e.g. opc.tcp://localhost:4840"""
    
    update_frequency: float = opcua.defaults.update_frequency
    """Fequency of the server 

    Dictate how fast changes in data are pushed into the server
    """
    
    log: core.ILogger = core.get_logger("fcfsim")
    """Main logger"""

    excludes: set[str] = field(default_factory=set)
    """A set of string indicated the runner to exclude 
    
    e.g.: {'opcua', 'sm'} will exclude the opcua server and 
        any state machines. 
        This can be handy for test purposes 
    """
    def __post_init__(self, endpoint: str) -> None:
        if "opcua" in self.excludes:
            self._server_mgr = None 
            self._runners = core.RunnerGroup()
        else:
            self._server_mgr = opcua.ServerManager( 
               endpoint= endpoint, 
               log = self.log
            )
            self._runners = core.RunnerGroup(
               self._server_mgr.new_runner(core.Looper(1./self.update_frequency))
            )
    
    @property 
    def server_mgr(self) -> opcua.ServerManager | None:
        return self._server_mgr 

    def attach(self, sim_interface: ISimInterface[T]) -> T:
        """ Attach a new simulatod device to this simulator"""
        if self.server_mgr is not None:
            sim_interface.add_to_server(self.server_mgr)
        self._runners.add(*sim_interface.iter_runners(
            excludes={'opcua'} | self.excludes))
        return sim_interface.get_engine() 

    def new_session(self) -> core.ISession:
        return core.Session(self._runners)

    def start(self) -> core.ISession:
        session = self.new_session()
        session.start()
        return session 

    async def run(self) -> int:
        return await self.new_session().run()


